<?php


class Vehiculo  extends baseDeDatos{
    protected $resulltado ;
    protected $NumeroPagina;
    protected $limiteDeDados;
    
    public function __construct() {
        $this->NumeroPagina = 1;
        $this->limiteDeDados  = 5;
    }
    //este metodo es para indicar el numero de pagina mostrar
    function SetNumeroPagina($numeroPagina){
        $this->NumeroPagina = $numeroPagina;
    }
    //este metodo limita los registros a mostrar 
    function SetLimiteDatos($limite){
        $this->limiteDeDados = $limite;
    }
    
    
    function getResultado(){
        return $this->resulltado;
    }
    
    //put your code here
    
    
    
    function AgregarVehiculo($placa,$modelo,$marca,$clase,$numerovehiculo,$tarjetaoperacion,$propio,$numerocontrato){
      try{
        $queryAgregarVehiculo = "INSERT INTO vehiculos (idempresa,placa,modelo,marca,clase,numerovehiculo,tarjetaoperacion,propio,numerocontrato)"
                                . "VALUES (:idempresa,:placa,:modelo,:marca,:clase,:numerovehiculo,:tarjetaoperacion,:propio,:numerocontrato)";
        
        $arrayAgrgarVehiculo = array(":idempresa"=>$_SESSION['idEmpresa'],
                                    ":placa"=>$placa,
                                    ":modelo"=>$modelo,
                                    ":marca"=>$marca,
                                    ":clase"=>$clase,
                                    ":numerovehiculo"=>$numerovehiculo,
                                    ":tarjetaoperacion"=>$tarjetaoperacion,
                                    ":propio"=>$propio,
                                    ":numerocontrato"=>$numerocontrato);
        
        $this->SetArray_insertar($arrayAgrgarVehiculo);
        $this->SetQuery($queryAgregarVehiculo);
        $this->Insertar();    
        
        if($this->filasAfectadas()){
            $this->resulltado = true;
        }else{
            $this->resulltado = false;
        }
        
        
      }catch(Exception $ex ){
          echo "[ERROR] codigo de error ".$ex->getCode();
      } 
    }
    
    function EliminarVehiculo($placa){
        try{
        
//            $documentoVehiculo = new DocumentoVehiculo();
//            $documentoVehiculo->EliminarFotoVehiculo($placa);
            
            
        $queryEliminarVehiculo = "DELETE FROM vehiculos WHERE vehiculos.placa =  :placa AND idempresa = :idempresa ";
        
        $arrayEliminarVehiculo = array(":placa"=>$placa,":idempresa"=>$_SESSION['idEmpresa']);
        
        
        $this->SetQuery($queryEliminarVehiculo);
        $this->SetArray_eliminar($arrayEliminarVehiculo);
        $this->Eliminar();
        
        if($this->filasAfectadas()){
            $this->resulltado = true;
        }else{
            $this->resulltado = false;
        }
        
         
        }catch(Exception $ex){
            die ("[ERROR] codigo de error ".$ex->getCode());
        }
    }
    
    function ActulizarVehiculo($placa,$modelo,$marca,$clase,$numerovehiculo,$tarjetaoperacion,$propio,$numerocontrato){
       try{
        $queryActulizarVehiculo = "UPDATE vehiculos SET  placa=:placa,modelo=:modelo,marca=:marca,clase=:clase,numerovehiculo=:numerovehiculo,tarjetaoperacion=:tarjetaoperacion,propio=:propio,numerocontrato=:numerocontrato  WHERE placa=:placa AND idempresa=:idempresa";
        
        $arrayActulizarVehiculo = array(":placa"=>$placa,
                                        ":modelo"=>$modelo,
                                        ":marca"=>$marca,
                                        ":clase"=>$clase,
                                        ":numerovehiculo"=>$numerovehiculo,
                                        ":tarjetaoperacion"=>$tarjetaoperacion,
                                        ":propio"=>$propio,
                                        ":numerocontrato"=>$numerocontrato,
                                        ":idempresa"=>$_SESSION['idEmpresa']);
        
                                    $this->SetArray_actualizar($arrayActulizarVehiculo);
                                    $this->SetQuery($queryActulizarVehiculo);
                                    $this->Actualizar();
        if($this->filasAfectadas()){
            $this->resulltado = true;
        }else{
            $this->resulltado = false;
        }                            
                                    
       }catch(Exception $ex){
           echo "[Error] codigo de error ".$ex->getCode();
       }
        
        
    }
    
    function ListarVehiculo(){
        try{
        $queryListarVehiculo = "SELECT * FROM vehiculos WHERE idempresa = :idempresa";
        
        $arrayListarVehiculo = array(":idempresa"=>$_SESSION['idEmpresa']);
        
        $this->Setarray_selecionar($arrayListarVehiculo);
        
        $this->SetQuery($queryListarVehiculo);
        $this->resulltado =  $this->seleccionar();
     }catch(Exception $ex){
         echo "[Error] codigo de error ".$ex->getCode();
     } 
    }//fin de la funcion listar vehiculo
    
    function PaginaVehiculo(){
        try{
        $queryVehiculo = "SELECT * FROM vehiculos  WHERE idempresa = :idempresa ";
       $arrayVehiculo = array (":idempresa"=>$_SESSION['idEmpresa']);
       
       $this->Setarray_selecionar($arrayVehiculo);
       $this->SetQuery($queryVehiculo);
       $this->resulltado = $this->paginasionMostrar($this->NumeroPagina,$this->limiteDeDados);
        
        }catch(Exception $ex){
            echo "[ERROR] codigo de error ".$ex->getCode();
        }
    }//fin de la funcion pafinar
    
    
    function ContarVehiculo(){
        try{
         $queryListarVehiculo = "SELECT * FROM vehiculos WHERE idempresa=:idempresa";
        
        $arrayListarVehiculo = array(":idempresa"=>$_SESSION['idEmpresa']);
        
        $this->Setarray_selecionar($arrayListarVehiculo);
        
        $this->SetQuery($queryListarVehiculo);
        
        $this->resulltado = $this->contar_registro();
        }catch(Exception $ex){
            echo "[Error] codigo de error ".$ex->getCode();
        }
    }//contar vehiculo
            
    function BuscarVehiculo($dato){
        try{
         if(empty($dato)){
          //  $queryBuscarVehiculo = "SELECT * FROM vehiculos";
        }else{
            $queryBuscarVehiculo = "SELECT * FROM vehiculos WHERE idempresa=:idempresa HAVING"
                    . "  modelo LIKE :dato  OR"
                    . " placa LIKE :dato OR"
                    . " marca LIKE :dato OR"
                    . " numerovehiculo LIKE :dato OR"
                    . " tarjetaoperacion like :dato OR"
                    . " estado LIKE :dato OR"
                    . " propio LIKE :dato OR"
                    . " numerocontrato LIKE :dato LIMIT 10";
        }
        
        
        $arrayBuscarVehiculo = array(":idempresa"=>$_SESSION['idEmpresa'],":dato"=>'%'.$dato.'%');
        $this->Setarray_selecionar($arrayBuscarVehiculo);
        $this->SetQuery($queryBuscarVehiculo);
        $this->resulltado = $this->seleccionar();
        
        } catch (Exception $ex ){
            echo "[Error] codigo de error ".$ex->getCode();
        }
        
        
        
    }//fin de la funcion buscar vehiculo
    
    function ListarVehiculoPlaca($numeroContrato=null){
        
        try {
            
                    //si por la funcion se le pasa un numero de contrato listara los vehiculos activos
                if(isset($numeroContrato)){
                    $listarVehiculoActivo = "SELECT placa FROM vehiculos  WHERE  idempresa = :idempresa AND numerocontrato = :numerocontrato AND estado = 'activo' AND estado <> '' ";
                    $arrayVehiculoActivo = array(":idempresa"=>$_SESSION['idEmpresa'],":numerocontrato"=>$numeroContrato);
                }else{
                    $listarVehiculoActivo = "SELECT placa FROM vehiculos  WHERE idempresa = :idempresa AND   estado = 'activo' AND estado <> '' ";
                    $arrayVehiculoActivo = array(":idempresa"=>$_SESSION['idEmpresa']);
                }
        
        $this->Setarray_selecionar($arrayVehiculoActivo);
        $this->SetQuery($listarVehiculoActivo);
        $this->resulltado =  $this->seleccionar();
            
            
        } catch (Exception $ex) {
            echo "[ERROR] Linea de error ".$ex->getFile();
        }
       }
       
       
          function NumeroVehiculo(){
                $Activo = "";
                $Inactivo = "";
                $Cantidad = "";
              
                $VehiculoActivo = "SELECT vehiculos.placa FROM vehiculos WHERE vehiculos.estado = 'activo' AND"
                                . " vehiculos.idempresa = :idempresa ";
                                
                         $array_selecionar = array(":idempresa"=>$_SESSION['idEmpresa']);       
                        $this->Setarray_selecionar($array_selecionar);        
                        $this->SetQuery($VehiculoActivo);
                        $Activo = $this->contar_registro();
                        
                $VehiculoInactivo = "SELECT vehiculos.placa FROM vehiculos WHERE vehiculos.estado = 'inactivo' AND"
                                . " vehiculos.idempresa = :idempresa ";
                                
//                        $array_selecionarInactivo = array(":idempresa"=>$_SESSION['idEmpresa']);       
                        $this->Setarray_selecionar($array_selecionar);        
                        $this->SetQuery($VehiculoInactivo);
                        $Inactivo = $this->contar_registro(); 
                        
                $Vehiculo = "SELECT vehiculos.placa FROM vehiculos WHERE "
                                . " vehiculos.idempresa = :idempresa ";
                                
//                        $array_selecionar = array(":idempresa"=>$_SESSION['idEmpresa']);       
                        $this->Setarray_selecionar($array_selecionar);        
                        $this->SetQuery($Vehiculo);
                        $Cantidad = $this->contar_registro();         
                        
                        
                        
                        
                        $this->resulltado = array("Activo"=>$Activo,"Inactivo"=>$Inactivo,"Cantidad"=>$Cantidad);
                
             }//fin de la funcion
    
    
}


class DocumentoVehiculo extends Vehiculo {
    //esto es para se cre la fila donde se guardara los documento
    function InsertarPlacaDocumento($placa){
        
        $queryInsrtarplaca = "INSERT INTO documentos_vehiculos (placa) VALUES (:placa)";
        
        $arrayInsertarplaca = array(":placa"=>$placa);
        
        $this->SetArray_insertar($arrayInsertarplaca);
        $this->SetQuery($queryInsrtarplaca);
        $this->Insertar();
//        $queryFotoplaca = "INSERT INTO fotos_vehiculos (placa) VALUES (:placa)";
//        $arrayfotoPlaca = array(":placa"=>$placa);
//        $this->SetArray_insertar($arrayfotoPlaca);
//        $this->SetQuery($queryFotoplaca);
//        
//        $this->Insertar();
        
        
    }
    //========================================================================================================
    //esta funcion se encarga de subir fotos del los vehiculos 
    function SubirImagenesVehculo($placa,$fotos){
        
        try {
            
            $queryImagenVehiculo = "INSERT INTO fotos_vehiculos (placa,fotos) VALUES (:placa,:fotos)";
            
            $ArrayImagenVehiculo =  array(":placa"=>$placa,":fotos"=>$fotos);
            
            $this->SetArray_insertar($ArrayImagenVehiculo);
            $this->SetQuery($queryImagenVehiculo);
            $this->Insertar();
            
            if($this->filasAfectadas()){
                $this->resulltado = true;
            }else{
                $this->resulltado = false;
            }
            
            
            
        } catch (Exception $ex) {
            Echo "[Error] en la linea ".$ex;
        }
    }
    
        function ListarFotosVehiculo($placa){
            
            $queryFotoVehiculo = "SELECT id,placa,fotos FROM fotos_vehiculos WHERE placa=:placa OR id=:placa ";
            $ArrayFotoVehiculo = array(":placa"=>$placa);
            
            $this->Setarray_selecionar($ArrayFotoVehiculo);
            $this->SetQuery($queryFotoVehiculo);
            
            $this->resulltado = $this->seleccionar();
            
            
        }
        
        
        function  EliminarFotoVehiculo($id){
            $queryEliminarFotoVehiculo = "DELETE FROM fotos_vehiculos WHERE  placa=:id OR id=:id ";
            $arrayEliminarFotoVehiculo = array(":id"=>$id);
            
            $this->SetArray_eliminar($arrayEliminarFotoVehiculo);
            $this->SetQuery($queryEliminarFotoVehiculo);
            $this->Eliminar();
            
            if($this->filasAfectadas()){
                $this->resulltado = true;
            }else{
                $this->resulltado = false;;
            }
            
        }




        //========================================================================================================
    
    
    
    
    //esta funcion sirve para agregar o cambiar un rigistro en la tabla documento_vehiculo
    function AgregarDocumentoVehiculo($placa,$soat,$fecha_veci_soat,$tecnicomecanica,$fecha_veci_tecnomecanica,$mantenimento,$fecha_veci_mantenimento,$mantenimentoCorrectivo,$fecha_veci_correctivo,$tarjeta_de_operacion,$fecha_veci_tarjeta_operacion,$tarjeta_de_propiedad,$fecha_veci_propiedad,$administracion_pago,$simit,$supertrasporte)
            {
        try {
            $queryAgregarDcocumentoVehiculo = "INSERT INTO documentos_vehiculos ("
                                                    . "placa,"
                                                    . "soat,"
                                                    . "fecha_veci_soat,"
                                                    . "tecnicomecanica,"
                                                    . "fecha_veci_tecnomecanica,"
                                                    . "mantenimento,"
                                                    . "fecha_veci_mantenimento,"
                                                    . "mantenimentoCorrectivo,"
                                                    . "fecha_veci_correctivo,"
                                                    . "tarjeta_de_operacion,"
                                                    . "fecha_veci_tarjeta_operacion,"
                                                    . "tarjeta_de_propiedad,"
                                                    . "fecha_veci_propiedad,"
                                                    . "administracion_pago,"
                                                    . "simit,"
                                                    . "supertrasporte) VALUES"
                                                    . "(:placa,
                                                        :soat,
                                                        :fecha_veci_soat,
                                                        :tecnicomecanica,
                                                        :fecha_veci_tecnomecanica,
                                                        :mantenimento,
                                                        :fecha_veci_mantenimento,
                                                        :mantenimentoCorrectivo,
                                                        :fecha_veci_correctivo,
                                                        :tarjeta_de_operacion,
                                                        :fecha_veci_tarjeta_operacion,
                                                        :tarjeta_de_propiedad,
                                                        :fecha_veci_propiedad,
                                                        :administracion_pago,
                                                        :simit,
                                                        :supertrasporte)";

            
            $arrayAgregarDcoumentoVehiculo = array(":placa"=>$placa,
                                                   ":soat"=>$soat,
                                                   ":fecha_veci_soat"=>$fecha_veci_soat,
                                                   ":tecnicomecanica"=>$tecnicomecanica,
                                                   ":fecha_veci_tecnomecanica"=>$fecha_veci_tecnomecanica,
                                                   ":mantenimento"=>$mantenimento,
                                                   ":fecha_veci_mantenimento"=>$fecha_veci_mantenimento,
                                                   ":mantenimentoCorrectivo"=>$mantenimentoCorrectivo,
                                                   ":fecha_veci_correctivo"=>$fecha_veci_correctivo,
                                                   ":tarjeta_de_operacion"=>$tarjeta_de_operacion,
                                                   ":fecha_veci_tarjeta_operacion"=>$fecha_veci_tarjeta_operacion,
                                                   ":tarjeta_de_propiedad"=>$tarjeta_de_propiedad,
                                                   ":fecha_veci_propiedad"=>$fecha_veci_propiedad,  
                                                   ":administracion_pago"=>$administracion_pago,
                                                   ":simit"=>$simit,
                                                   ":supertrasporte"=>$supertrasporte);
            $this->SetQuery($queryAgregarDcocumentoVehiculo);
            $this->SetArray_insertar($arrayAgregarDcoumentoVehiculo);
            $this->Insertar();
            if($this->filasAfectadas()){
                $this->resulltado = true;
            }else{
                $this->resulltado = false;
            }
            
            
        }catch(Exception $ex) {
            echo "[Error] codigo de error ".$ex->getCode();
        }
        
    }//fin de la funcion Agregar documento 
    
    function ActulizarDocumentoVehiculo($placa,$soat,$fecha_veci_soat,
            $tecnicomecanica,$fecha_veci_tecnomecanica,$mantenimento,
            $fecha_veci_mantenimento,$mantenimentoCorrectivo,$fecha_veci_correctivo,
            $tarjeta_de_operacion,$fecha_veci_tarjeta_operacion,$tarjeta_de_propiedad,
            $fecha_veci_propiedad,$administracion_pago,$simit,$supertrasporte){
        try {
            
            
            $queryActulizarDocumentoVehiculo = "UPDATE documentos_vehiculos SET "
                                                . "placa=:placa,"
                                                . "soat=:soat,"
                                                . "fecha_veci_soat=:fecha_veci_soat,"
                                                . "tecnicomecanica=:tecnicomecanica,"
                                                . "fecha_veci_tecnomecanica=:fecha_veci_tecnomecanica,"
                                                . "mantenimento=:mantenimento,"
                                                . "fecha_veci_mantenimento=:fecha_veci_mantenimento,"
                                                . "mantenimentoCorrectivo=:mantenimentoCorrectivo,"
                                                . "fecha_veci_correctivo=:fecha_veci_correctivo,"
                                                . "tarjeta_de_operacion=:tarjeta_de_operacion,"
                                                . "fecha_veci_tarjeta_operacion=:fecha_veci_tarjeta_operacion,"
                                                . "tarjeta_de_propiedad=:tarjeta_de_propiedad,"
                                                . "fecha_veci_propiedad=fecha_veci_propiedad,"
                                                . "administracion_pago=:administracion_pago,"
                                                . "simit=:simit,"
                                                . "supertrasporte=:supertrasporte WHERE placa=:placa";
            
            
            
            $arrayActuizarDocumentoVehiculo = $arrayAgregarDcoumentoVehiculo = array(":placa"=>$placa,
                                                   ":soat"=>$soat,
                                                   ":fecha_veci_soat"=>$fecha_veci_soat,
                                                   ":tecnicomecanica"=>$tecnicomecanica,
                                                   ":fecha_veci_tecnomecanica"=>$fecha_veci_tecnomecanica,
                                                   ":mantenimento"=>$mantenimento,
                                                   ":fecha_veci_mantenimento"=>$fecha_veci_mantenimento,
                                                   ":mantenimentoCorrectivo"=>$mantenimentoCorrectivo,
                                                   ":fecha_veci_correctivo"=>$fecha_veci_correctivo,
                                                   ":tarjeta_de_operacion"=>$tarjeta_de_operacion,
                                                   ":fecha_veci_tarjeta_operacion"=>$fecha_veci_tarjeta_operacion,
                                                   ":tarjeta_de_propiedad"=>$tarjeta_de_propiedad,
                                                   ":fecha_veci_propiedad"=>$fecha_veci_propiedad,  
                                                   ":administracion_pago"=>$administracion_pago,
                                                   ":simit"=>$simit,
                                                   ":supertrasporte"=>$supertrasporte);
            
            
            $this->SetQuery($queryActulizarDocumentoVehiculo);
            $this->SetArray_actualizar($arrayActuizarDocumentoVehiculo);
            $this->Actualizar();
            
            if($this->filasAfectadas()){
                $this->resulltado = true;
            }else{
                $this->resulltado = false;
            }
            
        } catch (Exception $ex) {
                echo "[Error] codigo de error  ".$ex->getCode();
        }
        
    }//fin de la funcion Actulizar
    
    
    
    //este metodo sirve para actulizar algun documento de forma individual
    function actulizarDocumentoVehiculoTipo($campo,$campo_fecha,$valor,$fecha,$placa){
        //esta metodo sirve para actulizar los documento de forma individual
            //$campo = aqui pasa el nombre de la tabla 
            //$campo_fecha = Es el nombre del campo fecha
            //$valor = es la ruta del donde esta guardado el archivo pdf
            //$fecha = es la fecha de vencimiento del archivo
            //$placa = es indici alcual se le va hacer cambio
        try{
        
        $queryDocumentoTipo = "UPDATE documentos_vehiculos SET"
                               . " ".$campo."=:valor,"
                               . " ".$campo_fecha."=:fecha WHERE placa=:placa";
        
        $arrayDocumentoTipo = array(":valor"=>$valor,":fecha"=>$fecha,":placa"=>$placa);
        
        $this->SetQuery($queryDocumentoTipo);
        $this->SetArray_actualizar($arrayDocumentoTipo);
        $this->Actualizar();
        
        if($this->filasAfectadas()){
            $this->resulltado = true;
        }else{
            $this->resulltado = false;
        }
        //fin try
        } catch (Exception $e){
            die ("[Error] codigo de error  ".$e->getCode());
        }
        
    }
        
    function EliminarDocumentoVehiculo($id,$placa){
        try{
            $queryEliminarDocumentoVehiculo = "DELETE FROM  documentos_vehiculos WHERE id=:id AND placa=:placa";
            
            $arrayEliminarDocumentoVehiculo = array(":id"=>$id,":placa"=>$placa);
            
            
            $this->SetQuery($queryEliminarDocumentoVehiculo);
            $this->SetArray_eliminar($arrayEliminarDocumentoVehiculo);
            $this->Eliminar();
            if($this->filasAfectadas()){
                $this->resulltado = true;
            }else{
                $this->resulltado = false;
            }
            
        }catch(Exception $ex){
            echo "[ERROR] codigo de error ".$ex->getCode();
        }
        
     }//fin de la funcion eliminar    
        
     function EliminarDocumentoVehiculoPlaca($placa){
        try{
            $queryEliminarDocumentoVehiculo = "DELETE FROM documentos_vehiculos WHERE  placa = :placa";
            
            $arrayEliminarDocumentoVehiculo = array(":placa"=>$placa);
            
            
            $this->SetQuery($queryEliminarDocumentoVehiculo);
            $this->SetArray_eliminar($arrayEliminarDocumentoVehiculo);
            $this->Eliminar();
            if($this->filasAfectadas()){
                $this->resulltado = true;
            }else{
                $this->resulltado = false;
            }
            
        }catch(Exception $ex){
            echo "[ERROR] codigo de error ".$ex->getCode();
        }
        
     }//fin de la funcion eliminar   
     
     
        function ListarDocumentoVehiculo(){
            try{
                  $queryListarDocumentoVehiculo = "SELECT documentos_vehiculos.id,
                                                        documentos_vehiculos.placa,
                                                        documentos_vehiculos.soat,
                                                        documentos_vehiculos.fecha_veci_soat,
                                                        documentos_vehiculos.tecnicomecanica,
                                                        documentos_vehiculos.fecha_veci_tecnomecanica,
                                                        documentos_vehiculos.mantenimento,
                                                        documentos_vehiculos.fecha_veci_mantenimento,
                                                        documentos_vehiculos.mantenimentoCorrectivo,
                                                        documentos_vehiculos.fecha_veci_correctivo,
                                                        documentos_vehiculos.tarjeta_de_operacion,
                                                        documentos_vehiculos.fecha_veci_tarjeta_operacion,
                                                        documentos_vehiculos.tarjeta_de_propiedad,
                                                        documentos_vehiculos.fecha_veci_propiedad,
                                                        documentos_vehiculos.administracion_pago,
                                                        documentos_vehiculos.simit,
                                                        documentos_vehiculos.supertrasporte FROM vehiculos RIGHT  JOIN documentos_vehiculos   ON vehiculos.placa = documentos_vehiculos.placa";
                  
                  $this->SetQuery($queryListarDocumentoVehiculo);
                  $this->resulltado =   $this->seleccionar();
                  
                  
                
            } catch (Exception $ex){
                echo "[Error] codigo de error ".$ex->getCode();
                
            }
            
            
        }//fin de la funcion eleminar
        
        
        function PaginarDocumentoVehiculo(){
            try{
                
            }catch(Exception $ex){
                echo "[ERROR] codigo errot ".$ex->getCode(); 
            }
            
        }//fin de la funcion elimnar
    
    
        function BuscarDocumentoVehiculo($dato){
            try{
                
                
                $queryBuscarDocumentoVehiculo = "SELECT * FROM documentos_vehiculos 
                INNER JOIN vehiculos 
                ON vehiculos.placa = documentos_vehiculos.placa
                HAVING vehiculos.idempresa = :idempresa AND documentos_vehiculos.placa LIKE  :dato ";
                
                $arrayBuscarDocumentoVehiculo = array(":dato" =>"%".$dato."%",":idempresa"=>$_SESSION['idEmpresa']);
                
                $this->SetQuery($queryBuscarDocumentoVehiculo);
                $this->Setarray_selecionar($arrayBuscarDocumentoVehiculo);
                $this->resulltado = $this->seleccionar();
                
                
                
            }catch(Exception $ex){
                
                echo "[ERROR] codigo de error ".$ex->getCode();
            }
            
         }//fin de la funcion buscar
    
    
}


//$vehiculo = new Vehiculo();
//$vehiculo->EliminarVehiculo("veo568");
//echo $vehiculo->getResultado();


//este metodo sirve para guardar un vehiculo
            /*
            $vehiculo->AgregarVehiculo("1042350126", "gat-965", "2055", "tesla", "buss", "001", "565689223 2", "activo", "si", "");
             */

//este metodo sirve para eliminar un vehiculo
        /*
            $vehiculo->ActulizarVehiculo("gat-965","2088","tesla","buss","001","5656892232","activo","si","");
          */


//este metodo lista los vehiculo segun el nit de la empresa
        /*$vehiculo->ListarVehiculo();
        $ver = $vehiculo->getResultado();

        foreach  ($ver as $row){
            echo $row['placa']."<br>";
        }*/

//este medoto pagina lista de vehiculos

          /*          $vehiculo->SetNumeroPagina(1);
                    $vehiculo->SetLimiteDatos(2);
                    $vehiculo->PaginaVehiculo();
                    $ver = $vehiculo->getResultado();
                    foreach ($ver as $row){
                        echo $row['placa']."<br>";
                    }
           */ 
            
            
//esta funcion sirve para contar los regitros
            /*
            $vehiculo->ContarVehiculo();
            echo "el numero de registros que hay actualmente: ".$vehiculo->getResultado();*/
           
        

//-------------------------------------------------------------------------------
//$documentoVehiculo = new DocumentoVehiculo();
/*esta funcion sirve para guardar un documento vehiculos*/
     /*   $documentoVehiculo = new DocumentoVehiculo();
        
        $documentoVehiculo->AgregarDocumentoVehiculo("car-366",
                                                    "soat",
                                                    "2019-06-08",
                                                    "tecnicomecanica",
                                                    "2019-08-08",
                                                    "mantenimento",
                                                    "2019-07-08",
                                                    "mantenimentoCorrectivo",
                                                    "2022-09-09",
                                                    "tarjeta_de_operacion",
                                                    "2019-08-08",
                                                    "tarjeta_de_propiedad",
                                                    "2019-08-08",
                                                    "administracion_pago",
                                                    "simit",
                                                    "supertran");
        echo $documentoVehiculo->getResultado();*/




///actulizar un documento  

//$documentoVehiculo->ActulizarDocumentoVehiculo("car-366",
//                                                    "soat",
//                                                    "2019-06-08",
//                                                    "tecnicomecanica",
//                                                    "2019-08-08",
//                                                    "mantenimento",
//                                                    "2019-07-08",
//                                                    "mantenimentoCorrectivo",
//                                                    "2022-09-09",
//                                                    "tarjeta_de_operacion",
//                                                    "2019-08-08",
//                                                    "tarjeta_de_propiedad",
//                                                    "2019-08-08",
//                                                    "administracion_pago",
//                                                    "simit",
//                                                    "supertran");
// echo $documentoVehiculo->getResultado();  



//funcion para eliminar un documento con tan solo el id y el numero de placa

 /* $documentoVehiculo->EliminarDocumentoVehiculo(2,"car-366");  */ 




