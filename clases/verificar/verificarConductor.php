<?php

require_once '../../conexion/sessionlogin.php';
require_once '../../conexion/conexion.php';
require_once '../../clases/baseDeDatos.php';



class VerificarConductor extends baseDeDatos{
    
     private  $respuesta  ;
    
    function getRepuesta(){
        return $this->respuesta;
    }
    
    
    function VerificarLicencia(){
        //este query valida si estado soat es menor a la fecha actual 
        
       $queryInactivo = "UPDATE documento_conductor  
        INNER JOIN conductores ON (documento_conductor.documento_conductore = conductores.Cedulaconductor)
        SET documento_conductor.estado_licencia = 'inactivo' 
        WHERE  documento_conductor.fecha_veci_licencia < CURDATE() AND conductores.idempresa = :idempresa";
       
//este query valida si estado soat es mayor a la fecha actual
       $queryActivo = "UPDATE documento_conductor  
        INNER JOIN conductores ON (documento_conductor.documento_conductore = conductores.Cedulaconductor)
        SET documento_conductor.estado_licencia = 'activo' 
        WHERE  documento_conductor.fecha_veci_licencia > CURDATE() AND conductores.idempresa = :idempresa ";
       
       $array = array(":idempresa"=>$_SESSION['idEmpresa']);
       
       
       //Ejecutando metodo para hacer cambios en la bd 
       $this->SetQuery($queryInactivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
       
        if($this->filasAfectadas()){
           
           $Inactivos = true;
           
       }else{
           $Inactivos = false;
           
       }
       
       //
       $this->SetQuery($queryActivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
       
       
       if($this->filasAfectadas()){
          
           $Activos = true;
           $Cambios = $this->filasAfectadas();
       }else{
           $Activos = false;
           $Cambios ="";
           
       }
       //este es un array donde muestra si hay cambios en la bd
       $this->respuesta = array("Activos"=>$Activos,"Inactivos"=>$Inactivos,"Cambios"=>$Cambios);
       
       
       
        
    }//VerificarLicencia
    
    //------------------------------------------------///
    
    
    
    
    function VerificarCapacitacion(){
        //este query valida si estado soat es menor a la fecha actual 
        
       $queryInactivo = "UPDATE documento_conductor  
        INNER JOIN conductores ON (documento_conductor.documento_conductore = conductores.Cedulaconductor)
        SET documento_conductor.estado_capacitacion = 'inactivo' 
        WHERE  documento_conductor.ultima_capacitacion < CURDATE() AND conductores.idempresa = :idempresa";
       
//este query valida si estado soat es mayor a la fecha actual
       $queryActivo = "UPDATE documento_conductor  
        INNER JOIN conductores ON (documento_conductor.documento_conductore = conductores.Cedulaconductor)
        SET documento_conductor.estado_capacitacion = 'activo' 
        WHERE  documento_conductor.ultima_capacitacion > CURDATE() AND conductores.idempresa = :idempresa";
       
       $array = array(":idempresa"=>$_SESSION['idEmpresa']);
       
       
       //Ejecutando metodo para hacer cambios en la bd 
       $this->SetQuery($queryInactivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
       
        if($this->filasAfectadas()){
           
           $Inactivos = true;
       }else{
           $Inactivos = false;
           
       }
       
       //
       $this->SetQuery($queryActivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
       
         if($this->filasAfectadas()){
          
           $Activos = true;
           $Cambios = $this->filasAfectadas();
       }else{
           $Activos = false;
           $Cambios ="";
       }
       //este es un array donde muestra si hay cambios en la bd
       $this->respuesta = array("Activos"=>$Activos,"Inactivos"=>$Inactivos,"Cambios"=>$Cambios);
       
        
    }//VerificarCapacitacion
    
    
    //------------------------------------------------------//
    
    
    
    function VerificarSimit(){
        //este query valida si estado soat es menor a la fecha actual 
        
       $queryInactivo = "UPDATE documento_conductor  
        INNER JOIN conductores ON (documento_conductor.documento_conductore = conductores.Cedulaconductor)
        SET documento_conductor.esta_simit = 'inactivo' 
        WHERE  documento_conductor.fecha_veci_simit_conductor < CURDATE() AND conductores.idempresa = :idempresa";
       
//este query valida si estado soat es mayor a la fecha actual
       $queryActivo = "UPDATE documento_conductor  
        INNER JOIN conductores ON (documento_conductor.documento_conductore = conductores.Cedulaconductor)
        SET documento_conductor.esta_simit = 'activo' 
        WHERE  documento_conductor.fecha_veci_simit_conductor > CURDATE() AND conductores.idempresa = :idempresa";
       
       $array = array(":idempresa"=>$_SESSION['idEmpresa']);
       $Inactivos = "";
       $Activos = "";
       //Ejecutando metodo para hacer cambios en la bd 
       $this->SetQuery($queryInactivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
        if($this->filasAfectadas()){
           
           $Inactivos = true;
       }else{
           $Inactivos = false;
           $Cambios ="";
       }
       
       //
       $this->SetQuery($queryActivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
         if($this->filasAfectadas()){
          
           $Activos = true;
           $Cambios = $this->filasAfectadas();
       }else{
           $Activos = false;
           
       }
       //este es un array donde muestra si hay cambios en la bd
       $this->respuesta = array("Activos"=>$Activos,"Inactivos"=>$Inactivos,"Cambios"=>$Cambios);
       
        
    }//VerificarSimit
    
    function EstadoTablaConductor(){
        $queryActivo = "UPDATE conductores 
                        INNER JOIN documento_conductor
                        ON (conductores.Cedulaconductor = documento_conductor.documento_conductore)
                        SET conductores.estado = 'activo'
                        WHERE  (documento_conductor.estado_licencia = 'activo' 
                        AND     documento_conductor.esta_simit = 'activo')
                        AND conductores.idempresa = :idempresa ";
        
        
        
        $queryInactivo = "UPDATE conductores 
                        INNER JOIN documento_conductor
                        ON (conductores.Cedulaconductor = documento_conductor.documento_conductore)
                        SET conductores.estado = 'inactivo'
                        WHERE  (documento_conductor.estado_licencia = 'inactivo' 
                        OR    documento_conductor.esta_simit = 'inactivo')
                        AND conductores.idempresa = :idempresa";
        
        
        
        
        $array = array(":idempresa"=>$_SESSION['idEmpresa']);
        $Inactivos = "";
        $Activos = "";
        
        //Ejecutando metodo para hacer cambios en la bd 
       $this->SetQuery($queryInactivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
        if($this->filasAfectadas()){
           
           $Inactivos = true;
       }else{
           $Inactivos = false;
           $Cambios ="";
       }
       
       //
       $this->SetQuery($queryActivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
         if($this->filasAfectadas()){
          
           $Activos = true;
           $Cambios = $this->filasAfectadas();
       }else{
           $Activos = false;
           
       }
       //este es un array donde muestra si hay cambios en la bd
       $this->respuesta = array("Activos"=>$Activos,"Inactivos"=>$Inactivos,"Cambios"=>$Cambios);
        
    }
    
    
    
    }//fin de la clase
  
    
    
    
    
//$provando = new VerificarConductor();
//$provando->VerificarLicencia();
//echo "SEA HAN ACTIVADO ". $provando->getRepuesta()['Activos'];
//echo "<br>";
//echo "SEA HAN INACTIVADO ".$provando->getRepuesta()['Inactivos'];
// 
// 
//echo "<br>";
//
////$provando = new VerificarConductor();
//$provando->VerificarCapacitacion();
//echo "SEA HAN ACTIVADO ". $provando->getRepuesta()['Activos'];
//echo "<br>";
//echo "SEA HAN INACTIVADO ".$provando->getRepuesta()['Inactivos'];
// 
// echo "<br>";
//
////$provando = new VerificarConductor();
//$provando->VerificarSimit();
//echo "SEA HAN ACTIVADO ". $provando->getRepuesta()['Activos'];
//echo "<br>";
//echo "SEA HAN INACTIVADO ".$provando->getRepuesta()['Inactivos'];
//echo "<br>";
//echo "Cambios ".$provando->getRepuesta()['Cambios'];
//
//
//echo "<br>";
//
//$provando->EstadoTablaConductor();
//echo "SEA HAN ACTIVADO ". $provando->getRepuesta()['Activos'];
//echo "<br>";
//echo "SEA HAN INACTIVADO ".$provando->getRepuesta()['Inactivos'];
//echo "<br>";
//echo "Cambios ".$provando->getRepuesta()['Cambios'];

 
 
