<?php
require_once '../../conexion/sessionlogin.php';
require_once '../../conexion/conexion.php';
require_once '../../clases/baseDeDatos.php';



class VericarVehiculo extends baseDeDatos{
  
    private  $respuesta  ;
    
    function getRepuesta(){
        return $this->respuesta;
    }
    
    
    function VerificarEstadoSoat(){
        //este query valida si estado soat es menor a la fecha actual 
        
       $queryInactivo = "UPDATE documentos_vehiculos  
                INNER JOIN vehiculos ON (documentos_vehiculos.placa = vehiculos.placa)
                SET documentos_vehiculos.estado_soat = 'inactivo' 
                WHERE  documentos_vehiculos.fecha_veci_soat < CURDATE() AND vehiculos.idempresa = :idempresa";
       
//este query valida si estado soat es mayor a la fecha actual
       $queryActivo = "UPDATE documentos_vehiculos  
                INNER JOIN vehiculos ON (documentos_vehiculos.placa = vehiculos.placa)
                SET documentos_vehiculos.estado_soat = 'activo' 
                WHERE  documentos_vehiculos.fecha_veci_soat > CURDATE() AND vehiculos.idempresa = :idempresa";
       
       $array = array(":idempresa"=>$_SESSION['idEmpresa']);
       
       
       //Ejecutando metodo para hacer cambios en la bd 
       $this->SetQuery($queryInactivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
       if($this->filasAfectadas()){
           
           $Inactivos = true;
       }else{
           $Inactivos = false;
           
       }
       
       //
       $this->SetQuery($queryActivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
       if($this->filasAfectadas()){
          
           $Activos = true;
           $Cambios = $this->filasAfectadas();
       }else{
           $Activos = false;
           $Cambios ="";
           
       }
       //este es un array donde muestra si hay cambios en la bd
       $this->respuesta = array("Activos"=>$Activos,"Inactivos"=>$Inactivos,"Cambios"=>$Cambios);
        
    }//VerificarEstadoSoat
    
    
    
    
    //-------------------------------------------------------------//
    
    
    
    function VerificarEstadoTecnicomecanica(){
        //este query valida si estado soat es menor a la fecha actual 
        
       $queryInactivo = "UPDATE documentos_vehiculos  
                        INNER JOIN vehiculos ON (documentos_vehiculos.placa = vehiculos.placa)
                        SET documentos_vehiculos.estado_tecnicomecanica = 'inactivo' 
                        WHERE  documentos_vehiculos.fecha_veci_tecnomecanica < CURDATE() AND vehiculos.idempresa = :idempresa ";
       
//este query valida si estado soat es mayor a la fecha actual
       $queryActivo = "UPDATE documentos_vehiculos  
                    INNER JOIN vehiculos ON (documentos_vehiculos.placa = vehiculos.placa)
                    SET documentos_vehiculos.estado_tecnicomecanica = 'activo' 
                    WHERE  documentos_vehiculos.fecha_veci_tecnomecanica > CURDATE() AND vehiculos.idempresa = :idempresa ";
       
       $array = array(":idempresa"=>$_SESSION['idEmpresa']);
       
       
       //Ejecutando metodo para hacer cambios en la bd 
       $this->SetQuery($queryInactivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
        if($this->filasAfectadas()){
           
           $Inactivos = true;
       }else{
           $Inactivos = false;
           
       }
       
       //
       $this->SetQuery($queryActivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
       if($this->filasAfectadas()){
          
           $Activos = true;
           $Cambios = $this->filasAfectadas();
       }else{
           $Activos = false;
           $Cambios ="";
           
       }
       //este es un array donde muestra si hay cambios en la bd
       $this->respuesta = array("Activos"=>$Activos,"Inactivos"=>$Inactivos,"Cambios"=>$Cambios);
        
    }//VerificarEstadoTecnicomecanica
    
    
    
    //----------------------------------------------//
    
    function VerificarEstadoMantenimento(){
        //este query valida si estado soat es menor a la fecha actual 
        
       $queryInactivo = "UPDATE documentos_vehiculos  
                    INNER JOIN vehiculos ON (documentos_vehiculos.placa = vehiculos.placa)
                    SET documentos_vehiculos.estado_mantenimento = 'inactivo' 
                    WHERE  documentos_vehiculos.fecha_veci_mantenimento < CURDATE() AND vehiculos.idempresa = :idempresa ";
       
//este query valida si estado soat es mayor a la fecha actual
       $queryActivo = "UPDATE documentos_vehiculos  
                    INNER JOIN vehiculos ON (documentos_vehiculos.placa = vehiculos.placa)
                    SET documentos_vehiculos.estado_mantenimento = 'activo' 
                    WHERE  documentos_vehiculos.fecha_veci_mantenimento > CURDATE() AND vehiculos.idempresa = :idempresa ";
       
       $array = array(":idempresa"=>$_SESSION['idEmpresa']);
       
       
       //Ejecutando metodo para hacer cambios en la bd 
       $this->SetQuery($queryInactivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
        if($this->filasAfectadas()){
           
           $Inactivos = true;
       }else{
           $Inactivos = false;
           
       }
       
       //
       $this->SetQuery($queryActivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
       if($this->filasAfectadas()){
          
           $Activos = true;
           $Cambios = $this->filasAfectadas();
       }else{
           $Activos = false;
           $Cambios ="";
           
       }
       //este es un array donde muestra si hay cambios en la bd
       $this->respuesta = array("Activos"=>$Activos,"Inactivos"=>$Inactivos,"Cambios"=>$Cambios);
        
    }//VerificarEstadoMantenimento
    
    //----------------------------------------------------------------//
    
    
    function VerificarEstadoCorrectivo(){
        //este query valida si estado soat es menor a la fecha actual 
        
       $queryInactivo = "UPDATE documentos_vehiculos  
        INNER JOIN vehiculos ON (documentos_vehiculos.placa = vehiculos.placa)
        SET documentos_vehiculos.estado_mantenimentoCorrectivo = 'inactivo' 
        WHERE  documentos_vehiculos.fecha_veci_correctivo < CURDATE() AND vehiculos.idempresa = :idempresa ";
       
//este query valida si estado soat es mayor a la fecha actual
       $queryActivo = "UPDATE documentos_vehiculos  
        INNER JOIN vehiculos ON (documentos_vehiculos.placa = vehiculos.placa)
        SET documentos_vehiculos.estado_mantenimentoCorrectivo = 'activo' 
        WHERE  documentos_vehiculos.fecha_veci_correctivo > CURDATE() AND vehiculos.idempresa = :idempresa ";
       
       $array = array(":idempresa"=>$_SESSION['idEmpresa']);
       
       
       //Ejecutando metodo para hacer cambios en la bd 
       $this->SetQuery($queryInactivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
        if($this->filasAfectadas()){
           
           $Inactivos = true;
       }else{
           $Inactivos = false;
           
       }
       
       //
       $this->SetQuery($queryActivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
       if($this->filasAfectadas()){
          
           $Activos = true;
           $Cambios = $this->filasAfectadas();
       }else{
           $Activos = false;
           $Cambios ="";
           
       }
       //este es un array donde muestra si hay cambios en la bd
       $this->respuesta = array("Activos"=>$Activos,"Inactivos"=>$Inactivos,"Cambios"=>$Cambios);
        
    }//VerificarEstadoCorrectivo
    
    
    
    
    //----------------------------------------------//
    
    
    
        function VerificarTarjetaOperacion(){
        //este query valida si estado soat es menor a la fecha actual 
        
       $queryInactivo = "UPDATE documentos_vehiculos  
        INNER JOIN vehiculos ON (documentos_vehiculos.placa = vehiculos.placa)
        SET documentos_vehiculos.estado_tarjeta_de_operacion = 'inactivo' 
        WHERE  documentos_vehiculos.fecha_veci_tarjeta_operacion < CURDATE() AND vehiculos.idempresa = :idempresa";
       
//este query valida si estado soat es mayor a la fecha actual
       $queryActivo = "UPDATE documentos_vehiculos  
        INNER JOIN vehiculos ON (documentos_vehiculos.placa = vehiculos.placa)
        SET documentos_vehiculos.estado_tarjeta_de_operacion = 'activo' 
        WHERE  documentos_vehiculos.fecha_veci_tarjeta_operacion > CURDATE() AND vehiculos.idempresa = :idempresa ";
       
       $array = array(":idempresa"=>$_SESSION['idEmpresa']);
       
       
       //Ejecutando metodo para hacer cambios en la bd 
       $this->SetQuery($queryInactivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
        if($this->filasAfectadas()){
           
           $Inactivos = true;
       }else{
           $Inactivos = false;
           
       }
       
       //
       $this->SetQuery($queryActivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
       if($this->filasAfectadas()){
          
           $Activos = true;
           $Cambios = $this->filasAfectadas();
       }else{
           $Activos = false;
           $Cambios ="";
           
       }
       //este es un array donde muestra si hay cambios en la bd
       $this->respuesta = array("Activos"=>$Activos,"Inactivos"=>$Inactivos,"Cambios"=>$Cambios);
    }//VerificarTarjetaOperacion
    
    
    
    //---------------------------------------------------///
    
    function VerificarTarjetaPropiedad(){
        //este query valida si estado soat es menor a la fecha actual 
        
       $queryInactivo = "UPDATE documentos_vehiculos  
INNER JOIN vehiculos ON (documentos_vehiculos.placa = vehiculos.placa)
SET documentos_vehiculos.estado_tarjeta_de_propiedad = 'inactivo' 
WHERE  documentos_vehiculos.fecha_veci_propiedad < CURDATE() AND vehiculos.idempresa = :idempresa ";
       
//este query valida si estado soat es mayor a la fecha actual
       $queryActivo = "UPDATE documentos_vehiculos  
INNER JOIN vehiculos ON (documentos_vehiculos.placa = vehiculos.placa)
SET documentos_vehiculos.estado_tarjeta_de_propiedad = 'activo' 
WHERE  documentos_vehiculos.fecha_veci_propiedad > CURDATE() AND vehiculos.idempresa = :idempresa ";
       
       $array = array(":idempresa"=>$_SESSION['idEmpresa']);
       
       
       //Ejecutando metodo para hacer cambios en la bd 
       $this->SetQuery($queryInactivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
        if($this->filasAfectadas()){
           
           $Inactivos = true;
       }else{
           $Inactivos = false;
           
       }
       
       //
       $this->SetQuery($queryActivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
       if($this->filasAfectadas()){
          
           $Activos = true;
           $Cambios = $this->filasAfectadas();
       }else{
           $Activos = false;
           $Cambios ="";
           
       }
       //este es un array donde muestra si hay cambios en la bd
       $this->respuesta = array("Activos"=>$Activos,"Inactivos"=>$Inactivos,"Cambios"=>$Cambios);
        
    }//VerificarTarjetaPropiedad
    
       
    
    
    //--------------------------------------------//
    
    
    function VerificarSimit(){
        //este query valida si estado soat es menor a la fecha actual 
        
       $queryInactivo = "UPDATE documentos_vehiculos  
        INNER JOIN vehiculos ON (documentos_vehiculos.placa = vehiculos.placa)
        SET documentos_vehiculos.estado_simit = 'inactivo' 
        WHERE  documentos_vehiculos.fecha_veci_simit < CURDATE() AND vehiculos.idempresa = :idempresa ";
       
//este query valida si estado soat es mayor a la fecha actual
       $queryActivo = "UPDATE documentos_vehiculos  
        INNER JOIN vehiculos ON (documentos_vehiculos.placa = vehiculos.placa)
        SET documentos_vehiculos.estado_simit = 'activo' 
        WHERE  documentos_vehiculos.fecha_veci_simit > CURDATE() AND vehiculos.idempresa = :idempresa ";
       
       $array = array(":idempresa"=>$_SESSION['idEmpresa']);
       
       
       //Ejecutando metodo para hacer cambios en la bd 
       $this->SetQuery($queryInactivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
        if($this->filasAfectadas()){
           
           $Inactivos = true;
       }else{
           $Inactivos = false;
           
       }
       
       //
       $this->SetQuery($queryActivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
       if($this->filasAfectadas()){
          
           $Activos = true;
           $Cambios = $this->filasAfectadas();
       }else{
           $Activos = false;
           $Cambios ="";
           
       }
       //este es un array donde muestra si hay cambios en la bd
       $this->respuesta = array("Activos"=>$Activos,"Inactivos"=>$Inactivos,"Cambios"=>$Cambios);
    }//VerificarSimit
    
    
    
    function VerificarSupertrasporte(){
        //este query valida si estado soat es menor a la fecha actual 
        
       $queryInactivo = "UPDATE documentos_vehiculos  
INNER JOIN vehiculos ON (documentos_vehiculos.placa = vehiculos.placa)
SET documentos_vehiculos.estado_supertrasporte = 'inactivo' 
WHERE  documentos_vehiculos.fecha_veci_supertrasporte < CURDATE() AND vehiculos.idempresa = :idempresa ";
       
//este query valida si estado soat es mayor a la fecha actual
       $queryActivo = "UPDATE documentos_vehiculos  
INNER JOIN vehiculos ON (documentos_vehiculos.placa = vehiculos.placa)
SET documentos_vehiculos.estado_supertrasporte = 'activo' 
WHERE  documentos_vehiculos.fecha_veci_supertrasporte > CURDATE() AND vehiculos.idempresa = :idempresa ";
       
       $array = array(":idempresa"=>$_SESSION['idEmpresa']);
       
       
       //Ejecutando metodo para hacer cambios en la bd 
       $this->SetQuery($queryInactivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
        if($this->filasAfectadas()){
           
           $Inactivos = true;
       }else{
           $Inactivos = false;
           
       }
       
       //
       $this->SetQuery($queryActivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
       if($this->filasAfectadas()){
          
           $Activos = true;
           $Cambios = $this->filasAfectadas();
       }else{
           $Activos = false;
           $Cambios ="";
           
       }
       //este es un array donde muestra si hay cambios en la bd
       $this->respuesta = array("Activos"=>$Activos,"Inactivos"=>$Inactivos,"Cambios"=>$Cambios);
        
    }//VerificarSimit
    
    
    
        function VerificarAlistamiento(){
        //este query valida si estado soat es menor a la fecha actual 
        
       $queryInactivo = "UPDATE documentos_vehiculos
        INNER JOIN alistamiento 
        ON (documentos_vehiculos.placa = alistamiento.placa_vehiculo )
        SET documentos_vehiculos.estado_alitamiento_Diario = 'inactivo' 
        WHERE alistamiento.fecha_actual <= CURDATE() AND alistamiento.idempresa  = :idempresa ";
       
       //otra ocion del mismo query para comprobar
       /*
        UPDATE documentos_vehiculos
        INNER JOIN alistamiento 
        ON (documentos_vehiculos.placa = alistamiento.placa_vehiculo )
        SET documentos_vehiculos.estado_alitamiento_Diario = 'inactivo00' 
        WHERE EXISTS (SELECT  alistamiento.fecha_actual FROM alistamiento WHERE  alistamiento.fecha_actual < CURDATE() AND alistamiento.idempresa = '10423501260')         */

//este query valida si estado soat es mayor a la fecha actual
       $queryActivo = "UPDATE documentos_vehiculos
        INNER JOIN alistamiento 
        ON (documentos_vehiculos.placa = alistamiento.placa_vehiculo )
        SET documentos_vehiculos.estado_alitamiento_Diario = 'activo' 
        WHERE alistamiento.fecha_actual = CURDATE() AND alistamiento.idempresa  = :idempresa ";
       
       $array = array(":idempresa"=>$_SESSION['idEmpresa']);
       
       
       //Ejecutando metodo para hacer cambios en la bd 
       $this->SetQuery($queryInactivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
        if($this->filasAfectadas()){
           
           $Inactivos = true;
       }else{
           $Inactivos = false;
           
       }
       
       //
       $this->SetQuery($queryActivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
       if($this->filasAfectadas()){
          
           $Activos = true;
           $Cambios = $this->filasAfectadas();
       }else{
           $Activos = false;
           $Cambios ="";
           
       }
       //este es un array donde muestra si hay cambios en la bd
       $this->respuesta = array("Activos"=>$Activos,"Inactivos"=>$Inactivos,"Cambios"=>$Cambios);
        
    }//VerificarSimit
    
    
    
    
    
    function EstadoTablaVehiculo(){
        
        $queryActivo = "UPDATE vehiculos 
                        INNER JOIN documentos_vehiculos ON (vehiculos.placa = documentos_vehiculos.placa)
                        SET vehiculos.estado = 'activo'
                        WHERE  documentos_vehiculos.estado_soat = 'activo' 
                        AND  documentos_vehiculos.estado_tecnicomecanica = 'activo' 
                        AND documentos_vehiculos.estado_mantenimento = 'activo'
                        AND documentos_vehiculos.estado_mantenimentoCorrectivo = 'activo' 
                        AND documentos_vehiculos.estado_tarjeta_de_operacion = 'activo'
                        AND documentos_vehiculos.estado_tarjeta_de_propiedad = 'activo'
                        AND documentos_vehiculos.estado_simit = 'activo'
                        AND documentos_vehiculos.estado_supertrasporte = 'activo'
                        AND documentos_vehiculos.estado_alitamiento_Diario = 'activo' 
                        AND vehiculos.idempresa = :idempresa ";
        
        
        $queryInactivo = "UPDATE vehiculos 
                        INNER JOIN documentos_vehiculos ON (vehiculos.placa = documentos_vehiculos.placa)
                        SET vehiculos.estado = 'inactivo'
                        WHERE  documentos_vehiculos.estado_soat = 'inactivo' 
                        OR   documentos_vehiculos.estado_tecnicomecanica = 'inactivo' 
                        OR documentos_vehiculos.estado_mantenimento = 'inactivo'
                        OR documentos_vehiculos.estado_mantenimentoCorrectivo = 'inactivo' 
                        OR documentos_vehiculos.estado_tarjeta_de_operacion = 'inactivo'
                        OR documentos_vehiculos.estado_tarjeta_de_propiedad = 'inactivo'
                        OR documentos_vehiculos.estado_simit = 'inactivo'
                        OR documentos_vehiculos.estado_supertrasporte = 'inactivo'
                        OR documentos_vehiculos.estado_alitamiento_Diario <> 'activo' 
                        AND  vehiculos.idempresa = :idempresa ";
        
        $array = array(":idempresa"=>$_SESSION['idEmpresa']);
        
              //Ejecutando metodo para hacer cambios en la bd 
       $this->SetQuery($queryInactivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
        if($this->filasAfectadas()){
           
           $Inactivos = true;
       }else{
           $Inactivos = false;
           
       }
        
        
        
        
        
         //
       $this->SetQuery($queryActivo);
       $this->SetArray_actualizar($array);
       $this->Actualizar();
       if($this->filasAfectadas()){
          
           $Activos = true;
           $Cambios = $this->filasAfectadas();
       }else{
           $Activos = false;
           $Cambios ="";
           
       }
       //este es un array donde muestra si hay cambios en la bd
       $this->respuesta = array("Activos"=>$Activos,"Inactivos"=>$Inactivos,"Cambios"=>$Cambios);
        
        
        
        
        
    }
    
    
    
    
}//fin de la clase




//$provando = new VericarVehiculo();
//$provando->VerificarEstadoSoat();
//echo "SEA HAN ACTIVADO " . $provando->getRepuesta()['Activos'];
//echo "<br>";
//echo "SEA HAN INACTIVADO " . $provando->getRepuesta()['Inactivos'];
// 
//echo "<br>";
// 
//
////$provando = new VericarVehiculo();
//$provando->VerificarEstadoTecnicomecanica();
//echo "SEA HAN ACTIVADO ". $provando->getRepuesta()['Activos'];
//echo "<br>";
//echo "SEA HAN INACTIVADO ".$provando->getRepuesta()['Inactivos'];
//
//
//echo "<br>";
//
////$provando = new VericarVehiculo();
//$provando->VerificarEstadoMantenimento();
//echo "SEA HAN ACTIVADO ". $provando->getRepuesta()['Activos'];
//echo "<br>";
//echo "SEA HAN INACTIVADO ".$provando->getRepuesta()['Inactivos'];
//
//echo "<br>";
//
////$provando = new VericarVehiculo();
//$provando->VerificarEstadoCorrectivo();
//echo "SEA HAN ACTIVADO ". $provando->getRepuesta()['Activos'];
//echo "<br>";
//echo "SEA HAN INACTIVADO ".$provando->getRepuesta()['Inactivos'];
//
//echo "<br>";
//
////$provando = new VericarVehiculo();
//$provando->VerificarTarjetaOperacion();
//echo "SEA HAN ACTIVADO ". $provando->getRepuesta()['Activos'];
//echo "<br>";
//echo "SEA HAN INACTIVADO ".$provando->getRepuesta()['Inactivos'];
// 
//echo "<br>"; 
//
////$provando = new VericarVehiculo();
//$provando->VerificarTarjetaPropiedad();
//echo "SEA HAN ACTIVADO ". $provando->getRepuesta()['Activos'];
//echo "<br>";
//echo "SEA HAN INACTIVADO ".$provando->getRepuesta()['Inactivos'];
// 
//echo "<br>"; 
//
////$provando = new VericarVehiculo();
//$provando->VerificarSimit();
//echo "SEA HAN ACTIVADO ". $provando->getRepuesta()['Activos'];
//echo "<br>";
//echo "SEA HAN INACTIVADO ".$provando->getRepuesta()['Inactivos'];
// 
//echo "<br>";
//
////$provando = new VericarVehiculo();
//$provando->VerificarSupertrasporte();
//echo "SEA HAN ACTIVADO ". $provando->getRepuesta()['Activos'];
//echo "<br>";
//echo "SEA HAN INACTIVADO ".$provando->getRepuesta()['Inactivos'];
//
//echo "<br>";
//
//echo "CAMBIO ".$provando->getRepuesta()['Cambios'];
//
//
//$provando->VerificarAlistamiento();
//echo "SEA HAN ACTIVADO ".$provando->getRepuesta()['Activos'];
//echo "<br>";
//echo "SEA HAN INACTIVADO ".$provando->getRepuesta()['Inactivos'];
//
//echo "<br>";
//
//echo "CAMBIO ".$provando->getRepuesta()['Cambios'];
//
//
//
//
//echo "<br>";
//$provando->EstadoTablaVehiculo();
//echo "SEA HAN ACTIVADO ". $provando->getRepuesta()['Activos'];
//echo "<br>";
//echo "SEA HAN INACTIVADO ".$provando->getRepuesta()['Inactivos'];
//
//echo "CAMBIO ".$provando->getRepuesta()['Cambios'];