<?php

class baseDeDatos  {
    //atributos estos son array por donde vendran los datos
    private $array_insertar ;
    private $array_actualizar;
    private $array_eliminar;
    private $query;
    private $array_selecionar;
    private $filasAfectadas;

    //con esta funcion podremos agregar datos en el array
    protected function SetArray_insertar($array_insertar){
        $this->array_insertar  = $array_insertar;
        
    }
    //con esta funcion podremos Actuzalizar datos en el array
    protected function SetArray_actualizar($array_actualizar){
        $this->array_actualizar = $array_actualizar;
        
    }
    //con esta funcion podremos eliminar datos en el array
    protected function SetArray_eliminar($array_eliminar){
        $this->array_eliminar = $array_eliminar;
        
    }

   protected function SetQuery($query){
        $this->query = $query;
    }    
    
    protected function Setarray_selecionar($array_selecionar){
        $this->array_selecionar = $array_selecionar;
    }
    
    
    //funcion para hacer conexion a la base de datos 
    
    
    //funcion para insertar datos
    //colocar protected
    protected function Insertar(){
       try{
        $conexion = new conexion();
        $insertar = $conexion->prepare($this->query);
        $insertar->execute($this->array_insertar);
        $this->filasAfectadas =  $insertar->rowCount();
        $conexion = NULL;
       }catch(Exception $ex){
           echo "[Error] codigo de error ".$ex->getCode();
       }
    }
    
    //funcion para actualizar  datos
    //colocar protected
    protected function Actualizar(){
       
       $conexion = new conexion();
        $insertar = $conexion->prepare($this->query);
        $insertar->execute($this->array_actualizar);
        $this->filasAfectadas =  $insertar->rowCount();
        $conexion = NULL;
        
    }
    
    //funcion para eliminar  datos
    //colocar protected
    protected function Eliminar(){
        
        $conexion = new conexion();
        $insertar = $conexion->prepare($this->query);
        $insertar->execute($this->array_eliminar);
        $this->filasAfectadas =  $insertar->rowCount();
        $conexion = NULL;
        
        
    }
    //colocar protected
    protected function seleccionar(){
       
         $conexion = new conexion();
         ///qui esta recibiendo el query y de buelve el resultado
         $resultado = $conexion->prepare($this->query);
         $resultado->execute($this->array_selecionar);
         return $resultado;
         $conexion = NULL;
//        
//        return $this->query;
        
    }
    
    //colocar protected
    function contar_registro(){
        $conexion  = new conexion();
        $resultado = $conexion->prepare($this->query);
        $resultado->execute($this->array_selecionar);
        $resultado = $resultado->rowCount();
         return $resultado ;
         $conexion = NULL;
    }
 //esta uncio es para paginar registros grandes
 protected function paginasionMostrar($pagina=null,$limite=null){
//        crando instacia a la base de datos ;
       
        $conexion = new conexion();
//        si no ha valor le asigna uno  
        if($pagina< 1){
            $paginas =1;
        }else{
           $paginas =  (int) $pagina ;
     }
        //        si no ha valor le asigna uno 
        if($limite< 1){
            $limites =10;
        }else{
            $limites =  (int) $limite ; 
        }
        $offset = ($paginas-1)*$limites;
//        $query = "SELECT * FROM  empresas";
//        queri complementaria para la paginacion
        $complementoQuery =  (" ORDER BY id DESC ". " LIMIT ".$offset." , ".$limites);
//        esto aqui ejecuta el query enviado y el complemtario 
//        $busqueda = $conexion->query($this->query.$complementoQuery);
        $busqueda = $conexion->prepare($this->query.$complementoQuery);
        $busqueda->execute($this->array_selecionar);
        return $busqueda;
    
        $conexion = NULL;
    
}

        //esta funcion sirve para saber si hay filas afectadas
        //color car protected
        function filasAfectadas(){
           if($this->filasAfectadas > 0){
               return true;
           }else{
               return false;
           }
            
        } 
        
        //esta funcion calcula las fechas 
        function calcularSemanas($fecha_ano_mes_dia){ 
                $datetime1 = new DateTime(date('Y-m-d'));
                $datetime2 = new DateTime($fecha_ano_mes_dia);
                //calcula la diferencia entre fechas 
                if($datetime1 < $datetime2){
                $interval = $datetime1->diff($datetime2);
                //muestra la semanas restantes entre 
               //si las semana es igual a cero
                if(floor(($interval->format('%a') / 7)) == 0){
                    return '<img  src="../../img/Alerta/AlertIconAmarillo.svg" style="width: 22px;" title="Alerta" alt="" class="parpadea" >'
                    .'<span style="color:C99105;"> Quedan '. ($interval->format('%a') % 7) . ' días </span>'; 
                }
                //si las semana es Mayor a cero
                if(floor(($interval->format('%a') / 7)) > 0){
                    return '<span style="color:107200;">'. floor(($interval->format('%a') / 7)) . ' semanas con '. ($interval->format('%a') % 7) . ' días </span>';
                }
                
                }else{
                    //devuelve cero para indicar que el conteo termino
                    return '<span style="color:BA0900;"><img  src="../../img/Alerta/alerticoRojo.svg" style="width: 22px;" title="Vencido" alt="" class="parpadea" > Vencido</span>';
                }
                 }//fincalculador de semanas
                 
              
               
                 
                 
                 
        function EvaluarEstado($fechaVigencia){ 
                $datetime1 = new DateTime(date('Y-m-d'));
                $datetime2 = new DateTime($fechaVigencia);
                //calcula la diferencia entre fechas 
                if($datetime1 < $datetime2){
                
                //muestra la semanas restantes entre 
                return  true;
                }else{
                    //devuelve cero para indicar que el conteo termino
                    return false;
                }
                 }//fincalculador de semanas   
                 
                 
                 
                
                 
        


        }// fin baseDeDatos

//ESTA FUNCION ES DE EJEMPLO PARA INSERTAR
// *HACIENDO INSTANCIA 
//$insertar = new baseDeDatos();
// * 
//* CREANDO EL QUERY O PETION 
//$sql = "INSERT INTO usuarios (nombre,apellido) VALUES (:nombre,:apellido)";
// * 
// * CREANDO EL ARRAY PORDONDE SE PASARAN LOS PARAMETROS 
//$arrary = array ("nombre"=>"jesus","apellido"=>"pena");
// 
//  LLAMANDO A LA FUNCION 
//$insertar->SetArray_insertar($arrary);
//$insertar->Insertar($sql);




