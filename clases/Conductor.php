<?php

//*********************************************************//

// CLASE  CONDUCTOR 

//*******************************************************//


class Conductor extends baseDeDatos {
    //put your code here
    protected $resulltado ;
    protected $NumeroPagina;
    protected $limiteDeDados;
    
    public function __construct() {
        $this->NumeroPagina = 1;
        $this->limiteDeDados  = 5;
    }
    //este metodo es para indicar el numero de pagina mostrar
    function SetNumeroPagina($numeroPagina){
        $this->NumeroPagina = $numeroPagina;
    }
    //este metodo limita los registros a mostrar 
    function SetLimiteDatos($limite){
        $this->limiteDeDados = $limite;
    }
    
    
    function getResultado(){
        return $this->resulltado;
        
        
    }
    
    
    function AgregarConductor($nombreapellido,$Cedulaconductor,$numero_contacto,$Fecha_nacimento){
        try{
                $queryConductor = "INSERT INTO conductores (idempresa,nombreapellido,Cedulaconductor,numero_contacto,Fecha_nacimento)"
                            . "VALUES (:idempresa,:nombreapellido,:Cedulaconductor,:numero_contacto,:Fecha_nacimento)";
                $arrayConductor = array(":idempresa"=>$_SESSION['idEmpresa'],
                                    ":nombreapellido"=>$nombreapellido,
                                    ":Cedulaconductor"=>$Cedulaconductor,
                                    ":numero_contacto"=>$numero_contacto,
                                    ":Fecha_nacimento"=>$Fecha_nacimento);
                
                  //pasando el query              
                $this->SetQuery($queryConductor);
                //pasando el array con los datos
                
                $this->SetArray_insertar($arrayConductor);
                //llamando el metodo para  insrtar
                $this->Insertar();
                    //SI UNA FILA A SIDO AFECTADA 
                    if($this->filasAfectadas()){
                        $this->resulltado = true;
                    }else{
                        $this->resulltado = false;
                    } 
                    
                   
        
        } catch (Exception $ex ){
            echo "[Error] codigo de error ".$ex->getCode();
        }
    }
    
    function ActulizarConductor($Cedulaconductor,$nombreapellido,$numero_contacto,$Fecha_nacimento){
       try{
        
        $queryActulizarConductor = "UPDATE conductores SET nombreapellido = :nombreapellido,"
                                . " Cedulaconductor = :Cedulaconductor,"
                                . " numero_contacto = :numero_contacto, Fecha_nacimento = :Fecha_nacimento "
                                . " WHERE Cedulaconductor = :Cedulaconductor AND idempresa = :idempresa";
        
        
        $arrayActulizarConductor = array (":nombreapellido"=>$nombreapellido,
                                          ":Cedulaconductor"=>$Cedulaconductor,
                                          ":numero_contacto"=>$numero_contacto,
                                          ":Fecha_nacimento"=>$Fecha_nacimento,
                                          ":idempresa"=>$_SESSION['idEmpresa']);
        
        $this->SetQuery($queryActulizarConductor);
        $this->SetArray_actualizar($arrayActulizarConductor);
        $this->Actualizar();
                if($this->filasAfectadas()){
                        $this->resulltado = true;
                    }else{
                        $this->resulltado = false;
                    } 
       } catch (Exception $ex){
           echo "[Error] codigo de error ".$ex->getCode();
       }
    }
    
    
    //Este metodo se encarga de eliminar un con ductor por numero de documento
    function EliminarConductorCecula($cedulaConductor){
       try{ 
        $queryEliminarConductor = "DELETE FROM conductores WHERE Cedulaconductor = :Cedulaconductor AND idempresa = :idempresa";
        
        $arrayEliminarConductor = array(":Cedulaconductor"=>$cedulaConductor,":idempresa"=>$_SESSION['idEmpresa']);
        
        $this->SetArray_eliminar($arrayEliminarConductor);
        
        $this->SetQuery($queryEliminarConductor);
        $this->Eliminar();
                 if($this->filasAfectadas()){
                        $this->resulltado = true;
                    }else{
                         $this->resulltado = false;
                    } 
       } catch (Exception $ex){
           echo "[Error] codigo de error ".$ex->getCode();
       }
    }
    
    //esta funcion se encarga de eliminar por su estado "VENCIDO"
    function EliminarConductorEstado(){
        try{
        
        $queryEliminarConductor = "DELETE FROM conductores WHERE estado=:estado AND idempresa = :idempresa";
        
        $arrayEliminarConductor = array(":estado"=>"vencido",":idempresa"=>$_SESSION['idEmpresa']);
        
        $this->SetArray_eliminar($arrayEliminarConductor);
        
        $this->SetQuery($queryEliminarConductor);
        $this->Eliminar();
                    if($this->filasAfectadas()){
                        $this->resulltado = true;
                    }else{
                        $this->resulltado = false;
                    }
        
        
        } catch (Exception $ex){
            echo "[Error] codigo de error ".$ex->getCode();
        } 
        
    }
    //esta funcion se en carga d listar todo los conductores
    function ListarConductor(){
        $queryListarConductore = "SELECT * FROM  conductores   WHERE idempresa = :idempresa  ORDER BY id ASC";
        $array = array(":idempresa"=>$_SESSION['idEmpresa']);
        $this->SetQuery($queryListarConductore);
        $this->Setarray_selecionar($array);
        $this->resulltado =  $this->seleccionar();
       
    }
    
    
    //este metodo se encarga de mostrar los conductores para ser paginado 
    function PaginaConductor(){
        $queryListarConductore = "SELECT * FROM  conductores  WHERE idempresa = :idempresa ";
        $arrayPaginarConductores = array(":idempresa"=>$_SESSION['idEmpresa']);
        
        $this->SetQuery($queryListarConductore);
        $this->Setarray_selecionar($arrayPaginarConductores);
        
        $this->resulltado = $this->paginasionMostrar($this->NumeroPagina, $this->limiteDeDados);
        
    }
    
    //esta metodo se en carga de contar los conductores 
    function ContarConductor(){
        $queryListarConductore = "SELECT * FROM  conductores  WHERE idempresa = :idempresa ";
        $arrayPaginarConductores = array(":idempresa"=>$_SESSION['idEmpresa']);
        
        $this->SetQuery($queryListarConductore);
        $this->Setarray_selecionar($arrayPaginarConductores);
        $this->resulltado = $this->contar_registro();
        
    }
    
    //esta metodo sirve para buscar un coductor por el valor que se le pase al funcion todo el resultado 
    function BuscarConductor($dato=null){
        if(empty($dato)){
            
        }else{
            
            $queryBucarConductor = "SELECT * FROM conductores WHERE idempresa = :idempresa"
                    . " HAVING nombreapellido LIKE :dato "
                    . " OR Cedulaconductor LIKE :dato "
                    . " OR numero_contacto LIKE :dato "
                    . " OR Fecha_nacimento LIKE :dato "
                    . " OR estado LIKE :dato";
            
        }
        
            $arrayBuscarConductor = array(":dato"=>'%'.$dato.'%',":idempresa"=>$_SESSION['idEmpresa']);
            $this->Setarray_selecionar($arrayBuscarConductor);
            $this->SetQuery($queryBucarConductor);
            $this->resulltado =  $this->seleccionar();
        
    }
    
    function BuscarConductorEstadoActivo($dato=null){
        if(empty($dato)){
            
        }else{
            
            $queryBucarConductor = "SELECT *  FROM conductores INNER JOIN documento_conductor"
                                 . " ON conductores.Cedulaconductor = documento_conductor.documento_conductore "
                                 . " AND  conductores.estado = 'activo' "
                                 . " AND  conductores.idempresa = :idempresa "
                                 . "HAVING  conductores.Cedulaconductor LIKE  :dato "
                                 . "OR conductores.nombreapellido LIKE :dato ";
            
        }
        
            $arrayBuscarConductor = array(":dato"=>'%'.$dato.'%',":idempresa"=>$_SESSION['idEmpresa']);
            $this->Setarray_selecionar($arrayBuscarConductor);
            $this->SetQuery($queryBucarConductor);
            $this->resulltado =  $this->seleccionar();
        
    }//fin de la funcion BuscarConductorEstadoActivo
    
    
    function NumeroCoductor(){
                $Activo = "";
                $Inactivo = "";
                $Cantidad = "";
              
                $ConductorActivo = "SELECT conductores.Cedulaconductor 
                                    FROM conductores
                                    WHERE conductores.estado = 'activo' 
                                    AND conductores.idempresa = :idempresa ";
                                
                         $array_selecionar = array(":idempresa"=>$_SESSION['idEmpresa']);       
                        $this->Setarray_selecionar($array_selecionar);        
                        $this->SetQuery($ConductorActivo);
                        $Activo = $this->contar_registro();
                        
                $ConductorInactivo = "SELECT conductores.Cedulaconductor 
                                    FROM conductores
                                    WHERE conductores.estado = 'inactivo' 
                                    AND conductores.idempresa = :idempresa ";
                                
//                        $array_selecionar = array(":idempresa"=>$_SESSION['idEmpresa']);       
                        $this->Setarray_selecionar($array_selecionar);        
                        $this->SetQuery($ConductorInactivo);
                        $Inactivo = $this->contar_registro(); 
                        
                $Conductor = "SELECT conductores.Cedulaconductor 
                                FROM conductores
                                WHERE  conductores.idempresa = :idempresa ";
                                
//                        $array_selecionar = array(":idempresa"=>$_SESSION['idEmpresa']);       
                        $this->Setarray_selecionar($array_selecionar);        
                        $this->SetQuery($Conductor);
                        $Cantidad = $this->contar_registro();         
                      
                        $this->resulltado = array("Activo"=>$Activo,"Inactivo"=>$Inactivo,"Cantidad"=>$Cantidad);
                
             }//fin de la funcion
    
    
    
    
}//fin de la clase

//*********************************************************//

// CLASE DOCUMENTO CONDUCTOR 

//*******************************************************//
class DocumentoConductor extends Conductor {
    
            function InsertarDocumentoConductor($documento_conductore) {
                try {
                    
                    $querydocumento_conductore = "INSERT INTO documento_conductor (documento_conductore) VALUES (:documento_conductore)";
                    
                    $arraydocumento_conductore = array(":documento_conductore"=>$documento_conductore);
                    
                    $this->SetArray_insertar($arraydocumento_conductore);
                    $this->SetQuery($querydocumento_conductore);
                    $this->Insertar();
                    
                    if($this->filasAfectadas()){
                        $this->resulltado = true;
                    }else{
                        $this->resulltado = false;
                    }
                    
                    
                    
                    
                } catch (Exception $ex) {
                
                    echo "ERRROR: ".$ex->getLine();
                }
            }
    
    
//            function AgregarDocumentoConductor ($documento_conductor,$licencia,$hojadevida,$capacitacion,$ultima_alcholimetria,$simit){
//                //manejador de errrores 
//                try {
//                    $queryDocumentoConductor = "INSERT INTO documento_conductore (documento_conductor,licencia,hojadevida,capacitacion,ultima_alcholimetria,simit)"
//                                             . " VALUES (:documento_conductor,:licencia,:hojadevida,:capacitacion,:ultima_alcholimetria,:simit)";
//                    
//                    $arrayDocumentoConductor = array (":documento_conductor"=>$documento_conductor,
//                                                      ":licencia"=>$licencia,
//                                                      ":hojadevida"=>$hojadevida,
//                                                      ":capacitacion"=>$capacitacion,
//                                                      ":ultima_alcholimetria"=>$ultima_alcholimetria,
//                                                      ":simit"=>$simit);
//                    $this->SetArray_insertar($arrayDocumentoConductor);
//                    $this->SetQuery($queryDocumentoConductor);
//                    $this->Insertar();
//                    //si hay una fila afectada mustra este mensaje de confirmacion
//                    if($this->filasAfectadas()){
//                        $this->resulltado = true;
//                    }else{
//                        $this->resulltado = false;
//                    } 
//                    
//                    
//                } catch (Exception $ex) {
//                   echo  "Error codigo : ". $ex->getCode();
//                 }//fin try
//               }//AgregarDocumentoConductor
               
               
//            function ActulizarDocumentoConductor($licencia,
//                                                 $fecha_veci_licencia,
//                                                 $hojadevida,
//                                                 $capacitacion,
//                                                 $ultima_capacitacion,
//                                                 $resultado_alcholimetria,
//                                                 $ultima_alcholimetria,
//                                                 $simit,
//                                                 $fecha_veci_simit_conductor,
//                                                 $documento_conductor){
//                    try {
//                        $queryActulizarDocunetoConductor = "UPDATE documento_conductor SET "
//                                                          . " licencia = :licencia , "
//                                                          . " fecha_veci_licencia = :fecha_veci_licencia , "
//                                                          . " hojadevida = :hojadevida , "
//                                                          . " capacitacion = :capacitacion , "
//                                                          . " ultima_capacitacion = :ultima_capacitacion , "
//                                                          . " resultado_alcholimetria = :resultado_alcholimetria , "
//                                                          . " ultima_alcholimetria = :ultima_alcholimetria , "
//                                                          . " simit = :simit , "
//                                                          . " fecha_veci_simit_conductor = :fecha_veci_simit_conductor , "
//                                                          . " WHERE documento_conductore = :documento_conductore ";
//                        $arrayActulizarDocunetoConductor = array (':licencia'=>$licencia ,
//                                                                  ':fecha_veci_licencia'=>$fecha_veci_licencia ,
//                                                                  ':hojadevida'=>$hojadevida ,
//                                                                  ':capacitacion'=>$capacitacion ,
//                                                                  ':ultima_capacitacion'=>$ultima_capacitacion ,
//                                                                  ':resultado_alcholimetria'=>$resultado_alcholimetria ,
//                                                                  ':ultima_alcholimetria'=>$ultima_alcholimetria ,
//                                                                  ':simit'=>$simit ,
//                                                                  ':fecha_veci_simit_conductor'=>$fecha_veci_simit_conductor ,
//                                                                  ':documento_conductore'=>$documento_conductor);
//                        
//                        $this->SetQuery($queryActulizarDocunetoConductor);
//                        $this->SetArray_actualizar($arrayActulizarDocunetoConductor);
//                        $this->Actualizar();
//                                if($this->filasAfectadas()){
//                                    $this->resulltado = true;
//                                }else{
//                                    $this->resulltado = false;
//                                }
//                     
//                       } catch (Exception $ex) {
//                            echo "Error Codigo: ". $ex->getCode();
//                    }//fin try
//                     }  //ActulizarDocumentoConductor
                     
                     
                     
                //este metodo sirve para actulizar algun documento de forma individual
            function actulizarDocumentoConductorLicencia($columna,$columna_fecha,$archivo,$fecha,$documento,$licencia_numero){
                //esta metodo sirve para actulizar los documento de forma individual
                    //$campo = aqui pasa el nombre de la tabla 
                    //$campo_fecha = Es el nombre del campo fecha
                    //$valor = es la ruta del donde esta guardado el archivo pdf
                    //$fecha = es la fecha de vencimiento del archivo
                    //$placa = es indici alcual se le va hacer cambio
                try{

                $queryDocumentoTipo = " UPDATE documento_conductor SET "
                                    . " `".$columna."` = :archivo, "
                                    . " `".$columna_fecha."` = :fecha, "
                                    . " `licencia_numero` = :licencia_numero "
                                    . "  WHERE documento_conductore = :numero ";
               $arrayDocumentoTipo = array(":archivo"=>$archivo,
                                           ":fecha"=>$fecha,
                                           ":numero"=>$documento,
                                           ":licencia_numero"=>$licencia_numero);

                $this->SetQuery($queryDocumentoTipo);
                $this->SetArray_actualizar($arrayDocumentoTipo);
                $this->Actualizar();

                if($this->filasAfectadas()){
                    $this->resulltado = true;
                }else{
                    $this->resulltado = false;
                }
                //fin try
                } catch (Exception $e){
                    die ("[Error] codigo de error  ".$e->getCode());
                }

            }         
            
            
            
            
            
            
            
            
            function  actulizarDocumentoHojaDeVida($hojavida,$numeroDocumento){
                try{
                
                $queryHojaVida = "UPDATE documento_conductor SET hojadevida = :hojadevida WHERE  documento_conductore = :documento_conductore";
                
                $arrayHojaVida = array(":documento_conductore"=>$numeroDocumento,":hojadevida"=>$hojavida);
                
                
                $this->SetQuery($queryHojaVida);
                $this->SetArray_actualizar($arrayHojaVida);
                $this->Actualizar();
                if($this->filasAfectadas()){
                    $this->resulltado = true;
                }else{
                    $this->resulltado = false;
                } 
                
                
                
                } catch (Exception $e){
                    die ("[Error] Linea".$e->getFile());
                }
                
            }


                     //este metodo sirve para actulizar algun documento de forma individual
            function actulizarDocumentoConductor($columna,$columna_fecha,$archivo,$fecha,$documento){
                //esta metodo sirve para actulizar los documento de forma individual
                    //$campo = aqui pasa el nombre de la tabla 
                    //$campo_fecha = Es el nombre del campo fecha
                    //$valor = es la ruta del donde esta guardado el archivo pdf
                    //$fecha = es la fecha de vencimiento del archivo
                    //$placa = es indici alcual se le va hacer cambio
                try{

                $queryDocumentoTipo = " UPDATE documento_conductor SET "
                                    . " `".$columna."` = :archivo, "
                                    . " `".$columna_fecha."` = :fecha "
                                    . "  WHERE documento_conductore = :numero ";
               $arrayDocumentoTipo = array(":archivo"=>$archivo,
                                           ":fecha"=>$fecha,
                                           ":numero"=>$documento);

                $this->SetQuery($queryDocumentoTipo);
                $this->SetArray_actualizar($arrayDocumentoTipo);
                $this->Actualizar();

                if($this->filasAfectadas()){
                    $this->resulltado = true;
                }else{
                    $this->resulltado = false;
                }
                //fin try
                } catch (Exception $e){
                    die ("[Error] codigo de error  ".$e->getCode());
                }

            } 
            
            
            
            function  actulizarDocumentoFoto($Foto,$numeroDocumento){
                try{
                
                $queryHojaVida = "UPDATE documento_conductor SET foto_conductor = :foto WHERE  documento_conductore = :documento_conductore";
                
                $arrayHojaVida = array(":documento_conductore"=>$numeroDocumento,":foto"=>$Foto);
                
                
                $this->SetQuery($queryHojaVida);
                $this->SetArray_actualizar($arrayHojaVida);
                $this->Actualizar();
                if($this->filasAfectadas()){
                    $this->resulltado = true;
                }else{
                    $this->resulltado = false;
                } 
                
                
                
                } catch (Exception $e){
                    die ("[Error] Linea".$e->getFile());
                }
                
            }
            







            //esta funcion elemina un regisro si se le pasa el id y documento o licencia      
            function EliminarDocumentoConductor($id,$Documento){
                try{
                    $queryEliminarDocumentoConductor = "DELETE FROM documento_conductore WHERE id=:id AND documento_conductore = :documento OR licencia = :documento";
                    $arrayEliminarDocumentoConductor = array(":id"=>$id,
                                                             ":documento"=>$Documento);
                    
                    
                    $this->SetQuery($queryEliminarDocumentoConductor);
                    $this->SetArray_eliminar($arrayEliminarDocumentoConductor);
                    $this->Eliminar();  
                    if($this->filasAfectadas()){
                        $this->resulltado = true;
                    }else{
                        $this->resulltado = false;
                    }
                    
                } catch ( Exception $ex){
                    echo "Error codigo".$ex->getCode();
                    
                }
                
                
                
            }//fin documento conductor 
            
               function EliminarDocumentoConductorCedula($Documento){
                try{
                    $queryEliminarDocumentoConductor = "DELETE FROM documento_conductor WHERE documento_conductore = :documento ";
                    $arrayEliminarDocumentoConductor = array(":documento"=>$Documento);
                    
                    
                    $this->SetQuery($queryEliminarDocumentoConductor);
                    $this->SetArray_eliminar($arrayEliminarDocumentoConductor);
                    $this->Eliminar();  
                    if($this->filasAfectadas()){
                        $this->resulltado = true;
                    }else{
                        $this->resulltado = false;
                    }
                    
                } catch ( Exception $ex){
                    echo "Error codigo".$ex->getCode();
                    
                }
             }//fin documento conductor   
            
            
        
        
      
        
        function BuscarDocumentoConductor($dato=null){
            try {
                $queryBuscarDocumentos = "SELECT * FROM documento_conductor WHERE documento_conductore LIKE  :dato OR licencia_numero LIKE :dato ";
                
                $this->SetQuery($queryBuscarDocumentos);
                $arrayBuscarDocumento = array(":dato"=>'%'.$dato.'%');
                $this->Setarray_selecionar($arrayBuscarDocumento);
                $this->resulltado = $this->seleccionar();
                
            } catch (Exception $ex) {
                 echo "Error codigo". $ex->getCode();  
            }
            
            
        }
        
        
                 }// fin DocumentoConductor
                 
 //*********************************************************//

// CLASE ALISTAMIENTO DIARIO

//*******************************************************//                
                 
                 
class AlistamientoDiario extends baseDeDatos{
               //put your code here
                protected $resulltado ;
                protected $NumeroPagina;
                protected $limiteDeDados;
                protected $dato;

                public function __construct() {
                    $this->NumeroPagina = 1;
                    $this->limiteDeDados  = 10;
                }
                
               
                function getResultado(){
                    return $this->resulltado;


                }
                
                
                function SetDato($dato){                    
                    $this->dato = $dato;
                }
                
                function SetNumeroPagina($NumeroPagina){

                    $this->NumeroPagina = $NumeroPagina;
                }
    
    
          
            function  AgregarAlistamientoDiario($idempresa,
                                                $nombre_conductor,
                                                $cedula_conductor,
                                                $ciudad,
                                                $fecha_actual,
                                                $placa_vehiculo,
                                                $kilometraje,
                                                $direccionales_delanteras,
                                                $direccionales_delanteras_observacion ,
                                                $direccionales_traseras ,
                                                $direccionales_traseras_observacion ,
                                                $luces_altas ,
                                                $luces_altas_observacion ,
                                                $luces_bajas ,
                                                $luces_bajas_observacion , 
                                                $luces_stops ,
                                                $luces_stops_observacion ,
                                                $luces_reversa ,
                                                $luces_reversa_observacion ,
                                                $luces_parqueo ,
                                                $luces_parqueo_observacion ,
                                                $limpiabrisas_der_izq ,
                                                $limpiabrisas_der_izq_observacion , 
                                                $limpiabrisas_atras ,
                                                $limpiabrisas_atras_observacion ,
                                                $frenos_principal ,
                                                $frenos_principal_observacion ,
                                                $frenos_emergencia ,
                                                $frenos_emergencia_observacion ,
                                                $llantas_delanteras ,
                                                $llantas_delanteras_observacion , 
                                                $llantas_traseras ,
                                                $llantas_traseras_observacion ,
                                                $llantas_repuestos ,
                                                $llantas_repuestos_observacion ,
                                                $espejos_lateral_der_izq ,
                                                $espejos_lateral_der_izq_observacion ,
                                                $espejos_retrovisor ,
                                                $espejos_retrovisor_observacion , 
                                                $pito ,
                                                $pito_observacion ,
                                                $fluidos_frenos ,
                                                $fluidos_frenos_observacion ,
                                                $fluidos_aceites ,
                                                $fluidos_aceites_observacion ,
                                                $fluidos_refriguerante ,
                                                $fluidos_refriguerante_observacion , 
                                                $herraminetas ,
                                                $herramientas_observacion ,
                                                $crucetas ,
                                                $crucetas_observacion ,
                                                $gato ,
                                                $gato_observacion ,
                                                $taco ,
                                                $taco_observacion , 
                                                $senales ,
                                                $senales_observacion ,
                                                $chaleco ,
                                                $chaleco_observacion ,
                                                $botiquin ,
                                                $botiquin_observacion,
                                                $visto){
                try {
                    
                    $query = "INSERT INTO alistamiento "
                            . " (idempresa,"
                            . " nombre_conductor,"
                            . " cedula_conductor,"
                            . " ciudad,"
                            . " fecha_actual,"
                            . " placa_vehiculo,"
                            . " kilometraje,"
                            . " direccionales_delanteras,"
                            . " direccionales_delanteras_observacion,"
                            . " direccionales_traseras,"
                            . " direccionales_traseras_observacion,"
                            . " luces_altas,"
                            . " luces_altas_observacion,"
                            . " luces_bajas,"
                            . " luces_bajas_observacion,"
                            . " luces_stops,"
                            . " luces_stops_observacion,"
                            . " luces_reversa,"
                            . " luces_reversa_observacion,"
                            . " luces_parqueo,"
                            . " luces_parqueo_observacion,"
                            . " limpiabrisas_der_izq,"
                            . " limpiabrisas_der_izq_observacion,"
                            . " limpiabrisas_atras,"
                            . " limpiabrisas_atras_observacion,"
                            . " frenos_principal,"
                            . " frenos_principal_observacion,"
                            . " frenos_emergencia,"
                            . " frenos_emergencia_observacion,"
                            . " llantas_delanteras,"
                            . " llantas_delanteras_observacion,"
                            . " llantas_traseras,"
                            . " llantas_traseras_observacion,"
                            . " llantas_repuestos,"
                            . " llantas_repuestos_observacion,"
                            . " espejos_lateral_der_izq,"
                            . " espejos_lateral_der_izq_observacion,"
                            . " espejos_retrovisor,"
                            . " espejos_retrovisor_observacion,"
                            . " pito,"
                            . " pito_observacion,"
                            . " fluidos_frenos,"
                            . " fluidos_frenos_observacion,"
                            . " fluidos_aceites,"
                            . " fluidos_aceites_observacion,"
                            . " fluidos_refriguerante,"
                            . " fluidos_refriguerante_observacion,"
                            . " herraminetas,"
                            . " herramientas_observacion,"
                            . " crucetas,"
                            . " crucetas_observacion,"
                            . " gato,"
                            . " gato_observacion,"
                            . " taco,"
                            . " taco_observacion,"
                            . " senales,"
                            . " senales_observacion,"
                            . " chaleco,"
                            . " chaleco_observacion,"
                            . " botiquin,"
                            . " botiquin_observacion,"
                            . "	visto"
                            . " )VALUES ("
                            . " :idempresa,"
                            . " :nombre_conductor,"
                            . " :cedula_conductor,"
                            . " :ciudad,"
                            . " :fecha_actual,"
                            . " :placa_vehiculo,"
                            . " :kilometraje,"
                            . " :direccionales_delanteras,"
                            . " :direccionales_delanteras_observacion,"
                            . " :direccionales_traseras,"
                            . " :direccionales_traseras_observacion,"
                            . " :luces_altas,"
                            . " :luces_altas_observacion,"
                            . " :luces_bajas,"
                            . " :luces_bajas_observacion,"
                            . " :luces_stops,"
                            . " :luces_stops_observacion,"
                            . " :luces_reversa,"
                            . " :luces_reversa_observacion,"
                            . " :luces_parqueo,"
                            . " :luces_parqueo_observacion,"
                            . " :limpiabrisas_der_izq,"
                            . " :limpiabrisas_der_izq_observacion,"
                            . " :limpiabrisas_atras,"
                            . " :limpiabrisas_atras_observacion,"
                            . " :frenos_principal,"
                            . " :frenos_principal_observacion,"
                            . " :frenos_emergencia,"
                            . " :frenos_emergencia_observacion,"
                            . " :llantas_delanteras,"
                            . " :llantas_delanteras_observacion,"
                            . " :llantas_traseras,"
                            . " :llantas_traseras_observacion,"
                            . " :llantas_repuestos,"
                            . " :llantas_repuestos_observacion,"
                            . " :espejos_lateral_der_izq,"
                            . " :espejos_lateral_der_izq_observacion,"
                            . " :espejos_retrovisor,"
                            . " :espejos_retrovisor_observacion,"
                            . " :pito,"
                            . " :pito_observacion,"
                            . " :fluidos_frenos,"
                            . " :fluidos_frenos_observacion,"
                            . " :fluidos_aceites,"
                            . " :fluidos_aceites_observacion,"
                            . " :fluidos_refriguerante,"
                            . " :fluidos_refriguerante_observacion,"
                            . " :herraminetas,"
                            . " :herramientas_observacion,"
                            . " :crucetas,"
                            . " :crucetas_observacion,"
                            . " :gato,"
                            . " :gato_observacion,"
                            . " :taco,"
                            . " :taco_observacion,"
                            . " :senales,"
                            . " :senales_observacion,"
                            . " :chaleco,"
                            . " :chaleco_observacion,"
                            . " :botiquin,"
                            . " :botiquin_observacion,"
                            . " :visto)";
                    
                    $array = array( 
                                    ":idempresa"=>$idempresa,
                                    ":nombre_conductor"=>$nombre_conductor,
                                    ":cedula_conductor"=>$cedula_conductor,
                                    ":ciudad"=>$ciudad,
                                    ":fecha_actual"=>$fecha_actual,
                                    ":placa_vehiculo"=>$placa_vehiculo,
                                    ":kilometraje"=>$kilometraje,
                                    ":direccionales_delanteras"=>$direccionales_delanteras,
                                    ":direccionales_delanteras_observacion"=>$direccionales_delanteras_observacion,
                                    ":direccionales_traseras"=>$direccionales_traseras,
                                    ":direccionales_traseras_observacion"=>$direccionales_traseras_observacion,
                                    ":luces_altas"=>$luces_altas,
                                    ":luces_altas_observacion"=>$luces_altas_observacion,
                                    ":luces_bajas"=>$luces_bajas,
                                    ":luces_bajas_observacion"=>$luces_bajas_observacion,
                                    ":luces_stops"=>$luces_stops,
                                    ":luces_stops_observacion"=>$luces_stops_observacion,
                                    ":luces_reversa"=>$luces_reversa,
                                    ":luces_reversa_observacion"=>$luces_reversa_observacion,
                                    ":luces_parqueo"=>$luces_parqueo,
                                    ":luces_parqueo_observacion"=>$luces_parqueo_observacion,
                                    ":limpiabrisas_der_izq"=>$limpiabrisas_der_izq,
                                    ":limpiabrisas_der_izq_observacion"=>$limpiabrisas_der_izq_observacion,
                                    ":limpiabrisas_atras"=>$limpiabrisas_atras,
                                    ":limpiabrisas_atras_observacion"=>$limpiabrisas_atras_observacion,
                                    ":frenos_principal"=>$frenos_principal,
                                    ":frenos_principal_observacion"=>$frenos_principal_observacion,
                                    ":frenos_emergencia"=>$frenos_emergencia,
                                    ":frenos_emergencia_observacion"=>$frenos_emergencia_observacion,
                                    ":llantas_delanteras"=>$llantas_delanteras,
                                    ":llantas_delanteras_observacion"=>$llantas_delanteras_observacion,
                                    ":llantas_traseras"=>$llantas_traseras,
                                    ":llantas_traseras_observacion"=>$llantas_traseras_observacion,
                                    ":llantas_repuestos"=>$llantas_repuestos,
                                    ":llantas_repuestos_observacion"=>$llantas_repuestos_observacion,
                                    ":espejos_lateral_der_izq"=>$espejos_lateral_der_izq,
                                    ":espejos_lateral_der_izq_observacion"=>$espejos_lateral_der_izq_observacion,
                                    ":espejos_retrovisor"=>$espejos_retrovisor,
                                    ":espejos_retrovisor_observacion"=>$espejos_retrovisor_observacion,
                                    ":pito"=>$pito,
                                    ":pito_observacion"=>$pito_observacion,
                                    ":fluidos_frenos"=>$fluidos_frenos,
                                    ":fluidos_frenos_observacion"=>$fluidos_frenos_observacion,
                                    ":fluidos_aceites"=>$fluidos_aceites,
                                    ":fluidos_aceites_observacion"=>$fluidos_aceites_observacion,
                                    ":fluidos_refriguerante"=>$fluidos_refriguerante,
                                    ":fluidos_refriguerante_observacion"=>$fluidos_refriguerante_observacion,
                                    ":herraminetas"=>$herraminetas,
                                    ":herramientas_observacion"=>$herramientas_observacion,
                                    ":crucetas"=>$crucetas,
                                    ":crucetas_observacion"=>$crucetas_observacion,
                                    ":gato"=>$gato,
                                    ":gato_observacion"=>$gato_observacion,
                                    ":taco"=>$taco,
                                    ":taco_observacion"=>$taco_observacion,
                                    ":senales"=>$senales,
                                    ":senales_observacion"=>$senales_observacion, 
                                    ":chaleco"=>$chaleco,
                                    ":chaleco_observacion"=>$chaleco_observacion,
                                    ":botiquin"=>$botiquin,
                                    ":botiquin_observacion"=>$botiquin_observacion,
                                    ":visto"=>$visto);
                 $this->SetQuery($query);
                 $this->SetArray_insertar($array);
                 $this->Insertar();
                 if($this->filasAfectadas()){
                     $this->resulltado = true;
                 }else{
                     $this->resulltado = false;
                 }
                    
                    
                    
                } catch (Exception $ex) {
                    die("[Error] linea ".$ex->getFile()); 
                }
                
            }//fin AgregarAlistamientoDiario
            
            function EliminarAlistamientoDiario($id){
                try {
                    $query = "DELETE FROM alistamiento WHERE idempresa = :idempresa AND id = :id ";
                    
                    
                     $array = array(":idempresa"=>$_SESSION['idEmpresa'],":id"=>$id);
                     
                     $this->SetQuery($query);
                     $this->SetArray_eliminar($array);
                     $this->Eliminar();
                     if($this->filasAfectadas()){
                     $this->resulltado = true;
                     }else{
                         $this->resulltado = false;
                     }
                } catch (Exception $ex) {
                    die("[Error] linea ".$ex->getFile()); 
                }
                
            }//fin EliminarAlistamientoDiario
            
             function EliminarAlistamientoDiarioPlaca($placa){
                try {
                    $query = "DELETE FROM alistamiento WHERE idempresa = :idempresa AND placa_vehiculo = :placa_vehiculo ";
                    
                    
                     $array = array(":idempresa"=>$_SESSION['idEmpresa'],":placa_vehiculo"=>$placa);
                     
                     $this->SetQuery($query);
                     $this->SetArray_eliminar($array);
                     $this->Eliminar();
                     if($this->filasAfectadas()){
                     $this->resulltado = true;
                     }else{
                         $this->resulltado = false;
                     }
                } catch (Exception $ex) {
                    die("[Error] linea ".$ex->getFile()); 
                }
                
            }//fin EliminarAlistamientoDiario
            
            
            
            function CambiarEstadoAlistamientoDiario($id){
                try {
                    
                    $query = "UPDATE alistamiento SET visto = 'si'  WHERE idempresa = :idempresa AND id = :id ";
                    
                    $array = array(":idempresa"=>$_SESSION['idEmpresa'],":id"=>$id);
                    
                    
                    $this->SetQuery($query);
                    $this->SetArray_actualizar($array);
                    $this->Actualizar();
                    if($this->filasAfectadas()){
                        $this->resulltado = true;
                    }else{
                        $this->resulltado = false;
                    }
                    
                } catch (Exception $ex) {
                    die("[Error] linea ".$ex->getFile()); 
                }
                
            }//fin EliminarAlistamientoDiario
            
            //ESTE METODO LISTA LOS ALITAMIENTO ECHOS POR EL CONDUCTOR QUE TIENE LA CUENTA ABIERTA 
            //SOLO LISTA DEACUERDO A SU NUMERO DE CEDULA 
            function ListarBuscarAlistamientoDiario(){
                try {
                    
                    $query = "SELECT * FROM alistamiento WHERE alistamiento.cedula_conductor = :cedula_conductor AND  alistamiento.idempresa = :idempresa  "
                                            . "HAVING  alistamiento.placa_vehiculo LIKE  :dato OR alistamiento.fecha_actual LIKE :dato  OR alistamiento.visto LIKE :dato";
                    
                    $array = array(":idempresa"=>$_SESSION['idEmpresa'],":cedula_conductor"=>$_SESSION['documento'],":dato"=>'%'.$this->dato.'%' );
                    
                    $this->SetQuery($query);
                    $this->Setarray_selecionar($array);
                    $this->resulltado =  $this->paginasionMostrar($this->NumeroPagina, $this->limiteDeDados);
                    
                } catch (Exception $ex) {
                    die("[Error] linea ".$ex->getFile()); 
                }
                
            }//fin EliminarAlistamientoDiario
            
            //ESTE METODO ES SILIMIAR AL DE ARRIBA LA DIFERENCIA QUE LISTA TODO LOS ALISTAMINETO ECHO POR TODO LOS CONDUCTORES
            function ListarBuscarAlistamientoDiarioUsuarioAdminstrador(){
                try {
                    
                    $query = "SELECT * FROM alistamiento WHERE  alistamiento.idempresa = :idempresa  "
                                            . "HAVING  alistamiento.placa_vehiculo LIKE  :dato OR alistamiento.fecha_actual LIKE :dato  OR alistamiento.visto LIKE :dato ";
                    
                    $array = array(":idempresa"=>$_SESSION['idEmpresa'],":dato"=>'%'.$this->dato.'%' );
                    
                    $this->SetQuery($query);
                    $this->Setarray_selecionar($array);
                    $this->resulltado =  $this->paginasionMostrar($this->NumeroPagina, $this->limiteDeDados);
                    
                } catch (Exception $ex) {
                    die("[Error] linea ".$ex->getFile()); 
                }
                
            }//fin EliminarAlistamientoDiario
            
            
            function BuscarAlitamientoPorID(){
                try {
                    
                    $query = "SELECT * FROM alistamiento WHERE  alistamiento.idempresa = :idempresa  "
                                            . "HAVING  id LIKE :dato ";
                    
                    $array = array(":idempresa"=>$_SESSION['idEmpresa'],":dato"=>'%'.$this->dato.'%' );
                    
                    $this->SetQuery($query);
                    $this->Setarray_selecionar($array);
                    $this->resulltado =  $this->paginasionMostrar($this->NumeroPagina, $this->limiteDeDados);
                    
                } catch (Exception $ex) {
                    die("[Error] linea ".$ex->getFile()); 
                }
                
            }//fin EliminarAlistamientoDiario
            
            
            
          
          
          
                }//FIN CLASE AlISTAMIENTO DIARIO            




//haciendo instancia de las classes 
//$conductor = new Conductor();
///*
//haciendo llamando del metodo para guardar un registro
//$conductor->AgregarConductor("Jesus peña", 114312566844 , "456465456554654","2022-05-03");*/

//haciendo llamada al metodo que esta encargado de actulizar los datos
    /*
    $conductor->ActulizarConductor(4564564, "arcangel", "565694949", "2026-02-26");*/

//haciendo llamado a la  function para eliminar un conductor con tan solo el numero de cedula

    /* $conductor->EliminarConductorCecula(4567765);*/

//y este medodo elimina los que tienen la liencia vencida
/* 
$conductor->EliminarConductorEstado();
 */

//este metodo es para listar los conductores
         /* $conductor->ListarConductor();
          $ver =  $conductor->getResultado();
          foreach ($ver as $row){
              echo $row['nombreapellido']."<br>";

          }*/


//este metodo es para listar paginado

/*        $conductor->SetNumeroPagina(1);
        $conductor->SetLimiteDatos(1);
        $conductor->PaginaConductor();
        $ver =  $conductor->getResultado();

        foreach ($ver as $row){
                      echo $row['nombreapellido']."<br>";

                  }
                  
        $conductor->ContarConductor();
        echo "hay dos registros ".$conductor->getResultado()."<br> ";
        $numeroRegistro = $conductor->getResultado();
        //y aqui solo falta diviir en tre la contidad de registros que muestra 
        $pagina =  ceil($numeroRegistro/1);
        
        for($i=1;$i<=$pagina;$i++){
            
            echo "pagina ".$i. " ";
        }
          */
  // --------------------------------------------------------------------------    

//    $documentoconductor = new DocumentoConductor();
    
   /*esta funcion sirve para guardar un documento*/ 
   // $documentoconductor->AgregarDocumentoConductor("4564564","9", "8", "9", "-","simit");
    // echo  $documentoconductor->getResultado();
    
    
    /* esta funcion sirve para actulizar un registro por su id */
  //  $documentoconductor->ActulizarDocumentoConductor(10,"4564564","/8", "/8", "*", "+","simit");
  //  echo $documentoconductor->getResultado();
    
    
    /*esta funcion sirve para eliminar un regitro por si id o numero de documento o licencia */
    
    //$documentoconductor->EliminarDocumentoConductor(12,4564564);
   // echo  $documentoconductor->getResultado();
    
    /* listar documento */
    
    
    /*Esta funcion sirver para listar todos los documento*/
  // $documentoconductor->ListarDocumentoConductor();
   //$documentos = $documentoconductor->getResultado();
    
    /*foreach ($documentos as $ver){
        echo $ver['documento_conductor']."\n";
         echo $ver['licencia']."\n";
          echo $ver['hojadevida']."\n";
           echo $ver['capacitacion']."\n";
           echo $ver['ultima_alcholimetria']."\n";
            echo $ver['simit']."\n";
            echo $documentoconductor->calcularSemanas($ver['UltimaActulizacion']) ."<br>";
        
    }*/ 
    
   //--------------------------------------------------------- 
    
    /*esta funcion para paginar documento*/
//    $documentoconductor->PaginarDocumentoConductor();
//    $ver =  $documentoconductor->getResultado();
//    
//    foreach ($ver as $row){
//        
//        echo $row['documento_conductor'];
//    }
    
    
    //----------------------------------------------
  /*  esta funcion sirve para buscar un documento solo con pasarle un valor*/
//    $documentoconductor->BuscarDocumentoConductor($dato);
//    $ver = $documentoconductor->getResultado();
//    foreach ($ver as $row){
//        
//        echo $row['documento_conductor']."<br>\t";
//    }
    
    
    
    /**/