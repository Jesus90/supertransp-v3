<?php

//ESTA CLASE TIENE COMO FUNCION ANALIZAR LA FECHA DE VENCIMIENTO DE LOS DOCUMENTO Y NOTIFICAR EN LA 
//PAGINA DE ESTADO SI ESTA VENCIDO SOLO MOSTRARA EL NUMERO DE DOCUMENTO Y EL NOMBRE
class SeleccionarConductor extends baseDeDatos{
    
    
       protected $resulltado;
      
      
       function getResultado(){
        return $this->resulltado;
    }
    
    //esta funcion se encarga de analizar las licencias que esta por vencer dando 20 de anticipacion
    function SelecionarLicenciaConductor(){
        $licencia = "SELECT SUBDATE(documento_conductor.fecha_veci_licencia,INTERVAL 20 DAY) AS fechas_Restada ,
                        conductores.Cedulaconductor,
                        conductores.nombreapellido
                        FROM  conductores
                        INNER JOIN documento_conductor
                        ON (conductores.Cedulaconductor = documento_conductor.documento_conductore)
                        WHERE conductores.idempresa = :idEmpresa 
                      ";
        $array = array(":idEmpresa"=>'6565656e');
        
        $this->Setarray_selecionar($array);
        $this->SetQuery($licencia); 
        $this->resulltado = $this->seleccionar();
       }
       
        //esta funcion se encarga de analizar las ultima capacitacion que esta por vencer dando 20 de anticipacion
    function SelecionarUltimaCapacitacion(){
        $licencia = "SELECT SUBDATE(documento_conductor.ultima_capacitacion,INTERVAL 20 DAY) AS fechas_Restada ,
                        conductores.Cedulaconductor,
                        conductores.nombreapellido
                        FROM  conductores
                        INNER JOIN documento_conductor
                        ON (conductores.Cedulaconductor = documento_conductor.documento_conductore)
                        WHERE conductores.idempresa = :idEmpresa 
                      ";
        $array = array(":idEmpresa"=>'6565656e');
        
        $this->Setarray_selecionar($array);
        $this->SetQuery($licencia); 
        $this->resulltado = $this->seleccionar();
       }
       
       
           //esta funcion se encarga de analizar las ultima capacitacion que esta por vencer dando 20 de anticipacion
    function SelecionarSimitConductor(){
        $licencia = "SELECT SUBDATE(documento_conductor.fecha_veci_simit_conductor,INTERVAL 20 DAY) AS fechas_Restada ,
                        conductores.Cedulaconductor,
                        conductores.nombreapellido
                        FROM  conductores
                        INNER JOIN documento_conductor
                        ON (conductores.Cedulaconductor = documento_conductor.documento_conductore)
                        WHERE conductores.idempresa = :idEmpresa 
                      ";
        $array = array(":idEmpresa"=>'6565656e');
        
        $this->Setarray_selecionar($array);
        $this->SetQuery($licencia); 
        $this->resulltado = $this->seleccionar();
       } 
    
    
    
}

