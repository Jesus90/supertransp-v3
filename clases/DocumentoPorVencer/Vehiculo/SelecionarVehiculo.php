<?php


//ESTA CLASE TIENE COMO FINALIDAD BUSCAR LOS DOCUMENTO QUE ESTAN POR VENCER DANDO
//UN DATO 20 DIAS DE ANTICIPACION 


class SelecionarVehculo extends baseDeDatos{
      protected $resulltado;
      
      
       function getResultado(){
        return $this->resulltado;
    }
    
    //SELECIOADO LICENCIA QUE ESTAN POR VENCER 
    function SelecionarSoat(){
                        
        $licencia = "SELECT SUBDATE(documentos_vehiculos.fecha_veci_soat, INTERVAL 20 DAY) AS fecha_Restada ,
                        vehiculos.placa
                        FROM vehiculos
                        INNER JOIN documentos_vehiculos 
                        ON (vehiculos.placa = documentos_vehiculos.placa)
                        WHERE vehiculos.idempresa  = :idEmpresa 
                      ";
        $array = array(":idEmpresa"=>'6565656e');
        
        $this->Setarray_selecionar($array);
        $this->SetQuery($licencia); 
        $this->resulltado = $this->seleccionar();
        
     }//fin metodo
     
     
      //SELECIOADO LICENCIA QUE ESTAN POR VENCER 
    function SelecionarTecnicoMecanica(){
                        
        $licencia = "SELECT  SUBDATE(documentos_vehiculos.fecha_veci_tecnomecanica, INTERVAL 20 DAY) AS fecha_Restada ,
                    vehiculos.placa 
                    FROM vehiculos
                    INNER JOIN documentos_vehiculos 
                    ON (vehiculos.placa = documentos_vehiculos.placa)
                    WHERE vehiculos.idempresa  = :idEmpresa 
                      ";
        $array = array(":idEmpresa"=>'6565656e');
        
        $this->Setarray_selecionar($array);
        $this->SetQuery($licencia); 
        $this->resulltado = $this->seleccionar();
        
     }//fin metodo
     
      //muestra la cantidad de vehiculo a los que se les vence el documento
     function SelecionarMantenimento(){
                        
        $licencia = "SELECT  SUBDATE(documentos_vehiculos.fecha_veci_mantenimento, INTERVAL 20 DAY) AS fecha_Restada ,
                    vehiculos.placa 
                    FROM vehiculos
                    INNER JOIN documentos_vehiculos 
                    ON (vehiculos.placa = documentos_vehiculos.placa)
                    WHERE vehiculos.idempresa  =:idEmpresa 
                      ";
        $array = array(":idEmpresa"=>'6565656e');
        
        $this->Setarray_selecionar($array);
        $this->SetQuery($licencia); 
        $this->resulltado = $this->seleccionar();
        
     }//fin metodo
     
     
      //muestra la cantidad de vehiculo a los que se les vence el documento
      function SelecionarMantenimentoCorrectivo(){
                        
        $licencia = "SELECT  SUBDATE(documentos_vehiculos.fecha_veci_correctivo, INTERVAL 20 DAY) AS fecha_Restada ,
                    vehiculos.placa 
                    FROM vehiculos
                    INNER JOIN documentos_vehiculos 
                    ON (vehiculos.placa = documentos_vehiculos.placa)
                    WHERE vehiculos.idempresa  = :idEmpresa 
                      ";
        $array = array(":idEmpresa"=>'6565656e');
        
        $this->Setarray_selecionar($array);
        $this->SetQuery($licencia); 
        $this->resulltado = $this->seleccionar();
        
     }//fin metodo
     
      //muestra la cantidad de vehiculo a los que se les vence el documento
        function SelecionarTarjetaOperacion(){
                        
        $licencia = "SELECT  SUBDATE(documentos_vehiculos.fecha_veci_tarjeta_operacion, INTERVAL 20 DAY) AS fecha_Restada ,
                    vehiculos.placa 
                    FROM vehiculos
                    INNER JOIN documentos_vehiculos 
                    ON (vehiculos.placa = documentos_vehiculos.placa)
                    WHERE vehiculos.idempresa  = :idEmpresa 
                      ";
        $array = array(":idEmpresa"=>'6565656e');
        
        $this->Setarray_selecionar($array);
        $this->SetQuery($licencia); 
        $this->resulltado = $this->seleccionar();
        
     }//fin metodo
     
      //muestra la cantidad de vehiculo a los que se les vence el documento
           function SelecionarTarjetaPropiedad(){
                        
            $licencia = "SELECT  SUBDATE(documentos_vehiculos.fecha_veci_propiedad, INTERVAL 20 DAY) AS fecha_Restada ,
                        vehiculos.placa 
                        FROM vehiculos
                        INNER JOIN documentos_vehiculos 
                        ON (vehiculos.placa = documentos_vehiculos.placa)
                        WHERE vehiculos.idempresa  = :idEmpresa 
                      ";
        $array = array(":idEmpresa"=>'6565656e');
        
        $this->Setarray_selecionar($array);
        $this->SetQuery($licencia); 
        $this->resulltado = $this->seleccionar();
        
     }//fin metodo
     
     
      //muestra la cantidad de vehiculo a los que se les vence el documento
            function SelecionarSimit(){
                        
            $licencia = "SELECT  SUBDATE(documentos_vehiculos.fecha_veci_simit, INTERVAL 20 DAY) AS fecha_Restada ,
                        vehiculos.placa 
                        FROM vehiculos
                        INNER JOIN documentos_vehiculos 
                        ON (vehiculos.placa = documentos_vehiculos.placa)
                        WHERE vehiculos.idempresa  = :idEmpresa 
                      ";
        $array = array(":idEmpresa"=>'6565656e');
        
        $this->Setarray_selecionar($array);
        $this->SetQuery($licencia); 
        $this->resulltado = $this->seleccionar();
        
     }//fin metodo
     
     //muestra la cantidad de vehiculo a los que se les vence el documento
     function SelecionarSupertrasporte(){
                        
            $licencia = "SELECT  SUBDATE(documentos_vehiculos.fecha_veci_supertrasporte, INTERVAL 20 DAY) AS fecha_Restada ,
                        vehiculos.placa 
                        FROM vehiculos
                        INNER JOIN documentos_vehiculos 
                        ON (vehiculos.placa = documentos_vehiculos.placa)
                        WHERE vehiculos.idempresa  = :idEmpresa 
                      ";
        $array = array(":idEmpresa"=>'6565656e');
        
        $this->Setarray_selecionar($array);
        $this->SetQuery($licencia); 
        $this->resulltado = $this->seleccionar();
        
     }//fin metodo
     
    
 }

//
//$Vencida = new SelecionarVehculo();
//$Vencida->SelecionarLicencias();
//$ver = $Vencida->getResultado();
//
//foreach ($ver as $row ){
//    if($row['fecha_Restada'] != null &&  $row['fecha_Restada'] < date("Y-m-j")  ){
//    echo $row['placa'] ." - ".$row['fecha_Restada']." "  ;
//   
//    }
//}


