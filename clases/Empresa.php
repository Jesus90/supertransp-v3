<?php


class Empresa extends baseDeDatos {
    //put your code here
    private $resulltado;
    private $NumeroPagina;
    private $limiteDeDados;
    
    public function __construct() {
        $this->NumeroPagina = 1;
        $this->limiteDeDados = 5;
    }
    
    
    function getResultado(){
     return $this->resulltado;
        
        
    }
    
    function SetNumero($numero){
        $this->NumeroPagina = $numero;
    }
    
    function SetlimiteDeDatos($limite){
        $this->limiteDeDados = $limite;
    }
            
    
    function AtulizarDatosEmpresaImg($razonsocial,$representante,
        $cedula,$telefono,$direccion,$movil,$email,$ciudad,$firma,$logotranscito,$logoempresa){
                //crando query para la base de datos
                $queryactulizar = "UPDATE  empresas SET razonsocial = :razonsocial, "
                    . "representante = :representante,"
                    . "cedula = :cedula,"
                    . "telefono = :telefono,"
                    . "direccion = :direccion,"
                    . "movil = :movil,"
                    . "email = :email,"
                    . "ciudad = :ciudad,"
                    . "firma = :firma,"
                    . "logotranscito  = :logotranscito,"
                    . "logoempresa = :logoempresa WHERE nit = :nit";
                //array que contiene los losdatos 
                $arryactulizar = array(":razonsocial"=>$razonsocial,
                                   ":representante"=>$representante,
                                   ":cedula"=>$cedula,
                                   ":telefono"=>$telefono,
                                   ":direccion"=>$direccion,
                                   ":movil"=>$movil,
                                   ":email"=>$email,
                                   ":ciudad"=>$ciudad,
                                   ":firma"=>$firma,
                                   ":logotranscito"=>$logotranscito, 
                                   ":logoempresa"=>$logoempresa,
                                   ":nit"=>$_SESSION['idEmpresa']);




                //pasando la consulta
                $this->SetQuery($queryactulizar);
                //pasando el array con los datos
                $this->SetArray_actualizar($arryactulizar);
                //haciendo la llamada al metodo
                $this->Actualizar();
                
                if($this->filasAfectadas()){
                    $this->resulltado = true;
                }else{
                    $this->resulltado = false;
                }
        
    }//fin de la AtulizarDatosEmpresa
    
    
        function AtulizarDatosEmpresa($razonsocial,$representante,
        $cedula,$telefono,$direccion,$movil,$email,$ciudad){
                //crando query para la base de datos
                $queryactulizar = "UPDATE  empresas SET razonsocial = :razonsocial, "
                    . "representante = :representante,"
                    . "cedula = :cedula,"
                    . "telefono = :telefono,"
                    . "direccion = :direccion,"
                    . "movil = :movil,"
                    . "email = :email,"
                    . "ciudad = :ciudad"
                    . " WHERE nit = :nit";
                //array que contiene los losdatos 
                $arryactulizar = array(":razonsocial"=>$razonsocial,
                                   ":representante"=>$representante,
                                   ":cedula"=>$cedula,
                                   ":telefono"=>$telefono,
                                   ":direccion"=>$direccion,
                                   ":movil"=>$movil,
                                   ":email"=>$email,
                                   ":ciudad"=>$ciudad,
                                   ":nit"=>$_SESSION['idEmpresa']);
                //pasando la consulta
                $this->SetQuery($queryactulizar);
                //pasando el array con los datos
                $this->SetArray_actualizar($arryactulizar);
                //haciendo la llamada al metodo
                $this->Actualizar();
                
                if($this->filasAfectadas()){
                    $this->resulltado = true;
                }else{
                    $this->resulltado = false;
                }
        
    }//fin de la AtulizarDatosEmpresa
    
    
    
    
    //esta funcion sirve para insetar una nueva empresa
    function InsertarNuevaEmpresa($razonsocial,$nit,$email){
        try{
        
        $queryInsertarEmpresa = "INSERT INTO empresas (razonsocial,nit,email) VALUES (:razonsocial,:nit,:email)";
        
        $arrayInsertarEmpresa = array(":razonsocial"=>$razonsocial,":nit"=>$nit,":email"=>$email);
        //pasando la consulta
        $this->SetQuery($queryInsertarEmpresa);
        //pasando el array con los datos
        $this->SetArray_insertar($arrayInsertarEmpresa);
        //haciendo la llamada
        $this->Insertar();
        
        if($this->filasAfectadas()){
            $this->resulltado = true;
        }else{
            $this->resulltado = false;
        }
        
        
        } catch ( Exception $e){
            echo "Error ".$e->getCode();
        }
        
     
    }//fin de la funcion InsertarNuevaEmpresa
    
    
    function InformacionEmpresa($nit){
                
            $queryInformacionEmpresa = "SELECT * FROM empresas WHERE nit = :nit";
            $arrayInformacionEmpresa = array(":nit"=>$nit);
            
            $this->SetQuery($queryInformacionEmpresa);
            $this->Setarray_selecionar($arrayInformacionEmpresa);
            $this->resulltado =  $this->seleccionar();
            
    }
    
    
    
    
    function EliminarEmpresa($nit){
        $queryEliminarEmpresa = "DELETE FROM empresas WHERE nit = :nit";
        $arrayEliminarEmpresa = array(":nit"=>$nit);
        
        $this->SetQuery($queryEliminarEmpresa);
        
        $this->SetArray_eliminar($arrayEliminarEmpresa);
        
        $this->Eliminar();
        
        
    }//fin de la funcion Eliminar
    
    
    
    function listarEmpresa(){
        $queryListarEmpresas = "SELECT * FROM empresas ";
        $this->SetQuery($queryListarEmpresas);
        $this->resulltado = $this->seleccionar();
        
    }
    
    function paginarEmpresa(){
       $queryPaginarEmpresa = "SELECT * FROM empresas ";
       $this->SetQuery($queryPaginarEmpresa);
       $this->resulltado = $this->paginasionMostrar($this->NumeroPagina,$this->limiteDeDados);
       
       
   } 
    
    
}//fin de la clase 

//crando insancia de la clase empresa
//$empresa = new Empresa();
//metodo para actlizar datos de las empresa 
    /* $empresa->AtulizarDatosEmpresa("-Dato-","-Dato-",
    123456,132123,"-Dato-",123465,"-Dato-","-Dato-","-Dato-","-Dato-","-Dato-");*/

//esta metodo de la clase sirve para insertar una nueva empresa

      /*  $empresa->InsertarNuevaEmpresa("CooChofal", 11432569845, "jd1990@hotmail.es");*/

//este medoto sirve para elimimar una empresa suscrita solo pasando el nit de la empresa
      /*  $empresa->EliminarEmpresa(11432569845);*/
    
//este metodo sirve para listar la empresas

            /*$empresa->listarEmpresa();  

                $ver = $empresa->getResultado();
                        foreach ($ver as $row){
                          echo   $row['razonsocial'];

                        }*/ 

        //este metodo es para mostrar un listado de los productos paginado                 
             /*  $empresa->paginarEmpresa();
                 $mostar = $empresa->getResultado();
                 
                        foreach ($mostar as $row){
                          echo   $row['razonsocial'];

                        }*/
                        





