<?php


class Usuarios extends baseDeDatos {
    //put your code here
    /*atributos del usuario */
    private $NombreUsuario;
    private $Password;
    private $documento;
    private $rangoUsurio;
    private $NitEmpresa;
    private $email;
    private $NuevaPassword;
    private $foto;
     /*Fin */
    
    private $resulltado ;
    private $NumeroPagina;
    private $limiteDeDados;
    
    
    //*Buscar**/
    private $dato;
    
    public function __construct() {
        $this->NumeroPagina = 1;
        $this->limiteDeDados  = 5;
    }
    
    function NuevoPassword($nuevaPasswords){
        $this->NuevaPassword = md5($nuevaPasswords);
    }
    
    //este metodo es para indicar el numero de pagina mostrar
    function SetNumeroPagina($numeroPagina){
        $this->NumeroPagina = $numeroPagina;
    }
    //este metodo limita los registros a mostrar 
    function SetLimiteDatos($limite){
        $this->limiteDeDados = $limite;
    }
    
    
    function getResultado(){
        return $this->resulltado;
     }
     
     
    function SetDato($dato){
        $this->dato = $dato;
    }
    
   
    
    
    //asignado  nombre usuario
    function SetNombreUsuario($nombreUsuaro){
        $this->NombreUsuario = $nombreUsuaro;
        
    }
    
    //function para obtener el nombre del usuario
    function  GetNombreUsuario(){
        return $this->NombreUsuario;
        
    }
    
    function GetEmail(){
        return $this->email;
    }
    //asignando contraseña
    function Setpassword($password){
        $this->Password = md5($password);
    }
    //asignando documento
    function SetDocumento($documento){
        $this->documento = $documento;
    }
    //asignando el Rango
    function SetRango($rango){
        $this->rangoUsurio =  $rango;
    }
    
    function SetEmail($email){
        $this->email =$email;
    }
            
    function GetRango(){
        return $this->rangoUsurio;
    }
    
    function SetNitEmpresa($nitEmpresa){
        $this->NitEmpresa = $nitEmpresa;
        
    }
    
    function GetNitEmpresa(){
        return $this->NitEmpresa;
    }
    
    
    function SetFotoPerfil($foto){
        $this->foto = $foto;
    }
    
   
       
    
    //-----------------------------------------------------------------------
    
    //esta funcion espara crear usuario
    function crearUsuario(){
        if($this->rangoUsurio == "usuarios" || $this->rangoUsurio === "administrador"){
            $queryCrearUsuario = "INSERT INTO usuarios "
                    . "(idEmpresa,"
                    . "usuario,"
                    . "password,"
                    . "documento,"
                    . "rangoUsuario,"
                    . "email,"
                    . "foto_usuario)"
                    . " VALUES ( "
                    . ":idEmpresa,"
                    . ":usuario,"
                    . ":password,"
                    . ":documento,"
                    . ":rangoUsuario,"
                    . ":email,"
                    . ":foto_usuario)";
        }else{
            $queryCrearUsuario = "INSERT INTO usuario_conductores "
                    . "(idEmpresa,"
                    . "usuario,"
                    . "password,"
                    . "documento,"
                    . "rangoUsuario,"
                    . "email,"
                    . "foto_usuario)"
                    . " VALUES ( "
                    . ":idEmpresa,"
                    . ":usuario,"
                    . ":password,"
                    . ":documento,"
                    . ":rangoUsuario,"
                    . ":email,"
                    . ":foto_usuario)";
        }
        
        
        

       $arryCrearUsuario = array(
           ":idEmpresa"=>$this->NitEmpresa,
           ":usuario"=>$this->NombreUsuario,
           ":password"=> $this->Password,
           ":documento"=> $this->documento,
           ":rangoUsuario"=> $this->rangoUsurio,
           ":email"=> $this->email,
           ":foto_usuario"=> $this->foto);     
        
       $this->SetArray_insertar($arryCrearUsuario);
       $this->SetQuery($queryCrearUsuario);
       $this->Insertar();
       if($this->filasAfectadas()){
           $this->resulltado = $this->filasAfectadas();
       }else{
           $this->resulltado = $this->filasAfectadas();
       }
       
    }
    
    function elimnarUsuario(){
         if($this->rangoUsurio =="usuarios" || $this->rangoUsurio === "administrador"){
        $queryEliminarUsuario = "DELETE FROM usuarios  WHERE documento =:documento AND idEmpresa=:idEmpresa  ";
         }else{
           $queryEliminarUsuario = "DELETE FROM usuario_conductores  WHERE documento =:documento AND idEmpresa=:idEmpresa  ";   
         }
        
        $arrayEliminarUsuario = array (":documento"=>$this->documento,":idEmpresa"=>$this->NitEmpresa);
        
        $this->SetArray_eliminar($arrayEliminarUsuario);
        $this->SetQuery($queryEliminarUsuario);
        $this->Eliminar();
        if($this->filasAfectadas()){
            $this->resulltado = true;
        }else{
            $this->resulltado = false;
        }
        
    }
    
    
    function elimnarUsuarioCoductor(){
       
           $queryEliminarUsuario = "DELETE FROM usuario_conductores  WHERE documento = :documento AND idEmpresa = :idEmpresa  ";   
          
        
        $arrayEliminarUsuario = array (":documento"=>$this->documento,":idEmpresa"=>$this->NitEmpresa);
        
        $this->SetArray_eliminar($arrayEliminarUsuario);
        $this->SetQuery($queryEliminarUsuario);
        $this->Eliminar();
        if($this->filasAfectadas()){
            $this->resulltado = true;
        }else{
            $this->resulltado = false;
        }
        
        
        
    }
    
    //actuliza contenido sin foto
    function actulizarUsuario(){
        if($this->rangoUsurio =="usuarios" || $this->rangoUsurio === "administrador"){
            $queryActulizarUsuario = "UPDATE usuarios SET usuario=:usuario,password=:password,documento=:documento,rangoUsuario=:rangoUsuario,email=:email WHERE documento=:documento AND  idEmpresa = :idEmpresa";
        }else{
            $queryActulizarUsuario = "UPDATE usuario_conductores SET usuario=:usuario,password=:password,documento=:documento,rangoUsuario=:rangoUsuario,email=:email WHERE documento=:documento AND idEmpresa = :idEmpresa";
        }
        
        
        $arrayActulizarUsuario = array(
            ":usuario"=> $this->NombreUsuario,
            ":password"=> $this->Password ,
            ":documento"=> $this->documento ,
            ":rangoUsuario"=> $this->rangoUsurio ,
            ":email"=> $this->email ,
            ":idEmpresa"=> $this->NitEmpresa
           
        );
        
        $this->SetQuery($queryActulizarUsuario);
        $this->SetArray_actualizar($arrayActulizarUsuario);
        $this->Actualizar();
        if($this->filasAfectadas()){
            $this->resulltado = true;
        }else{
            $this->resulltado = false;
        }
        
        
        
    }//fin metodo actulizar 
    //actuliza contenido con foto 
    function actulizarUsuarioFoto(){
        if($this->rangoUsurio =="usuarios" || $this->rangoUsurio === "administrador"){
            $queryActulizarUsuario = "UPDATE usuarios SET usuario=:usuario,password=:password,documento=:documento,rangoUsuario=:rangoUsuario,email=:email,foto_usuario=:foto_usuario WHERE documento=:documento AND  idEmpresa = :idEmpresa";
        }else{
            $queryActulizarUsuario = "UPDATE usuario_conductores SET usuario=:usuario,password=:password,documento=:documento,rangoUsuario=:rangoUsuario,email=:email,foto_usuario=:foto_usuario WHERE documento=:documento AND idEmpresa = :idEmpresa";
        }
        
        
        $arrayActulizarUsuario = array(
            ":usuario"=> $this->NombreUsuario,
            ":password"=> $this->Password ,
            ":documento"=> $this->documento ,
            ":rangoUsuario"=> $this->rangoUsurio ,
            ":email"=> $this->email ,
            ":foto_usuario"=>$this->foto,
            ":idEmpresa"=> $this->NitEmpresa
           
        );
        
        $this->SetQuery($queryActulizarUsuario);
        $this->SetArray_actualizar($arrayActulizarUsuario);
        $this->Actualizar();
        
        if($this->filasAfectadas()){
            $this->resulltado = true;
        }else{
            $this->resulltado = false;
        }
        
        
    }//fin metodo actulizar 
    
    
    function PaginarUsuario(){
        if($this->rangoUsurio =="usuarios" || $this->rangoUsurio === "administrador"){
            $queryPaginarUsuario = "SELECT  * FROM usuarios WHERE idEmpresa=:idEmpresa ";
        }else{
           $queryPaginarUsuario = "SELECT  * FROM usuario_conductores WHERE idEmpresa=:idEmpresa "; 
        }
        
        
        $arrayPagianUsuaro = array(":idEmpresa"=>$_SESSION['idEmpresa']);
        
      
        $this->Setarray_selecionar($arrayPagianUsuaro);
          $this->SetQuery($queryPaginarUsuario);
        
          $this->resulltado =   $this->paginasionMostrar($this->NumeroPagina,$this->limiteDeDados);
     }//fin PaginarUsuario
     
     
     function BuscarUsuario(){
            if($this->rangoUsurio =="usuarios" || $this->rangoUsurio === "administrador"){
                $queryPaginarUsuario = "SELECT  * FROM usuarios WHERE idEmpresa=:idEmpresa AND documento LIKE :dato OR rangoUsuario  LIKE :dato ";
                    }else{
                $queryPaginarUsuario = "SELECT  * FROM usuario_conductores WHERE idEmpresa=:idEmpresa AND documento LIKE :dato OR rangoUsuario  LIKE :dato ";       
                    }
                    
              $ArrayBuscar = array(":idEmpresa"=>$this->NitEmpresa,
                                   ":dato"=>"%".$this->dato."%"); 
              
               $this->Setarray_selecionar($ArrayBuscar);
               $this->SetQuery($queryPaginarUsuario);
              $this->resulltado = $this->seleccionar();
                         }//fin funcion
            function BuscarUsuarioConductor(){
            if($this->rangoUsurio =="usuarios" || $this->rangoUsurio === "administrador"){
              $queryPaginarUsuario = "SELECT  * FROM usuario_conductores WHERE idEmpresa = :idEmpresa AND documento LIKE :dato  ";       
                    }
                    
              $ArrayBuscar = array(":idEmpresa"=>$this->NitEmpresa,
                                   ":dato"=>"%".$this->dato."%"); 
              
               $this->Setarray_selecionar($ArrayBuscar);
               $this->SetQuery($queryPaginarUsuario);
              $this->resulltado = $this->seleccionar();
                         }//fin funcion
                         //
                         //
     //  ESTE METODO SIRVE PARA LISTAR LOS USUARIO TANTO USUARIO COMO ADMINISTRADORES Y CONDUCTORES                 
     function listarUsuario(){
            if($this->rangoUsurio === "administrador"){
                $queryPaginarUsuario = "SELECT * FROM  usuarios WHERE :idEmpresa = :idEmpresa  AND usuario LIKE :dato OR documento LIKE :dato OR rangoUsuario LIKE :dato OR email LIKE :dato ";   
                    }
                    
              $ArrayBuscar = array(":idEmpresa"=>$_SESSION['idEmpresa'],
                                   ":dato"=>"%".$this->dato."%"); 
              
                               $this->Setarray_selecionar($ArrayBuscar);
                               $this->SetQuery($queryPaginarUsuario);
                              $this->resulltado = $this->paginasionMostrar($this->NumeroPagina,$this->limiteDeDados);
                         }//fin funcion 
                         
    function listarConductores(){
            if($this->rangoUsurio === "usuarios"){
                $queryPaginarUsuario = "SELECT * FROM usuario_conductores WHERE :idEmpresa =:idEmpresa  AND usuario LIKE :dato OR documento LIKE :dato OR rangoUsuario LIKE :dato OR email LIKE :dato ";   
                    }
                    
              $ArrayBuscar = array(":idEmpresa"=>$_SESSION['idEmpresa'],
                                   ":dato"=>"%".$this->dato."%"); 
              
                               $this->Setarray_selecionar($ArrayBuscar);
                               $this->SetQuery($queryPaginarUsuario);
                              $this->resulltado = $this->paginasionMostrar($this->NumeroPagina,$this->limiteDeDados);
                         }//fin funcion                      
    

   function CambiarPasswords(){
       if($this->rangoUsurio === "usuarios" || $this->rangoUsurio === "administrador"){
           $queryCambiarPassword = "SELECT password FROM usuarios WHERE documento=:documento AND idEmpresa=:idEmpresa";
       }else{
           $queryCambiarPassword = "SELECT password FROM usuario_conductores WHERE documento=:documento AND idEmpresa=:idEmpresa";
       }
       
       
       $arrayCambarPassword = array(":documento"=>$this->documento,":idEmpresa"=>$_SESSION['idEmpresa']);
       
       $this->SetQuery($queryCambiarPassword);
       $this->Setarray_selecionar($arrayCambarPassword);
        $ver =  $this->seleccionar();
        foreach ($ver as $row){
            if($row['password'] === $this->Password){
                if($this->rangoUsurio == "usuarios" || $this->rangoUsurio === "administrador" ){
                $queryActuliarPassword = "UPDATE usuarios SET password=:password WHERE documento=:documento AND idEmpresa=:idEmpresa";
                }else{
                 $queryActuliarPassword = "UPDATE usuario_conductores SET password=:password WHERE documento=:documento AND idEmpresa=:idEmpresa";   
                }
                
                
                $arrayActulizarPassword = array(":documento"=>$this->documento,":password"=>$this->NuevaPassword,":idEmpresa"=>$_SESSION['idEmpresa']);
                
                $this->SetArray_actualizar($arrayActulizarPassword);
                $this->SetQuery($queryActuliarPassword);
                $this->Actualizar();
                echo 1;
            }else{
                echo 0;
            }
            
        }//fin foreach
        }//fin CambiarPasswords(
            
     
        
    
   function validarUsuario(){
       
       if($this->rangoUsurio == "usuarios" || $this->rangoUsurio == "administrador"){
            $queryValidarUsuario = "SELECT documento,usuario,email,idEmpresa,rangoUsuario,password,foto_usuario FROM usuarios WHERE usuario = :usuario AND password = :password  AND rangoUsuario=:rangoUsuario ";
       }else{
            $queryValidarUsuario = "SELECT documento,usuario,email,idEmpresa,rangoUsuario,password,foto_usuario FROM usuario_conductores WHERE usuario = :usuario AND password = :password AND rangoUsuario=:rangoUsuario ";
       }
      
       
       
       $arrayValidadUsuario = array(":usuario"=>$this->NombreUsuario,
                                    ":password"=>$this->Password,
                                    ":rangoUsuario"=> $this->rangoUsurio );
       
       $this->Setarray_selecionar($arrayValidadUsuario);
       $this->SetQuery($queryValidarUsuario);
//       $this->resulltado = $this->contar_registro();
        $this->resulltado =  $this->seleccionar();
      
       
   }//validad usuario
    
   function destruirSession(){
      header("Location: ../conexion/sessionout.php");
       
   } 
    
    
    
    
}

//haciendo instancia de la clase usuario
//$usuario = new Usuarios();
//pasando los valorea a los atributos
              /* $usuario->SetNombreUsuario("David");
                $usuario->SetDocumento(114312566844 );
                //si el rango el conductor se guarda en una tabla distinta
                $usuario->SetRango("conductor");
                $usuario->Setpassword(md5(123456789));
                $usuario->SetNitEmpresa(1042350126);
                $usuario->SetEmail("jjjj00");
                $usuario->crearUsuario(); */
                
                
//ESTE METOSO SIRVE PARA ELIMINAR USUARIO
        //pasando los valores con los siguientes metodos
         /*  $usuario->SetDocumento(24545656558989);
            $usuario->SetNitEmpresa(1042350126);
            $usuario->SetRango("conductor");
            $usuario->elimnarUsuario(); */

//esta funcion puede actulizar el dato de usuatio o un conductor 

/*
        $usuario->SetNombreUsuario("esther");
        $usuario->SetNitEmpresa(1042350126);
        $usuario->SetDocumento(114312566844);
        $usuario->Setpassword(md5("falta"));
        $usuario->SetRango("coductor");
        $usuario->SetEmail("esther-osoario@hotmail.es");
        $usuario->actulizarUsuario();   */


//esta funcion espara paginar los usuario o los conductores

  /*
     $usuario->PaginarUsuario();
     
     
    $ver = $usuario->getResultado();
     foreach ($ver as $row){
        echo $row['usuario']."<br>";
    }*/
   
//este metodo es para cambiar la contraseña
//
           /* //NUMERO DE DOCUMENTO DE LA PERSONA O CONDUCTOR 
            $usuario->SetDocumento(1143122971);
            //HAY QUE ESPECIFICAR EL RANGO 
            $usuario->SetRango("usuarios");
            //CONTRASEÑA ANTERIOR 
            $usuario->Setpassword(md5("jesus1234"));
            //CONTRASEÑA NUEVA
            $usuario->NuevoPassword(md5("jesus1234"));
            //EL MATODO PARA CAMBIAR LA CONTRASEÑA
            $usuario->CambiarPasswords();
            
            //elresultado es un cero o un uno*/
        
                            

//este metodo es para validar la existencia de un usuario y si esxite devuelve 

    /* $usuario->SetNombreUsuario("jesus90");
        $usuario->Setpassword("jesus1234");
        $usuario->SetRango("usuarios");
        //se hace llamado del metodo
        $usuario->validarUsuario();
        //aqui obtenenos la respuesta 
       $ver = $usuario->getResultado();
        foreach($ver as $row){
            echo $row['documento']."<br>";
            
        }*/


//este ultimo metodo lo que hace es destruir la sesseion 

           /*   $usuario->destruirSession();*/