<?php

class Fuec extends baseDeDatos{
    private $respuesta;
    protected $NumeroPagina;
    protected $limiteDeDados;
    public function __construct() {
        $this->NumeroPagina = 1;
        $this->limiteDeDados  = 5;
    }
    
    function GetRespuesta(){
        return $this->respuesta;
    }
    
    function SetNumeroPagina($numero=null){
        $this->NumeroPagina = $numero;
    }
    
    
    
    private function GenerarIdFuec (){
        ///////////////////////////////////////////////////
        //esta secccion de la web se encarga de generar la llave fuec
        //se crea un vector donde se al macenara 
        //luego tenenos el metodo rand donde de forma aletoria genera numero y letras
        $id  = array();
        for ($i=0;$i<=4;$i++){
              $string = chr(rand(ord("a"), ord("z")));
             $numero = rand(1, 99).$string.rand(1, 99);
              $id[$i] =  $numero;
        }       //pos ultimo se guardara en la variable $idFuec
         return  $idfuec =  $id[0].$id[1].$id[2].$id[3];
    }////////////////////////////////////////////////////////////
    
    
    private function estadosFuec(){
//        $estado ="";
        if($_SESSION['rangoUsuario'] == "usuarios" || $_SESSION['rangoUsuario'] == "administrador"){
            return  $estado = "activo";
        }
        
        if($_SESSION['rangoUsuario'] == "conductor" ){
            return  $estado = "procesando";
        }
    }
    
    function IngresarFuec($fecha_vencimineto,$nombreEmpresa,$numeroContrato,$nombreContratante,
                          $objetocontrato,$recorido,$conevio,$fechaInicontrato,
                          $fechaFincontrato,$placa,$modelovehiculo,$marcaVehiculo,
                          $clasevehiculo,$numInterVehiculo,$tarjetaOperacion,
                          $nombreconductor1,$cedulaconductor1,
                          $licenciaconductor1,$vigencia1,$nombreconductor2=null,
                          $cedulaconductor2=null,$licenciaconductor2=null,$vigencia2=null,$Codigo_Departamento){
        
        
        
                          
        
        try {
            //esta seccion evalua el rango del usuario de acuerdo rango le da un estado
            $estado = $this->estadosFuec();
            
            
            
            //esta funcion se encarga de renerar un id
            $idFuec  =  $this->GenerarIdFuec();
            $queryIngresarFuec = "INSERT INTO fuec (idfuec,"
                                            . "fecha_vencimineto,"
                                            . "nombreEmpresa,"
                                            . "nitEmpresa,"
                                            . "numeroContrato,"
                                            . "nombreContratante,"
                                            . "objetocontrato,"
                                            . "recorido,"
                                            . "conevio,"
                                            . "fechaInicontrato,"
                                            . "fechaFincontrato,"
                                            . "placa,"
                                            . "modelovehiculo,"
                                            . "marcaVehiculo,"
                                            . "clasevehiculo,"
                                            . "numInterVehiculo,"
                                            . "tarjetaOperacion,"
                                            . "nombreconductor1,"
                                            . "cedulaconductor1,"
                                            . "licenciaconductor1,"
                                            . "vigencia1,"
                                            . "nombreconductor2,"
                                            . "cedulaconductor2,"
                                            . "licenciaconductor2,"
                                            . "vigencia2,"
                                            . "estado) "
                    . "VALUES"
                    . " (:idfuec,"
                    . ":fecha_vencimineto,"
                    . ":nombreEmpresa,"
                    . ":nitEmpresa,"
                    . ":numeroContrato,"
                    . ":nombreContratante,"
                    . ":objetocontrato,"
                    . ":recorido,"
                    . ":conevio,"
                    . ":fechaInicontrato,"
                    . ":fechaFincontrato,"
                    . ":placa,"
                    . ":modelovehiculo,"
                    . ":marcaVehiculo,"
                    . ":clasevehiculo,"
                    . ":numInterVehiculo,"
                    . ":tarjetaOperacion,"
                    . ":nombreconductor1,"
                    . ":cedulaconductor1,"
                    . ":licenciaconductor1,"
                    . ":vigencia1,"
                    . ":nombreconductor2,"
                    . ":cedulaconductor2,"
                    . ":licenciaconductor2,"
                    . ":vigencia2,"
                    . ":estado)";
            
            
            $arrayIngresarFuec = array(":idfuec"=>$Codigo_Departamento.'-'.$idFuec,
                                       ":fecha_vencimineto"=>$fecha_vencimineto,
                                       ":nombreEmpresa"=>$nombreEmpresa,
                                       ":nitEmpresa"=>$_SESSION['idEmpresa'],
                                       ":numeroContrato"=>$numeroContrato,
                                       ":nombreContratante"=>$nombreContratante,
                                       ":objetocontrato"=>$objetocontrato,
                                       ":recorido"=>$recorido,
                                       ":conevio"=>$conevio,
                                       ":fechaInicontrato"=>$fechaInicontrato,
                                       ":fechaFincontrato"=>$fechaFincontrato,
                                       ":placa"=>$placa,
                                       ":modelovehiculo"=>$modelovehiculo,
                                       ":marcaVehiculo"=>$marcaVehiculo,
                                       ":clasevehiculo"=>$clasevehiculo,
                                       ":numInterVehiculo"=>$numInterVehiculo,
                                       ":tarjetaOperacion"=>$tarjetaOperacion,
                                       ":nombreconductor1"=>$nombreconductor1,
                                       ":cedulaconductor1"=>$cedulaconductor1, 
                                       ":licenciaconductor1"=>$licenciaconductor1,
                                       ":vigencia1"=>$vigencia1,
                                       ":nombreconductor2"=>$nombreconductor2,
                                       ":cedulaconductor2"=>$cedulaconductor2,
                                       ":licenciaconductor2"=>$licenciaconductor2,
                                       ":vigencia2"=>$vigencia2,
                                       ":estado"=>$estado);
            
            
                                   $this->SetArray_insertar($arrayIngresarFuec);
                                   $this->SetQuery($queryIngresarFuec);
                                   $this->Insertar();
                                   
                   if($_SESSION['rangoUsuario'] == "administrador" || $_SESSION['rangoUsuario'] == "usuarios")
                   {
                       if($this->filasAfectadas()){
                        $this->respuesta = "Se a Creado un Fuec con codigo : ".$idFuec;

                        }else{
                         $this->respuesta = "No se a reliado la operacion ";
                           
                       }
                       
                   }
                       
       
                       
                   if($_SESSION['rangoUsuario'] == "conductor")
                   {
                       if($this->filasAfectadas()){
                        $this->respuesta = "Se a Creado un Fuec con codigo : ".$idFuec;

                        }else{
                         $this->respuesta = "No se a reliado la operacion ";
                           
                       }
                       
                   }    
            
            
        } catch (Exception $ex) {
                Echo "Error".$ex->getFile();
        }
       
        
    }//ingresar Fuec
    
    
    
    
    
    function BucarFuec($valor){
        try {
         if(isset($_SESSION['rangoUsuario']) && $_SESSION['rangoUsuario'] != "conductor"  ){ 
        $querybuscarFuec = "SELECT * FROM fuec WHERE nitEmpresa = :idempresa HAVING "
                                            . "fechaCreacion LIKE :valor OR "
                                            . "nombreEmpresa LIKE :valor OR "
                                            . "nitEmpresa LIKE :valor OR "
                                            . "numeroContrato LIKE :valor OR "
                                            . "nombreContratante LIKE :valor OR "
                                            . "objetocontrato LIKE :valor OR "
                                            . "recorido LIKE :valor OR "
                                            . "placa LIKE :valor OR "
                                            . "modelovehiculo LIKE :valor OR "
                                            . "marcaVehiculo LIKE :valor OR "
                                            . "numInterVehiculo LIKE :valor OR "
                                            . "tarjetaOperacion LIKE :valor OR "
                                            . "nombreconductor1 LIKE :valor OR "
                                            . "cedulaconductor1 LIKE :valor OR "
                                            . "licenciaconductor1 LIKE :valor OR "
                                            . "nombreconductor2 LIKE :valor OR "
                                            . "cedulaconductor2 LIKE :valor OR "
                                            . "licenciaconductor2 LIKE :valor OR "
                                            . "estado LIKE :valor" ;
         $arrayQueryFuec = array(":valor"=>'%'.$valor.'%',":idempresa"=>$_SESSION['idEmpresa']);
         }//fin if
         
         if(isset($_SESSION['rangoUsuario']) && $_SESSION['rangoUsuario'] == "conductor"  ) {
             $querybuscarFuec = "SELECT * FROM fuec WHERE nitEmpresa = :idempresa AND numeroContrato = :numeroDocumento  HAVING "
                                            . "fechaCreacion LIKE :valor OR "
                                            . "nombreEmpresa LIKE :valor OR "
                                            . "nitEmpresa LIKE :valor OR "
                                            
                                            . "nombreContratante LIKE :valor OR "
                                            . "objetocontrato LIKE :valor OR "
                                            . "recorido LIKE :valor OR "
                                            . "placa LIKE :valor OR "
                                            . "modelovehiculo LIKE :valor OR "
                                            . "marcaVehiculo LIKE :valor OR "
                                            . "numInterVehiculo LIKE :valor OR "
                                            . "tarjetaOperacion LIKE :valor OR "
                                            . "nombreconductor1 LIKE :valor OR "
                                            . "cedulaconductor1 LIKE :valor OR "
                                            . "licenciaconductor1 LIKE :valor OR "
                                            . "nombreconductor2 LIKE :valor OR "
                                            . "cedulaconductor2 LIKE :valor OR "
                                            . "licenciaconductor2 LIKE :valor OR "
                                            . "estado LIKE :valor" ;
             $arrayQueryFuec = array(":valor"=>'%'.$valor.'%',":idempresa"=>$_SESSION['idEmpresa'],":numeroDocumento"=>$_SESSION['documento']); 
         }
         
         
         
        $this->Setarray_selecionar($arrayQueryFuec);
        $this->SetQuery($querybuscarFuec);
        $this->respuesta =  $this->paginasionMostrar($this->NumeroPagina, $this->limiteDeDados);
        
        
        } catch (Exception $ex) {
                echo $ex->getLine();
        }
     }
     
     
     function ActulizarEstadoFuec($idfuec,$estado){
         try {
             
             $queryActulizarEstadoFuec = "UPDATE fuec SET fuec.estado = :estado WHERE fuec.idfuec = :idfuec";
             
             $arrayActulizarEstadoFuec =  array(":idfuec"=>$idfuec,":estado"=>$estado);
             
             
             $this->SetArray_actualizar($arrayActulizarEstadoFuec);
             $this->SetQuery($queryActulizarEstadoFuec);
             $this->Actualizar();
             
             if($this->filasAfectadas()){
                 $this->respuesta = "Se a Marcado Como ".$estado." el Fuec Con ID: ".$idfuec;
             }else{
                 $this->respuesta = "No se a Relizado Cambio de Estado ";
             }
             
             
             
         } catch (Exception $ex) {
             echo  "[Error] liena  ".$ex->getFile();
         }
         
         
     }
     
     
     function BucarFuecid($valor=null){
        try {
            
        
        
        $querybuscarFuec = "SELECT * FROM empresas , fuec WHERE fuec.idfuec LIKE :valor AND fuec.nitEmpresa = :nit  AND empresas.nit = :nit ";
        $arrayQueryFuec = array(":valor"=>'%'.$valor.'%',':nit'=>$_SESSION['idEmpresa']);
        $this->Setarray_selecionar($arrayQueryFuec);
        $this->SetQuery($querybuscarFuec);
        $this->respuesta =  $this->seleccionar();
        
        
        } catch (Exception $ex) {
                echo $ex->getLine();
        }
     }
     
     
    
    function PaginarFuec(){
        try {
            //si el usuario es diferente a conductor
        if(isset($_SESSION['rangoUsuario']) && $_SESSION['rangoUsuario'] != "conductor"  ){
            
        $queryPaginarFuec = "SELECT * FROM fuec WHERE  fuec.nitEmpresa = :idempresa ";
        $arrayPaginatFuec = array(":idempresa"=>$_SESSION['idEmpresa'] );
        
        }
        //si es igual a conductor solo listar los que el halla echo
        if(isset($_SESSION['rangoUsuario']) && $_SESSION['rangoUsuario'] == "conductor"  ) {
            $queryPaginarFuec = "SELECT * FROM fuec WHERE  fuec.nitEmpresa = :idempresa HAVING  cedulaconductor1 = :numeroDocumento OR cedulaconductor2 = :numeroDocumento";
            $arrayPaginatFuec = array(":idempresa"=>$_SESSION['idEmpresa'],":numeroDocumento"=>$_SESSION['documento']);
        }
        $this->SetQuery($queryPaginarFuec);
        $this->Setarray_selecionar($arrayPaginatFuec);
        $this->respuesta =  $this->paginasionMostrar($this->NumeroPagina, $this->limiteDeDados);
        } catch (Exception $ex) {
                echo $ex->getLine();
        }
    }
    
    function ContarFuec(){
        try {
            
        
        
        $queryPaginarFuec = "SELECT * FROM fuec WHERE  fuec.nitEmpresa = :idempresa ";
        $arrayPaginatFuec = array(":idempresa"=>$_SESSION['idEmpresa'] );
        $this->SetQuery($queryPaginarFuec);
        $this->Setarray_selecionar($arrayPaginatFuec);
        $this->respuesta =   $this->contar_registro();
        
        } catch (Exception $ex) {
                echo $ex->getLine();
        }
    }
            
    function MarcarErrorFuec(){}
    
     
}


//$fuec = new Fuec();
//$fuec->IngresarFuec("coochofal","12536478","Jesus","murillo","murrilo derecho","122545","2019-06-01","2019-12-02","voi125","buss", "toyota", "-","025","125369447","david","12352884", "145258631","2019-07-02","jose","12358796","1254795486","2019-03-05");
// echo $fuec->GetRespuesta();

