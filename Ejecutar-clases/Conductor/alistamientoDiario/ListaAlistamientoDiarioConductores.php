<?php
require_once '../../../conexion/sessionlogin.php';
require_once '../../../conexion/conexion.php';
require_once '../../../clases/baseDeDatos.php';
require_once '../../../clases/Conductor.php';
require_once '../../../clases/Config.php';

//los usuario con rol conductor tienen una vista distinta de la tabla que lista los alistamiento diarios de los vehiculos
if(isset($_SESSION['rangoUsuario'])  && $_SESSION['rangoUsuario'] == 'conductor' ){
    
    //hago instancia de la clase alistamineto que esta dentro de archivo clases/conductor.php
    $AlistamientoDiario = new AlistamientoDiario();
    //si el usuario quiere hacer una busqueda solo es pasar el valor por el input con name=BuscarAlistamientoDiario
    if(!empty($_POST['buscarAlitamiento'])){
        $AlistamientoDiario->SetDato($_POST['buscarAlitamiento']);
    }
    //este es para pasar el numero pagina 
//    $_POST['paginaAlistamiento'] = 3;
    if(!empty($_POST['paginaAlistamiento']) ){
      $AlistamientoDiario->SetNumeroPagina($_POST['paginaAlistamiento']);  
    }
    
    
    $AlistamientoDiario->ListarBuscarAlistamientoDiario();
    $ver = $AlistamientoDiario->getResultado();
    $numeroRegistro = $AlistamientoDiario->contar_registro();

}//fin si


//ESTA PORCION DE CODIGO ES SIMILAR A LA QUE ESTA ARRIBA LA DIFRENCIA ES QUE SOLO LOS USUARIOS CON 
//ROL USUARIO PUEDE VER TODO LOS ALISTAMIENTO DIARIO QUE SE HAN REALIZADO LOS CONDUCTORES CAMBIA LA FUNCION QUE SE 
//LLAMA DESDE LA CLASE PRINCIPAL 
if(isset($_SESSION['rangoUsuario'])  && $_SESSION['rangoUsuario'] != 'conductor' ){
    
    //hago instancia de la clase alistamineto que esta dentro de archivo clases/conductor.php
    $AlistamientoDiario = new AlistamientoDiario();
    //si el usuario quiere hacer una busqueda solo es pasar el valor por el input con name=BuscarAlistamientoDiario
    if(!empty($_POST['buscarAlitamiento'])){
        $AlistamientoDiario->SetDato($_POST['buscarAlitamiento']);
    }
    //este es para pasar el numero pagina 
//    $_POST['paginaAlistamiento'] = 3;
    if(!empty($_POST['paginaAlistamiento']) ){
      $AlistamientoDiario->SetNumeroPagina($_POST['paginaAlistamiento']);  
    }
    
    
    $AlistamientoDiario->ListarBuscarAlistamientoDiarioUsuarioAdminstrador();
    $ver = $AlistamientoDiario->getResultado();
    $numeroRegistro = $AlistamientoDiario->contar_registro();

}//fin si






///--------------------------------------------------////

//    LISTANDO DOCUNETO VEHICULO

///--------------------------------------------------///

if(!empty($ver)){
$html =  '<table class="table table-hover">
              <tbody>
        <tr>
        <th>            
        </th>
        <th>
            Nombre Conductor
        </th>
        <th>
            <label>placa Vehiculo</label>
        </th>
        <th>
            <label>Fecha </label>
        </th>
        <th>
            <label>Kilometraje</label>
        </th>
        <th>
            <label>Ver </label>
        </th>'; 
        //los usuario con rol conductor no puede eliminar
      if(isset($_SESSION['rangoUsuario'])  && $_SESSION['rangoUsuario'] != 'conductor'){
      $html .=  '<th>
            <label>Elimnar</label>
        </th>';
                }
     $html .= '<th>
            <label>Visto</label>
        </th>
     </tr>';
    
        
    
    foreach($ver as $row){
       
        $html .= '<tr>
                <td>
                    <a href="'.$row['id'].'" ></a>
                </td>
                <td>
                    '.$row['nombre_conductor'].'
                </td>
                ';
                
                  //el usuario con el rol de conductor solo puede ver fotos del vehiculo  
        $html .='<td>
                    <a href="'.$row['placa_vehiculo'].'"  class="VerFotoVehiculo">'.$row['placa_vehiculo'].'</a>
                </td>';
        
        $html .=  ' <td>
                    <label>'.$row['fecha_actual'].'</label>
                </td>
                <td>
                    <label>'.$row['kilometraje'].'</label>
                </td>
                <td>
                    <a href="'.$row['id'].'"  class="VerPdf"> '.IconPDF("../../../", "").' </a>
                </td>';
                //los usuario con rol conductor no puede eliminar
                if(isset($_SESSION['rangoUsuario'])  && $_SESSION['rangoUsuario'] != 'conductor'){
      $html .=  '<td>
                    <a href="'.$row['id'].'" class="EliminarAlistamineto">'.IconElimnar("../../../"). ' </a>
                </td>';}
                   //esto permiete que no se ejecute en conductor  
                if(isset($_SESSION['rangoUsuario'])  && $_SESSION['rangoUsuario'] !== 'conductor' && $row['visto'] !=='si'  )
                { $link = ' <a  href="'.$row['id'].'"  class="Marcar_Visto"  > '.visto("../../../", "",$row['visto']).' </a>';
                }else{  
                    
                    $link = visto("../../../", "",$row['visto']) ;
                }
     $html .= '   <td>
                        '.$link.'
                  </td>
                <tr>
                ';
        
        
    }
    
    $html .= '</tbody></table>';
    
    
    for ($i=1;$i<=ceil($numeroRegistro/10);$i++){        
        $html .= ' <a href="'.$i.'" class="PaginaAlistamineto"> '.$i.'</a>  ';
    }
    
    
    echo $html;

}