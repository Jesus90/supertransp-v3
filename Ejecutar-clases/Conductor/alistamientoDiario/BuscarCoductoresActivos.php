<?php
require_once '../../../conexion/conexion.php';
require_once '../../../conexion/sessionlogin.php';
require_once '../../../clases/baseDeDatos.php';
require_once '../../../clases/Conductor.php';


//ESTO SE EJECUTA DENTRO DEL FORMULARIO
// DE ALISTAMIENTO DIARIO
//SIRVE PARA LISTAR LOS CONDUCTORES QUE HACEN PARTE DE LA EMPRESA 
//QUE ESTAN DISPONIBLES PARA HACER UN ALISTAMIENTO DIARIO
if($_SESSION['rangoUsuario'] != "conductor"){
    //HACIENDO ISTANCIA DE LA CLASE CONDUCTOR
    $buscarConductor = new Conductor();
//    $buscarConductor->BuscarConductor("activo");
      $buscarConductor->ListarConductor();    
    //aqui se visualiza la informacion 
    $ver = $buscarConductor->getResultado();
    
    foreach($ver as $row){
        //MOSTRANDO EL RESULTADO DE LA CONSULTA
      echo   '<option value="'.$row['nombreapellido'].'" class="Conductores_activos" >Estado: '.$row['estado'].'</option>';
        
        
    }
}