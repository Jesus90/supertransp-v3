<?php
require_once '../../../conexion/conexion.php';
require_once '../../../conexion/sessionlogin.php';
require_once '../../../clases/baseDeDatos.php';
require_once '../../../clases/Conductor.php';



if( isset($_POST['placa_vehiculo'] )  &&
    isset($_POST['idempresa'] )  &&
    isset($_POST['nombre_conductor'] )  &&
    isset($_POST['fecha_actual'] )  &&
    isset($_POST['ciudad'] )  &&
    isset($_POST['kilometraje'] )  &&
    isset($_POST['direccionales_delanteras'] )  &&
    isset($_POST['direccionales_traseras'] )  &&
    isset($_POST['luces_altas'] )  &&
    isset($_POST['luces_bajas'] )  &&
    isset($_POST['luces_stops'] )  &&
    isset($_POST['luces_reversa'] )  &&
    isset($_POST['luces_parqueo'] )  &&
    isset($_POST['limpiabrisas_der_izq'] )  &&
    isset($_POST['limpiabrisas_atras'] )  &&
    isset($_POST['frenos_principal'] )  &&
    isset($_POST['frenos_emergencia'] )  &&
    isset($_POST['llantas_delanteras'] )  &&
    isset($_POST['llantas_traseras'] )  &&
    isset($_POST['llantas_repuestos'] )  &&
    isset($_POST['espejos_lateral_der_izq'] )  &&
    isset($_POST['espejos_retrovisor'] )  &&
    isset($_POST['pito'] )  &&
    isset($_POST['fluidos_frenos'] )  &&
    isset($_POST['fluidos_aceites'] )  &&
    isset($_POST['fluidos_refriguerante'] )  &&
    isset($_POST['herraminetas'] )  &&
    isset($_POST['crucetas'] )  &&    
    isset($_POST['gato'] )  &&
    isset($_POST['taco'] )  &&
    isset($_POST['senales'] )  && 
    isset($_POST['chaleco'] )  && 
    isset($_POST['botiquin'] )  ){
    
    
        $visto = "no";
    
    
    
    $AgregarAlistamineto = new AlistamientoDiario();
    $AgregarAlistamineto->AgregarAlistamientoDiario($_POST['idempresa'],
                                                    $_POST['nombre_conductor'],
                                                    $_SESSION['documento'],
                                                    $_POST['ciudad'],
                                                    $_POST['fecha_actual'],
                                                    $_POST['placa_vehiculo'],
                                                    $_POST['kilometraje'],
                                                    $_POST['direccionales_delanteras'],
                                                    $_POST['direccionales_delanteras_observacion'],
                                                    $_POST['direccionales_traseras'],
                                                    $_POST['direccionales_traseras_observacion'],
                                                    $_POST['luces_altas'],
                                                    $_POST['luces_altas_observacion'],
                                                    $_POST['luces_bajas'],
                                                    $_POST['luces_bajas_observacion'],
                                                    $_POST['luces_stops'],
                                                    $_POST['luces_stops_observacion'],
                                                    $_POST['luces_reversa'],
                                                    $_POST['luces_reversa_observacion'],
                                                    $_POST['luces_parqueo'],
                                                    $_POST['luces_parqueo_observacion'],
                                                    $_POST['limpiabrisas_der_izq'],
                                                    $_POST['limpiabrisas_der_izq_observacion'],
                                                    $_POST['limpiabrisas_atras'],
                                                    $_POST['limpiabrisas_atras_observacion'],
                                                    $_POST['frenos_principal'],
                                                    $_POST['frenos_principal_observacion'],
                                                    $_POST['frenos_emergencia'],
                                                    $_POST['frenos_emergencia_observacion'],
                                                    $_POST['llantas_delanteras'],
                                                    $_POST['llantas_delanteras_observacion'],
                                                    $_POST['llantas_traseras'],
                                                    $_POST['llantas_traseras_observacion'],
                                                    $_POST['llantas_repuestos'],
                                                    $_POST['llantas_repuestos_observacion'],
                                                    $_POST['espejos_lateral_der_izq'],
                                                    $_POST['espejos_lateral_der_izq_observacion'],
                                                    $_POST['espejos_retrovisor'],
                                                    $_POST['espejos_retrovisor_observacion'],
                                                    $_POST['pito'],
                                                    $_POST['pito_observacion'],
                                                    $_POST['fluidos_frenos'],
                                                    $_POST['fluidos_frenos_observacion'],
                                                    $_POST['fluidos_aceites'],
                                                    $_POST['fluidos_aceites_observacion'],
                                                    $_POST['fluidos_refriguerante'],
                                                    $_POST['fluidos_refriguerante_observacion'],
                                                    $_POST['herraminetas'],
                                                    $_POST['herramientas_observacion'],
                                                    $_POST['crucetas'],
                                                    $_POST['crucetas_observacion'],
                                                    $_POST['gato'],
                                                    $_POST['gato_observacion'],
                                                    $_POST['taco'],
                                                    $_POST['taco_observacion'],
                                                    $_POST['senales'],
                                                    $_POST['senales_observacion'],
                                                    $_POST['chaleco'],
                                                    $_POST['chaleco_observacion'],
                                                    $_POST['botiquin'],
                                                    $_POST['botiquin_observacion'],
                                                    $visto);
    echo $AgregarAlistamineto->getResultado();
    
}else{
    
    echo "faltan campos";
} 

