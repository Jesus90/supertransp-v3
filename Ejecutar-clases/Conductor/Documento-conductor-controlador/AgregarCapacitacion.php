<?php
require_once '../../../conexion/conexion.php';
require_once '../../../conexion/sessionlogin.php';
require_once '../../../clases/baseDeDatos.php';
require_once '../../../clases/Conductor.php';

if(isset($_POST['EliminarCapacitacion'])){
    
    unlink($_POST['EliminarCapacitacion']);
    
     $documentoConductor = new DocumentoConductor();
     $documentoConductor->actulizarDocumentoConductor("capacitacion", "ultima_capacitacion","", "", $_POST['numeroDocumento']);
}


if(isset($_POST['numeroDocumento']) &&
   isset($_POST['capacitacion_fecha']) &&
   isset($_FILES['capacitacion_file']['name']) && $_FILES['capacitacion_file']['type'] == 'application/pdf'){
    //ruta donde se guardara el documento
    $urlArchivo = "../../../Documentos/".$_SESSION['idEmpresa']."/CONDUCTOR/"."PDF-".$_POST['numeroDocumento']."/CAPACITACION-".$_POST['numeroDocumento']."/";
    //ruta que se guardara en la bd
    $urlBaseDato = "Documentos/".$_SESSION['idEmpresa']."/CONDUCTOR/"."PDF-".$_POST['numeroDocumento']."/CAPACITACION-".$_POST['numeroDocumento']."/".$_FILES['capacitacion_file']['name'];
    
    if(!file_exists($urlArchivo)){
        mkdir($urlArchivo,0777,true) or die ("No se peude crear la ruta, reintentar");
        
    }
    
    
    $documentoConductor = new DocumentoConductor();
    $documentoConductor->actulizarDocumentoConductor("capacitacion", "ultima_capacitacion", $urlBaseDato, $_POST['capacitacion_fecha'], $_POST['numeroDocumento']);
    if($documentoConductor->getResultado()){
           //si hay repsuesta positiva hace el cambio 
        if($_FILES['capacitacion_file']['name'] && move_uploaded_file($_FILES['capacitacion_file']['tmp_name'], $urlArchivo.$_FILES['capacitacion_file']['name'])){
            
            sleep(2);
            echo $documentoConductor->getResultado();
        }
        
    }else{
         echo $documentoConductor->getResultado();
    }
    
    
   }

