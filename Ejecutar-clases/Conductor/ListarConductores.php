<?php
require_once '../../conexion/conexion.php';
require_once '../../conexion/sessionlogin.php';
require_once '../../clases/baseDeDatos.php';
require_once '../../clases/Conductor.php';
require_once '../../clases/Config.php';

$listarConductores = new Conductor();

if(isset($_POST['pagina'])){
    
    $listarConductores->SetNumeroPagina($_POST['pagina']);
}


if(isset($_POST['buscar'])){
    
    $listarConductores->BuscarConductor($_POST['buscar']);
}else{
    $listarConductores->SetLimiteDatos(10);
    $listarConductores->PaginaConductor();
    
}

   
      $ver_conductor = $listarConductores->getResultado();
                     $listarConductores->ContarConductor();
                     $numConductor = $listarConductores->getResultado();
    
    
    $html = '
     <table class="table table-hover">
              <tbody>
                <tr>
                <th></th>
                <th>Nombres y Apellido</th>
                <th>Cedula</th>
                <th>Nº Contacto</th>
                <th>Fecha nacimento</th>
                <th>Estado</th>
                <th>Semana por Vencer</th>
                <th>Eliminar</th>
                <th>Actulizar</th>
            </tr>
        
        <tbody>
        ';
       foreach ($ver_conductor as $row){
         $html .= '
            <tr><td><a href="'.$row['Cedulaconductor'].'" class="DocumentoConductor">'. Documentos("", "").'</a></td>
                <td>'.$row['nombreapellido'].'</td>
                <td>'.$row['Cedulaconductor'].'</td>
                <td>'.$row['numero_contacto'].'</td>
                <td>'.$row['Fecha_nacimento'].'</td>
                <td>'.Semaforo("","",$row['estado']).$row['estado'].'</td>
                <td>'.$row['Fecha_nacimento'].'</td>
                <td><a href="'.$row['Cedulaconductor'].'" class="elimiarConductor" >'.IconElimnar().'</a></td>
                <td><a href="Actulizar-conductor.php?numero='.$row['Cedulaconductor'].'" class="actulizarConductor" >'.IconActulizar().'</a></td>
              
            </tr>';
       }
        $html .=  ' </tbody>
                </table>';
        
        
                    //esta seccion sirve para paginar conductores que estan afiliador a la empresa
                    $html .=  ' <div>';
                        for($i=1 ;$i<= ceil($numConductor/10);$i++ ){ 

                     $html .= '  <a href="'.$i.'" class="paginaConductor">'.$i.'</a>  ';

                         }
                     $html .=  '  </div>'; 
    
    
    echo $html;

    