<?php
require_once '../../conexion/sessionlogin.php';
require_once '../../conexion/conexion.php';

require_once '../../clases/baseDeDatos.php';
require_once '../../clases/verificar/verificarVehiculo.php';
require_once '../../clases/verificar/verificarConductor.php';
require_once '../../clases/verificar/verificarContrato.php';






function VerificarEstadoSoat(){
    $provando = new VericarVehiculo();
    $provando->VerificarEstadoSoat();
     return $provando->getRepuesta()['Activos'];
 }


function VerificarEstadoTecnicomecanica(){
    $provando = new VericarVehiculo();
    $provando->VerificarEstadoTecnicomecanica();
    return $provando->getRepuesta()['Activos'];
}
 

function VerificarEstadoMantenimento() {
    $provando = new VericarVehiculo();
    $provando->VerificarEstadoMantenimento();
    return $provando->getRepuesta()['Activos']; 
}



function VerificarEstadoCorrectivo(){
    $provando = new VericarVehiculo();
    $provando->VerificarEstadoCorrectivo();
    return $provando->getRepuesta()['Activos'];
}


function VerificarTarjetaOperacion(){
    $provando = new VericarVehiculo();
    $provando->VerificarTarjetaOperacion();
    return $provando->getRepuesta()['Activos'];
}


function VerificarTarjetaPropiedad(){
    $provando = new VericarVehiculo();
    $provando->VerificarTarjetaPropiedad();
    return $provando->getRepuesta()['Activos'];
}

function VerificarSimit(){
    $provando = new VericarVehiculo();
    $provando->VerificarSimit();
    return $provando->getRepuesta()['Activos'];
}

function VerificarSupertrasporte(){
    $provando = new VericarVehiculo();
    $provando->VerificarSupertrasporte();
    return $provando->getRepuesta()['Activos'];;
}

function VerificarAlistamiento(){
    $provando = new VericarVehiculo();
    $provando->VerificarAlistamiento();
    return $provando->getRepuesta()['Activos'];
}


function EstadoTablaVehiculo(){
    $provando = new VericarVehiculo();
    $provando->EstadoTablaVehiculo();
    return $provando->getRepuesta()['Activos'];
}



//COMPROBANDO ESTADO DOCUMENTACION CONDUCTOR
function VerificarConductor(){
    $provando = new VerificarConductor();
    $provando->VerificarLicencia();
    return $provando->getRepuesta()['Activos'];;
}

function VerificarCapacitacion(){
    $provando = new VerificarConductor();
    $provando->VerificarCapacitacion();
    return $provando->getRepuesta()['Activos'];
} 


function VerificarSimitConductor(){
    $provando = new VerificarConductor();
    $provando->VerificarSimit();
    return $provando->getRepuesta()['Activos'];
}

function EstadoTablaConductor(){
    $provando = new VerificarConductor();
    $provando->EstadoTablaConductor();
    return $provando->getRepuesta()['Activos'];
}


//COMPROBANDO ESTADO CONTRATO
function VerificarContratos(){
    $provando = new VerificarContratos();
    $provando->VerificarContratos();
    return $provando->getRepuestaa()['Activos'];
}





//verificando estado y documentacion de vehiculos
VerificarEstadoSoat();
VerificarEstadoTecnicomecanica();
VerificarEstadoMantenimento();
VerificarEstadoCorrectivo();
VerificarTarjetaOperacion();
VerificarTarjetaPropiedad();
VerificarSimit();
VerificarSupertrasporte();
VerificarAlistamiento();
EstadoTablaVehiculo();


//verificando estado y documentacion de Conductor

VerificarConductor();
VerificarCapacitacion();
VerificarSimitConductor();
EstadoTablaConductor();


//verificando estado y documentacion de contrato
VerificarContratos();