<?php


require_once '../../conexion/sessionlogin.php';
if(isset($_SESSION)){
require_once '../../conexion/conexion.php';
require_once '../../clases/baseDeDatos.php';
require_once '../../clases/Vehiculo.php';
require_once '../../clases/Config.php';
}
//haciendo instancia de las clases Vehiculo
$listarVehiculo = new Vehiculo();

 


//si existe un valor 
if(isset($_POST['buscar'])){
    //llamara el metodo buscar para liste el contenido que busca el usuario
    $listarVehiculo->BuscarVehiculo($_POST['buscar']);
    
}else{      //si se pasa el numero de la pagina pasara a mostrar el resto de los datos
            if(isset($_GET['pagina'])){
                    
            $listarVehiculo->SetNumeroPagina($_GET['pagina']);
            $listarVehiculo->SetLimiteDatos(10);
            $listarVehiculo->PaginaVehiculo();
            }else{
            $listarVehiculo->SetLimiteDatos(10);
            $listarVehiculo->PaginaVehiculo();
                
            }
    
    
    
}
///aqui esta el resultado de llamada de la clase y del metodo
$ver = $listarVehiculo->getResultado();
//cuenta el numero de registros que tiene la consulta 
$numVehiculo = $listarVehiculo->contar_registro();

   //se crea la tabla html para que sea mostrada     
 $html = '  <table class="table table-hover">
              <tbody>
        <tr>
        <th></th>
        <th>Placa</th>
        <th>Modelo</th>
        <th>Marca</th>
        <th>Clase</th>
        <th># Vehiculo</th>
        <th>Tarjeta de operacion</th>
        <th>Estado</th>
        <th>propio</th>
        <th># Contrato</th>
        <th>Eliminar</th>
        <th>Actulizar</th>
        </tr>';

         foreach ($ver as $row){ 
        
        $html .= '<tr>
        <td>
            <a href="'.$row['placa'].'" class="documentoVehiculo" style="text-decoration : none;">'.Documentos().'</a>'
          . '<a href="'.$row['placa'].'" class="imagenesVehiculo" style="text-decoration : none;">'.imagenes().'</a>
        </td>
        <td>'.$row['placa'].'</td>
        <td> '.$row['modelo'].'</td>
        <td>'.$row['marca'].'</td>
        <td>'.$row['clase'].'</td>
        <td>'.$row['numerovehiculo'].'</td>
        <td>'.$row['tarjetaoperacion'].'</td>
        <td>'.Semaforo("","",$row['estado']). $row['estado'].'</td>
        <td>'. $row['propio'].'</td>
        

        <td>'. $row['numerocontrato'] .'</td>
            <td><a href="'. $row['placa']. '" class="eliminarVehiculo">'.IconElimnar().'</a></td>
            <td><a href="Actulizar-Vehiculo.php?placaVehiculo='. $row['placa'].'"  class="actulizarVehiculo">'.IconActulizar().'</a></td>
        </tr> '; 
       
         }
         
         $html .=  '</tbody></table>';
                //esta seccion del codigo es para paginar
         $html .=  ' <div>';
            for($i=1 ;$i<= ceil($numVehiculo/10);$i++ ){ 

         $html .= '  <a href="'.$i.'" class="paginaVehiculo">'.$i.'</a>  ';
            
             }
         $html .=  '  </div>';  
                
     echo $html;           