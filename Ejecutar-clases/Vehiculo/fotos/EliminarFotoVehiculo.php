<?php

require_once '../../../conexion/sessionlogin.php';
if(isset($_SESSION)){
require_once '../../../conexion/conexion.php';
require_once '../../../clases/baseDeDatos.php';
require_once '../../../clases/Vehiculo.php';
}
//si exite el id de la foto procede a ejecutar el codigo
if(isset($_POST['idFoto'])){
    //haciendo la instancia de la classe documento vehiculo
    $EliminarFotoVehiculo = new DocumentoVehiculo();
    //listando los dato desde la base de datos para que  nos de la ruta de la foto
    $EliminarFotoVehiculo->ListarFotosVehiculo($_POST['idFoto']);
    //obtenemos la respuesta, que son los datos como placa, id y la ruta de la foto
    $ver = $EliminarFotoVehiculo->getResultado();
    
    foreach ($ver as $row){
        //ruta de el archivo es proporcionada por la base de datos 
        //si exixte la ruta y no esta vacia procedera a eliminar el archivo fisico primero
        if(isset($row['fotos'])){
            unlink("../../../".$row['fotos']);
        }
        
        
        
        //luego de eliminar el archivo lo elimina por la base de datos
        $EliminarFotoVehiculo->EliminarFotoVehiculo($row['id']);
        
        //respuesta de la operacion
       echo  $EliminarFotoVehiculo->getResultado();
        
    }
    
}
