<?php
require_once '../../conexion/conexion.php';
require_once '../../clases/baseDeDatos.php';
require_once '../../clases/Empresa.php';
require_once '../../conexion/sessionlogin.php';



//validando que los post no vengan vacios
if(isset($_POST['razonsocial']) &&
   isset($_POST['nit']) &&
   isset($_POST['email'])
    ){
    
  
    //haciendo instancia de la clase empresa para llamar el metodo 
    $ingresarEmpresa = new Empresa();
    $ingresarEmpresa->InsertarNuevaEmpresa($_POST['razonsocial'], $_POST['nit'], $_POST['email']);
    
    //si exite un valor => procedera a crear una carpeta
    if($ingresarEmpresa->getResultado()){
        //carpeta principal
        $ruta = "../../Documentos/";
        //ruta conpleta donde van a ir los pdf
        $carpetapdf = $ruta.$_SESSION['idEmpresa']."/PDF-".$_SESSION['idEmpresa'];
        //ruta completa donde van a ir las imganes 
        $carpetaImg = $ruta.$_SESSION['idEmpresa']."/IMG-".$_SESSION['idEmpresa'];
        
        //si no exite la carpeta la crea donde se guardaran los pdf
        if(!file_exists($carpetapdf)){
            mkdir($carpetapdf,0777,true);
        }
        //si no exite la carpeta la crea donde se guardaran los IMG
        if(!file_exists($carpetaImg)){
            mkdir($carpetaImg,0777,true);
        }
        
        echo "<strong>La Empresa ".$_POST['razonsocial']." Fue creada!</strong>";
    }else{
        echo false;
    }
    }








