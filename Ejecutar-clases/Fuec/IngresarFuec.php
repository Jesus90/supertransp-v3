<?php
require_once '../../conexion/conexion.php';
require_once '../../conexion/sessionlogin.php';
require_once '../../clases/baseDeDatos.php';
require_once '../../clases/Fuec.php';

//verificando que los campo esten lleno
if(isset($_POST['nombreEmpresa']) && isset($_POST['nitEmpresa'])  &&
   isset($_POST['placa']) && isset($_POST['modelovehiculo']) && isset($_POST['marcaVehiculo']) &&
   isset($_POST['clasevehiculo']) && isset($_POST['numInterVehiculo']) && isset($_POST['tarjetaOperacion']) &&
   isset($_POST['nombreconductor1']) && isset($_POST['cedulaconductor1']) && isset($_POST['licenciaconductor1']) &&
   isset($_POST['vigencia1']) && isset($_POST['Codigo_Departamento'])){
    
    //si el campo del segundo conductor llega vacio
    if(empty($_POST['nombreconductor2']) && empty($_POST['cedulaconductor2']) &&  empty($_POST['licenciaconductor2']) && empty($_POST['vigencia2']) ){
        $_POST['nombreconductor2'] = "-";
        $_POST['cedulaconductor2'] = "-";
        $_POST['licenciaconductor2'] = "-";
        $_POST['vigencia2'] ="-"; 
        
    }
    //si el cambo de fecha de vigencia llega vacio
    if(empty($_POST['fecha_vencimineto'])){
        $fecha_actual = date("Y-m-j");
        $_POST['fecha_vencimineto'] = date('Y-m-j', strtotime($fecha_actual."+ 1 days"));
    }
    
    
    //haciendo instancia de la clase fuec
    $fuec = new Fuec();
    //llamado al metodo y pasando los valores
    $fuec->IngresarFuec($_POST['fecha_vencimineto'],
                        $_POST['nombreEmpresa'],
                        $_POST['numeroContrato'],
                        $_POST['nombreContratante'],
                        $_POST['objetocontrato'],
                        $_POST['recorido'],
                        $_POST['conevio'],
                        $_POST['fechaInicontrato'],
                        $_POST['fechaFincontrato'],
                        $_POST['placa'],
                        $_POST['modelovehiculo'],
                        $_POST['marcaVehiculo'],
                        $_POST['clasevehiculo'],
                        $_POST['numInterVehiculo'],
                        $_POST['tarjetaOperacion'],
                        $_POST['nombreconductor1'],
                        $_POST['cedulaconductor1'],
                        $_POST['licenciaconductor1'],
                        $_POST['vigencia1'],
                        $_POST['nombreconductor2'],
                        $_POST['cedulaconductor2'],
                        $_POST['licenciaconductor2'],
                        $_POST['vigencia2'],
                        $_POST['Codigo_Departamento']
                        ); //
    //respuesta de la peticion
    echo   $fuec->GetRespuesta();
    
   }



//
//
//
// $html = $_POST['nombreEmpresa'] ." ".
//            $_POST['nitEmpresa'] ." ".
//            $_POST['numeroContrato'] ." ".
//            $_POST['nombreContratante'] ." ".
//            $_POST['objetocontrato'] ." ".
//            $_POST['recorido'] ." ".
//            $_POST['conevio'] ." ".
//            $_POST['fechaInicontrato'] ." ".
//            $_POST['fechaFincontrato'] ." ".
//            $_POST['placa'] ." ".
//            $_POST['modelovehiculo'] ." ".
//            $_POST['marcaVehiculo'] ." ".
//            $_POST['clasevehiculo'] ." ".
//            $_POST['numInterVehiculo'] ." ".
//            $_POST['tarjetaOperacion'] ." ".
//            $_POST['nombreconductor1'] ." ".
//            $_POST['cedulaconductor1'] ." ".
//            $_POST['licenciaconductor1'] ." ".
//            $_POST['vigencia1'] ." ".
//            $_POST['nombreconductor2'] ." ".
//            $_POST['cedulaconductor2'] ." ".
//            $_POST['licenciaconductor2'] ." ".
//            $_POST['vigencia2'] ." ";
//    
//    
//    echo $html;
//            