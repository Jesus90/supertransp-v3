
<?php

require_once '../../conexion/conexion.php';
require_once '../../conexion/sessionlogin.php';
require_once '../../clases/baseDeDatos.php';
require_once '../../clases/Config.php';
require_once '../../clases/Fuec.php';


$fuec = new Fuec();


if(!empty($_POST['buscar'])){
    $fuec->BucarFuec($_POST['buscar']);
    $ver = $fuec->GetRespuesta();
    
}else{
    if(isset($_POST['pagina'])){
        $fuec->SetNumeroPagina($_POST['pagina']);
    }
    
   $fuec->PaginarFuec();
   $ver = $fuec->GetRespuesta();
 
}

                        $fuec->SetNumeroPagina();
                        $fuec->ContarFuec();
                        $numeroFuec = $fuec->GetRespuesta();

$html = '<table  class="listarFuec table table-hover">
             <tbody>
                        <tr>
                         <th>Pdf</th>  
                        <th>Numero Contrato</th>
                        <th>Nombre Contratante</th>
                        <th>Numero Placa</th>
                        <th>Tarjeta Operacion</th>
                        <th>Nombre Conductor 1</th>
                        <th>Cedula Conductor 1</th>
                        <th>Nombre Conductor 2</th>
                        <th>Cedula Conductor 2</th>
                        <th>Estado Fuec</th>
                        <th>Marcar Error</th></tr>'; 
   
   foreach ($ver as $row){
        
       
       $html .= '<tr>
                <td><a href="../../pdf/Fuec/fuecPDF.php?imprimirPDF='.$row['idfuec'].'" class="" target="_blank">'. IconPDF().'</a></td>   
                <td>'.$row['numeroContrato'].'</td>
                <td>'.$row['nombreContratante'].'</td>
                <td>'.$row['placa'].'</td>
                <td>'.$row['tarjetaOperacion'].'</td>
                <td>'.$row['nombreconductor1'].'</td>
                <td>'.$row['cedulaconductor1'].'</td>
                <td>'.$row['nombreconductor2'].'</td>
                <td>'.$row['cedulaconductor2'].'</td>
                <td>'. Semaforo("","",$row['estado'])." ".$row['estado'].'</td>';
                 
                    if(isset($_SESSION['rangoUsuario']) && $_SESSION['rangoUsuario'] != "conductor" ){
                       $link =  '<a href="'.$row['idfuec'].'" class="MarcarError" title="'.$row['estado'].'">'.documentoError("","",$row['estado']).'</a></tr>';
                    }else{
                        $link = documentoError("","",$row['estado']);
                    }
             $html .= '<td>'.$link.'</td>';       
       
                
       
   }//fin del siglo for
   
   $html .= ' </tbody></table>';

   //esta seccion sirve para paginar conductores que estan afiliador a la empresa
                    $html .=  ' <div>';
                        for($i=1 ;$i<= ceil($numeroFuec/10);$i++ ){ 

                     $html .= '  <a href="'.$i.'" class="paginaFuec" style="text-decoration:none;">'.$i.'</a>  ';

                         }
                     $html .=  '  </div>'; 
   

echo $html;