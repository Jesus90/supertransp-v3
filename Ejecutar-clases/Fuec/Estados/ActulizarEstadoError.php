<?php
    require_once '../../../conexion/conexion.php';
    require_once '../../../conexion/sessionlogin.php';
    require_once '../../../clases/baseDeDatos.php';
    require_once '../../../clases/Fuec.php';

if(isset($_POST['idFuec'])){
    
    $actulizarEstadoFuec = new Fuec();
    $actulizarEstadoFuec->ActulizarEstadoFuec($_POST['idFuec'],"inactivo");
    
    echo $actulizarEstadoFuec->GetRespuesta();
    
}
