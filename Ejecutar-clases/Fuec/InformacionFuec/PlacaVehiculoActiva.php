<?php
require_once '../../../conexion/sessionlogin.php';
require_once '../../../conexion/conexion.php';

require_once '../../../clases/baseDeDatos.php';
require_once '../../../clases/Vehiculo.php';
require_once '../../../clases/Config.php';


$placasActivas = new Vehiculo();

if(isset($_POST['numeroContrato'])){
    $placasActivas->ListarVehiculoPlaca(trim($_POST['numeroContrato']));
}else{
    $placasActivas->ListarVehiculoPlaca("");
}


$ver =  $placasActivas->getResultado();
$html = '<select name="placa" class="placa" style="width: 180px;">';
$html .= "<option value='' >selecinar</option> ";
foreach ($ver as $row){
    
    $html .= '<option value="'.$row['placa'].'">'.$row['placa'].'</option>';
                    
}

$html .= '</select>';
echo $html;
    