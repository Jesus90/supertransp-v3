<?php

require_once '../../conexion/sessionlogin.php';
require_once '../../conexion/conexion.php';
require_once '../../clases/baseDeDatos.php';
require_once '../../clases/Usuarios.php';


//-----------------------si viene con imagen ------------------//
if(isset( $_POST['usuario'] )  &&
   isset( $_POST['password_2'] )  &&
   isset( $_POST['numero'] )  &&
   isset( $_POST['rango'] )  &&
   isset( $_POST['email'] )  &&
   isset( $_FILES['perfil_file']['name'] )  &&
        $_FILES['perfil_file']['type'] == 'image/jpeg' ||  $_FILES['perfil_file']['type'] == 'image/png'    ){
    
             //DEPENDIENE EL RANGO DEL USUARIO CAMBIARA LA RUTA DONDE IRA LA FOTO      
    if($_POST['rango'] == 'usuarios'){
        
        $urlArchivo = "../../Documentos/".$_SESSION['idEmpresa']."/USUARIO/IMG-".$_SESSION['idEmpresa']."/FOTO-".$_POST['numero']."/";
             $urlBasedato = "Documentos/".$_SESSION['idEmpresa']."/USUARIO/IMG-".$_SESSION['idEmpresa']."/FOTO-".$_POST['numero']."/".$_FILES['perfil_file']['name'];
    }
    
    
    if($_POST['rango'] == 'conductor'){
        $urlArchivo = "../../Documentos/".$_SESSION['idEmpresa']."/CONDUCTOR/IMG-".$_SESSION['idEmpresa']."/FOTO-".$_POST['numero']."/";
             $urlBasedato = "Documentos/".$_SESSION['idEmpresa']."/CONDUCTOR/IMG-".$_SESSION['idEmpresa']."/FOTO-".$_POST['numero']."/".$_FILES['perfil_file']['name'];
    }
    
    
    if($_POST['rango'] == 'administrador'){
        $urlArchivo = "../../Documentos/".$_SESSION['idEmpresa']."/ADMINISTRADOR/IMG-".$_SESSION['idEmpresa']."/FOTO-".$_POST['numero']."/";
             $urlBasedato = "Documentos/".$_SESSION['idEmpresa']."/ADMINISTRADOR/IMG-".$_SESSION['idEmpresa']."/FOTO-".$_POST['numero']."/".$_FILES['perfil_file']['name'];
    }
    
    //CREANDO LA CARPETAS
    if(!file_exists($urlArchivo) ){
        mkdir($urlArchivo,0777,true) or die ('No se puede crear la carpetas, volver a intentar');
    }
    
            $ActulizaUsuarioConFoto = new Usuarios();
            $ActulizaUsuarioConFoto->SetNitEmpresa($_SESSION['idEmpresa']);
            $ActulizaUsuarioConFoto->SetNombreUsuario($_POST['usuario']);
            $ActulizaUsuarioConFoto->Setpassword($_POST['password_2']);
            $ActulizaUsuarioConFoto->SetDocumento($_POST['numero']);
            $ActulizaUsuarioConFoto->SetRango($_POST['rango']);
            $ActulizaUsuarioConFoto->SetEmail($_POST['email']);
            $ActulizaUsuarioConFoto->SetFotoPerfil($urlBasedato);           
            $ActulizaUsuarioConFoto->actulizarUsuarioFoto();
            
            if($ActulizaUsuarioConFoto->getResultado()){
            if($_FILES['perfil_file']['name'] && move_uploaded_file($_FILES['perfil_file']['tmp_name'], $urlArchivo.$_FILES['perfil_file']['name']))
                {
                        sleep(2);
                        echo $ActulizaUsuarioConFoto->getResultado();
                }
        
                }
            
            
        }
        
   //------------------si  no viene imagen --------------//     
if(isset( $_POST['usuario'] )  &&
   isset( $_POST['password_2'] )  &&
   isset( $_POST['numero'] )  &&
   isset( $_POST['rango'] )  &&
   isset( $_POST['email'] )   ){
    
    $ActulizaUsuarioConFoto = new Usuarios();
            $ActulizaUsuarioConFoto->SetNitEmpresa($_SESSION['idEmpresa']);
            $ActulizaUsuarioConFoto->SetNombreUsuario($_POST['usuario']);
            $ActulizaUsuarioConFoto->Setpassword($_POST['password_2']);
            $ActulizaUsuarioConFoto->SetDocumento($_POST['numero']);
            $ActulizaUsuarioConFoto->SetRango($_POST['rango']);
            $ActulizaUsuarioConFoto->SetEmail($_POST['email']);                       
            $ActulizaUsuarioConFoto->actulizarUsuario();
            if($ActulizaUsuarioConFoto->getResultado()){
                
                echo $ActulizaUsuarioConFoto->getResultado();
            }
    
        }        
