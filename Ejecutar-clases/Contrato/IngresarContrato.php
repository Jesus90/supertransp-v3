<?php 
require_once '../../conexion/conexion.php';
require_once '../../conexion/sessionlogin.php';
require_once '../../clases/baseDeDatos.php';
require_once '../../clases/Contrato.php';


if(isset($_POST['numerocontrato'])&& isset($_POST['contratante'])&&  isset($_POST['objetocontrato'])&&
   isset($_POST['empresacontratada'])&& isset($_POST['fechainicial'])&&  isset($_POST['fechavencimineto']) && isset($_FILES['contratopdf'])){
    
    //ruta que se sara para trasladar el archivo fisico
    $rutadestino = "../../Documentos/".$_SESSION['idEmpresa']."/PDF-".$_SESSION['idEmpresa']."/"."CONTRATOS"."/Contrato-".$_POST['numerocontrato']."/";
    
    if(!file_exists($rutadestino)){
    mkdir($rutadestino,0777,true) or die ("No se puede Crear el archivo");}//fin if

    //ruta que se usara para guardar en la base de datos
    $rutaBaseDetatos= "Documentos/".$_SESSION['idEmpresa']."/PDF-".$_SESSION['idEmpresa']."/"."CONTRATOS"."/Contrato-".$_POST['numerocontrato']."/".$_FILES['contratopdf']['name'];
    
    
    $ingresarContrato = new Contrato();
    $ingresarContrato->AgregarContrato($_POST['numerocontrato'],
                                       $_POST['contratante'],
                                       $_POST['objetocontrato'],
                                       $_POST['recorido'],
                                       $_POST['conevio'],
                                       $_POST['empresacontratada'],
                                       $_POST['fechainicial'],
                                       $_POST['fechavencimineto'],
                                       $_POST['descriptcioncontrato'],$rutaBaseDetatos);
    
   if($ingresarContrato->getResultado()){
       echo $ingresarContrato->getResultado();
     //este se encarga de trasladar el archivo a la  carpeta 
        if($_FILES['contratopdf']['name'] && move_uploaded_file($_FILES['contratopdf']['tmp_name'], $rutadestino.$_FILES['contratopdf']['name'])){
            sleep(2);
            
        }
        
        
        }
    
   }
