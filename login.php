<!DOCTYPE html>
<html>
    

<?php if(isset($_SESSION['idEmpresa'])) {echo $_SESSION['idEmpresa'];} ?>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>S.Transp | Login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="paginas/desing/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="paginas/desing/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="paginas/desing/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="paginas/desing/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="paginas/desing/plugins/iCheck/square/blue.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page" style="background-image: url('img/fondo-login.jpg');" >
<div class="login-box">
  <div class="login-logo">
    <img style="width: 70%;" src="img/logo.png">
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"><strong> administrador de supertrnasp</strong></p>

    <form name="login" method="POST" enctype="multipart/form-data" id="login-usuario">
      <div class="form-group has-feedback">
        <input type="text" name="usuario" value="Jesus90" class="form-control" aria-label="Sizing example input" id="usuario" placeholder="Usuario" required/>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" value="Jesus90*" placeholder="Contraseña" id="password" aria-label="Sizing example input" name="password" required/>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
        <select name="rango" class="usuario_rango" style="width: 100%; height: 100%;" >
                 <option value=""></option>
                  <option value="administrador">Administrador</option>
                   <option value="usuarios">usuario</option>
                    <option value="conductor">Conductor</option>
                  </select>
      </div>

      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox">Recordarme
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" id="boton-login" class="btn btn-primary btn-block btn-flat">Iniciar</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a href="#">Olvide mi contraseña</a><br>
    <a href="register.html" class="text-center">Solicitar inscripción como nuevo miembro.</a>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<!-- jQuery 3 -->
 <script src="jquery/jquery.js" type="text/javascript" ></script>
  <script src="javascriptClases/ValidarCampos.js" type="text/javascript" >
  </script>
  
 <script src="javascriptClases/usuario/usuarios.js" type="text/javascript" >
 </script>
 <script src="javascriptClases/Ajax.js" type="text/javascript" >  
 </script>

<script src="paginas/desing/bower_components/jquery/dist/jquery.min.js">
</script>
<!-- Bootstrap 3.3.7 -->
<script src="paginas/desing/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="paginas/desing/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>

</body>
</html>
