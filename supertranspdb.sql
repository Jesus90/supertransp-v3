-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-01-2020 a las 22:29:40
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `supertranspdb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alistamiento`
--

CREATE TABLE `alistamiento` (
  `id` int(11) NOT NULL,
  `idempresa` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `nombre_conductor` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `cedula_conductor` int(15) NOT NULL,
  `ciudad` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_actual` date NOT NULL,
  `placa_vehiculo` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `kilometraje` int(8) NOT NULL,
  `direccionales_delanteras` tinyint(1) NOT NULL,
  `direccionales_delanteras_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `direccionales_traseras` tinyint(1) NOT NULL,
  `direccionales_traseras_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `luces_altas` tinyint(1) NOT NULL,
  `luces_altas_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `luces_bajas` tinyint(1) NOT NULL,
  `luces_bajas_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `luces_stops` tinyint(1) NOT NULL,
  `luces_stops_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `luces_reversa` tinyint(1) NOT NULL,
  `luces_reversa_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `luces_parqueo` tinyint(1) NOT NULL,
  `luces_parqueo_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `limpiabrisas_der_izq` tinyint(1) NOT NULL,
  `limpiabrisas_der_izq_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `limpiabrisas_atras` tinyint(1) NOT NULL,
  `limpiabrisas_atras_observacion` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `frenos_principal` tinyint(1) NOT NULL,
  `frenos_principal_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `frenos_emergencia` tinyint(1) NOT NULL,
  `frenos_emergencia_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `llantas_delanteras` tinyint(1) NOT NULL,
  `llantas_delanteras_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `llantas_traseras` tinyint(1) NOT NULL,
  `llantas_traseras_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `llantas_repuestos` tinyint(1) NOT NULL,
  `llantas_repuestos_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `espejos_lateral_der_izq` tinyint(1) NOT NULL,
  `espejos_lateral_der_izq_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `espejos_retrovisor` tinyint(1) NOT NULL,
  `espejos_retrovisor_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `pito` tinyint(1) NOT NULL,
  `pito_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `fluidos_frenos` tinyint(1) NOT NULL,
  `fluidos_frenos_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `fluidos_aceites` tinyint(1) NOT NULL,
  `fluidos_aceites_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `fluidos_refriguerante` tinyint(1) NOT NULL,
  `fluidos_refriguerante_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `herraminetas` tinyint(1) NOT NULL,
  `herramientas_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `crucetas` tinyint(1) NOT NULL,
  `crucetas_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `gato` tinyint(1) NOT NULL,
  `gato_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `taco` tinyint(1) NOT NULL,
  `taco_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `senales` tinyint(1) NOT NULL,
  `senales_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `chaleco` tinyint(1) NOT NULL,
  `chaleco_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `botiquin` tinyint(1) NOT NULL,
  `botiquin_observacion` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `visto` char(8) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alistamiento`
--

INSERT INTO `alistamiento` (`id`, `idempresa`, `nombre_conductor`, `cedula_conductor`, `ciudad`, `fecha_actual`, `placa_vehiculo`, `kilometraje`, `direccionales_delanteras`, `direccionales_delanteras_observacion`, `direccionales_traseras`, `direccionales_traseras_observacion`, `luces_altas`, `luces_altas_observacion`, `luces_bajas`, `luces_bajas_observacion`, `luces_stops`, `luces_stops_observacion`, `luces_reversa`, `luces_reversa_observacion`, `luces_parqueo`, `luces_parqueo_observacion`, `limpiabrisas_der_izq`, `limpiabrisas_der_izq_observacion`, `limpiabrisas_atras`, `limpiabrisas_atras_observacion`, `frenos_principal`, `frenos_principal_observacion`, `frenos_emergencia`, `frenos_emergencia_observacion`, `llantas_delanteras`, `llantas_delanteras_observacion`, `llantas_traseras`, `llantas_traseras_observacion`, `llantas_repuestos`, `llantas_repuestos_observacion`, `espejos_lateral_der_izq`, `espejos_lateral_der_izq_observacion`, `espejos_retrovisor`, `espejos_retrovisor_observacion`, `pito`, `pito_observacion`, `fluidos_frenos`, `fluidos_frenos_observacion`, `fluidos_aceites`, `fluidos_aceites_observacion`, `fluidos_refriguerante`, `fluidos_refriguerante_observacion`, `herraminetas`, `herramientas_observacion`, `crucetas`, `crucetas_observacion`, `gato`, `gato_observacion`, `taco`, `taco_observacion`, `senales`, `senales_observacion`, `chaleco`, `chaleco_observacion`, `botiquin`, `botiquin_observacion`, `visto`) VALUES
(16, '10423501260', 'Jesus David peña', 1143122971, 'barranquilla', '2019-12-08', 'veo356', 1545454, 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 'no'),
(17, '6565656e', 'dayana cepeda', 1143122972, 'barranquilla', '2020-01-08', 'cat215', 1545454, 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 1, ' ', 'no');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conductores`
--

CREATE TABLE `conductores` (
  `id` int(11) NOT NULL,
  `idempresa` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `nombreapellido` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `Cedulaconductor` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `numero_contacto` int(12) NOT NULL,
  `Fecha_nacimento` date NOT NULL,
  `estado` varchar(10) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `conductores`
--

INSERT INTO `conductores` (`id`, `idempresa`, `nombreapellido`, `Cedulaconductor`, `numero_contacto`, `Fecha_nacimento`, `estado`) VALUES
(18, '10423501260', 'Carlos90 cepeda', '104210', 2147483647, '2019-12-28', 'activo'),
(16, '10423501260', 'Jesus David peña', '104235012699', 2147483647, '2020-01-03', 'inactivo'),
(19, '6565656e', 'dayana cepeda', '11223344', 2147483647, '2019-12-28', 'activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contratosempresa`
--

CREATE TABLE `contratosempresa` (
  `id` int(11) NOT NULL,
  `idempresa` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `numerocontrato` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `contratante` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `objetocontrato` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `recorido` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `conevio` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `empresacontratada` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `fechainicial` date NOT NULL,
  `fechavencimineto` date NOT NULL,
  `descriptcioncontrato` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `contratopdf` longtext COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(10) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `contratosempresa`
--

INSERT INTO `contratosempresa` (`id`, `idempresa`, `numerocontrato`, `contratante`, `objetocontrato`, `recorido`, `conevio`, `empresacontratada`, `fechainicial`, `fechavencimineto`, `descriptcioncontrato`, `contratopdf`, `estado`) VALUES
(133, '6565656e', '1545455456', 'Jesus peña', 'cubrir rutas faltante', ' cincuvalar, calle 30 ', '565644', 'Coochofal', '2020-01-08', '2020-02-01', 'se proporcionan rutas', 'Documentos/6565656e/PDF-6565656e/CONTRATOS/Contrato-1545455456/supertransp.pdf', 'activo'),
(129, '10423501260', '1545455999', 'David peña ', 'cubrir rutas faltante', ' cincuvalar, calle 30 ', '565644', 'Coochofal', '2019-12-03', '2020-03-27', 'se proporcionan rutas', 'Documentos/10423501260/PDF-10423501260/CONTRATOS/Contrato-1545455999/supertransp.pdf', 'activo'),
(130, '10423501260', '154545666', 'Jesus peña', 'cubrir rutas faltante', ' cincuvalar, calle 30 ', '565644', 'Coochofal', '2019-12-02', '2019-12-17', 'se proporcionan rutas', 'Documentos/10423501260/PDF-10423501260/CONTRATOS/Contrato-154545666/supertransp.pdf', 'activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos_vehiculos`
--

CREATE TABLE `documentos_vehiculos` (
  `id` int(11) NOT NULL,
  `placa` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `soat` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_veci_soat` date NOT NULL,
  `estado_soat` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `tecnicomecanica` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_veci_tecnomecanica` date NOT NULL,
  `estado_tecnicomecanica` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `mantenimento` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_veci_mantenimento` date NOT NULL,
  `estado_mantenimento` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `mantenimentoCorrectivo` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_veci_correctivo` date NOT NULL,
  `estado_mantenimentoCorrectivo` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `tarjeta_de_operacion` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_veci_tarjeta_operacion` date NOT NULL,
  `estado_tarjeta_de_operacion` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `tarjeta_de_propiedad` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_veci_propiedad` date NOT NULL,
  `estado_tarjeta_de_propiedad` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `simit` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_veci_simit` date NOT NULL,
  `estado_simit` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `supertrasporte` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_veci_supertrasporte` date NOT NULL,
  `estado_supertrasporte` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `administracion_pago` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `estado_alitamiento_Diario` char(8) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `documentos_vehiculos`
--

INSERT INTO `documentos_vehiculos` (`id`, `placa`, `soat`, `fecha_veci_soat`, `estado_soat`, `tecnicomecanica`, `fecha_veci_tecnomecanica`, `estado_tecnicomecanica`, `mantenimento`, `fecha_veci_mantenimento`, `estado_mantenimento`, `mantenimentoCorrectivo`, `fecha_veci_correctivo`, `estado_mantenimentoCorrectivo`, `tarjeta_de_operacion`, `fecha_veci_tarjeta_operacion`, `estado_tarjeta_de_operacion`, `tarjeta_de_propiedad`, `fecha_veci_propiedad`, `estado_tarjeta_de_propiedad`, `simit`, `fecha_veci_simit`, `estado_simit`, `supertrasporte`, `fecha_veci_supertrasporte`, `estado_supertrasporte`, `administracion_pago`, `estado_alitamiento_Diario`) VALUES
(21, 'veo257', 'Documentos/10423501260/PDF-10423501260/SOAT/SOAT-VEO257/supertransp.pdf', '2020-01-04', 'activo', '', '0000-00-00', 'inactivo', '', '2019-12-30', 'activo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', ''),
(22, 'veo258', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', ''),
(23, 'veo259', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', '2019-12-24', 'activo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', 'Documentos/10423501260/PDF-10423501260/SUPERTRANPOSTE/SUPERTRANPOSTE-VEO259/supertransp.pdf', '2019-12-27', 'activo', '', ''),
(25, 'veo356', 'Documentos/10423501260/PDF-10423501260/SOAT/SOAT-VEO356/supertransp.pdf', '2020-01-04', 'activo', 'Documentos/10423501260/PDF-10423501260/TECNICO-MECANICA/TECNICO-MECANICA-VEO356/supertransp.pdf', '2019-12-19', 'activo', 'Documentos/10423501260/PDF-10423501260/MANTENIMENTO/MANTENIMENTO-VEO356/supertransp.pdf', '2019-12-28', 'activo', 'Documentos/10423501260/PDF-10423501260/MANTENIMENTO-CORRECTIVO/MANTENIMENTO-CORRECTIVO-VEO356/supertransp.pdf', '2019-12-28', 'activo', 'Documentos/10423501260/PDF-10423501260/TARJETA-OPERACION/TARJETA-OPERACION-VEO356/supertransp.pdf', '2020-03-05', 'activo', 'Documentos/10423501260/PDF-10423501260/TARJETA-PROPIEDAD/TARJETA-PROPIEDAD-VEO356/supertransp.pdf', '2020-01-04', 'activo', 'Documentos/10423501260/PDF-10423501260/SIMIT/SIMIT-VEO356/supertransp.pdf', '2020-01-04', 'activo', 'Documentos/10423501260/PDF-10423501260/SUPERTRANPOSTE/SUPERTRANPOSTE-VEO356/supertransp.pdf', '2020-01-04', 'activo', '', 'inactivo'),
(41, 'veo256', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', '0000-00-00', 'inactivo', '', ''),
(42, 'cat215', 'Documentos/6565656e/PDF-6565656e/SOAT/SOAT-CAT215/supertransp.pdf', '2020-01-25', 'activo', 'Documentos/6565656e/PDF-6565656e/TECNICO-MECANICA/TECNICO-MECANICA-CAT215/supertransp.pdf', '2020-01-31', 'activo', 'Documentos/6565656e/PDF-6565656e/MANTENIMENTO/MANTENIMENTO-CAT215/supertransp.pdf', '2020-01-31', 'activo', 'Documentos/6565656e/PDF-6565656e/MANTENIMENTO-CORRECTIVO/MANTENIMENTO-CORRECTIVO-CAT215/supertransp.pdf', '2020-01-31', 'activo', 'Documentos/6565656e/PDF-6565656e/TARJETA-OPERACION/TARJETA-OPERACION-CAT215/supertransp.pdf', '2020-01-31', 'activo', 'Documentos/6565656e/PDF-6565656e/TARJETA-PROPIEDAD/TARJETA-PROPIEDAD-CAT215/supertransp.pdf', '2020-01-31', 'activo', 'Documentos/6565656e/PDF-6565656e/SIMIT/SIMIT-CAT215/supertransp.pdf', '2020-01-31', 'activo', 'Documentos/6565656e/PDF-6565656e/SUPERTRANPOSTE/SUPERTRANPOSTE-CAT215/supertransp.pdf', '2020-01-31', 'activo', '', 'activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documento_conductor`
--

CREATE TABLE `documento_conductor` (
  `id` int(11) NOT NULL,
  `documento_conductore` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `licencia` longtext COLLATE utf8_spanish_ci NOT NULL,
  `licencia_numero` char(15) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_veci_licencia` date NOT NULL,
  `estado_licencia` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `hojadevida` longtext COLLATE utf8_spanish_ci NOT NULL,
  `capacitacion` longtext COLLATE utf8_spanish_ci NOT NULL,
  `ultima_capacitacion` date NOT NULL,
  `estado_capacitacion` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `resultado_alcholimetria` char(6) COLLATE utf8_spanish_ci NOT NULL,
  `ultima_alcholimetria` date NOT NULL,
  `simit` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_veci_simit_conductor` date NOT NULL,
  `esta_simit` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `foto_conductor` longtext COLLATE utf8_spanish_ci NOT NULL,
  `UltimaActulizacion` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `documento_conductor`
--

INSERT INTO `documento_conductor` (`id`, `documento_conductore`, `licencia`, `licencia_numero`, `fecha_veci_licencia`, `estado_licencia`, `hojadevida`, `capacitacion`, `ultima_capacitacion`, `estado_capacitacion`, `resultado_alcholimetria`, `ultima_alcholimetria`, `simit`, `fecha_veci_simit_conductor`, `esta_simit`, `foto_conductor`, `UltimaActulizacion`) VALUES
(4, '1143122100', '', '', '0000-00-00', 'inactivo', '', '', '0000-00-00', 'inactivo', '', '0000-00-00', '', '0000-00-00', 'inactivo', '', '2019-12-05 20:04:04'),
(5, '1143122101', '', '', '0000-00-00', 'inactivo', '', '', '0000-00-00', 'inactivo', '', '0000-00-00', '', '0000-00-00', 'inactivo', '', '2019-12-05 20:04:04'),
(6, '1143122102', '', '', '0000-00-00', 'inactivo', '', '', '0000-00-00', 'inactivo', '', '0000-00-00', '', '0000-00-00', 'inactivo', '', '2019-12-05 20:04:04'),
(7, '1143122103', '', '', '0000-00-00', 'inactivo', '', '', '0000-00-00', 'inactivo', '', '0000-00-00', '', '0000-00-00', 'inactivo', '', '2019-12-05 20:04:04'),
(8, '1143122104', '', '', '0000-00-00', 'inactivo', '', '', '0000-00-00', 'inactivo', '', '0000-00-00', '', '0000-00-00', 'inactivo', '', '2019-12-05 20:04:04'),
(9, '1143122104', '', '', '0000-00-00', 'inactivo', '', '', '0000-00-00', 'inactivo', '', '0000-00-00', '', '0000-00-00', 'inactivo', '', '2019-12-05 20:04:04'),
(10, '1143122105', '', '', '0000-00-00', 'inactivo', '', '', '0000-00-00', 'inactivo', '', '0000-00-00', '', '0000-00-00', 'inactivo', '', '2019-12-05 20:04:04'),
(11, '1143122106', '', '', '0000-00-00', 'inactivo', '', '', '0000-00-00', 'inactivo', '', '0000-00-00', '', '0000-00-00', 'inactivo', '', '2019-12-05 20:04:04'),
(12, '1143122107', '', '', '0000-00-00', 'inactivo', '', '', '0000-00-00', 'inactivo', '', '0000-00-00', '', '0000-00-00', 'inactivo', '', '2019-12-05 20:04:04'),
(13, '1143122108', '', '', '0000-00-00', 'inactivo', '', '', '0000-00-00', 'inactivo', '', '0000-00-00', '', '0000-00-00', 'inactivo', '', '2019-12-05 20:04:04'),
(14, '1143122109', '', '', '0000-00-00', 'inactivo', '', '', '0000-00-00', 'inactivo', '', '0000-00-00', '', '0000-00-00', 'inactivo', '', '2019-12-05 20:04:04'),
(16, '104235012699', '', '777777', '0000-00-00', 'inactivo', '', '', '0000-00-00', 'inactivo', '', '0000-00-00', '', '0000-00-00', 'inactivo', '', '2019-12-09 21:41:06'),
(17, '56565565555', 'Documentos/10423501260/CONDUCTOR/PDF-56565565555/LICENCIA-56565565555/supertransp.pdf', '2569884', '2019-12-10', '', '', '', '0000-00-00', '', '', '0000-00-00', '', '0000-00-00', '', '', '2019-12-04 18:01:41'),
(18, '104210', 'Documentos/10423501260/CONDUCTOR/PDF-104210/LICENCIA-104210/supertransp.pdf', '222222222', '2019-12-16', 'activo', 'Documentos/10423501260/CONDUCTOR/PDF-104210/HOJA-DE-VIDA-104210/supertransp.pdf', 'Documentos/10423501260/CONDUCTOR/PDF-104210/CAPACITACION-104210/supertransp.pdf', '2019-12-26', 'activo', '2.6', '2019-12-24', 'Documentos/10423501260/CONDUCTOR/PDF-104210/SIMIT-104210/supertransp.pdf', '2019-12-25', 'activo', '', '2019-12-09 21:41:06'),
(19, '11223344', 'Documentos/6565656e/CONDUCTOR/PDF-11223344/LICENCIA-11223344/supertransp.pdf', '1042356841', '2020-01-04', 'activo', 'Documentos/6565656e/CONDUCTOR/PDF-11223344/HOJA-DE-VIDA-11223344/supertransp.pdf', 'Documentos/6565656e/CONDUCTOR/PDF-11223344/CAPACITACION-11223344/supertransp.pdf', '2020-02-21', 'activo', '2.6', '2020-01-04', 'Documentos/6565656e/CONDUCTOR/PDF-11223344/SIMIT-11223344/supertransp.pdf', '2020-01-01', 'activo', 'Documentos/6565656e/CONDUCTOR/PDF-11223344/FOTO-11223344/estudio-comportamiento-espanoles-en-coche.jpg', '2019-12-08 04:25:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `id` int(11) NOT NULL,
  `razonsocial` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `nit` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `representante` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `cedula` int(14) NOT NULL,
  `telefono` int(14) NOT NULL,
  `direccion` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `movil` bigint(14) NOT NULL,
  `email` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `ciudad` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `firma` longtext COLLATE utf8_spanish_ci NOT NULL,
  `logotranscito` longtext COLLATE utf8_spanish_ci NOT NULL,
  `logoempresa` longtext COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `empresas`
--

INSERT INTO `empresas` (`id`, `razonsocial`, `nit`, `representante`, `cedula`, `telefono`, `direccion`, `movil`, `email`, `ciudad`, `firma`, `logotranscito`, `logoempresa`) VALUES
(14, 'tranposte', '10423501-10', '', 0, 0, '', 0, 'jd1990@hotmail.es', '', '', '', ''),
(12, 'jesus', '10423501260', 'Jesus David Peña Osorio', 1143122971, 2147483647, 'calle falsa', 3182227003, 'jd1990@hotmail.es', 'soledad', '../../Documentos/10423501260/IMG-10423501260/Firma_Humberto_Gordon.png', '../../Documentos/10423501260/IMG-10423501260/Miniterio_de_Transporte-logo-A36CDB2DCE-seeklogo.com.png', '../../Documentos/10423501260/IMG-10423501260/logo-adif.png'),
(13, 'jesus', '656456', '', 0, 0, '', 0, 'jd1990@hotmail.es', '', '', '', ''),
(11, 'coochofall', '6565656e', 'Jesus Peña', 1143122971, 3727984, 'calle 56', 318222703, 'coochofal@hotmail.es', 'soledad', '../../Documentos/6565656e/IMG-6565656e/Firma_Humberto_Gordon.png', '../../Documentos/6565656e/IMG-6565656e/descarga.png', '../../Documentos/6565656e/IMG-6565656e/logo.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotos_vehiculos`
--

CREATE TABLE `fotos_vehiculos` (
  `id` int(11) NOT NULL,
  `placa` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `fotos` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `fotos_vehiculos`
--

INSERT INTO `fotos_vehiculos` (`id`, `placa`, `fotos`) VALUES
(85, 'cat215', 'Documentos/6565656e/IMG-VEHICULO-6565656e/cat215/bus_nuevo.png'),
(86, 'veo256', 'Documentos/6565656e/IMG-VEHICULO-6565656e/veo256/bus_nuevo.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fuec`
--

CREATE TABLE `fuec` (
  `id` int(11) NOT NULL,
  `idfuec` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `fechaCreacion` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `fecha_vencimineto` date NOT NULL,
  `nombreEmpresa` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `nitEmpresa` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `numeroContrato` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `nombreContratante` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `objetocontrato` text COLLATE utf8_spanish_ci NOT NULL,
  `recorido` text COLLATE utf8_spanish_ci NOT NULL,
  `conevio` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `fechaInicontrato` date NOT NULL,
  `fechaFincontrato` date NOT NULL,
  `placa` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `modelovehiculo` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `marcaVehiculo` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `clasevehiculo` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `numInterVehiculo` int(6) NOT NULL,
  `tarjetaOperacion` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `nombreconductor1` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `cedulaconductor1` int(20) NOT NULL,
  `licenciaconductor1` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `vigencia1` date NOT NULL,
  `nombreconductor2` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `cedulaconductor2` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `licenciaconductor2` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `vigencia2` date NOT NULL,
  `estado` varchar(12) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `fuec`
--

INSERT INTO `fuec` (`id`, `idfuec`, `fechaCreacion`, `fecha_vencimineto`, `nombreEmpresa`, `nitEmpresa`, `numeroContrato`, `nombreContratante`, `objetocontrato`, `recorido`, `conevio`, `fechaInicontrato`, `fechaFincontrato`, `placa`, `modelovehiculo`, `marcaVehiculo`, `clasevehiculo`, `numInterVehiculo`, `tarjetaOperacion`, `nombreconductor1`, `cedulaconductor1`, `licenciaconductor1`, `vigencia1`, `nombreconductor2`, `cedulaconductor2`, `licenciaconductor2`, `vigencia2`, `estado`) VALUES
(86, '10h1948l2636n7758t9613p85', '2019-12-04 17:12:45', '0000-00-00', 'coochofall', '6565656e', '', '', '', '', '', '0000-00-00', '0000-00-00', 'veo256', '2016', 'cherolet', 'buss', 86, '2568984', 'Jesus David peña oso', 1143122971, '', '0000-00-00', '-', '-', '-', '0000-00-00', 'inactivo'),
(106, '10k566y378g6583a1774n25', '2019-12-05 16:22:12', '0000-00-00', 'coochofall', '6565656e', '22334455', 'Jesus peña', 'cubrir rutas faltante', ' cincuvalar, calle 30 ', '565644', '2019-12-17', '2020-01-04', 'ceo777', '2016', 'cherolet', 'buss', 86, '2568984', 'jose', 1143122100, '', '0000-00-00', 'jose', '1143122100', '', '0000-00-00', 'inactivo'),
(72, '12g152i873u9838w5761z72', '2019-12-04 17:11:20', '0000-00-00', 'coochofall', '6565656e', '', '', '', '', '', '0000-00-00', '0000-00-00', 'poo256', '2016', 'cherolet', 'buss', 86, '2568984', 'Olfer  Angarita', 2147483647, '454564654', '2025-10-16', '-', '-', '-', '0000-00-00', 'procesando'),
(89, '14p846h6163d3120z3638x86', '2019-12-05 16:23:07', '0000-00-00', 'coochofall', '6565656e', '', '', '', '', '', '0000-00-00', '0000-00-00', 'veo256', '2016', 'cherolet', 'buss', 86, '2568984', 'diana', 1143122971, '123456789', '2019-11-29', '-', '-', '-', '0000-00-00', 'activo'),
(108, '208-37o557v8938p3871a8472p35', '2020-01-08 12:30:20', '2020-01-23', 'coochofall', '6565656e', '1545455456', 'Jesus peña', 'cubrir rutas faltante', ' cincuvalar, calle 30 ', '565644', '2020-01-08', '2020-02-01', 'cat215', '2019', 'bmw', 'van', 605, '2568984', 'dayana cepeda', 11223344, '1042356841', '2020-01-04', '-', '-', '-', '0000-00-00', 'activo'),
(107, '30i232d5097n3262y8417p45', '2020-01-08 11:55:31', '0000-00-00', 'coochofall', '6565656e', '1545455456', 'Jesus peña', 'cubrir rutas faltante', ' cincuvalar, calle 30 ', '565644', '2020-01-08', '2020-02-01', 'cat215', '2019', 'bmw', 'van', 605, '2568984', 'dayana cepeda', 11223344, '1042356841', '2020-01-04', '-', '-', '-', '0000-00-00', 'activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `idEmpresa` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `usuario` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `password` longtext COLLATE utf8_spanish_ci NOT NULL,
  `documento` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `rangoUsuario` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `foto_usuario` longtext COLLATE utf8_spanish_ci NOT NULL,
  `estado_usuario` char(8) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `idEmpresa`, `usuario`, `password`, `documento`, `rangoUsuario`, `email`, `foto_usuario`, `estado_usuario`) VALUES
(54, '6565656e', 'Jesus97', 'e62df4bf6d4cebb81bf5499d3153801e', '114312296', 'usuarios', 'jd1990@hotmail.es', 'Documentos/6565656e/USUARIO/IMG-6565656e/FOTO-114312296/usuario.jpg', ''),
(55, '6565656e', 'Jesus98', 'e62df4bf6d4cebb81bf5499d3153801e', '114312297', 'usuarios', 'jd1990@hotmail.es', 'Documentos/6565656e/USUARIO/IMG-6565656e/FOTO-114312297/usuario.jpg', ''),
(56, '10423501260', 'jesus500', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122971', 'administrador', 'jd1990@hotmail.es', '', 'activo'),
(44, '6565656e', 'Jesus90', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122972', 'administrador', 'jd1990@hotmail.es', 'Documentos/6565656e/ADMINISTRADOR/IMG-6565656e/FOTO-1143122972/54514322.jpg', ''),
(45, '6565656e', 'Jesus90', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122973', 'usuarios', 'jd1990@hotmail.es', 'Documentos/6565656e/USUARIO/IMG-6565656e/FOTO-1143122973/perfil.jpg', ''),
(46, '6565656e', 'Jesus91', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122980', 'usuarios', 'jd1990@hotmail.es', 'Documentos/6565656e/USUARIO/IMG-6565656e/FOTO-1143122980/usuario.jpg', ''),
(47, '6565656e', 'Jesus92', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122981', 'usuarios', 'jd1990@hotmail.es', 'Documentos/6565656e/USUARIO/IMG-6565656e/FOTO-1143122981/usuario.jpg', ''),
(49, '6565656e', 'Jesus93', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122982', 'usuarios', 'jd1990@hotmail.es', 'Documentos/6565656e/USUARIO/IMG-6565656e/FOTO-1143122982/usuario.jpg', ''),
(50, '6565656e', 'Jesus94', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122983', 'usuarios', 'jd1990@hotmail.es', 'Documentos/6565656e/USUARIO/IMG-6565656e/FOTO-1143122983/usuario.jpg', ''),
(51, '6565656e', 'Jesus95', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122984', 'usuarios', 'jd1990@hotmail.es', 'Documentos/6565656e/USUARIO/IMG-6565656e/FOTO-1143122984/usuario.jpg', ''),
(52, '6565656e', 'Jesus96', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122985', 'usuarios', 'jd1990@hotmail.es', 'Documentos/6565656e/USUARIO/IMG-6565656e/FOTO-1143122985/usuario.jpg', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_conductores`
--

CREATE TABLE `usuario_conductores` (
  `id` int(11) NOT NULL,
  `idEmpresa` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `usuario` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `password` longtext COLLATE utf8_spanish_ci NOT NULL,
  `documento` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `rangoUsuario` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `foto_usuario` longtext COLLATE utf8_spanish_ci NOT NULL,
  `estado_Conductor` char(8) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario_conductores`
--

INSERT INTO `usuario_conductores` (`id`, `idEmpresa`, `usuario`, `password`, `documento`, `rangoUsuario`, `email`, `foto_usuario`, `estado_Conductor`) VALUES
(29, '6565656e', 'Jesus81', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122100', 'conductor', 'jd1990@hotmail.es', 'Documentos/6565656e/CONDUCTOR/IMG-6565656e/FOTO-1143122100/conductor.png', ''),
(30, '6565656e', 'Jesus82', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122101', 'conductor', 'jd1990@hotmail.es', 'Documentos/6565656e/CONDUCTOR/IMG-6565656e/FOTO-1143122101/conductor.png', ''),
(31, '6565656e', 'Jesus83', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122102', 'conductor', 'jd1990@hotmail.es', 'Documentos/6565656e/CONDUCTOR/IMG-6565656e/FOTO-1143122102/conductor.png', ''),
(32, '6565656e', 'Jesus84', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122103', 'conductor', 'jd1990@hotmail.es', 'Documentos/6565656e/CONDUCTOR/IMG-6565656e/FOTO-1143122103/conductor.png', ''),
(33, '6565656e', 'Jesus85', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122104', 'conductor', 'jd1990@hotmail.es', 'Documentos/6565656e/CONDUCTOR/IMG-6565656e/FOTO-1143122104/conductor.png', ''),
(34, '6565656e', 'Jesus86', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122105', 'conductor', 'jd1990@hotmail.es', 'Documentos/6565656e/CONDUCTOR/IMG-6565656e/FOTO-1143122105/conductor.png', ''),
(35, '6565656e', 'Jesus87', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122106', 'conductor', 'jd1990@hotmail.es', 'Documentos/6565656e/CONDUCTOR/IMG-6565656e/FOTO-1143122106/conductor.png', ''),
(37, '6565656e', 'Jesus87', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122107', 'conductor', 'jd1990@hotmail.es', 'Documentos/6565656e/CONDUCTOR/IMG-6565656e/FOTO-1143122107/conductor.png', ''),
(38, '6565656e', 'Jesus88', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122108', 'conductor', 'jd1990@hotmail.es', 'Documentos/6565656e/CONDUCTOR/IMG-6565656e/FOTO-1143122108/conductor.png', ''),
(39, '6565656e', 'Jesus89', 'e62df4bf6d4cebb81bf5499d3153801e', '1143122109', 'conductor', 'jd1990@hotmail.es', 'Documentos/6565656e/CONDUCTOR/IMG-6565656e/FOTO-1143122109/conductor.png', ''),
(40, '10423501260', 'Carlos90', 'e62df4bf6d4cebb81bf5499d3153801e', '104210', 'conductor', 'Carlos90@hotmail.es', 'Documentos/10423501260/CONDUCTOR/IMG-10423501260/FOTO-104210/conductor.png', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculos`
--

CREATE TABLE `vehiculos` (
  `id` int(11) NOT NULL,
  `idempresa` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `placa` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `modelo` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  `marca` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `clase` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `numerovehiculo` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `tarjetaoperacion` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `propio` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `numerocontrato` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `vehiculos`
--

INSERT INTO `vehiculos` (`id`, `idempresa`, `placa`, `modelo`, `marca`, `clase`, `numerovehiculo`, `tarjetaoperacion`, `estado`, `propio`, `numerocontrato`) VALUES
(24, '6565656e', 'cat215', '2019', 'bmw', 'van', '605', '2568984', 'activo', 'no', '1545455456'),
(23, '6565656e', 'veo256', '2020', 'toyota', 'bus', '502', '2568984', 'inactivo', 'si', ''),
(4, '10423501260', 'veo257', '2016', 'cherolet', 'buss', '086', '2568984', 'inactivo', 'si', ''),
(5, '10423501260', 'veo258', '2016', 'cherolet', 'buss', '086', '2568984', 'inactivo', 'no', '1545455999'),
(6, '10423501260', 'veo259', '2016', 'cherolet', 'buss', '086', '2568984', 'inactivo', 'si', ''),
(9, '10423501260', 'veo356', '2016', 'cherolet', 'buss', '086', '2568984', 'inactivo', 'no', '1545455999');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alistamiento`
--
ALTER TABLE `alistamiento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idempresa` (`idempresa`);

--
-- Indices de la tabla `conductores`
--
ALTER TABLE `conductores`
  ADD PRIMARY KEY (`Cedulaconductor`),
  ADD KEY `idempresa` (`idempresa`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `contratosempresa`
--
ALTER TABLE `contratosempresa`
  ADD PRIMARY KEY (`numerocontrato`),
  ADD KEY `idempresa` (`idempresa`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `documentos_vehiculos`
--
ALTER TABLE `documentos_vehiculos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `placa` (`placa`);

--
-- Indices de la tabla `documento_conductor`
--
ALTER TABLE `documento_conductor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documento_conductor` (`documento_conductore`);

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`nit`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `fotos_vehiculos`
--
ALTER TABLE `fotos_vehiculos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `placa` (`placa`);

--
-- Indices de la tabla `fuec`
--
ALTER TABLE `fuec`
  ADD PRIMARY KEY (`idfuec`),
  ADD KEY `id` (`id`),
  ADD KEY `nitEmpresa` (`nitEmpresa`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`documento`),
  ADD KEY `idEmpresa` (`idEmpresa`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `usuario_conductores`
--
ALTER TABLE `usuario_conductores`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `documento` (`documento`);

--
-- Indices de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD PRIMARY KEY (`placa`),
  ADD KEY `id` (`id`,`idempresa`),
  ADD KEY `idempresa` (`idempresa`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alistamiento`
--
ALTER TABLE `alistamiento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `conductores`
--
ALTER TABLE `conductores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `contratosempresa`
--
ALTER TABLE `contratosempresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;

--
-- AUTO_INCREMENT de la tabla `documentos_vehiculos`
--
ALTER TABLE `documentos_vehiculos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `documento_conductor`
--
ALTER TABLE `documento_conductor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `fotos_vehiculos`
--
ALTER TABLE `fotos_vehiculos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT de la tabla `fuec`
--
ALTER TABLE `fuec`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de la tabla `usuario_conductores`
--
ALTER TABLE `usuario_conductores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `conductores`
--
ALTER TABLE `conductores`
  ADD CONSTRAINT `conductores_ibfk_1` FOREIGN KEY (`idempresa`) REFERENCES `empresas` (`nit`);

--
-- Filtros para la tabla `contratosempresa`
--
ALTER TABLE `contratosempresa`
  ADD CONSTRAINT `contratosempresa_ibfk_1` FOREIGN KEY (`idempresa`) REFERENCES `empresas` (`nit`);

--
-- Filtros para la tabla `documentos_vehiculos`
--
ALTER TABLE `documentos_vehiculos`
  ADD CONSTRAINT `documentos_vehiculos_ibfk_1` FOREIGN KEY (`placa`) REFERENCES `vehiculos` (`placa`);

--
-- Filtros para la tabla `fotos_vehiculos`
--
ALTER TABLE `fotos_vehiculos`
  ADD CONSTRAINT `fotos_vehiculos_ibfk_1` FOREIGN KEY (`placa`) REFERENCES `vehiculos` (`placa`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
