<?php
require_once '../../conexion/sessionlogin.php'; 
include '../views/header-menu.php';
?>

<div class="content-wrapper">
 <section class="content">
       <div class="row">

          <!-- /.box -->
        <!-- /.col -->
    <div class="col-md-9">
      <div class="box box-primary">
        <div class="box-header">
        <i class="fa fa-book"></i>
        <h3 class="box-title">Informacion De empresa</h3>
         </div>
          
            <div class="content">
              <!-- /.tab-pane -->
              <div class="tab-pane" id="settings">
                 <form method="post" class="IngresarFuec form-horizontal">
<!--        Aqui muestra la informacion de la empresa-->
                <div class="informacionEmpresa form-group">
                    <label for="inputName" class="control-label">Nombre De la Empresa</label>
                    <div class="col-sm-4">
                    <input type="text" class="razonsocial form-control" name="nombreEmpresa" value="" pattern="" required="" readonly="">
                    </div>
                  <label for="inputName" class="col-sm-4 control-label">Nit De la Empresa</label>
                    <div class="col-sm-4">
                    <input type="text" class="nit form-control" name="nitEmpresa" value="" pattern="" required="" readonly="">
                    </div>
                  </div>

        <!--        Aqui lista los contratos activos o que estan disponible-->
      
           <h3>Informacion Contrato</h3>
        
     <table>   
        
        <tr>   
            <td><label>Contratos </label></td>
            <td class="informacionContrato">  
                 <select name="numeroContrato" style="width: 180px;" class="numerocontrato form-control">
                 <option value="" class=""></option> 
                 </select>
            </td>
        </tr>        
      
    </table>

       

        
<!--        dependiendo el numero del contra mostrara la informacion-->
       
           <h4>Datos Contrato</h4>
        
        
        
        
        <div class="InformacionContratoSelecionado">
            <table>
            <tr>
            <td><label>Nombre Contratante</label></td>
            <td><input type="text" class="nombreContratante form-control" name="nombreContratante" value="" pattern="" readonly=""></td>
            </tr>
            <tr>
            <td><label>Objeto Contrato</label></td>
            <td><input type="text" class="objetocontrato form-control" name="objetocontrato" value="" pattern="" ></td>
            </tr>
            <tr>
            <td><label>Recorido</label></td>
            <td><input type="text" class="recorido form-control" name="recorido" value="" pattern="" ></td> 
            </tr>
            <tr>
            <td><label>Convenio</label></td>
            <td><input type="text" class="conevio form-control" name="conevio" value="" pattern="" ></td> 
            </tr>
            <tr>
            <td><label>Fecha De Inicio Contrato</label></td>
            <td><input type="date" class="fechaInicontrato form-control" name="fechaInicontrato" value="" pattern=""  readonly=""></td>
            </tr>
            <tr>
            <td><label>Fecha De Fin Contrato</label></td>
            <td><input type="date" class="fechaFincontrato form-control" name="fechaFincontrato" value="" pattern="" readonly=""></td>            
            </tr>
            </table> 
            
         </div>
        
        
        
        
        <h3>Informacion Vehiculo</h3>
        <table>
            <td><label>Selecionar placa</label></td>
            <td class="selecionarPlaca">
                <select name="placa" class="placa" style="width: 180px;">
                    <option value=""></option>
                </select>
            </td>
        </table>
        
        <h4>Datos Vehiculo</h4>
        
        <div class="detalleVehiculo">
            <table>
            <tr>
                <td><label>modelovehiculo</label></td>
                <td><input type="text" class="modelovehiculo" name="modelovehiculo" readonly="" value=""></td>
            </tr>
            <tr>
                <td><label>Marca Vehiculo</label></td>
                <td><input type="text" class="marcaVehiculo" name="marcaVehiculo" readonly="" value=""></td>
            </tr>
            <tr>
                <td><label>Clase vehiculo</label></td>
                <td><input type="text" class="clasevehiculo" name="clasevehiculo" readonly="" value=""></td>
            </tr>
            <tr>
                <td><label>Numero Vehiculo</label></td>
                <td><input type="text" class="numInterVehiculo" name="numInterVehiculo" readonly="" value=""></td>
            </tr>
            <tr>
                <td><label>Tarjeta Operacion </label></td>
                <td><input type="text" class="tarjetaOperacion" name="tarjetaOperacion" readonly="" value=""></td>
            </tr>
            </table>
        </div>
        
        <!--HABILITAR Y DESABILITAR FECHA DE VENCIMINETO-->
        <table>
            <tr>
                <td><h4> Fecha de Vencimento</h4></td>
            </tr>
            <tr>
                <td><input type="checkbox" name="" class="fecha_vencimineto_check" ><label> Habilitar</label></td>
            </tr>
            <tr style="visibility: hidden;" class="mostrar_fechaVencimiento">
                <td><input type="date" name="fecha_vencimineto" class="fecha_vencimineto"  min="<?php echo date('Y-m-d'); ?>"> </td>
            </tr>
            <!--CODIGO DEPARTAMENO -->
            <tr>
                <td><h4>Codigo Departamento</h4></td>
            </tr>
            <tr>
                <td>
                    <input list="Codigo" name="Codigo_Departamento" class="Codigo_Departamento" style="text-transform: capitalize;">
                    <datalist id="Codigo" class="Codigo_Departamento_list">
                        
                    </datalist>
                </td>
            </tr>
        </table>
        
        
        
<!--   BUSCAR CONDUCOR-->
        <h3>Buscar Conductores</h3>
        <div>
            <table>
                <tr>
<!--            descuerdo a la letra o nombre que se escrbiba lista el  conductor-->
                <td><input type="text" name="BuscarConductor" class="BuscarConductor" placeholder="" required="" style="width: 250px;" ></td>
                <td>
                <select name="conductordiv" id="conductordiv" class="conductordiv">
                            <option value="1" >1</option>
                            <option value="2" >2</option>
                </select>
                </td>
            </tr>
            </table> 
            
        </div>
<!--        //el nombre del conductor se listara aqui-->
        <div class="respuestaBusquedaConductor" >
            <a href="" id="" class="respuestaBusqueda"></a>

        </div>

        <h3>Conductores</h3>  
<!--        //dentro de este contenedor se colocaran los conductores-->
        <div class="listaConductoresfuec"> </div>
        <table>
            <tr>
                <td><span class="respuestafuec">
                    </span></td>
            </tr>
            
            <tr>
                <!--        si el existe un usurio y deccuerdo al rango podra crear un fuec o crearlo pero pedir autorizacion para imprimir -->
                 <?php if(isset($_SESSION['rangoUsuario']) && $_SESSION['rangoUsuario'] === "usuarios" ||  $_SESSION['rangoUsuario'] === 'administrador' ){?>
                <td><button class="CrearFuecUsuario" > Crear Fuec </button></td>
                    <?php } ?>
         
                <?php if(isset($_SESSION['rangoUsuario']) && $_SESSION['rangoUsuario'] === "conductor" ){?>
                <td><button class="solicitar_Fuec_conductor" > Solicitar Fuec </button></td>
               <?php } ?>
                
                
            </tr>

             </form>
          </div>
       </div>
     </div>
   </div>
 </div>
 </section>
</div>
         

<script type="text/javascript" src="../../jquery/jquery.js"></script>
<script type="text/javascript" src="../../javascriptClases/ValidarCampos.js"></script>
<script type="text/javascript" src="../../javascriptClases/Fuec/Fuec.js"></script>
<script type="text/javascript" src="../../javascriptClases/Ajax.js"></script>

<?php  include '../views/footer.php'; ?>