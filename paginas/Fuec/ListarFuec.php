<?php
require_once '../../conexion/sessionlogin.php'; 
include '../views/header-menu.php';
?>
<div class="content-wrapper">
 <section class="content">
   <div class="box box-primary">
     <div class="box-header">
       <i class="fa fa-edit"></i>
        <h3 class="box-title">Lista de FUECS</h3>
         </div>
       <div class="box-body pad table-responsive">

       <!--esta etiqueta input que sirve para buscar en la bd-->
        <div class="input-group margin col-xs-2">
       <div class="input-group-btn">
         <button type="button" class="btn btn-danger"><i class="fa fa-search"></i></button>
       </div>
    <input type="text" name="BuscarFuec" value="" placeholder="Buscar Veiculo.."  class="BuscarFuec form-control">
</div>
  
<!-- {{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}} -->

 <div class="col-xs-12">     
            <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">

<div class="listartablaFuec">
<table  class="listarFuec">    
</table>
</div>
<a href="" title=""></a>
                       </div>
<!-- {{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}} -->  
<!--    aqui se listan los vehiculos de la empresa-->
                </div>
            </div>
        </div>
    </section>
  </div>   
</div>

<script type="text/javascript" src="../../jquery/jquery.js"></script>
<script type="text/javascript" src="../../javascriptClases/ValidarCampos.js"></script>
<script type="text/javascript" src="../../javascriptClases/Fuec/Fuec.js"></script>
<script type="text/javascript" src="../../javascriptClases/Ajax.js"></script>

<?php  include '../views/footer.php'; ?>