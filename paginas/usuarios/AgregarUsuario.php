<?php require_once '../../conexion/sessionlogin.php'; 
      include '../views/header-menu.php'; ?>

<div class="content-wrapper">
 <section class="content">
       <div class="row">
          <!-- /.box -->
        <!-- /.col -->
        <div class="col-md-10">
          <div class="box box-primary">
        <div class="box-header">
        <i class="fa fa-book"></i>
        <h3 class="box-title">Nuevo Usuario</h3>
         </div>
          
            <div class="content">
              <!-- /.tab-pane -->
              <div class="tab-pane" id="settings">
                <form method="post" class="Formulario_usuario form-horizontal" enctype="multipart/form-data">

               <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Nombre de Usuario</label>
                    <div class="col-sm-10">
                    <input type="text" name="usuario" class="usuario form-control" value="Jesus90" required="">
                    </div>
                  </div>

<!--             <td rowspan="7" >
                <ul class="requerimiento_pass" style="visibility: hidden;">
                    <li>Minimo 8 caracteres</li>
                    <li>Maximo 15</li>
                    <li>Al menos una letra mayúscula</li>
                    <li>Al menos una letra minucula</li>
                    <li>Al menos un dígito</li>
                    <li>No espacios en blanco</li>
                    <li>Al menos 1 caracter especial</li>
                </ul>
            </td> -->

               <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Contraseña de Usuario</label>
                    <div class="col-sm-10">
                    <input type="password" name="password" class="usuario_password form-control" value="Jesus90*">
                    </div>
                 </div>


                   <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Contraseña </label>
                    <div class="col-sm-10">
                    <input type="password" name="password" class="usuario_password form-control" value="Jesus90*">
                    </div>
                 </div>
                   <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Repetir Contraseña</label>
                    <div class="col-sm-10">
                    <input type="password" name="password_2" class="usuario_password_2 form-control" value="Jesus90*">
                    </div>
                 </div>
                   <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Documento</label>
                    <div class="col-sm-10">
                    <input type="number" name="numero" class="usuario_numero form-control" value="1143122971" >
                    </div>
                 </div>


                   <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                    <input type="email" name="email" class="usuario_email form-control" value="jd1990@hotmail.es"  autocomplete="off" placeholder>
                    </div>
                 </div>


                   <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Foto perfil</label>
                    <div class="col-sm-10">
                    <input type="file" name="perfil_file" class="usuario_perfil_file form-control" value="" autocomplete="off" accept="image/png,image/jpeg">
                    </div>
                 </div>
 
                 </div>
                   <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Rango de Usuario</label>
                    <div class="col-sm-10">
                    <select name="rango" class="usuario_rango form-control">
                    <option value="">Selecionar</option>
                    <option value="administrador">Administrador</option>
                    <option value="usuarios">usuario</option>
                    <option value="conductor">Conductor</option>
                    </select>
                    </div>
                 </div>
                <br>
                <br>
                <br>



           <button class="usuario_guardar btn btn-success" >Guardar</button>
       
       
            <td colspan="3"><span class="usuario_respuesta"></span></td>
   
            </form>
          </div>
       </div>
     </div>
   </div>
 </section>
</div>

<script type="text/javascript" src="../../jquery/jquery.js"></script>
<script type="text/javascript" src="../../javascriptClases/ValidarCampos.js"></script>
<script type="text/javascript" src="../../javascriptClases/usuario/usuarios.js"></script>

<?php  include '../views/footer.php'; ?>

