<?php require_once '../../Ejecutar-clases/Usuarios/ListarUsuario.php';?>
<?php if(isset($_SESSION['rangoUsuario']) &&  $_SESSION['rangoUsuario'] == 'administrador'){ ?>
<!--TABLA PARA LISTAR USUARIOS y ADMINISTRADORES-->
<?php
require_once '../../conexion/sessionlogin.php'; 
include '../views/header-menu.php';
?>
<div class="content-wrapper">
 <section class="content">
   <div class="box box-primary">
     <div class="box-header">
       <i class="fa fa-edit"></i>
        <h3 class="box-title">Tabla Usuarios</h3>
         </div>
       <div class="box-body pad table-responsive">

    <div class="col-xs-12">     
            <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
<table class="table table-hover">
    <tbody>
           
    <tr>
        <th>Nombre Usuario</th>
        <th>Numero Documento</th>
        <th>Perfil</th>
        <th>Email</th>
        <th>Foto</th>
        <th>Estado</th>
        <th>Eliminar</th>
        <th></th>     
    </tr>
       
    <?php foreach ($Usuario as $row) {?>
    <tr class="tabla_usuario" >
        <td><?php echo $row['usuario']; ?></td>
        <td><?php echo $row['documento'];?></td>
        <td><?php echo $row['rangoUsuario'];?></td>
        <td><?php echo $row['email'];?></td>
        <td><a href="<?php echo "../../".$row['foto_usuario'];?>" class="VerFotoUsuario"  >Ver</a></td>
        <td><?php echo $row['estado_usuario'];?></td>
        <td></td>
        <td></td>
        
    </tr>
    <?php }?>
   
    
    <tr>
        <td colspan="8">
            <?php 
            //calculando el numero de registro
                $Nregistros = $listarUsuario->contar_registro(); 
                for($pagina = 1;$pagina< ceil($Nregistros/5);$pagina++){
                        //de aqui se obtiene el numero de pagina se envia atravez de get
                       echo '<a href="?NpaginaUsuario='.$pagina.'" class="NpaginaUsuario" > '.$pagina.' </a>';
                        
                }
            ?> 
            
        </td>
    </tr>

    <div>
    <img src="../../img/usuario.svg" class="MostrarFotoUsuario" style="width: 10%; height: 10%; ">
</div> 
                      </tbody>
                     </table>
                   </div>
<!-- {{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}} -->  
<!--    aqui se listan los vehiculos de la empresa-->
                </div>
            </div>
        </div>
<?php } ?>

<!-- {{{{{{{{{{{{{{{{}}}}}}}}}}}}}}}} -->
 <div class="box box-primary">
     <div class="box-header">
       <i class="fa fa-edit"></i>
        <h3 class="box-title">Tabla Usuarios Conductores</h3>
         </div>
       <div class="box-body pad table-responsive">

    <div class="col-xs-12">     
            <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
<table class="table table-hover">
    <tbody>
<!-- {{{{{{{{{{{{{{{{}}}}}}}}}}}}}}}} -->


    
    <tr>
        
        <th>Nombre Usuario</th>
        <th>Numero Documento</th>
        <th>Perfil</th>
        <th>Email</th>
        <th>Foto</th>
        <th>Estado</th>
        <th>Eliminar</th>
        <th></th>
        
        
    </tr>
    <?php foreach ($Conductor as $row) {?>
    <tr>
        <td><?php echo $row['usuario']; ?></td>
        <td><?php echo $row['documento'];?></td>
        <td><?php echo $row['rangoUsuario'];?></td>
        <td><?php echo $row['email'];?></td>
        <td><a href="<?php echo "../../".$row['foto_usuario'];?>" class="VerFotoUsuario"  >Ver</a></td>
        <td><?php echo $row['estado_Conductor'];?></td>
        <td></td>
        <td></td>
        
    </tr>
    <?php }?>
    
    <tr>
        <td colspan="8">
            <?php 
            //calculando el numero de registro
                $NregistrosConductor = $listarConductor->contar_registro(); 
                for($pagina = 1;$pagina< ceil($NregistrosConductor/5);$pagina++){
                        //de aqui se obtiene el numero de pagina se envia atravez de get
                       echo '<a href="?NpaginaConductor='.$pagina.'" class="NpaginaUsuario" > '.$pagina.' </a>';
                        
                } ?> 
            
        </td>
    </tr>


<div>
    <img src="../../img/usuario.svg" class="MostrarFotoUsuario" style="width: 10%; height: 10%; ">
</div>    


<!-- {{{{{{{{{{{{{{{{{{{{{{{{{{{{{{ -->  
                      </tbody>
                     </table>
                   </div>
                </div>
            </div>
        </div>

    </section>
  </div>   
</div>


<script type="text/javascript" src="../../jquery/jquery.js"></script>
<script type="text/javascript" src="../../javascriptClases/usuario/usuarios.js"></script>
 
 <?php  include '../views/footer.php'; ?>