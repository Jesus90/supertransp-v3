<?php
require_once '../../conexion/sessionlogin.php'; 
include '../views/header-menu.php';
?>


<div class="content-wrapper">
 <section class="content">
    
          



<div class="box box-primary">
     <div class="box-header">
       <i class="fa fa-edit"></i>
        <h3 class="box-title">Nueva Empresa</h3>
         </div>
       <div class="box-body pad table-responsive">
         <form method="post" id="ingresar-empresa" class="form-horizontal">


                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Nombre de la Empresa</label>
                    <div class="col-sm-10">
                    <input type="text" name="razonsocial" class="nombre-empresa form-control" placeholder="Nombre empresa" value="" size="50">
                  </div>
                </div>
            
                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Nit de la Empresa</label>
                    <div class="col-sm-10">
                    <input type="text" name="nit" class="nit-empresa form-control" placeholder="Nit de la Empresa Termnar con N" value="" size="50">
                  </div>
                </div>
            
                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Email Empresa</label>
                    <div class="col-sm-10">
                    <input type="text" name="email" class="email-empresa form-control" placeholder="Correo de la empresa" value=""  size="50">
                  </div>
                </div>
            
              
               <input type="button" class="ingresar-empresa-button btn btn-success" value="Crear">
               
             
                 <span class="respuesta_crearEmpresa"></span>
              
           </form>
       </div>
     </div>
 </section>
</div>
        

        
<!--        archivos necesarios para ejecutar el ajax-->
        <script type="text/javascript" src="../../jquery/jquery.js"></script>
        <script src="../../javascriptClases/ValidarCampos.js" type="text/javascript" ></script>
        <script src="../../javascriptClases/empresa/Empresa.js" type="text/javascript" ></script>
        <script src="../../javascriptClases/Ajax.js" type="text/javascript"></script>
        

<?php  include '../views/footer.php'; ?>
