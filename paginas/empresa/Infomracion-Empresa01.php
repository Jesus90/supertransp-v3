<?php
include '../views/header-menu.php';
?>


 <div class="content-wrapper">
<section class="content-fluid">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile informacion-Empresa" >
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <img src="" class="firma profile-user-img img-responsive img-circle" width="150" height="auto">
                  <h3 class="profile-username text-center">Firma</h3>
                </li>
                 <li class="list-group-item">
                  <img class="logotranscito profile-user-img img-responsive img-circle" src="" width="150" height="auto">
                  <h3 class="profile-username text-center">Logo Transcito </h3>
                </li>
                <li class="list-group-item">
                  <img class="logoempresa profile-user-img img-responsive img-circle" src="" width="150" height="auto">
                  <h3 class="profile-username text-center">Ciudad</h3>
                </li>
              </ul>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- /.box -->
        </div>
        <!-- /.col -->

        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

  </div>

<?php  include '../views/footer.php'; ?>
