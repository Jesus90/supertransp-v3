<?php 
require_once '../../conexion/sessionlogin.php'; 
include '../views/header-menu.php';?>
<script type="text/javascript" src="../../jquery/jquery.js"></script>
<script src="../../javascriptClases/ValidarCampos.js" type="text/javascript" ></script>
<script src="../../javascriptClases/empresa/Empresa.js" type="text/javascript" ></script>
<script src="../../javascriptClases/Ajax.js" type="text/javascript"></script> 
<div class="content-wrapper">
  
<section class="content">

       <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile" >
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <img src="" class="firma profile-user-img img-responsive img-circle" width="150" height="auto">
                  <h3 class="profile-username text-center">Firma</h3>
                </li>
                 <li class="list-group-item">
                  <img class="logotranscito profile-user-img img-responsive img-circle" src="" width="150" height="auto">
                  <h3 class="profile-username text-center">Logo Transcito </h3>
                </li>
                <li class="list-group-item">
                  <img class="logoempresa profile-user-img img-responsive img-circle" src="" width="150" height="auto">
                  <h3 class="profile-username text-center">Logo Empresa</h3>
                </li>
              </ul>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- /.box -->
        </div>
          <!-- /.box -->
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
             <li class="active"><a href="#settings">Datos De Empresa</a></li>
              </ul>
            <div class="content">
              <!-- /.tab-pane -->
              <div class="tab-pane" id="settings">
                <form id="informacion-Empresa"  enctype="multipart/form-data" class="form-horizontal">
          
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Razon Social</label>
                    <div class="col-sm-10">
                    <input type="text" name="razonsocial" value="" placeholder="" class="razonsocial form-control">
                    </div>
                  </div>
                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label"> Nit</label>
                    <div class="col-sm-10">
                    <td><input type="text" name="nit" value="" placeholder="" class="nit form-control">
                   </div>
                  </div>      
                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Representante</label>
                    <div class="col-sm-10">
                    <input type="text" name="representante" value="" placeholder="" class="representante form-control">
                  </div>
                </div>
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Cedula</label>
                    <div class="col-sm-10">
                    <input type="number" name="cedula" value="" placeholder="" class="cedula form-control">
                  </div>
                </div>
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Telefono</label>
                    <div class="col-sm-10">
                    <input type="number" name="telefono" value="" placeholder="" class="telefono form-control">
                  </div>
                </div>
                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Dirección</label>
                    <div class="col-sm-10">
                    <input type="text" name="direccion" value="" placeholder="" class="direccion form-control">
                  </div>
                </div>
                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label"> movil</label>
                    <div class="col-sm-10">
                    <input type="number" name="movil" value="" placeholder="" class="movil form-control">
                  </div>
                </div>
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label"> Email</label>
                    <div class="col-sm-10">
                     <input type="email" name="email" value="" placeholder="" class="email form-control">
                  </div>
                </div>
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Ciudad</label>
                    <div class="col-sm-10">
                    <input type="text" name="ciudad" value="" placeholder="" class="ciudad form-control">
                  </div>
                </div>
                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Firma</label>
                    <div class="col-sm-10">
                      <label for="introinput"> <i class="fa fa-upload"></i>Subir Imagen de Firma</label>
                     <input type="file" id="introinput" name="firma" value="" placeholder="" accept="image/*" class="firma form-control">
                  </div>
                </div>
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Logo Transcito</label>
                    <div class="col-sm-10">
                      <label for="introinput2"> <i class="fa fa-upload"></i> Subir Logo Transcito</label>
                     <input type="file" id="introinput2" name="logotranscito" value="" placeholder="" accept="image/*" class="logotranscito form-control">
                  </div>
                </div>
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Logo Empresa</label>
                    <div class="col-sm-10">
                    <label for="introinput3"> <i class="fa fa-upload"></i> Subir Logo De Empresa</label><input type="file" id="introinput3" name="logoempresa" value="" placeholder="" accept="image/*" class="logoempresa form-control">
                  </div>
                </div>
           
           <span class="Repueta_actulizarEmpresa"></span>
           

                <input type="button" value="Actulizar Informacion" class="btn btn-block btn-success" id="actulizar-informacion-empresa">

         
         
             </form>
          </div>
       </div>
     </div>
   </div>
</section>
</div>

<?php  include '../views/footer.php'; ?>
