<?php
require_once '../../conexion/sessionlogin.php'; 
include '../views/header-menu.php';
?>


<div class="content-wrapper">
 <section class="content">
       <div class="row">

          <!-- /.box -->
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
        <div class="box-header">
        <i class="fa fa-book"></i>
        <h3 class="box-title">Nueva Vehiculo</h3>
         </div>
          
            <div class="content">
              <!-- /.tab-pane -->
              <div class="tab-pane" id="settings">
                 <form method="post" class="Formulario_ingresar_vehiculo form-horizontal">
        

                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Placa</label>
                    <div class="col-sm-10">
                    <input type="text" name="placa" class="placa form-control" size="30" autocomplete="off" value="" placeholder="Ingrese numero de placa">
                    </div>
                  </div>
               
   
                    <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Modelo</label>
                    <div class="col-sm-10">
                    <input type="text" name="modelo" class="modelo form-control" size="30" autocomplete="off" value="" placeholder="Ingrese Modelo">
                    </div>
                  </div>
                  
                   <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Marca</label>
                    <div class="col-sm-10">
                    <input type="text" name="marca" class="marca form-control" size="30" autocomplete="off" value="" placeholder="Ingrese Marca">
                    </div>
                  </div>
               
   
                   <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Clase</label>
                    <div class="col-sm-10">
                    <input type="text" name="clase" class="clase form-control" size="30" autocomplete="off" value="" placeholder="Ingrese clase">
                    </div>
                  </div>
               
   
                   <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Numero Del Vehiculo</label>
                    <div class="col-sm-10">
                    <input type="text" name="numerovehiculo" class="numerovehiculo form-control" size="30" autocomplete="off" value="" placeholder="Numero Del Vehiculo">
                    </div>
                  </div>

   
                   <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Tarjeta De Operacion</label>
                    <div class="col-sm-10">
                    <input type="text" class="tarjetaoperacion form-control" value="2568984" name="tarjetaoperacion" placeholder="Ingrese tarjeta De Operacion" size="30">
                    </div>
                  </div>

                <!--            <tr>
                <td><label>Estado </label></td>
                <td><select name="estado" style="width: 100px;" class="estado" >
                    <option value="activo">Activo</option> 
                    <option value="Inactivo">Inactivo</option>
                    </select></td>
                 </tr>-->


                   <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Propio</label>
                    <div class="col-sm-10">
                     <select name="propio" class="propio ">
                        <option value="si">Si</option> 
                        <option value="no">No</option>
                         </select>
                    </div>
                  </div>

                <div class="ocultarContrato form-group">
                    <label for="inputName" class="col-sm-2 control-label">Numero Contrato</label>
                    <div class="listarContraActivo col-sm-10">
                     <select name="numerocontrato" class="numerocontratos">
                         <option value="" class=""></option> 
                         </select>
                    </div>
                  </div>

                <span style="color: green;" class="Respuesta_ingresar_vehiculo"></span><br>

            <button value="" class="ingresarVehiculo btn btn-success">Ingresar Vehiculo</button>

             </form>
          </div>
       </div>
     </div>
   </div>
 </section>
</div>


<script type="text/javascript" src="../../jquery/jquery.js"></script>
<script type="text/javascript" src="../../javascriptClases/Ajax.js"></script>
<script type="text/javascript" src="../../javascriptClases/ValidarCampos.js"></script>
<script type="text/javascript" src="../../javascriptClases/Vehiculo/vehiculo.js"></script>


<?php  include '../views/footer.php'; ?>