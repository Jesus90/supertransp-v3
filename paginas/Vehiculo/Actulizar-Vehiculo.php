<?php require_once '../../Ejecutar-clases/Vehiculo/BuscarVehiculo.php'; ?>

<?php
require_once '../../conexion/sessionlogin.php'; 
include '../views/header-popup.php';
?>

<div class="content-wrapper">
 <section class="content">
  
<div class="box box-primary">
     <div class="box-header">
       <i class="fa fa-edit"></i>
        <h3 class="box-title">Nueva Empresa</h3>
         </div>
       <div class="box-body pad table-responsive">

<div class="formularioActulizarVehiculo">
   <?php  foreach ($ver as $row){ ?>
<!--    Aqui se muestra el formulario para actulizar un vehiculo-->
    <form method="post" class="formulario_actulizar_vehiculo" class="form-horizontal">
        <table>
        <tr>
            <td><label>Placa</label></td>
            <td> <input type="text" class="placaACT" value="<?php echo $row['placa']; ?>" name="placa" placeholder="Ingrese Placa" size="30"  readonly=""><?php echo cancelar("../../", "")  ?></td>
        </tr>
        <tr>
            <td><label>Modelo</label></td>
            <td> <input type="text" class="modeloACT" value="<?php echo  $row['modelo']; ?>" name="modelo" placeholder="Ingrese Modelo" size="30" readonly=""><?php echo cancelar("../../", "")  ?></td>
        </tr>
        <tr>
            <td><label>Marca </label></td>
            <td><input type="text" class="marcaACT" value="<?Php echo  $row['marca']; ?>" name="marca" placeholder="Ingrese  Marca" size="30" readonly=""><?php echo cancelar("../../", "")  ?></td>
        </tr>
        <tr>
            <td><label>Clase </label></td>
            <td><input type="text" class="claseACT" value="<?php echo $row['clase']; ?>" name="clase" placeholder="Ingrese Clase" size="30"></td>
        </tr>
        <tr>
            <td><label>Numero Del Vehiculo </label></td>
            <td><input type="text" class="numerovehiculoACT" value="<?php echo $row['numerovehiculo']; ?>" name="numerovehiculo" placeholder="Ingrese Numero Del Vehiculo" size="30" readonly=""><?php echo cancelar("../../", "")  ?></td>
        </tr>
        <tr>
            <td><label>Tarjeta De Operacion </label></td>
            <td><input type="text" class="tarjetaoperacionACT" value="<?php echo $row['tarjetaoperacion']; ?>" name="tarjetaoperacion" placeholder="Ingrese tarjeta De Operacion" size="30"></td>
        </tr>
<!--        <tr>
            <td><label>Estado : </label></td> 
            <td><span> </span></td>
        </tr>-->
<!--        <tr>
            <td colspan="2">
            <select name="estado" style="width: 150px;" class="estadoACT"  >
             <option value=""></option>
            <option value="activo">Activo</option> 
            <option value="Inactivo">Inactivo</option>
            </select>
            </td>
        </tr>-->
         <tr>
            <td><label>Propio: </label>
            </td><br>
            <td><span style="color: green;"><?php echo  $row['propio']; ?></span></td>
            
        </tr>
        <tr><td colspan="2">
            <select name="propio" style="width: 150px;" class="propio propioACT">

                <option value="<?php echo  $row['propio']; ?>"></option>
                <option value="si">Si</option> 
                <option value="no">No</option>
            </select></td>
        </tr>
        <tr>
        
         <div class="ocultarContrato" style="display: none;">   
             <td><label>Numero Contrato: </label></td>
             <td><span class="contratoActul"> <?php echo $row['numerocontrato']; ?></span></td>
            
        </div>
        </tr>
        <tr>
        <td colspan="2">    
        <div class="listarContraActivo">  
        <select name="numerocontrato" style="width: 180px;" class="numerocontratos">
        <option value="" class=""></option> 
        </select>
        </div>
        </td>
        </tr>
        <tr>
            <td>
<br>
                <span class="Respuesta_actlizar_vehiculo"></span>  </td>
        </tr>
        <tr>
            <td><button  class="actulizaVehiculo btn btn-success">Actulizar Vehiculo</button></td>
        </tr>
        
        </table>
            </form>
           
   <?php  }?>
   
</div>
</div></div></section></div>


<script type="text/javascript" src="../../jquery/jquery.js"></script>
<script type="text/javascript" src="../../javascriptClases/Ajax.js"></script>
<script type="text/javascript" src="../../javascriptClases/ValidarCampos.js"></script>
<script type="text/javascript" src="../../javascriptClases/Vehiculo/vehiculo.js"></script>


<?php  include '../views/footer-popup.php'; ?>