<?php 
      require_once '../../../Ejecutar-clases/Vehiculo/Documentos-Vehiculo-Controlador/ListarDocumentosVehiculo.php'; 
      require_once '../../../clases/Config.php';
      ?>
<?php
require_once '../../../conexion/sessionlogin.php'; 
include '../../views/header-popup.php';
?>

<div class="content-wrapper">
 <section class="content">
  
<div class="box box-primary">
     <div class="box-header">
       <i class="fa fa-edit"></i>
        <h3 class="box-title">Nueva Empresa</h3>
         </div>
       <div class="box-body pad table-responsive">
         <form method="post" id="ingresar-empresa" class="form-horizontal">


  <table>
      
      
    <tr>
        <td colspan="8" ><h2>Placa : <?php echo strtoupper($_GET['placa']) ?></h2></td>      
    </tr>
    <input type="hidden" name="placa" class="placa" value="<?php echo strtoupper($_GET['placa']) ?>" >
   
        
        <?php 
        //siglo foreach que se encargara de mostrar lo documento subidos
                
                foreach($ver as $row){ 
        ?> 
    
    <tr>
          <th>Documento</th>
          <th>Subir </th>
          <th>Fecha Expedicion</th>
          <th></th>
          <th>Remplazar</th>
          <th>D Actual</th>
          <th>Estado</th>
          <th></th>
      </tr>
    
    <tr><!--SOAT-->
   
        <!--   Nombre del documento a Subir-->
        <td><label for="soat">Soat :</label></td>        
        <!--   Archivo a subir-->
        <td ><input type="file" class="soat" name="soat" value="" accept="application/pdf" disabled="" ></td>
        <!--   Fecha de Vencimento Dococumento -->
        <td><input type="date" class="soat-fecha" name="soat-fecha" value="<?php echo $row['fecha_veci_soat'] ?>" readonly min="<?php echo   date("Y") ."-". date("m") ."-". date("d"); ?>"  ></td>
        <!--   Boton para enviar Documento -->
        <td><button class="soat-enviar" style="display: none;" >Guardar</button></td>
        <!--   Visulizar el archivo que esta montando   -->
        <td><a href="" title="" class="Remplzar_soat" ><img src="../../../img/iconos/reemplazar.svg" width="28" > </a> </td>
        <td><?php if(isset($row['soat']) && $row['soat'] !=""  ){ ?>
            <a  href="<?php echo "../../../".$row['soat']; ?>" id="Pdf_soat"  ><?php echo  IconPDF("../../../");  ?></a> <?php  }?></td>
        <!--   Estado del documento Actual -->
        <td><?php echo Semaforo("../../../","",$row['estado_soat']) ?> </td>
        <!--   Respuesta del documento que se a subido -->
        <td><span class="soat-respuesta" style="display: none;"></span></td>
       
    </tr>
    
    
    <tr><!--TECNICO MECANICA -->
        <!--   Nombre del documento a Subir-->
        <td><label for="tecnicomecanica">Tecnico Mecanica :</label></td>        
        <!--   Archivo a subir-->
        <td ><input type="file" class="tecnicomecanica" name="tecnicomecanica" value="" accept="application/pdf" disabled="" ></td>
        <!--   Fecha de Vencimento Dococumento -->
        <td><input type="date" class="tecnicomecanica_fecha" name="tecnicomecanica_fecha" value="<?php echo $row['fecha_veci_tecnomecanica'] ?>" readonly  min="<?php echo   date("Y") ."-". date("m") ."-". date("d"); ?>" ></td>
        <!--   Boton para enviar Documento -->
        <td><button class="tecnicomecanica_enviar" style="display: none;" >Guardar</button></td>
        <!--   Visulizar el archivo que esta montando   -->
        <td><a href="" title=""  class="Remplzar_tecnicomecanica"  ><img src="../../../img/iconos/reemplazar.svg" width="28" > </a></td>
        <td><?php if(isset($row['tecnicomecanica']) && $row['tecnicomecanica'] != ""  )  {?>
            <a href="<?php echo "../../../".$row['tecnicomecanica'] ?>"  id="Pdf_tecnicomecanica"><?php echo  IconPDF("../../../") ?></a><?php } ?></td>
        <!--   Estado del documento Actual -->
        <td><?php echo Semaforo("../../../","",$row['estado_tecnicomecanica']) ?></td>
        <!--   Respuesta del documento que se a subido -->
        <td><span class="tecnicomecanica_respuesta" style="display: none;"></span></td>
    </tr>
    
    
    <tr><!--MANTENIMENTO-->
        <!--   Nombre del documento a Subir-->
        <td><label for="mantenimento">Mantenimento :</label></td>        
        <!--   Archivo a subir-->
        <td ><input type="file" class="mantenimento" name="mantenimento" value="" accept="application/pdf" disabled="" ></td>
        <!--   Fecha de Vencimento Dococumento -->
        <td><input type="date" class="mantenimento_fecha" name="mantenimentot_fecha" value="<?php echo $row['fecha_veci_mantenimento'] ?>"  readonly min="<?php echo   date("Y") ."-". date("m") ."-". date("d"); ?>" ></td>
        <!--   Boton para enviar Documento -->
        <td><button class="mantenimento_enviar" style="display: none;">Guardar</button></td>
        <!--   Visulizar el archivo que esta montando   -->
        <td><a href="" title="" class="Remplzar_mantenimento" ><img src="../../../img/iconos/reemplazar.svg" width="28" > </a></td>
        <td><?php if(isset($row['mantenimento']) && $row['mantenimento'] != ""  )  {?>
            <a href="<?php echo "../../../".$row['mantenimento'] ?>" id="Pdf_mantenimento" ><?php echo  IconPDF("../../../") ?></a><?php } ?></td>
        <!--   Estado del documento Actual -->
        <td><?php echo Semaforo("../../../","",$row['estado_mantenimento']) ?> </td>
        <!--   Respuesta del documento que se a subido -->
        <td><span class="mantenimento_respuesta" style="display: none;"></span></td>
    </tr>
    
    
    <tr><!--MANTENIMENTO CORRECTIVO-->
        <!--   Nombre del documento a Subir-->
        <td><label for="mantenimentoCorrectivo">Mantenimento Correctivo :</label></td>        
        <!--   Archivo a subir-->
        <td ><input type="file" class="mantenimentoCorrectivo" name="mantenimentoCorrectivo" value="" accept="application/pdf" disabled="" ></td>
        <!--   Fecha de Vencimento Dococumento -->
        <td><input type="date" class="mantenimentoCorrectivo_fecha" name="mantenimentoCorrectivo_fecha" value="<?php echo $row['fecha_veci_correctivo'] ?>" readonly min="<?php echo   date("Y") ."-". date("m") ."-". date("d"); ?>" ></td>
        <!--   Boton para enviar Documento -->
        <td><button class="mantenimentoCorrectivo_enviar" style="display: none;" >Guardar</button></td>
        <!--   Visulizar el archivo que esta montando   -->
        <td><a href="" title=""  class="Remplzar_mantenimentoCorrectivo"  ><img src="../../../img/iconos/reemplazar.svg" width="28" > </a></td>
        <td><?php if(isset($row['mantenimentoCorrectivo']) && $row['mantenimentoCorrectivo'] != ""  )  {?>
        <a href="<?php echo "../../../".$row['mantenimentoCorrectivo'] ?>" id="Pdf_mantenimentoCorrectivo" ><?php echo  IconPDF("../../../") ?></a><?php }?></td>
        <!--   Estado del documento Actual -->
        <td><?php echo Semaforo("../../../","",$row['estado_mantenimentoCorrectivo']) ?> </td>
        <!--   Respuesta del documento que se a subido -->
        <td><span class="mantenimentoCorrectivo_respuesta" style="display: none;"></span></td>
    </tr>
    
    
    <tr><!--TARJETA DE OPERACION-->
        <!--   Nombre del documento a Subir-->
        <td><label for="tarjeta_de_operacion">Tarjeta De Operacion</label></td>        
        <!--   Archivo a subir-->
        <td ><input type="file" class="tarjeta_de_operacion" name="tarjeta_de_operacion" value="" accept="application/pdf"  disabled="" ></td>
        <!--   Fecha de Vencimento Dococumento -->
        <td><input type="date" class="tarjeta_de_operacion_fecha" name="tarjeta_de_operacion-fecha" value="<?php echo $row['fecha_veci_tarjeta_operacion'] ?>"  readonly min="<?php echo   date("Y") ."-". date("m") ."-". date("d"); ?>" ></td>
        <!--   Boton para enviar Documento -->
        <td><button class="tarjeta_de_operacion_enviar" style="display: none;">Guardar</button></td>
        <!--   Visulizar el archivo que esta montando   -->
        <td><a href="" title="" class="Remplzartarjetaoperacion" ><img src="../../../img/iconos/reemplazar.svg" width="28" > </a></td>
        <td><?php if(isset($row['tarjeta_de_operacion']) && $row['tarjeta_de_operacion'] != ""  )  {?>
        <a href="<?php echo "../../../".$row['tarjeta_de_operacion'] ?>" id="Pdftarjetaoperacion" ><?php echo  IconPDF("../../../") ?></a><?php } ?></td>
        <!--   Estado del documento Actual -->
        <td><?php echo Semaforo("../../../","",$row['estado_tarjeta_de_operacion']) ?> </td>
        <!--   Respuesta del documento que se a subido -->
        <td><span class="tarjeta_de_operacion_respuesta" style="display: none;"></span></td>
    </tr>
    
    
    <tr><!--TARJETA DE PROPIEDAD-->
        <!--   Nombre del documento a Subir-->
        <td><label for="tarjeta_de_propiedad">Tarjeta De Propiedad</label></td>        
        <!--   Archivo a subir-->
        <td ><input type="file" class="tarjeta_de_propiedad" name="tarjeta_de_propiedad" value="" accept="application/pdf" disabled=""></td>
        <!--   Fecha de Vencimento Dococumento -->
        <td><input type="date" class="tarjeta_de_propiedad_fecha" name="tarjeta_de_propiedad_fecha" value="<?php echo $row['fecha_veci_propiedad'] ?>" readonly min="<?php echo   date("Y") ."-". date("m") ."-". date("d"); ?>" ></td>
        <!--   Boton para enviar Documento -->
        <td><button class="tarjeta_de_propiedad_enviar" style="display: none;">Guardar</button></td>
        <!--   Visulizar el archivo que esta montando   -->
        <td><a href="" title="" class="Remplazar_tarjeta_de_propiedad" ><img src="../../../img/iconos/reemplazar.svg" width="28" > </a></td>
        <td><?php if(isset($row['tarjeta_de_propiedad']) && $row['tarjeta_de_propiedad'] != ""  )  {?>
        <a href="<?php echo "../../../".$row['tarjeta_de_propiedad'] ?>" id="Pdf_tarjeta_de_propiedad" ><?php echo  IconPDF("../../../") ?></a><?php } ?></td>
        <!--   Estado del documento Actual -->
        <td><?php echo Semaforo("../../../","",$row['estado_tarjeta_de_propiedad']) ?> </td>
        <!--   Respuesta del documento que se a subido -->
        <td><span class="tarjeta_de_propiedad_respuesta" style="display: none;"></span></td>
    </tr>
    
    

    
    
    <tr><!--SIMIT-->
        <!--   Nombre del documento a Subir-->
        <td><label for="simit">Simit</label></td>        
        <!--   Archivo a subir-->
        <td ><input type="file" class="simit" name="simit" value="" accept="application/pdf" disabled="" ></td>
        <!--   Fecha de Vencimento Dococumento -->
        <td><input type="date" class="simit_fecha" name="simit_fecha" value="<?php echo $row['fecha_veci_simit'] ?>" readonly min="<?php echo   date("Y") ."-". date("m") ."-". date("d"); ?>"  ></td>
        <!--   Boton para enviar Documento -->
        <td><button class="simit_enviar" style="display: none;">Guardar</button></td>
        <!--   Visulizar el archivo que esta montando   -->
        <td><a href="" title="" class="Remplazar_Simit" ><img src="../../../img/iconos/reemplazar.svg" width="28" > </a></td>
        <td><?php if(isset($row['simit']) && $row['simit'] != ""  )  {?> 
        <a href="<?php echo "../../../".$row['simit'] ?>"  id="Pdf_simit" ><?php echo  IconPDF("../../../") ?></a><?php }?></td>
        <!--   Estado del documento Actual -->
        <td><?php echo Semaforo("../../../","",$row['estado_simit']) ?> </td>
        <!--   Respuesta del documento que se a subido -->
        <td><span class="simit_respuesta" style="display: none;"></span></td>
    </tr>
    
    <tr><!--SUPERTRANPOSTE-->
           <!--Nombre del documento a Subir-->
        <td><label for="supertrasporte">supertrasporte</label></td>        
         <!--  Archivo a subir-->
         <td ><input type="file" class="supertrasporte" name="supertrasporte" value="" accept="application/pdf" disabled="" ></td>
         <!--  Fecha de Vencimento Dococumento -->
        <td><input type="date" class="supertrasporte_fecha" name="supertrasporte_fecha" value="<?php echo $row['fecha_veci_supertrasporte'] ?>" readonly  min="<?php echo   date("Y") ."-". date("m") ."-". date("d"); ?>" ></td>
        <!--   Boton para enviar Documento -->
        <td><button class="supertrasporte_enviar" style="display: none;" >Guardar</button></td>
        <!--   Visulizar el archivo que esta montando  --> 
        <td><a href="" title="" class="Remplazar_supertrasporte"  ><img src="../../../img/iconos/reemplazar.svg" width="28" > </a></td>
        <td><?php if(isset($row['supertrasporte']) && $row['supertrasporte'] != ""  )  {?> 
        <a href="<?php echo "../../../".$row['supertrasporte'] ?>" id="Pdf_supertrasporte" ><?php echo  IconPDF("../../../") ?></a><?php } ?></td>
         <!--  Estado del documento Actual--> 
        <td><?php echo  Semaforo("../../../","",$row['estado_supertrasporte']) ?></td>
         <!--  Respuesta del documento que se a subido -->
        <td><span class="supertrasporte_respuesta" style="display: none;"></span></td>
    </tr>
    
    <tr>    
        <td><a href="" ><?php echo IconoPago("../../../", "","inactivo") ?> </a></td>
    </tr>
    <!--    <tr>Administracion de pago
           Nombre del documento a Subir
        <td><label for="administracion_pago">Administracion Pago</label></td>        
           Archivo a subir
        <td><input type="file" class="administracion_pago" name="administracion_pago" value="" accept="application/pdf"  ></td>
           Fecha de Vencimento Dococumento 
        <td><input type="date" class="administracion_pago-fecha" name="administracion_pago-fecha" value="administracion_pago-fecha" readonly  ></td>
           Boton para enviar Documento 
        <td><button class="administracion_pago-enviar" style="display: none;" >Guardar</button></td>
           Visulizar el archivo que esta montando   
        <td><p>Documento Actual :</p></td>
        <td><a href=""><?php// echo  IconPDF("../../../") ?></a></td>
           Estado del documento Actual 
        <td><?php //echo Semaforo("../../../","","activo") ?> Estado</td>
           Respuesta del documento que se a subido 
        <td><span class="administracion_pago-respuesta" style="display: none;"></span></td>
    </tr>-->
    
                <?php }//cierre del ciclo for   ?>
    </table>    
</form></div></div></section></div>

<script type="text/javascript" src="../../../jquery/jquery.js">  </script>
<script type="text/javascript" src="../../../javascriptClases/ValidarCampos.js"></script>
<script type="text/javascript" src="../../../javascriptClases/Vehiculo/Documento-vehiculo.js">  </script>

<?php  include '../../views/footer-popup.php'; ?>
