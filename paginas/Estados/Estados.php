<?php 
require_once '../../conexion/sessionlogin.php'; 
include '../views/header-menu.php';   
include_once '../../Ejecutar-clases/Vehiculo/DocumentoPorVencer-Controlador/SoatVencidos.php';
include_once '../../Ejecutar-clases/Vehiculo/DocumentoPorVencer-Controlador/TecnicoMecanicaVencido.php';
include_once '../../Ejecutar-clases/Vehiculo/DocumentoPorVencer-Controlador/MantenimentoVencido.php';
include_once '../../Ejecutar-clases/Vehiculo/DocumentoPorVencer-Controlador/MantenimentoCorrectivoVencido.php';
include_once '../../Ejecutar-clases/Vehiculo/DocumentoPorVencer-Controlador/TarjetaOperacion.php';
include_once '../../Ejecutar-clases/Vehiculo/DocumentoPorVencer-Controlador/TarjetaPropiedadVencido.php';
include_once '../../Ejecutar-clases/Vehiculo/DocumentoPorVencer-Controlador/SimitVencido.php';
include_once '../../Ejecutar-clases/Vehiculo/DocumentoPorVencer-Controlador/SupertrasporteVencido.php';
include_once '../../Ejecutar-clases/Vehiculo/DocumentoPorVencer-Controlador/VehiculoActivoInactivo.php';

include_once '../../Ejecutar-clases/Conductor/DocumentoPorVencer-Conductor-controlador/LicenciaConductorVencido.php';
include_once '../../Ejecutar-clases/Conductor/DocumentoPorVencer-Conductor-controlador/UltimaCapacitacionVencido.php';
include_once '../../Ejecutar-clases/Conductor/DocumentoPorVencer-Conductor-controlador/SimitVencido.php';
include_once '../../Ejecutar-clases/Conductor/DocumentoPorVencer-Conductor-controlador/ConductorAtivoInactivo.php';

?>
    
<!--eSECCION DE ESTADO  DOCUMENTO VEHICULO -->        

<!--            DOCUMENTOS -->
  <div class="content-wrapper">
      <section class="content container-fluid">
<!-- {{{{{{{{{{{}}}}}}}}}}} -->

<div class="form-group col-md-12">        
      <div class="box box-danger ">
            <div class="box-header">
              <h3 class="box-title"><i class="fa fa-book"></i> Documentos De Vehiculo Por Vencer
</h3>
            </div>
       </div>
</div>
<!-- {{{{{{{{{{{}}}}}}}}}}} -->
<!-- Soat -->
     <div class="form-group col-md-3">        
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">SOAT</h3>
             <select multiple="" class="form-control soatEstadoPlaca">
                 <?php foreach ($SoatPorVencer as $row ){
                 //si la fecha es direncte de vacio y menor a la fecha actual
                  
                   if($row['fecha_Restada'] != null &&  $row['fecha_Restada'] < date("Y-m-j")  ){ ?>
                 <option class=""  value="<?php echo $row['placa'] ?>"><?php echo $row['placa'] ?></option>
                 <?php } }?>
             </select>
            </div>
       </div>
</div>

<!-- tecnicomecanica -->

     <div class="form-group col-md-3">        
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">TecnicoMecanica</h3>
                  <select multiple="" class="form-control tecnicomecanicaEstadoplaca">
                                             <?php foreach ($TecnicoMecanicaPorVencer as $row ){
                            //si la fecha es direncte de vacio y menor a la fecha actual
                            
                              if($row['fecha_Restada'] != null &&  $row['fecha_Restada'] < date("Y-m-j")  ){ ?>
                        <option value="<?php echo $row['placa'] ?>"><?php echo $row['placa'] ?></option>
                        
                       <?php } }?>
             </select>
            </div>
       </div>
</div>

<!-- Mantenimento -->
     <div class="form-group col-md-3">        
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Mantenimento</h3>
                  <select multiple="" class="form-control MantenimentoEstadoPlaca">
                        <?php foreach ($MantenimentoPorVencer as $row ){
                        //si la fecha es direncte de vacio y menor a la fecha actual
                        
                        if($row['fecha_Restada'] != null &&  $row['fecha_Restada'] < date("Y-m-j")  ){ ?>
                        <option value="<?php echo $row['placa'] ?>"><?php echo $row['placa'] ?></option>
                        
                       <?php } }?>
             </select>
            </div>
       </div>
</div>
<!-- M. Correctivo -->
 <div class="form-group col-md-3">        
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">M. Correctivo</h3>
                  <select multiple="" class="form-control MCorrectivoEstadoPlaca">
                         <?php foreach ($SimitPorVencer as $row ){
                            //si la fecha es direncte de vacio y menor a la fecha actual
                            
                              if($row['fecha_Restada'] != null &&  $row['fecha_Restada'] < date("Y-m-j")  ){ ?>
                        <option value="<?php echo $row['placa'] ?>"><?php echo $row['placa'] ?></option>
                        
                       <?php } }?>
             </select>
            </div>
       </div>
</div>
<!-- T. Operación -->
 <div class="form-group col-md-3">        
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">T. Operación</h3>
                  <select multiple="" class="form-control TOperaciónEstadoPlaca">
                   
                         <?php foreach ($TarjetaOperacionPorVencer as $row ){
                            //si la fecha es direncte de vacio y menor a la fecha actual
                              if($row['fecha_Restada'] != null &&  $row['fecha_Restada'] < date("Y-m-j")  ){ ?>
                        <option value="<?php echo $row['placa'] ?>"><?php echo $row['placa'] ?></option>
                       <?php } }?>
             </select>
            </div>
       </div>
  </div>
<!-- T. Propiedad-->

     <div class="form-group col-md-3">        
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">T. Propiedad</h3>
                  <select multiple="" class="form-control  TPropiedadEstadoPlaca">
                        <?php foreach ($TarjetaPropiedadPorVencer as $row ){
                            //si la fecha es direncte de vacio y menor a la fecha actual
                            
                           if($row['fecha_Restada'] != null &&  $row['fecha_Restada'] < date("Y-m-j")  ){ ?>
                        <option value="<?php echo $row['placa'] ?>"><?php echo $row['placa'] ?></option>
                        
                       <?php } }?>
             </select>
            </div>
       </div>
</div>



     <div class="form-group col-md-3">        
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Supertrasporte</h3>
                  <select multiple="" class="form-control  SupertrasporteEstadoPlaca">
                        <?php foreach ($SimitPorVencer as $row ){
                            //si la fecha es direncte de vacio y menor a la fecha actual
                              if($row['fecha_Restada'] != null &&  $row['fecha_Restada'] < date("Y-m-j")  ){ ?>
                        <option value="<?php echo $row['placa'] ?>"><?php echo $row['placa'] ?></option>
                       <?php } }?>
             </select>
            </div>
       </div>
</div>

<!--Barra de Progreso-->
<div class="form-group col-md-3">   
     <div class="box">
            <div class="box-header">
              <h3 class="box-title">Barra de Progreso</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody>
                <tr>
                  <td>Inactivos</td>
                  <td>
                    <div class="progress progress-xl">
                      <div class="progress-bar progress-bar-danger ">
                          
                        <progress max="<?php echo $NumeroVehiculoActivo->getResultado()['Cantidad']; ?>" 
                         value="<?php echo $NumeroVehiculoActivo->getResultado()['Inactivo']; ?>"  
                        class="progress-bar-success" ></progress>
                      </div>
                    </div>
                  </td>
                  <td><span class="badge bg-red"><?php echo $NumeroVehiculoActivo->getResultado()['Inactivo']; ?></span></td>
                </tr>
                 <tr>
                  <td>Activos</td>
                  <td>
                    <div class="progress progress-xl">
                      <div class="progress-bar progress-bar-danger ">
                      <progress max="<?php echo $NumeroVehiculoActivo->getResultado()['Cantidad']; ?>"  
                              value="<?php echo $NumeroVehiculoActivo->getResultado()['Activo']; ?>" 
                              class="progress-verde" ></progress>
                      </div>
                    </div>
                  </td>
                  <td><span class="badge bg-green"><?php echo $NumeroVehiculoActivo->getResultado()['Activo']; ?></span></td>
                </tr>
            </tbody>
        </table>
           <!-- /.box-body -->
     </div>   
   </div>
  </div>


<!-- SESSION DE ESTADO CONDUCTOR-->

<div class="form-group col-md-12">        
      <div class="box box-danger">
            <div class="box-header box-info">
              <h3 class="box-title"><i class="fa fa-book"></i> Documento Conductor Por Vencer</h3>
            </div>
       </div>
</div>

     <div class="form-group col-md-3">        
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Licencia</h3>
             <select multiple="" class="form-control  LicenciaEstadoDocumento">
                <?php foreach ($licenciaPorVencer as $row){
                       if( $row['fechas_Restada'] < date("Y-m-d") ){ ?>
                       <option value="<?php echo $row['Cedulaconductor'] ?>"><?php echo $row['nombreapellido'] ?></option>
                   <?php }  }?>
             </select>
            </div>
       </div>
</div>

<!-- tecnicomecanica -->

     <div class="form-group col-md-3">        
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Ultima Capacitación</h3>
                  <select multiple="" class="form-control UltimaCapacitacionEstadoDocumento">
                        <?php foreach ($UltimaCapacitacionPorVencer as $row){
                       if( $row['fechas_Restada'] < date("Y-m-d") ){ ?>
                       <option value="<?php echo $row['Cedulaconductor'] ?>"><?php echo $row['nombreapellido'] ?></option>
                   <?php }  }?>
             </select>
            </div>
       </div>
</div>

     <div class="form-group col-md-3">        
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Simit</h3>
                  <select multiple="" class="form-control SimitEstadoDocumento">
                        <?php foreach ($SimitConductorPorVencer as $row){
                        if( $row['fechas_Restada'] < date("Y-m-d") ){ ?>
                        <option value="<?php echo $row['Cedulaconductor'] ?>"><?php echo $row['nombreapellido'] ?></option>
                    <?php }  }?>
             </select>
            </div>
       </div>
</div>

 

<div class="form-group col-md-3">   
     <div class="box">
            <div class="box-header">
              <h3 class="box-title">Barra de Progreso</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody>
                <tr>
                  <td>Inactivos</td>
                  <td>
                    <div class="progress progress-xl">
                      <div class="progress-bar progress-bar-dange">
                        <progress max="<?php echo $NumeroConductor->getResultado()['Cantidad']; ?>"  
                              value="<?php echo $NumeroConductor->getResultado()['Inactivo']; ?>"  
                              class="progress-rojo" ></progress></div>
                    </div>
                  </td>
                  <td><span class="badge bg-red"><?php echo $NumeroConductor->getResultado()['Inactivo']; ?></span></td>
                </tr>
                 <tr>
                  <td>Activos</td>
                  <td>
                    <div class="progress progress-xl">
                     <div class="progress-bar progress-bar-danger ">
                      <progress max="<?php echo $NumeroConductor->getResultado()['Cantidad']; ?>"  
                              value="<?php echo $NumeroConductor->getResultado()['Activo']; ?>" 
                              class="progress-verde" ></progress></div>
                    </div>
                  </td>
                  <td><span class="badge bg-green"><?php echo $NumeroConductor->getResultado()['Activo']; ?></span></td>
                </tr>
            </tbody>
        </table>
           <!-- /.box-body -->
     </div>   
   </div>
  </div>

 </section>
</div>

<script type="text/javascript" src="../../jquery/jquery.js"></script>
<script type="text/javascript" src="../../javascriptClases/Estado/Estado-Buses.js"> </script>
<script type="text/javascript" src="../../javascriptClases/Estado/Estado-Conductores.js"> </script>

<?php  include '../views/footer.php'; ?>

