<footer class="main-footer">
    <div class="pull-right hidden-xs">
      -
    </div>
    <strong>Copyright &copy; 2020 <a href="#">Supertransp</a>.</strong> All rights reserved.
  </footer>
  <div class="control-sidebar-bg"></div>
</div>
<script src="<?php echo $url; ?>/desing/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo $url; ?>/desing/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo $url; ?>/desing/dist/js/adminlte.min.js"></script>
</body>
</html>  <!-- Main Footer -->