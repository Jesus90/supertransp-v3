﻿<!DOCTYPE html>
<?php $url='http://localhost/supertransp-v3/paginas'; 
      $url_img='http://localhost/supertransp-v3/'; 
?>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo $url; ?>/desing/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo $url; ?>/desing/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo $url; ?>/desing/bower_components/font-awesome/css/inputs.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo $url; ?>/desing/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $url; ?>/desing/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo $url; ?>/desing/dist/css/skins/skin-blue.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <div  class="barraVerificacion"> </div>
  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
<!--    cuendo se presiones sobre el icono activara un clase javascrit para aplicar cualquier cambio-->
<a href="#" class="logo AplicarCambios" >
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">
        <img src="<?php echo $url; ?>/../img/logosupertransp.png">
      </span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
    <img src="<?php echo $url; ?>/../img/logo.png">
      </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            
<!--            //icono para recargar web y aplicar cambios-->
<!--<li class="dropdown"><a href="#"><img src="../../img/icon/favicon.png"></a>  </li>-->
            
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the messages -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <!-- User Image -->
                        <img src="<?php echo $url; ?>/desing/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <!-- Message title and timestamp -->
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <!-- The message -->
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                </ul>
                <!-- /.menu -->
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- /.messages-menu -->

          <!-- Notifications Menu -->
          <li class="dropdown notifications-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- Inner Menu: contains the notifications -->
                <ul class="menu">
                  <li><!-- start notification -->
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <!-- end notification -->
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks Menu -->
          <li class="dropdown tasks-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- Inner menu: contains the tasks -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <!-- Task title and progress text -->
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <!-- The progress bar -->
                      <div class="progress xs">
                        <!-- Change the css width attribute to simulate progress -->
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="<?php echo $url; ?>/desing/dist/img/user.png" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">Usuario</span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
             <img src="<?php /*si el usuario no tiene imagen muestra la s de supertransp*/
            if(isset($_SESSION['foto_usuario']) && $_SESSION['foto_usuario'] != ""  ) {echo "".$url_img."".$_SESSION['foto_usuario'];}else{ echo "../img/logosupertransp.png"; } ?>" >
                <p>
            <?php 
            if(isset($_SESSION['usuario']) && $_SESSION['usuario'] != ""  ) {echo "".$_SESSION['usuario'];} ?>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-8 text-center">
                    <a href="#">Id Empresa:
                   <?php if(isset($_SESSION['idEmpresa']) && $_SESSION['idEmpresa'] != ""  ) {echo "" .$_SESSION['idEmpresa'];} ?></a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#"></a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#"></a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo $url; ?>/empresa/Infomracion-Empresa.php" class="btn btn-default btn-flat">Perfil</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo $url; ?>/../sessionout.php" class="btn btn-default btn-flat">Cerrar sesión</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">

          <img src="<?php /*si el usuario no tiene imagen muestra la s de supertransp*/
            if(isset($_SESSION['foto_usuario']) && $_SESSION['foto_usuario'] != ""  ) {echo "".$url_img."".$_SESSION['foto_usuario'];}else{ echo "../img/logosupertransp.png"; } ?>" class="img-circle" alt="User Image" style=" height: 50px;" >
        </div>
        <div class="pull-left info">
          <p><?php if(isset($_SESSION['idEmpresa']) && $_SESSION['idEmpresa'] != ""  ) {echo "" .$_SESSION['idEmpresa'];} ?></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENÚ</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="<?php echo $url; ?>/index.php"><i class="fa fa-link"></i> <span>Inicio</span></a></li>

        <li class="active"><a href="<?php echo $url; ?>/Estados/Estados.php"><i class="fa fa-link"></i> <span>Estado</span></a></li>

      
<!--- -------------------- MENU empresa-------------------------------   -->
<?php if(isset($_SESSION['rangoUsuario']) && $_SESSION['rangoUsuario'] !== 'conductor' && $_SESSION['rangoUsuario'] !== 'usuarios' ){   ?>
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Empresa</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $url; ?>/empresa/Infomracion-Empresa.php">Infomracion</a></li>
            <li><a href="<?php echo $url; ?>/empresa/Ingresar-Empresa.php">Nueva empresa</a></li>
          </ul>
        </li>
<?php  } ?>
<!--- -------------------- -------------------------------   -->
<!--- -------------------- MENU Contrato-------------------------------   -->
<?php if(isset($_SESSION['rangoUsuario']) && $_SESSION['rangoUsuario'] !== 'conductor' ){   ?>
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Contrato</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $url; ?>/Contrato/Ingresar-Contrato.php">Nuevo Contrato</a></li>
            <li><a href="<?php echo $url; ?>/Contrato/Listar-Contrato.php">Lista de Contratos</a></li>
          </ul>
        </li>
<?php  } ?>
<!--- -------------------- -------------------------------   -->
<!--- -------------------- MENU empresa-------------------------------   -->
<?php if(isset($_SESSION['rangoUsuario']) && $_SESSION['rangoUsuario'] !== 'conductor' ){   ?>
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Vehiculo</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $url; ?>/Vehiculo/Ingresar-Vehiculo.php">Nuevo Vehiculo</a></li>
            <li><a href="<?php echo $url; ?>/Vehiculo/Listar-Vehiculo.php">Lista de Vehiculos</a></li>
          </ul>
        </li>
<?php  } ?>
<!--- --------------------  -------------------------------   -->
<!--- -------------------- MENU empresa-------------------------------   -->
<?php if(isset($_SESSION['rangoUsuario']) && $_SESSION['rangoUsuario'] !== 'conductor' ){   ?>
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Conductor</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $url; ?>/Conductores/Ingresar-Conductor.php">Nuevo Conductor</a></li>
            <li><a href="<?php echo $url; ?>/Conductores/Listar-Conductor.php">Lista de Conductores</a></li>
          </ul>
        </li>
<?php  } ?>
<!--- --------------------  -------------------------------   -->
       <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Fuecs</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $url; ?>/Fuec/IngresarFuec.php">Nuevo Conductor</a></li>
            <li><a href="<?php echo $url; ?>/Fuec/ListarFuec.php">Lista de Conductores</a></li>
          </ul>
        </li>
<!--- -------------------- MENU Usuarios-------------------------------   -->
<?php if(isset($_SESSION['rangoUsuario']) && $_SESSION['rangoUsuario'] !== 'conductor' ){   ?>
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Usuarios</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $url; ?>/usuarios/AgregarUsuario.php">Crear Usuario</a></li>
            <li><a href="<?php echo $url; ?>/usuarios/ListarUsuarios.php">Listar Usuarios</a></li>
           <?php  } ?>
           <li><a href="<?php echo $url; ?>/usuarios/ActulizarUsuario.php">Actualizar Usuarios</a></li>
        </ul>

        <?php //si el rango es de tipo conductor  
         if(isset($_SESSION['rangoUsuario']) && $_SESSION['rangoUsuario'] == 'conductor' )  { ?>
          <li><a href="<?php if(isset( $_SESSION['documento'])  ){echo $_SESSION['documento']; } ?>"  class="Actulizar_Conductor_link">Actulizar Documento</a></li>
           <?php  }?>
       

<!--- --------------------Listamiento diario  -------------------------------   -->
       <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Alistamiiento Diario</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo $url; ?>/Conductores/AlistamientoDiario/FormularioAlistamientoDiario.php">Crear Alistamiento Diario</a></li>
            <li><a href="<?php echo $url; ?>/Conductores/AlistamientoDiario/ListarAlistamientoDiario.php">Todos los Alistamientos</a></li>
          </ul>
        </li>

      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

<!--  //este archivo servira para recargar la web aplicar cambios-->
<script type="text/javascript" src="../../jquery/jquery.js"> </script>
<script type="text/javascript" src="../../javascriptClases/Verificar/Verificar.js"> </script>