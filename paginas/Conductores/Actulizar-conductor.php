<?php require_once '../../Ejecutar-clases/Conductor/BuscarConductor.php'; 
      require_once '../../conexion/sessionlogin.php'; 
      include '../views/header-popup.php'; ?>

<div class="content-wrapper">
 <section class="content">
       <div class="row">
          <!-- /.box -->
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
        <div class="box-header">
        <i class="fa fa-book"></i>
        <h3 class="box-title">Actualizar Conductor</h3>
         </div>
          
            <div class="content">
              <!-- /.tab-pane -->
              <div class="tab-pane" id="settings">
                <form method="post" class="actulizar-conductor form-horizontal">
              <?php if(isset($ver)){ //el resultado de la visualizacion de los datos vine de BuscarConducto.php
              foreach ($ver as $row){ ?>
               
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Numero y Apellido</label>
                    <div class="col-sm-10">
                    <input type="text" name="nombre" class="nombreACT" placeholder="" value="<?php echo $row['nombreapellido']; ?>">
                    </div>
                  </div>

                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">N° Cedula</label>
                    <div class="col-sm-10">
                    <input type="text" name="cedula" class="cedulaACT" placeholder="" value="<?php echo $row['Cedulaconductor']; ?>" readonly="">
                    </div>
                  </div>

                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">N° Contacto </label>
                    <div class="col-sm-10">
                   <input type="text" name="numero_contacto" class="numero_contactoACT" placeholder="" value="<?php echo $row['numero_contacto']; ?>">
                    </div>
                  </div>

                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Fecha De Nacimiento</label>
                    <div class="col-sm-10">
                <input type="date" name="Fecha_nacimento" class="Fecha_nacimentoACT" placeholder="" value="<?php echo $row['Fecha_nacimento']; ?>">
                    </div>
                  </div>

        <td colspan="3"><span class="respuestaActulizarConductor"></span></td>
   
        <td  colspan="2"><button  class="ActulizarCoductorBoton btn btn-success">Actulizar Conductor</button></td>

    <?php }}?>
             </form>
          </div>
       </div>
     </div>
   </div>
 </section>
</div>
    
<script type="text/javascript" src="../../jquery/jquery.js"></script>
<script type="text/javascript" src="../../javascriptClases/Ajax.js"></script>
<script type="text/javascript" src="../../javascriptClases/ValidarCampos.js"></script>
<script type="text/javascript" src="../../javascriptClases/Conductor/Conductor.js"></script>


<?php  include '../views/footer-popup.php'; ?>