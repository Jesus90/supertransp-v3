<?php
require_once '../../conexion/sessionlogin.php'; 
include '../views/header-menu.php';
?>


<div class="content-wrapper">
 <section class="content">
       <div class="row">

          <!-- /.box -->
        <!-- /.col -->
    <div class="col-md-9">
      <div class="box box-primary">
        <div class="box-header">
        <i class="fa fa-book"></i>
        <h3 class="box-title">Nueva Conductor</h3>
         </div>
          
            <div class="content">
              <!-- /.tab-pane -->
              <div class="tab-pane" id="settings">
                 <form method="post" class="Ingresar-conductor form-horizontal">
        
                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Nombre</label>
                    <div class="col-sm-10">
                    <input type="text" name="nombre" class="nombre form-control" placeholder="">
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Apellido</label>
                    <div class="col-sm-10">
                    <input type="text" name="apellido" class="apellido form-control" placeholder="">
                    </div>
                  </div>
                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Numero de Cedula</label>
                    <div class="col-sm-10">
                    <input type="text" name="cedula" class="cedula form-control" placeholder="">
                    </div>
                  </div>
                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Numero de Contacto</label>
                    <div class="col-sm-10">
                    <input type="text" name="numero_contacto" class="numero_contacto form-control" placeholder="">
                    </div>
                  </div>
                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Fecha De Nacimiento</label>
                    <div class="col-sm-10">
                    <input type="date" name="Fecha_nacimento" class="Fecha_nacimento form-control" placeholder="">
                    </div>
                  </div>

                 <span style="color: green;" class="respuestaInsertarConductor"></span>      
        
               <button  class="IngresarCoductorBoton btn btn-success">Ingresar Conductor</button>
             </form>
          </div>
       </div>
     </div>
   </div>
 </div>
 </section>
</div>
<script type="text/javascript" src="../../jquery/jquery.js"></script>
<script type="text/javascript" src="../../javascriptClases/Ajax.js"></script>
<script type="text/javascript" src="../../javascriptClases/ValidarCampos.js"></script>
<script type="text/javascript" src="../../javascriptClases/Conductor/Conductor.js"></script>

<?php  include '../views/footer.php'; ?>



