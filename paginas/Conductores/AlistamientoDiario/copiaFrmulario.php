


<style>
     table,td,th,tr {
	border: 1px solid black;}
     
    
    </style>
    
    
    
    <table>
        <tr>
            <td colspan="4" style="text-align: center" > Datos </td>
        </tr>
        <tr>
            <td colspan="1">Placa Vehiculo</td>
            <td colspan="2">
                <input list="browsers" name="placa_vehiculo_buscar">
                <datalist id="browsers" class="respuestas" >
                    <option value="Internet Explorer">
                    <option value="Internet Explorer">    
                  </datalist>
            </td>
        </tr>    
        <tr>
            <td>Nombre: </td>
            <td><input type="text" value="" class="nombre_conductor" name="nombre_conductor" size="48"></td>
            <td>Fecha Actual</td>
            <td><input type="date" value="<?php echo  date('Y-m-d'); ?>" class="fecha_actual" name="fecha_actual" readonly="" ></td>
        </tr>
        
        <tr>
            <td>Ciudad:</td>
            <td><input type="text" value="" name="ciudad" class="ciudad" placeholder="" size="48"></td>
            <td>Kilometraje</td>
            <td><input type="text" value="" name="Kilometraje" class="Kilometraje" placeholder="Km/h"></td>
        </tr>
        
            <!--        Elementos Que Se Inspeccionan-->
        <tr>
            <td colspan="4"  style="text-align: center">Elementos Que Se Inspeccionan</td>
        </tr>
        <tr>
            <td rowspan="3">
               Direccionales 
            </td>
        </tr>
        <tr>
            <td>
                Delanteras
            </td>
            <td>
                <input type="radio" class="direccionales_delanteras" name="direccionales_delanteras" value="1"><label>Si</label>
                <input type="radio" class="direccionales_delanteras" name="direccionales_delanteras" value="0"><label>No</label>
            
            </td>
            <td>
                <textarea name="direccionales_delanteras_observacion" class="direccionales_delanteras_observacion" > </textarea>
            </td>
            
        </tr>
        <tr>
            <td>
                Traseras
            </td>
            <td>
                <input type="radio" class="direccionales_traseras" name="direccionales_traseras" value="1"><label>Si</label>
                <input type="radio" class="direccionales_traseras" name="direccionales_traseras" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="direccionales_traseras_observacion" class="direccionales_traseras_observacion"  > </textarea>
            </td>
        </tr>
        
        
                <!--        luces-->
        <tr>
            <td rowspan="6">
                Luces    
            </td>
        </tr>
        <tr>
            <td>Altas</td>
            <td>
                <input type="radio" class="luces_altas" name="luces_altas" value="1"><label>Si</label>
                <input type="radio" class="luces_altas" name="luces_altas" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="luces_altas_observacion" class="luces_altas_observacion" > </textarea>
            </td>
        </tr>
        <tr>
            <td>Bajas</td>
            <td>
                <input type="radio" class="luces_bajas" name="luces_bajas" value="1"><label>Si</label>
                <input type="radio" class="luces_bajas" name="luces_bajas" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="luces_bajas_observacion" class="luces_bajas_observacion" > </textarea>
            </td>
        </tr>
        <tr>
            <td>Stop</td>
            <td>
                <input type="radio" class="luces_stops" name="luces_stops" value="1"><label>Si</label>
                <input type="radio" class="luces_stops" name="luces_stops" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="luces_stops_observacion" class="luces_stops_observacion" > </textarea>
            </td>
        </tr>
        <tr>
            <td>Reversa</td>
            <td>
                <input type="radio" class="luces_reversa" name="luces_reversa" value="1"><label>Si</label>
                <input type="radio" class="luces_reversa" name="luces_reversa" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="luces_reversa_observacion" class="luces_reversa_observacion" > </textarea>
            </td>
        </tr>
        <tr>
            <td>Parqueo</td>
            <td>
                <input type="radio" class="luces_parqueo" name="luces_parqueo" value="1"><label>Si</label>
                <input type="radio" class="luces_parqueo" name="luces_parqueo" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="luces_parqueo_observacion" class="luces_parqueo_observacion" > </textarea>
            </td>
        </tr>
        
<!--        limpiaBrisas-->
        <tr >
            <td rowspan="3">LimpiaBrisas</td>
        </tr>
        <tr>
            <td>Der/Izq</td>
            <td>
                <input type="radio" class="limpiabrisas_der_izq" name="limpiabrisas_der/izq" value="1"><label>Si</label>
                <input type="radio" class="limpiabrisas_der_izq" name="limpiabrisas_der/izq" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="limpiabrisas_der/izq_observacion" class="limpiabrisas_der_izq_observacion" > </textarea>
            </td>
        </tr>
        <tr>
            <td>Atraz</td>
            <td>
                <input type="radio" class="limpiabrisas_atras" name="limpiabrisas_atras" value="1"><label>Si</label>
                <input type="radio" class="limpiabrisas_atras" name="limpiabrisas_atras" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="limpiabrisas_atras_observacion" class="limpiabrisas_atras_observacion" > </textarea>
            </td>
        </tr>
        
<!--        principal-->
        <tr>
            <td rowspan="3">Frenos</td>
        </tr>
        <tr>
            <td>Principal</td>
            <td>
                <input type="radio" class="frenos_principal" name="frenos_principal" value="1"><label>Si</label>
                <input type="radio" class="frenos_principal" name="frenos_principal" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="frenos_principal_observacion" class="frenos_principal_observacion" > </textarea>
            </td>
        </tr>
        <tr>
            <td>Emergencia</td>
            <td>
                <input type="radio" class="frenos_emergencia" name="frenos_emergencia" value="1"><label>Si</label>
                <input type="radio" class="frenos_emergencia" name="frenos_emergencia" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="frenos_emergencia_observacion" class="frenos_emergencia_observacion" > </textarea>
            </td>
        </tr>
        
        <!--        Llantas-->
        <tr>
            <td rowspan="4"> Llantas</td>
        </tr>
        <tr>
            <td>Delanteras</td>
            <td>
                <input type="radio" class="llantas_delanteras" name="llantas_delanteras" value="1"><label>Si</label>
                <input type="radio" class="llantas_delanteras" name="llantas_delanteras" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="llantas_delanteras_observacion" class="llantas_delanteras_observacion" > </textarea>
            </td>
        </tr>

        <tr>
            <td>Traseras</td>
            <td>
                <input type="radio" class="llantas_traseras" name="llantas_traseras" value="1"><label>Si</label>
                <input type="radio" class="llantas_traseras" name="llantas_traseras" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="llantas_traseras_observacion" class="llantas_traseras_observacion" > </textarea>
            </td>
        </tr>

        <tr>
            <td>Respuesto</td>
            <td>
                <input type="radio" class="llantas_repuestos" name="llantas_repuestos" value="1"><label>Si</label>
                <input type="radio" class="llantas_repuestos" name="llantas_repuestos" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="llantas_repuestos_observacion" class="llantas_repuestos_observacion" > </textarea>
            </td>
        </tr>
        
        <!--        Espejos-->
        <tr>
            <td rowspan="3">Espejos</td>
        </tr>
        <tr>
            <td>Laterales DeR/Izq</td>
            <td>
                <input type="radio" class="espejos_lateral_der_izq" name="espejos_lateral_der_izq" value="1"><label>Si</label>
                <input type="radio" class="espejos_lateral_der_izq" name="espejos_lateral_der_izq" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="espejos_lateral_der_izq_observacion" class="espejos_lateral_der_izq_observacion" > </textarea>
            </td>
        </tr>
        <tr>
            <td>Retrovisor</td>
            <td>
                <input type="radio" class="espejos_retrovisor" name="espejos_retrovisor" value="1"><label>Si</label>
                <input type="radio" class="espejos_retrovisor" name="espejos_retrovisor" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="espejos_retrovisor_observacion"  class="espejos_retrovisor_observacion" > </textarea>
            </td>
        </tr>
        <!--Pito-->
        <tr>
            <td colspan="2">Pito</td>

            <td>
                <input type="radio" class="pito" name="pito" value="1"><label>Si</label>
                <input type="radio" class="pito" name="pito" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="pito_observacion"  class="pito_observacion" > </textarea>
            </td>
        </tr>
        <!--        Niveles De Fluido-->
        <tr>
            <td rowspan="4">Niveles De Fluidos</td>
        </tr>
        <tr>
            <td>Frenos</td>
            <td>
                <input type="radio" class="fluidos_frenos" name="fluidos_frenos" value="1"><label>Si</label>
                <input type="radio" class="fluidos_frenos" name="fluidos_frenos" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="fluidos_frenos_observacion" class="fluidos_frenos_observacion" > </textarea>
            </td>
        </tr>
        <tr>
            <td>Aceites</td>
            <td>
                <input type="radio" class="fluidos_aceites" name="fluidos_aceites" value="1"><label>Si</label>
                <input type="radio" class="fluidos_aceites" name="fluidos_aceites" value="0"><label>No</label>
            </td>
           <td>
               <textarea name="fluidos_aceites_observacion" class="fluidos_aceites_observacion" > </textarea>
            </td>
        </tr>
        <tr>
            <td>Refrigerante</td>
            <td>
                <input type="radio" class="fluidos_refriguerante" name="fluidos_refriguerante" value="1"><label>Si</label>
                <input type="radio" class="fluidos_refriguerante" name="fluidos_refriguerante" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="fluidos_refriguerante_observacion"  class="fluidos_refriguerante_observacion"> </textarea>
            </td>
        </tr>
        <tr >
            <td colspan="4" style="text-align: center">Equipo de Seguridad</td>
        </tr>
        
        <tr>
            <td >Herramientas</td>
            <td>
                <ul>
                    <li>Alicate</li>
                    <li>Destornilladores</li>
                    <li>Llaves De Expacion</li>
                    <li>Llaves Fijas</li>
                </ul>
            </td>
            <td>
                <input type="radio" class="herraminetas" name="herraminetas" value="1"><label>Si</label>
                <input type="radio" class="herraminetas" name="herraminetas" value="0"><label>No</label>
            </td>
            
            <td>
                <textarea name="herramientas_observacion" class="herramientas_observacion"  > </textarea>
            </td>
        </tr>
        <tr>
            <td colspan="1">Crucetas</td>
            <td>Apta Para Vehiculo</td>
            <td>
                <input type="radio" class="crucetas" name="crucetas" value="1"><label>Si</label>
                <input type="radio" class="crucetas" name="crucetas" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="crucetas_observacion" class="crucetas_observacion"  > </textarea>
            </td>
        </tr>
<!--        Gato-->
        <tr>
            <td colspan="1">Gato</td>
            <td >Con Capacidad para Elevar El Vehiculo</td>
            <td>
                <input type="radio" class="gato" name="gato_observacion" value="1"><label>Si</label>
                <input type="radio" class="gato" name="gato_observacion" value="0"><label>No</label>
            </td>
            <td>
                <textarea name=""  > </textarea>
            </td>
        </tr>
<!--        Tacos-->
        <tr>
            <td colspan="1">Taco</td>
            <td nowrap>Dos Tacos Apto Para Bloquear El Vehiculo</td>
            <td>
                <input type="radio" class="taco" name="taco" value="1"><label>Si</label>
                <input type="radio" class="taco" name="taco" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="taco_observacion" class="taco_observacion" > </textarea>
            </td>
        </tr>
<!--        Señales-->
        <tr>
            <td>Señales</td>
            <td>
                <ul>
                    <li>Dos Señales Forma de Triangulo</li>
                    <li>Material Reflectivo</li>
                    <li>Lampara De Señal Luz Amarilla</li>
                </ul>    
             </td>
             <td>
                 <input type="radio" class="senales" name="senales" value="1"><label>Si</label>
                 <input type="radio" class="senales" name="senales" value="0"><label>No</label>
            </td>
             <td>
                 <textarea name="senales_observacion" class="senales_observacion" > </textarea>
            </td>
        </tr>
        
        <!--        chaleco-->
        <tr>
            <td>Chaleco</td>
            <td>Debe Ser Reflectivo</td>
            <td>
                <input type="radio" class="chaleco" name="chaleco" value="1"><label>Si</label>
                <input type="radio" class="chaleco" name="chaleco" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="chaleco_observacion" class="chaleco_observacion" > </textarea>
            </td>
        </tr>
<!--        Botiquin-->
        <tr>
            <td>
                Botiquin
            </td>
            <td>
                <ul>
                    <li>Yodopovidona Solucion Antiseptico Bolsa (120 ML)</li>
                    <li>Jabon</li>
                    <li>Gasas</li>
                    <li>Curas</li>
                    <li>Venda Elastica</li>
                    <li>Micropore Rollo</li>
                    <li>Algodon Paquete (25 GR)</li>
                    <li>Acetaminofen Tabletas</li>
                    <li>Mareol Tabletas</li>
                    <li>Sales De Rehidratacion Oral</li>
                    <li>Baja Lenguas</li>
                    <li>Suero Fisiologico(250 ML)</li>
                    <li>Guante Latex Desechables</li>
                    <li>Toallas Higienicas</li>
                    <li>Tijeras</li>
                    <li>Termometro</li>
                </ul>
            </td>
            <td>
                <input type="radio" class="botiquin" name="botiquin" value="1"><label>Si</label>
                <input type="radio" class="botiquin" name="botiquin" value="0"><label>No</label>
            </td>
            <td>
                <textarea name="botiquin_observacion"  class="botiquin_observacion" > </textarea>
            </td>
        </tr>
        
        <tr>
            <td colspan="4">
                <span>Respuesta</span>
            </td>
        </tr>
        
        <tr>
            <td colspan="4">
                <button class="alistamiento_diario_F">Guardar</button>
            </td>
        </tr>
        
    </table>
    
<!--    <script type="text/javascript" src="../../../jquery/jquery.js"></script>
    <script type="text/javascript" src="../../../javascriptClases/ValidarCampos.js"></script>
    <script type="text/javascript" src="../../../javascriptClases/Conductor/alistamiento_diario.js"></script>-->



