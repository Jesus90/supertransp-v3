<?php require_once '../../conexion/sessionlogin.php'; 
      include '../views/header-menu.php'; ?>

<div class="content-wrapper">
 <section class="content">
       <div class="row">
          <!-- /.box -->
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
        <div class="box-header">
        <i class="fa fa-book"></i>
        <h3 class="box-title">Nuevo Contrato</h3>
         </div>
          
            <div class="content">
              <!-- /.tab-pane -->
              <div class="tab-pane" id="settings">
                <form method="post" class="ingresar-contrato form-horizontal" enctype="multipart/form-data">
          
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Numero de contrato</label>
                    <div class="col-sm-10">
                    <input type="text" name="numerocontrato" class="numerocontrato form-control" size="30" autocomplete="off" value="1545455456">
                    </div>
                  </div>
                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">contratante </label>
                    <div class="col-sm-10">
                    <td><input type="text" name="contratante" class="contratante form-control" size="30" autocomplete="off" value="Jesus peña">
                   </div>
                  </div>      
                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">bjeto del contrato </label>
                    <div class="col-sm-10">
                    <input type="text" name="objetocontrato" class="objetocontrato form-control" placeholder="" size="30" autocomplete="off" value="cubrir rutas faltante">
                  </div>
                </div>
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Recorido</label>
                    <div class="col-sm-10">
                    <input type="text" name="recorido" class="recorido form-control" placeholder="" size="30" autocomplete="off" value=" cincuvalar, calle 30 ">
                  </div>
                </div>
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Conevio</label>
                    <div class="col-sm-10">
                    <input type="text" name="conevio" class="conevio form-control" placeholder="" size="30" autocomplete="off" value="565644">
                  </div>
                </div>
                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Empresa contratada </label>
                    <div class="col-sm-10">
                    <input type="text" name="empresacontratada" class="empresacontratada form-control" placeholder="" size="30" value="Coochofal">
                  </div>
                </div>
                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label"> Fecha inicial de contrato</label>
                    <div class="col-sm-10">
                    <input type="date" name="fechainicial" class="fechainicial form-control" placeholder="" size="30" required="" value="23/09/2019">
                  </div>
                </div>
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label"> Fecha Vencimiento</label>
                    <div class="col-sm-10">
                     <input type="date" name="fechavencimineto" class="fechavencimineto form-control" placeholder="" size="30" required="" value="25-10-2019">
                  </div>
                </div>
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">decriptcion del contrato</label>
                    <div class="col-sm-10">
                    <input type="text" name="descriptcioncontrato" class="descriptcioncontrato form-control" placeholder="" size="30" value="se proporcionan rutas">
                  </div>
                </div>
                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Contrato</label>
                    <div class="col-sm-10">
                      <label for="introinput"> <i class="fa fa-upload"></i>Subir IContrato en pdf</label>
                     <input type="file" id="introinput" name="contratopdf" class="contratopdf form-control" accept=".pdf" required="" size="30" value="" >
                  </div>
                </div>
                
         
            <span style="color: green;" class="respuestasInsertarContrato"></span><br>
           
              <input type="button"  value="Guardar Nuevo contrato" class="ingresarContrato btn btn-success" id="actulizar-informacion-empresa">      
         
             </form>
          </div>
       </div>
     </div>
   </div>
 </section>
</div>

        
        <!--        archivos necesarios para ejecutar el ajax-->
  
<script type="text/javascript" src="../../jquery/jquery.js"> </script>
<script type="text/javascript" src="../../javascriptClases/ValidarCampos.js"></script>
<script type="text/javascript" src="../../javascriptClases/Ajax.js"></script>
<script type="text/javascript" src="../../javascriptClases/contrato/contrato.js"> </script>
        

<?php  include '../views/footer.php'; ?>
