class Empresas{
    
    
    //esta funcion esta conectada con Empresa/IngresarEmpresa.php para guardar datos basicos 
    IngresarEmpresa(){
        
        let ingresarempresa = new FormData($("#ingresar-empresa")[0]);
        let url = "../../Ejecutar-clases/Empresa/IngresarEmpresa.php";
        
        let ajax = new Ajax(url,ingresarempresa);
            ajax.funcionAjax().done(function(data){
                //cuendo hay una respuesta
               $('.respuesta_crearEmpresa').html(data);
            }).fail(function(){
                alert("error");
            });
     }
     
     //esta funcion sirve para mostrar la informacion por json
     MostrarInformacionEmpresa(){
         //esta url esta conectada con el archivo InformacionEmpresa.php 
         //para obtener el resultado de la base dedatos en formato json
         let url = "../../Ejecutar-clases/Empresa/InformacionEmpresa.php";
         
         //metodo jqeury para obtener el resutado
         $.getJSON(url,function(data){
             $(".razonsocial").val(data.razonsocial);
             $(".nit").val(data.nit);
             $(".representante").val(data.representante);
             $(".cedula").val(data.cedula);
             $(".telefono").val(data.telefono);
             $(".direccion").val(data.direccion);
             $(".movil").val(data.movil);
             $(".email").val(data.email);
             $(".ciudad").val(data.ciudad);
             $(".firma").attr('src',data.firma);
             $(".logotranscito").attr('src',data.logotranscito);
             $(".logoempresa").attr('src',data.logoempresa);
            
         });
         
         
     }
     
     ActulizarEmpresa(){
         let actulizarEmpresa = new FormData($("#informacion-Empresa")[0]);
         let url = "../../Ejecutar-clases/Empresa/ActulizarInformacionEmpresa.php";
         
         let ajax = new Ajax(url,actulizarEmpresa);
             ajax.funcionAjax().done(function(data){
                 $(".Repueta_actulizarEmpresa").html(data);
             });
         
     }
    
    
    
    
}


//esto se ejecuta en el archivo Ingresar-Empresa

$(document).on('click','.ingresar-empresa-button',function(){
    
    let validarEmpresas = new ValidarCampos();
    if(validarEmpresas.validarCampoVacio('.nombre-empresa') &&
       validarEmpresas.validarCampoVacio('.nit-empresa') &&
       validarEmpresas.validarEmail('.email-empresa')){
                    let empresa = new Empresas();
                        empresa.IngresarEmpresa();
        
       }
});

//este so ejecuta al inicio de la web para mostrar la informacion de la empresa en el
//archivo Infomracion-Empresa
$(document).ready(function(){
               let empresa = new Empresas();
               empresa.MostrarInformacionEmpresa(); 
    
});




//esto se ejecuta en el archivo InfomracionEmpresa
$(document).on('click','#actulizar-informacion-empresa',function(){
    let validarInformacionEmpresa = new ValidarCampos(); 
        
        //despues de validar que todo los campos esten llenos 
    if(validarInformacionEmpresa.validarletras(".razonsocial")  &&
       validarInformacionEmpresa.validarLEtrasNumeros(".nit")   &&
       validarInformacionEmpresa.validarletras(".representante")&&
       validarInformacionEmpresa.validarNumeros(".cedula")      &&
       validarInformacionEmpresa.validarNumeros(".telefono")    &&
       validarInformacionEmpresa.validarletras(".direccion")&&
       validarInformacionEmpresa.validarNumeros(".movil") &&
       validarInformacionEmpresa.validarEmail(".email")&&
       validarInformacionEmpresa.validarletras(".ciudad")){
    
            //hago instancia de la clase empresa para llamar el metodo ActulizarEmpresa()
            //para que se hagan efectvos los cambios
            let atulizarInfoEmpresa = new Empresas();
                atulizarInfoEmpresa.ActulizarEmpresa();
           }
 });
 
 
 
 
 


