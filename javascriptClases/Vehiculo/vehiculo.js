class Vehiculo{
        
        
        //esta funcion tiene como objetivo listar los contrato que estan activo en el formulario de agregar vehiculo
        listarContratoActivo(propio){
            //si el vehiculo es propio no listara contratos
            if(propio === "si"){
                $('.ocultarContrato').css('display','none');
                
                
            }else{
                //si NO es propio listara los contrato de los vehiculo
                 $('.ocultarContrato').css('display','block');
                 let url = "../../Ejecutar-clases/Vehiculo/listarContratoActivo.php";
                 $('.listarContraActivo').load(url);
            }
        }
        
        //este metodo sirve para guardar un nuevo vehiculo
        IngresarVehiculo(){
                let url = "../../Ejecutar-clases/Vehiculo/IngresarVehiculo.php";
                let valor = new FormData($('.Formulario_ingresar_vehiculo')[0]);
            let ajax = new Ajax(url,valor);
                ajax.funcionAjax().done(function(data){
                    //respuesta desues de ingresar vehiculo
                    $('.Respuesta_ingresar_vehiculo').html(data);
                });
        }
        
        //esta para cambiar de pagina
        Cambiarpagina(valor){
            let url = "../../Ejecutar-clases/Vehiculo/ListarVehiculo.php";
           
            $.get(url,{pagina:valor},function(dato){
                $('.listarVehiculo').html(dato);
            });
            return false;
        }
        
        //buscar registro
        BuscarRegistro(valor){
            
            let url = "../../Ejecutar-clases/Vehiculo/ListarVehiculo.php";
            
            if(valor !== ""){
                     $.post(url,{buscar:valor},function(dato){
                     $('.listarVehiculo').html(dato);
                 });
                 }else{
                      let url = "../../Ejecutar-clases/Vehiculo/ListarVehiculo.php";
                         $('.listarVehiculo').load(url);
                 }
            
          }
          
          
          
          EliminarVehiculoPlaca(placas){
                let url = "../../Ejecutar-clases/Vehiculo/EliminarVehiculo.php?placas="+placas;
//                 window.open(url,"Actulizar Contrato","width=50,height=50,scrollbars=NO,titlebar=no,left=450,top=160,menubar=no");
               $.post(url,{placas:placas},function(mensaje){
                   alert(mensaje);
                   setInterval(function(){location.reload()},3000);
               });
//              
          }
          
          
          ActulizarVehiculo(){
                let url = "../../Ejecutar-clases/Vehiculo/ActulizarVehiculo.php";
                let valor = new FormData($('.formulario_actulizar_vehiculo')[0]);
              let ajax = new Ajax(url,valor);
                  ajax.funcionAjax().done(function (data){
                     $('Respuesta_actlizar_vehiculo').html(data);
                      setInterval(location.reload(),3000);
                  });  

          }
          
          
//          SUBIR IMAGEN DE VEHICULO
          SubirImagenesVehiculo(){
              let subirImagenesVehiculo = new FormData($(".SubirImagenesVehiculo")[0]);
              let url = "../../../Ejecutar-clases/Vehiculo/fotos/SubirFotosVehiculo.php";
              
              let ajax = new Ajax(url,subirImagenesVehiculo);
                  ajax.funcionAjax().done(function(datos){
                        $(".imagenesVehiculos").html(datos);
                  });  
                  
                 
                
             }
             
//     ELIMINAR IMAGEN VEHICULO   
            EliminarImagenVehiculo(idFoto){
                //pasamos pos metodo post id de la foto a la siguiente ruta
                 let url = "../../../Ejecutar-clases/Vehiculo/fotos/EliminarFotoVehiculo.php";
                 
                 $.post(url,{idFoto:idFoto},function(respuesta){
                     alert(respuesta);
                     setInterval(location.reload(),3000);
                 });
              
            }
        
         
        
        
            
        
        
    
    
}

/////////////////////////////////////////////////////////////////////////////////////////

//este sirve para verificar si el estado del vehiculo es propio o no 
$(document).on('change','.propio',function(){
    
   let valor =  $(this).val();
   let contratoVehiculo = new Vehiculo();
       contratoVehiculo.listarContratoActivo(valor);    
   
});

//este sirve para validar si los campos estan vacio 
//si no estan vacio procedera a Guardar 

$(document).on('click','.ingresarVehiculo',function(){
        
        let validarVehiculo = new ValidarCampos();
           if( validarVehiculo.validarletras(".placa") &&
               validarVehiculo.validarLEtrasNumeros(".modelo") &&
               validarVehiculo.validarLEtrasNumeros(".marca") &&
               validarVehiculo.validarLEtrasNumeros(".clase") &&
               validarVehiculo.validarNumeros(".numerovehiculo") &&
               validarVehiculo.validarLEtrasNumeros(".tarjetaoperacion")
               
               ){
                                   
                    //si los campo requeridos esta completo Se hace la instancia a la clase vehiculo
                    let ingresarVehiculo  = new Vehiculo();
                    ingresarVehiculo.IngresarVehiculo();
                }
   
   return false;
});


//esto carga la lista de vehiculos que la empresa tenga guardado 
 $(document).ready(function(){
     
                //lista los vehiculo que estan en la base de datos
                let url = "../../Ejecutar-clases/Vehiculo/ListarVehiculo.php";
                 $('.listarVehiculo').load(url);
                 
                
                //obtenenos el numero de placa que da el formulario que sirve para subir la imagene de los vehiculos    
                let placa = $(".placalistarImagen").val();  
                //en url pasamos la direcion junto con la placa 
                let listarImagenVehiculo = "../../../Ejecutar-clases/Vehiculo/fotos/ListarFotosVehiculo.php?placa="+placa;
                $(".imagenesVehiculosbasededatos").load(listarImagenVehiculo);
               
                
                 
                 

 });

//este sirve para saber que numero de pagina a escojido la persona
$(document).on('click','.paginaVehiculo',function(){
               let valor =  $(this).attr('href');
//               alert(valor);
               let pagina = new Vehiculo();
                   pagina.Cambiarpagina(valor);
               
               return false;
});


//esta function sirve para buscar cada vez que preciona en 
$(document).on('keyup','.BuscarVehiculo',function(){
              let valor = $(this).val();
              
              let buscarRegistro = new Vehiculo();
                  buscarRegistro.BuscarRegistro(valor);  
                 
                 
                 
});

//este tiene como objetivo obtener el numero de la placa para que pueda ser eliminado
    //este llamara a la metodo eliminar por vehiculo por placa de la clase vehiculo
$(document).on('click','.eliminarVehiculo',function(){
                
                        if(confirm("¿ Esta Seguro Que Desea Eliminar Vehiculo ?")){
            //aqui obtenemos el numero de la placa
            let eliminarVehiculo = $(this).attr('href');
            //haciendo instancia a la clase vehiculo
             let vehiculo = new Vehiculo();
             //se le esta pasando el numero de placa por parametro
            vehiculo.EliminarVehiculoPlaca(eliminarVehiculo);  
                        }
    
    
 
             return false;
});

//al precionar sobre el boton actulizar validara que ningun campo este vacio

$(document).on('click','.actulizaVehiculo',function(){
   
            let validar = new ValidarCampos();
            
            if (
                validar.validarCampoVacio(".modeloACT")  &&
                validar.validarCampoVacio(".marcaACT")   &&
                validar.validarCampoVacio(".claseACT")   &&
                validar.validarCampoVacio(".numerovehiculoACT") &&
                validar.validarCampoVacio(".tarjetaoperacionACT")){
            
                   let actulizarVehiculo = new Vehiculo();
                       actulizarVehiculo.ActulizarVehiculo();     
            
                }
      
     return false;
});



//window.open(url,"Actulizar Contrato","width=500,height=350,scrollbars=NO,titlebar=no,left=450,top=160,menubar=no");

$(document).on('click','.actulizarVehiculo',function(){
                
                 let url = $(this).attr('href');
                  window.open(url,"Actulizar Contrato","width=550,height=350,scrollbars=NO,titlebar=no,left=450,top=160,menubar=no");
                  return false;
        
});




//---------------------------------
//    Subir Foto 



//----------------------------------------------
//    SUBIR IAMGENES 

//al hacer click sobre el icono con forma de imagen se despliega una ventana emergente
$(document).on('click','.imagenesVehiculo',function(){
    let placa = $(this).attr("href");
    let url = "FormularioFotosVehiculos/Formulariofotos.php?placa="+placa; 
    
     window.open(url,"Subir Imagenes","width=600,height=350,scrollbars=NO,titlebar=no,left=450,top=160,menubar=no" );
   
                 
   

   return false; 
});

//si se presenta un cambio o se sube una imagen ejecuta este codigo  para que pueda ser subada la imagen
$(document).on('change','.SubirFotoVehiculo',function(){
             let ImageVehiculo = new Vehiculo();
            ImageVehiculo.SubirImagenesVehiculo();
   return false; 
});


//cuendo el usuario quiera eliminar la foto y hace click sobre icono de x 
//esta funcion madara un alerta de confirmacion y Si el usuario confirma hace llamado  o instancia de la clase
//    Vehiculo para pasar los datos para la eliminacion de la foto
$(document).on('click','.EliminarFotoVehiculo',function(){
    if(confirm("Esta seguro que Desea Eliminar la imagen")){
            let idFoto = $(this).attr("href");
            let EliminarFotoVehiculo = new Vehiculo();
            EliminarFotoVehiculo.EliminarImagenVehiculo(idFoto);
        
    }
     
    
        
    
        
        return false;
});

//----------------------------------------------------------------------------


//CUENDO SE PRECIONA SOBRE LINK QUE TIENE FORMA DE DOCUMENTO
$(document).on('click','.documentoVehiculo',function(){
        let placa = $(this).attr("href");
        
        let url = "Documento-Vehiculo-Vista/AgregarDocumentoVehiculoFormulario.php?placa="+placa;
        window.open(url,"Subir Imagenes","width=980,height=400,scrollbars=NO,titlebar=no,left=180,top=120,menubar=no" );
   return false;
});