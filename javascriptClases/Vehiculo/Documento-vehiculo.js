class DocumentoVehiculo {
                
                    //FUNCION AJAX INDEPENDIENTE 
                        ajax(url,valores){
                                  var ajax =  $.ajax({   
                                                url: url, 
                                                data: valores,
                    //                            esto es necesario para subir archivos
                                                cache: false,
                                                contentType: false,
                                                processData: false,
                                                type: "POST"
                                               })
                         return ajax;                      
                        }//fin de la funcion
        
    //------------------------------SOAT------------------------------------//
    //  ESTA FUNCION SIRVE PARA ELIMINAR ARCHIVO EXISTENTE 
                    EliminarSoat(Eliminar_file,placa){
                        let url = "../../../Ejecutar-clases/Vehiculo/Documentos-Vehiculo-Controlador/AgregarSoatVehiculo.php"; 
                        $.post(url,{EliminarSoat:Eliminar_file,placa:placa},function(){
//                            $(".soat-respuesta").html()
                        });
                    }
    
                    //FUNCION PARA ENVAR Y GUARDAR ARCHIVO DE SOAT JUNTO CON SU FECHA
                    //esta funcion se le pasan 4 valores
                    //1- archivo y fecha en formData
                    //2- Dom de respuesta 
                    //3- Dom fecha para activar y desactivar el campo
                    //4- Dom el bottom para activa y desactivar 
                     AgregarSOAT(Dato_file){
                        //pansando la ruta donde se enviar los valores 
                        let url = "../../../Ejecutar-clases/Vehiculo/Documentos-Vehiculo-Controlador/AgregarSoatVehiculo.php"; 
                        //el metodo ajax para enviar 
                        this.ajax(url,Dato_file).done(function(respuesta){
                            if(respuesta){
                            $(".soat-respuesta").css("display","block");    
                            $(".soat-respuesta").html('<img src="../../../img/comprobando/true.svg" width="20">');
                            $(".soat-fecha").prop("readonly",true);
                            $(".soat-enviar").css("display","none");
                            
                             }else{
                              $(".soat-respuesta").css("display","block");              
                              $(".soat-respuesta").html('<img src="../../../img/comprobando/false.svg" width="20">');              
                                        }
                        });
                        
                    }//fin     
    //-----------------------------FIN-SOAT------------------------------------//
    
    
        //-----------------------------------TecnicoMecanica()--------------------//
      //  ESTA FUNCION SIRVE PARA ELIMINAR ARCHIVO EXISTENTE 
            EliminarTecnicoMecanica(Eliminar_file,placa){
                let url = "../../../Ejecutar-clases/Vehiculo/Documentos-Vehiculo-Controlador/AgregarTecnicoMecanicaVehiculo.php"; 
                $.post(url,{EliminarTecnicoMecanica:Eliminar_file,placa:placa},function(){
//                            $(".soat-respuesta").html()
                });
            }
        
        
            AgregarTecnicoMecanica(Dato_file){
                //pansando la ruta donde se enviar los valores 
                        let url = "../../../Ejecutar-clases/Vehiculo/Documentos-Vehiculo-Controlador/AgregarTecnicoMecanicaVehiculo.php"; 
                        //el metodo ajax para enviar 
                        this.ajax(url,Dato_file).done(function(respuesta){
                            if(respuesta){
                            $(".tecnicomecanica_respuesta").css("display","block");    
                            $(".tecnicomecanica_respuesta").html('<img src="../../../img/comprobando/true.svg" width="20">');
                           // $(".tecnicomecanica_respuesta").html(respuesta);
                            $(".tecnicomecanica_fecha").prop("readonly",true);
                            $(".tecnicomecanica_enviar").css("display","none");
                            
                             }else{
                              $(".tecnicomecanica_respuesta").css("display","block");              
                              $(".tecnicomecanica_respuesta").html('<img src="../../../img/comprobando/false.svg" width="20">');              
                              $(".tecnicomecanica_respuesta").html(respuesta);
                            }
                        });
                    }//fin 
            
            
             //----------------------------------FIN -TecnicoMecanica()--------------------//
            
            //-------------------------------------Mantenimento---------------------------------------//
                             //  ESTA FUNCION SIRVE PARA ELIMINAR ARCHIVO EXISTENTE 
            EliminarMantenimento(Eliminar_file,placa){
                let url = "../../../Ejecutar-clases/Vehiculo/Documentos-Vehiculo-Controlador/AgregarMantenimentoVehiculo.php";  
                $.post(url,{EliminarMantenimento:Eliminar_file,placa:placa},function(){
//                            $(".soat-respuesta").html()
                });
            }
    
    
            AgregarMantenimento(Dato_file){


                         //pansando la ruta donde se enviar los valores 
                                let url = "../../../Ejecutar-clases/Vehiculo/Documentos-Vehiculo-Controlador/AgregarMantenimentoVehiculo.php"; 
                                //el metodo ajax para enviar 
                                this.ajax(url,Dato_file).done(function(respuesta){
                                    if(respuesta){
                                    $(".mantenimento_respuesta").css("display","block");    
                                    $(".mantenimento_respuesta").html('<img src="../../../img/comprobando/true.svg" width="20">');
                                    //$(".mantenimento_respuesta").html(respuesta);
                                    $(".mantenimento_fecha").prop("readonly",true);
                                    $(".mantenimento_enviar").css("display","none");
                                    
                                     }else{$(".mantenimento_respuesta").css("display","block");              
                                      $(".mantenimento_respuesta").html('<img src="../../../img/comprobando/false.svg" width="20">');              
                                      $(".mantenimento_respuesta").html(respuesta);
                                    }
                                });

                    }// fin 
                    //-----------------------------fin Mantenimento-------------------//
            
            //----------------------------Mantenimento Correctivo----------------------------//
             //  ESTA FUNCION SIRVE PARA ELIMINAR ARCHIVO EXISTENTE 
            EliminarMantenimentoCorrectivo(Eliminar_file,placa){
                let url = "../../../Ejecutar-clases/Vehiculo/Documentos-Vehiculo-Controlador/AgregarMantenimentoCorrectivoVehiculo.php";
                $.post(url,{EliminarMantenimentoCorrectivo:Eliminar_file,placa:placa},function(){
//                            $(".soat-respuesta").html()
                });
            }
            
            
            
            AgregarMantenimentoCorrectivo(Dato_file){
                       
                        //pansando la ruta donde se enviar los valores 
                        let url = "../../../Ejecutar-clases/Vehiculo/Documentos-Vehiculo-Controlador/AgregarMantenimentoCorrectivoVehiculo.php"; 
                        //el metodo ajax para enviar 
                        this.ajax(url,Dato_file).done(function(respuesta){
                            if(respuesta){
                            $(".mantenimentoCorrectivo_respuesta").css("display","block");    
                            $(".mantenimentoCorrectivo_respuesta").html('<img src="../../../img/comprobando/true.svg" width="20">');
                           // $(".mantenimentoCorrectivo_respuesta").html(respuesta);
                            $(".mantenimentoCorrectivo_fecha").prop("readonly",true);
                            $(".mantenimentoCorrectivo_enviar").css("display","none");
                            setInterval(setTimeout(location.reload()),2000);
                             }else{
                                 $(".mantenimentoCorrectivo_respuesta").css("display","block");              
                              $(".mantenimentoCorrectivo_respuesta").html('<img src="../../../img/comprobando/false.svg" width="20">');              
                              $(".mantenimentoCorrectivo_respuesta").html(respuesta);
                            }
                        });
                
            }
    
            //---------------------------fin Mantenimento Correctivo--------------------------------//
    
            //--------------------------TARJETA DE OPERACION------------------------------//    
                         //  ESTA FUNCION SIRVE PARA ELIMINAR ARCHIVO EXISTENTE 
            EliminarTarjetaOperacion(Eliminar_file,placa){
                let  url = "../../../Ejecutar-clases/Vehiculo/Documentos-Vehiculo-Controlador/AgregarTarjetaOperacionVehiculo.php"; 
                $.post(url,{EliminartarjetaOperacion:Eliminar_file,placa:placa},function(){
//                            $(".soat-respuesta").html()
                });
            }
            
            AgregarTarjetaOperacion(Dato_file){
                
                //pansando la ruta donde se enviar los valores 
                        let url = "../../../Ejecutar-clases/Vehiculo/Documentos-Vehiculo-Controlador/AgregarTarjetaOperacionVehiculo.php"; 
                        //el metodo ajax para enviar 
                        this.ajax(url,Dato_file).done(function(respuesta){
                            if(respuesta){
                            $(".tarjeta_de_operacion_respuesta").css("display","block");    
                            $(".tarjeta_de_operacion_respuesta").html('<img src="../../../img/comprobando/true.svg" width="20">');
//                            $(".tarjeta_de_operacion_respuesta").html(respuesta);
                            $(".tarjeta_de_operacion_fecha").prop("readonly",true);
                            $(".tarjeta_de_operacion_enviar").css("display","none");
                            
                             }else{
                                 $(".tarjeta_de_operacion_respuesta").css("display","block");              
                             $(".tarjeta_de_operacion_respuesta").html('<img src="../../../img/comprobando/false.svg" width="20">');              
                              $(".tarjeta_de_operacion_respuesta").html(respuesta);
                            }
                        });
               
            }
    
            //---------------------------FIN TARJETA DE OPERACION---------------------------//
            
            
            //---------------------------TARJETA DE PROPIEDAD---------------------------------------//
                                    //  ESTA FUNCION SIRVE PARA ELIMINAR ARCHIVO EXISTENTE 
            EliminarTarjetaPropiedad(Eliminar_file,placa){
                let  url = "../../../Ejecutar-clases/Vehiculo/Documentos-Vehiculo-Controlador/AgregarTarjetaPropiedadVehiculo.php";  
                $.post(url,{EliminarTarjetaPropiedad:Eliminar_file,placa:placa},function(){
//                            $(".soat-respuesta").html()
                });
            } 
    
    
            AgregarTarjetaPropiedad(Dato_file){
                
                //pansando la ruta donde se enviar los valores 
                        let url = "../../../Ejecutar-clases/Vehiculo/Documentos-Vehiculo-Controlador/AgregarTarjetaPropiedadVehiculo.php"; 
                        //el metodo ajax para enviar 
                        this.ajax(url,Dato_file).done(function(respuesta){
                            if(respuesta){
                            $(".tarjeta_de_propiedad_respuesta").css("display","block");    
                            $(".tarjeta_de_propiedad_respuesta").html('<img src="../../../img/comprobando/true.svg" width="20">');
                           // $(".tarjeta_de_propiedad_respuesta").html(respuesta);
                            $(".tarjeta_de_propiedad_fecha").prop("readonly",true);
                            $(".tarjeta_de_propiedad_enviar").css("display","none");
                            
                             }else{
                                 $(".tarjeta_de_propiedad_respuesta").css("display","block");              
                              $(".tarjeta_de_propiedad_respuesta").html('<img src="../../../img/comprobando/false.svg" width="20">');              
                              $(".tarjeta_de_propiedad_respuesta").html(respuesta);
                            }
                        });
                
                
                
            }
            //-------------------------FIN--TARJETA DE PROPIEDAD---------------------------------------//
            

            //-------------------------------------SIMIT---------------------------------------//
            //  ESTA FUNCION SIRVE PARA ELIMINAR ARCHIVO EXISTENTE 
            EliminarSimit(Eliminar_file,placa){
                let  url = "../../../Ejecutar-clases/Vehiculo/Documentos-Vehiculo-Controlador/AgregarSimitVehiculo.php";  
                $.post(url,{EliminarSimit:Eliminar_file,placa:placa},function(){
//                            $(".soat-respuesta").html()
                });
            } 
            
            
            AgregarSimit(Dato_file){
                
                //pansando la ruta donde se enviar los valores 
                        let url = "../../../Ejecutar-clases/Vehiculo/Documentos-Vehiculo-Controlador/AgregarSimitVehiculo.php"; 
                        //el metodo ajax para enviar 
                        this.ajax(url,Dato_file).done(function(respuesta){
                            if(respuesta){
                            $(".simit_respuesta").css("display","block");    
                           $(".simit_respuesta").html('<img src="../../../img/comprobando/true.svg" width="20">');
//                            $(".simit_respuesta").html(respuesta);
                            $(".simit_fecha").prop("readonly",true);
                            $(".simit_enviar").css("display","none");
                            
                             }else{
                               $(".simit_respuesta").css("display","block");              
                            $(".simit_respuesta").html('<img src="../../../img/comprobando/false.svg" width="20">');              
                              $(".simit_respuesta").html(respuesta);
                            }
                        });
                
               }
               //---------------------------------FIN----SIMIT---------------------------------------//
               
               //-------------------------------------SUPERTRANSPORTE--------------------------------//
                           //  ESTA FUNCION SIRVE PARA ELIMINAR ARCHIVO EXISTENTE 
            EliminarSupertrasporte(Eliminar_file,placa){
                let  url = "../../../Ejecutar-clases/Vehiculo/Documentos-Vehiculo-Controlador/AgregarSupertrasporteVehiculo.php";   
                $.post(url,{EliminarSupertrasporte:Eliminar_file,placa:placa},function(){
//                            $(".soat-respuesta").html()
                });
            } 
    
                AgregarSupertrasporte(Dato_file){
                 
                 //pansando la ruta donde se enviar los valores 
                        let url = "../../../Ejecutar-clases/Vehiculo/Documentos-Vehiculo-Controlador/AgregarSupertrasporteVehiculo.php"; 
                        //el metodo ajax para enviar 
                        this.ajax(url,Dato_file).done(function(respuesta){
                            if(respuesta){
                            $(".supertrasporte_respuesta").css("display","block");    
                            $(".supertrasporte_respuesta").html('<img src="../../../img/comprobando/true.svg" width="20">');
//                            $(".supertrasporte_respuesta").html(respuesta);
                            $(".supertrasporte_fecha").prop("readonly",true);
                            $(".supertrasporte_enviar").css("display","none");
                             }else{
                                 $(".supertrasporte_respuesta").css("display","block");              
                              $(".supertrasporte_respuesta").html('<img src="../../../img/comprobando/false.svg" width="20">');              
                              $(".supertrasporte_respuesta").html(respuesta);
                            }
                        });
                 
             }  
            
            
            //---------------------------------FIN----SUPERTRANSPORTE--------------------------------//


}//fin de la clase

//------------------------------SOAT------------------------------------//
$(document).on('change','.soat',function(){
                //si hay cambio 
                if($(".soat").val().length > 0 ){
                   //si hay cambio se activa el campo de la fecha
                   $(".soat-fecha").prop("readonly",false);
                   $(".soat-fecha").css("border","solid 2px #5d9b9b");
                    
                 }else{
                   //pero si no hay valor se desactiva el campo de la fecha  
                   $(".soat-fecha").prop("readonly",true);
                   //y el campo de vacia
                    $(".soat-fecha").val("");
                   //se desactiva el boton para guardar
                   $(".soat-enviar").css("display","none");
                   $(".soat-fecha").css("border","solid 1px");
                 }
                 $(".soat-fecha").change(function(){
                          if($(".soat-fecha").val().length > 0) {
                     $(".soat-enviar").css("display","block");
                             }else{
                                 $(".soat-enviar").css("display","none");
                             }
                 });
                   
   
});
//cuendo se esta cargado el archivo y esta colocada la fecha
$(document).on('click',".soat-enviar",function(){
        
    
        //obteniendo la fecha
        let soat_fecha = $(".soat-fecha").val();
        //llamando la clase formData para enviar el archivo y la fecha
        let soat_file = new FormData();
            //mandando el valor del archivo
            soat_file.append("pdf_soat",$('.soat')[0].files[0]);
            //mandando la fecha del soat
            soat_file.append("fecha_soat",soat_fecha);
                        
            //obteniendo placa del vehiculo 
            soat_file.append("placa",$(".placa").val());
        //haciendo instancia del la clase DocumentoVehiculo y el metodo para enviar la informacion 
        let soat = new DocumentoVehiculo();
            soat.AgregarSOAT(soat_file);
    
});
//--------------------------SOAT FIN----------------------------------------//

//------------------------------TECNICO MECANICA-----------------------------------------//

$(document).on('change','.tecnicomecanica',function(){
                //si hay cambio 
                if($(".tecnicomecanica").val().length > 0 ){
                   //si hay cambio se activa el campo de la fecha
                   $(".tecnicomecanica_fecha").prop("readonly",false);
                   $(".tecnicomecanica_fecha").css("border","solid 2px #5d9b9b");
                    
                 }else{
                   //pero si no hay valor se desactiva el campo de la fecha  
                   $(".tecnicomecanica_fecha").prop("readonly",true);
                   //y el campo de vacia
                    $(".tecnicomecanica_fecha").val("");
                   //se desactiva el boton para guardar
                   $(".tecnicomecanica_enviar").css("display","none");
                   $(".tecnicomecanica_fecha").css("border","solid 1px");
                 }
                 $(".tecnicomecanica_fecha").change(function(){
                          if($(".tecnicomecanica_fecha").val().length > 0) {
                     $(".tecnicomecanica_enviar").css("display","block");
                             }else{
                                 $(".tecnicomecanica_enviar").css("display","none");
                             }
                 });
                   
   
});
//cuendo se esta cargado el archivo y esta colocada la fecha
$(document).on('click',".tecnicomecanica_enviar",function(){
        
    
        //obteniendo la fecha
        let tecnicomecanica_fecha = $(".tecnicomecanica_fecha").val();
        //llamando la clase formData para enviar el archivo y la fecha
        let tecnicomecanica_file = new FormData();
            //mandando el valor del archivo
            tecnicomecanica_file.append("pdf_tecnicomecanica",$('.tecnicomecanica')[0].files[0]);
            //mandando la fecha del soat
            tecnicomecanica_file.append("fecha_tecnicomecanica",tecnicomecanica_fecha);
                        
            //obteniendo placa del vehiculo 
            tecnicomecanica_file.append("placa",$(".placa").val());
        //haciendo instancia del la clase DocumentoVehiculo y el metodo para enviar la informacion 
        let tecnicomecanica = new DocumentoVehiculo();
           tecnicomecanica.AgregarTecnicoMecanica(tecnicomecanica_file);
    
});




//------------------------------FIN MECANICA-----------------------------------------//


//------------------------------MANTENIMIENTO----------------------------------------//


$(document).on('change','.mantenimento',function(){
                //si hay cambio 
                if($(".mantenimento").val().length > 0 ){
                   //si hay cambio se activa el campo de la fecha
                   $(".mantenimento_fecha").prop("readonly",false);
                   $(".mantenimento_fecha").css("border","solid 2px #5d9b9b");
                    
                 }else{
                   //pero si no hay valor se desactiva el campo de la fecha  
                   $(".mantenimento_fecha").prop("readonly",true);
                   //y el campo de vacia
                    $(".mantenimento_fecha").val("");
                   //se desactiva el boton para guardar
                   $(".mantenimento_enviar").css("display","none");
                   $(".mantenimento_fecha").css("border","solid 1px");
                 }
                 $(".mantenimento_fecha").change(function(){
                          if($(".mantenimento_fecha").val().length > 0) {
                     $(".mantenimento_enviar").css("display","block");
                             }else{
                                 $(".mantenimento_enviar").css("display","none");
                             }
                 });
                   
   
});
//cuendo se esta cargado el archivo y esta colocada la fecha
$(document).on('click',".mantenimento_enviar",function(){
          
        
        //obteniendo la fecha
        let mantenimento_fecha = $(".mantenimento_fecha").val();
        //llamando la clase formData para enviar el archivo y la fecha
        let mantenimento_file = new FormData();
            //mandando el valor del archivo
            mantenimento_file.append("pdf_mantenimento",$('.mantenimento')[0].files[0]);
            //mandando la fecha del soat
            mantenimento_file.append("fecha_mantenimento",mantenimento_fecha);
                        
            //obteniendo placa del vehiculo 
            mantenimento_file.append("placa",$(".placa").val());
        //haciendo instancia del la clase DocumentoVehiculo y el metodo para enviar la informacion 
        let mantenimento = new DocumentoVehiculo();
           mantenimento.AgregarMantenimento(mantenimento_file);
    
});



//-----------------------------FIN MANTENIMIENTO----------------------------------//

                                        
///-----------------------------------Mantenimento Correctivo------------------------------///


$(document).on('change','.mantenimentoCorrectivo',function(){
                //si hay cambio 
                if($(".mantenimentoCorrectivo").val().length > 0 ){
                   //si hay cambio se activa el campo de la fecha
                   $(".mantenimentoCorrectivo_fecha").prop("readonly",false);
                   $(".mantenimentoCorrectivo_fecha").css("border","solid 2px #5d9b9b");
                    
                 }else{
                   //pero si no hay valor se desactiva el campo de la fecha  
                   $(".mantenimentoCorrectivo_fecha").prop("readonly",true);
                   //y el campo de vacia
                    $(".mantenimentoCorrectivo_fecha").val("");
                   //se desactiva el boton para guardar
                   $(".mantenimentoCorrectivo_enviar").css("display","none");
                   $(".mantenimentoCorrectivo_fecha").css("border","solid 1px");
                 }
                 $(".mantenimentoCorrectivo_fecha").change(function(){
                          if($(".mantenimentoCorrectivo_fecha").val().length > 0) {
                     $(".mantenimentoCorrectivo_enviar").css("display","block");
                             }else{
                                 $(".mantenimentoCorrectivo_enviar").css("display","none");
                             }
                 });
                   
   
});
//cuendo se esta cargado el archivo y esta colocada la fecha
$(document).on('click',".mantenimentoCorrectivo_enviar",function(){
          
        
        //obteniendo la fecha
        let mantenimentoCorrectivo_fecha = $(".mantenimentoCorrectivo_fecha").val();
        //llamando la clase formData para enviar el archivo y la fecha
        let mantenimentoCorrectivo_file = new FormData();
            //mandando el valor del archivo
            mantenimentoCorrectivo_file.append("pdf_mantenimentoCorrectivo",$('.mantenimentoCorrectivo')[0].files[0]);
            //mandando la fecha del soat
            mantenimentoCorrectivo_file.append("fecha_mantenimentoCorrectivo",mantenimentoCorrectivo_fecha);
                        
            //obteniendo placa del vehiculo 
            mantenimentoCorrectivo_file.append("placa",$(".placa").val());
        //haciendo instancia del la clase DocumentoVehiculo y el metodo para enviar la informacion 
        let mantenimentoCorrectivo = new DocumentoVehiculo();
           mantenimentoCorrectivo.AgregarMantenimentoCorrectivo(mantenimentoCorrectivo_file);
    
});



///----------------------------------FIN Mantenimento Correctivo--------------------------///


//---------------------------------------TARJETA DE OPERACION---------------------------------------//

$(document).on('change','.tarjeta_de_operacion',function(){
                //si hay cambio 
                if($(".tarjeta_de_operacion").val().length > 0 ){
                   //si hay cambio se activa el campo de la fecha
                   $(".tarjeta_de_operacion_fecha").prop("readonly",false);
                   $(".tarjeta_de_operacion_fecha").css("border","solid 2px #5d9b9b");
                    
                 }else{
                   //pero si no hay valor se desactiva el campo de la fecha  
                   $(".tarjeta_de_operacion_fecha").prop("readonly",true);
                   //y el campo de vacia
                    $(".tarjeta_de_operacion_fecha").val("");
                   //se desactiva el boton para guardar
                   $(".tarjeta_de_operacion_enviar").css("display","none");
                   $(".tarjeta_de_operacion_fecha").css("border","solid 1px");
                 }
                 $(".tarjeta_de_operacion_fecha").change(function(){
                          if($(".tarjeta_de_operacion_fecha").val().length > 0) {
                     $(".tarjeta_de_operacion_enviar").css("display","block");
                             }else{
                                 $(".tarjeta_de_operacion_enviar").css("display","none");
                             }
                 });
                   
   
});
//cuendo se esta cargado el archivo y esta colocada la fecha
$(document).on('click',".tarjeta_de_operacion_enviar",function(){
          
        
        //obteniendo la fecha
        let tarjeta_de_operacion_fecha = $(".tarjeta_de_operacion_fecha").val();
        //llamando la clase formData para enviar el archivo y la fecha
        let tarjeta_de_operacion_file = new FormData();
            //mandando el valor del archivo
            tarjeta_de_operacion_file.append("pdf_tarjeta_de_operacion",$('.tarjeta_de_operacion')[0].files[0]);
            //mandando la fecha del soat
            tarjeta_de_operacion_file.append("fecha_tarjeta_de_operacion",tarjeta_de_operacion_fecha);
                        
            //obteniendo placa del vehiculo 
            tarjeta_de_operacion_file.append("placa",$(".placa").val());
        //haciendo instancia del la clase DocumentoVehiculo y el metodo para enviar la informacion 
        let tarjeta_de_operacion = new DocumentoVehiculo();
           tarjeta_de_operacion.AgregarTarjetaOperacion(tarjeta_de_operacion_file);
    
});



//-------------------------------------FIN TARJETA DE OPERACION----------------------------------------//

//--------------------------------------------TARJETA DE PROPIEDAD---------------------------------------//

$(document).on('change','.tarjeta_de_propiedad',function(){
                //si hay cambio 
                if($(".tarjeta_de_propiedad").val().length > 0 ){
                   //si hay cambio se activa el campo de la fecha
                   $(".tarjeta_de_propiedad_fecha").prop("readonly",false);
                   $(".tarjeta_de_propiedad_fecha").css("border","solid 2px #5d9b9b");
                    
                 }else{
                   //pero si no hay valor se desactiva el campo de la fecha  
                   $(".tarjeta_de_propiedad_fecha").prop("readonly",true);
                   //y el campo de vacia
                    $(".tarjeta_de_propiedad_fecha").val("");
                   //se desactiva el boton para guardar
                   $(".tarjeta_de_propiedad_enviar").css("display","none");
                   $(".tarjeta_de_propiedad_fecha").css("border","solid 1px");
                 }
                 $(".tarjeta_de_propiedad_fecha").change(function(){
                          if($(".tarjeta_de_propiedad_fecha").val().length > 0) {
                     $(".tarjeta_de_propiedad_enviar").css("display","block");
                             }else{
                                 $(".tarjeta_de_propiedad_enviar").css("display","none");
                             }
                 });
                   
   
});
//cuendo se esta cargado el archivo y esta colocada la fecha
$(document).on('click',".tarjeta_de_propiedad_enviar",function(){
          
        
        //obteniendo la fecha
        let tarjeta_de_propiedad_fecha = $(".tarjeta_de_propiedad_fecha").val();
        //llamando la clase formData para enviar el archivo y la fecha
        let tarjeta_de_propiedad_file = new FormData();
            //mandando el valor del archivo
            tarjeta_de_propiedad_file.append("pdf_tarjeta_de_propiedad",$('.tarjeta_de_propiedad')[0].files[0]);
            //mandando la fecha del soat
            tarjeta_de_propiedad_file.append("fecha_tarjeta_de_propiedad",tarjeta_de_propiedad_fecha);
                        
            //obteniendo placa del vehiculo 
            tarjeta_de_propiedad_file.append("placa",$(".placa").val());
        //haciendo instancia del la clase DocumentoVehiculo y el metodo para enviar la informacion 
        let tarjeta_de_propiedad = new DocumentoVehiculo();
           tarjeta_de_propiedad.AgregarTarjetaPropiedad(tarjeta_de_propiedad_file);
    
});



//------------------------------------------FIN--TARJETA DE PROPIEDAD----------------------------------------------//

//---------------------------------------------SIMIT------------------------------------------------//

$(document).on('change','.simit',function(){
                //si hay cambio 
                if($(".simit").val().length > 0 ){
                   //si hay cambio se activa el campo de la fecha
                   $(".simit_fecha").prop("readonly",false);
                   $(".simit_fecha").css("border","solid 2px #5d9b9b");
                    
                 }else{
                   //pero si no hay valor se desactiva el campo de la fecha  
                   $(".simit_fecha").prop("readonly",true);
                   //y el campo de vacia
                    $(".simit_fecha").val("");
                   //se desactiva el boton para guardar
                   $(".simit_enviar").css("display","none");
                   $(".simit_fecha").css("border","solid 1px");
                 }
                 $(".simit_fecha").change(function(){
                          if($(".simit_fecha").val().length > 0) {
                     $(".simit_enviar").css("display","block");
                             }else{
                                 $(".simit_enviar").css("display","none");
                             }
                 });
                   
   
});
//cuendo se esta cargado el archivo y esta colocada la fecha
$(document).on('click',".simit_enviar",function(){
          
        
        //obteniendo la fecha
        let simit_fecha = $(".simit_fecha").val();
        //llamando la clase formData para enviar el archivo y la fecha
        let simit_file = new FormData();
            //mandando el valor del archivo
            simit_file.append("pdf_simit",$('.simit')[0].files[0]);
            //mandando la fecha del soat
            simit_file.append("fecha_simit",simit_fecha);
                        
            //obteniendo placa del vehiculo 
            simit_file.append("placa",$(".placa").val());
        //haciendo instancia del la clase DocumentoVehiculo y el metodo para enviar la informacion 
        let simit = new DocumentoVehiculo();
            simit.AgregarSimit(simit_file);
    
});



//-----------------------------------------FIN--SIMIT------------------------------------------------//

//-----------------------------------------SUPERTRANSPORTE--------------------------------------------//

$(document).on('change','.supertrasporte',function(){
                //si hay cambio 
                if($(".supertrasporte").val().length > 0 ){
                   //si hay cambio se activa el campo de la fecha
                   $(".supertrasporte_fecha").prop("readonly",false);
                   $(".supertrasporte_fecha").css("border","solid 2px #5d9b9b");
                    
                 }else{
                   //pero si no hay valor se desactiva el campo de la fecha  
                   $(".supertrasporte_fecha").prop("readonly",true);
                   //y el campo de vacia
                    $(".supertrasporte_fecha").val("");
                   //se desactiva el boton para guardar
                   $(".supertrasporte_enviar").css("display","none");
                   $(".supertrasporte_fecha").css("border","solid 1px");
                 }
                 $(".supertrasporte_fecha").change(function(){
                          if($(".supertrasporte_fecha").val().length > 0) {
                     $(".supertrasporte_enviar").css("display","block");
                             }else{
                                 $(".supertrasporte_enviar").css("display","none");
                             }
                 });
                   
   
});
//cuendo se esta cargado el archivo y esta colocada la fecha
$(document).on('click',".supertrasporte_enviar",function(){
          
        
        //obteniendo la fecha
        let supertrasporte_fecha = $(".supertrasporte_fecha").val();
        //llamando la clase formData para enviar el archivo y la fecha
        let supertrasporte_file = new FormData();
            //mandando el valor del archivo
            supertrasporte_file.append("pdf_supertrasporte",$('.supertrasporte')[0].files[0]);
            //mandando la fecha del soat
            supertrasporte_file.append("fecha_supertrasporte",supertrasporte_fecha);
                        
            //obteniendo placa del vehiculo 
            supertrasporte_file.append("placa",$(".placa").val());
        //haciendo instancia del la clase DocumentoVehiculo y el metodo para enviar la informacion 
        let supertrasporte = new DocumentoVehiculo();
            supertrasporte.AgregarSupertrasporte(supertrasporte_file);
    
});




//---------------------------------------FIN --SUPERTRANSPORTE---------------------------------------------//


//ESTA SECCION DEL CODIGO VALIDA SI HAY UN PDF MONTADO 
//ESTO CON EL FIN DE HABILITAR O DESABILITAR EL IMPUT PARA ELIMINAR
//EL QUE ESTA MOSNTANDO EN LA CARPETA Y DEJAR EL ESPACIO PARA EL NUEVO DOCUMENTO



//eiminado el archivo exitente  SOAT
$('.Remplzar_soat').click(function(){
    
    
        if(confirm('Ingresar o Remplazar Un Arhivo Exixtente ?')){
            $('.soat').attr('disabled',false);
             //obteniendo la ruta del archivo existente
           let archivo = $('#Pdf_soat').attr('href');
           let placa = $(".placa").val();
           let EliminarSoat = new  DocumentoVehiculo();
               EliminarSoat.EliminarSoat(archivo,placa);
               $('#Pdf_soat').css('visibility','hidden');
               return false;
        }
 
});

//eiminado el archivo exitente TecnoMecanica
$('.Remplzar_tecnicomecanica').click(function(){
    
    //pregunando si el usuario a eliminar el arcivo
        if(confirm('Ingresar o Remplazar Un Arhivo Exixtente ?')){
            $('.tecnicomecanica').attr('disabled',false);
             //obteniendo la ruta del archivo existente
           let archivo = $('#Pdf_tecnicomecanica').attr('href');
           let placa = $(".placa").val();
           let Eliminar = new  DocumentoVehiculo();
               Eliminar.EliminarTecnicoMecanica(archivo,placa);
               $('#Pdf_tecnicomecanica').css('visibility','hidden');
               return false;
        }
 
});

//eiminado el archivo exitente mantenimento
$('.Remplzar_mantenimento').click(function(){
    
    //pregunando si el usuario a eliminar el arcivo
        if(confirm('Ingresar o Remplazar Un Arhivo Exixtente ?')){
            $('.mantenimento').attr('disabled',false);
            //obteniendo la ruta del archivo existente
           let archivo = $('#Pdf_mantenimento').attr('href');
           let placa = $(".placa").val();
           let Eliminar = new  DocumentoVehiculo();
               Eliminar.EliminarMantenimento(archivo,placa);
               $('#Pdf_mantenimento').css('visibility','hidden');
               return false;
        }
 
});


//eiminado el archivo exitente mantenimentoCorrectivo
$('.Remplzar_mantenimentoCorrectivo').click(function(){
    
    //pregunando si el usuario a eliminar el arcivo
        if(confirm('Ingresar o Remplazar Un Arhivo Exixtente ?')){
            $('.mantenimentoCorrectivo').attr('disabled',false);
            //obteniendo la ruta del archivo existente
           let archivo = $('#Pdf_mantenimentoCorrectivo').attr('href');
           let placa = $(".placa").val();
           let Eliminar = new  DocumentoVehiculo();
               Eliminar.EliminarMantenimentoCorrectivo(archivo,placa);
               $('#Pdf_mantenimentoCorrectivo').css('visibility','hidden');
               return false;
        }
 
});



//eiminado el archivo exitente tarjeta operacion
$('.Remplzartarjetaoperacion').click(function(){

//    //pregunando si el usuario a eliminar el arcivo
        if(confirm('Ingresar o Remplazar Un Arhivo Exixtente ?')){
            $('.tarjeta_de_operacion').attr('disabled',false);
            //obteniendo la ruta del archivo existente
           let archivo = $('#Pdftarjetaoperacion').attr('href');
           let placa = $(".placa").val();
           let Eliminar = new  DocumentoVehiculo();
               Eliminar.EliminarTarjetaOperacion(archivo,placa);
               $('#Pdftarjetaoperacion').css('visibility','hidden');
               return false;
        }
 
});


//eiminado el archivo exitente tarjeta_de_propiedad
$('.Remplazar_tarjeta_de_propiedad').click(function(){

//    //pregunando si el usuario a eliminar el arcivo
        if(confirm('Ingresar o Remplazar Un Arhivo Exixtente ?')){
            $('.tarjeta_de_propiedad').attr('disabled',false);
            //obteniendo la ruta del archivo existente
           let archivo = $('#Pdf_tarjeta_de_propiedad').attr('href');
           let placa = $(".placa").val();
           let Eliminar = new  DocumentoVehiculo();
               Eliminar.EliminarTarjetaPropiedad(archivo,placa);
               $('#Pdf_tarjeta_de_propiedad').css('visibility','hidden');
               return false;
        }
 
});



//eiminado el archivo exitente simit
$('.Remplazar_Simit').click(function(){

//    //pregunando si el usuario a eliminar el arcivo
        if(confirm('Ingresar o Remplazar Un Arhivo Exixtente ?')){
            $('.simit').attr('disabled',false);
            //obteniendo la ruta del archivo existente
           let archivo = $('#Pdf_simit').attr('href');
           let placa = $(".placa").val();
           let Eliminar = new  DocumentoVehiculo();
               Eliminar.EliminarSimit(archivo,placa);
               $('#Pdf_simit').css('visibility','hidden');
               return false;
        }
 
});



//eiminado el archivo exitente simit
$('.Remplazar_supertrasporte').click(function(){

//    //pregunando si el usuario a eliminar el arcivo
        if(confirm('Ingresar o Remplazar Un Arhivo Exixtente ?')){
            $('.supertrasporte').attr('disabled',false);
            //obteniendo la ruta del archivo existente
           let archivo = $('#Pdf_supertrasporte').attr('href');
           let placa = $(".placa").val();
           let Eliminar = new  DocumentoVehiculo();
               Eliminar.EliminarSupertrasporte(archivo,placa);
               $('#Pdf_supertrasporte').css('visibility','hidden');
               return false;
        }
 
});



