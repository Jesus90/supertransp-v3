class Contrato{
        
        //esta funcion sirve para ingresar contrato
         IngrsarContrato(){
//             alert("todo bien hasta qui");
            let url = "../../Ejecutar-clases/Contrato/IngresarContrato.php";
            let valor = new FormData($(".ingresar-contrato")[0]);
            
            $.ajax({
                    url: url,
                    type: "post",
                    dataType: "html",
                    data: valor,
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function(res){
                        if(res){
            $(".respuestasInsertarContrato").html(res);
                          }else{
            $(".respuestasInsertarContrato").html(res);

                        }
                         });
                           
        }
        
        //esta funcion o metodo tiene con fin listar los contrato que la empresa tiene
        ListarContrato(){
            $(".listar-contrato").load("../../Ejecutar-clases/Contrato/Listar-contrato.php");
        }
        
        //metodo eliminar contrato -> solo con pasar el numero del contrato se elimina 
        EliminarContrato(numeroContrato){
           let url = "../../Ejecutar-clases/Contrato/EliminarContrato.php";
            //este ajax es para enviar datos atravez de json
            $.post(url,{datos_json:numeroContrato},function(data){
                alert(data);
            });
            
//            
                return false;
             }
             
          //-----------------------------------------------------------------   
         //esta funcion tiene con fin pasar el numero del contrato para que sea buscado en base de datos
         ListarDatosActulizar(valor){
             
                //esto redireciona a la persona a un formulario para actulizar los registros
                    let url = "Actulizar-Contrato.php?numeroContrato="+valor;
//                        location.href=url;
                        window.open(url,"Actulizar Contrato","width=500,height=350,scrollbars=NO,titlebar=no,left=450,top=160,menubar=no");
                        
                 }
                 
                 
         ActulizarContrato(){
             
            let url = "../../Ejecutar-clases/Contrato/ActulizarContrato.php";
            let valor = new FormData($("#actulizar_contrato")[0]);
            
            $.ajax({
                    url: url,
                    type: "post",
                    dataType: "html",
                    data: valor,
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function(ress){
                        if(ress){
                            $(".respuesta_Actulizar_Contrato").html(ress);
                          }else {
                            $(".respuesta_Actulizar_Contrato").html(ress);

                        }
                         });
              
            
                   
                   
               
//                return false;
          }   
          
                   
    
    
}


///////////////////////////////
//FIN DE LA CLASE ACTULIZAR////
///////////////////////////////

//esto se ejecuta para listar los contrato que tiene la empresa
$(document).ready(function(){
    let listarContrato = new Contrato();
           listarContrato.ListarContrato(); 
           
           
//     let listarDatosContrato = new Contrato();
//         listarDatosContrato.recartarDatos();   
       
});


//ests evento se ejecuta oara guardar un contrato
$(document).on('click','.ingresarContrato',function(){
            //haciendo instancia de la clase validar campo para verificar que los campos importantes no se ballan vacios
            let validarFormulario = new ValidarCampos();
            
                if(validarFormulario.validarCampoVacio(".numerocontrato") &&
                   validarFormulario.validarCampoVacio(".contratante") && 
                   validarFormulario.validarCampoVacio(".objetocontrato") &&
                   validarFormulario.validarCampoVacio(".empresacontratada") &&
                   validarFormulario.validarCampoVacio(".fechainicial") &&
                   validarFormulario.validarCampoVacio(".fechavencimineto") &&
                   validarFormulario.validarCampoVacio(".descriptcioncontrato") &&
                   validarFormulario.validarFormato(".contratopdf",".pdf")){
                    //despues de validar que no esten vacio los campo 
                    
                    //haciendo instancia de la clase contro 
                    //para luego llamar el metodo ingesar contrato 
                    
                    let IngresarContrato = new Contrato();
                        IngresarContrato.IngrsarContrato();
        
                    
                    
                }
    return false;
});


$(document).on('click','.eliminarContrato',function(){
    
            if(confirm("¿ Esta Seguro Que Desea Eliminar Contrato ?")){
               //con este script se obtiene el valor de la etiqueta a href
                let valor = $(this).attr('href');
                    //luego de obtener el valor de la etiqueta, se hace instancia de la clase Contrato y el
                    //metodo elimiarContrato y se le pasa el valor
                let eliminarContrato = new Contrato();
                    eliminarContrato.EliminarContrato(valor);
                    setInterval(location.reload(),1000);
            }
              return false;
});


$(document).on('click','.actulizarContrato',function(){
        
            let valor = $(this).attr('href');
            
              let actulizarContrato  = new Contrato();
                  actulizarContrato.ListarDatosActulizar(valor);  
                return false;  
});

//esto se ejecuta en Actulizar-contrato.php donde valida que los campos esten llenos
$(document).on('click','.Actulizar_Contrato',function(){
//    haciendo instancia de la clase validar campo
    let validarCampoContrato = new ValidarCampos();
//        haciendo llamada uno de los metodos para validar el estado el campo
      if(validarCampoContrato.validarletras(".numerocontratoACT") &&
         validarCampoContrato.validarletras(".contratanteACT") &&
         validarCampoContrato.validarletras(".objetocontratoACT") &&
         validarCampoContrato.validarletras(".empresacontratadaACT") &&
         validarCampoContrato.validarletras(".descriptcioncontratoACT") &&
         validarCampoContrato.validarCampoVacio(".contratopdfACT"))
            {
                    
                let actulizarContrato = new Contrato();
                    actulizarContrato.ActulizarContrato();
            
            }
   
});









