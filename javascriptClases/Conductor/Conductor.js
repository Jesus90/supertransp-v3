class Conductor {
        //ingresando un conductor 
        IngresarConductor(){
            
            let url = "../../Ejecutar-clases/Conductor/IngresarConductor.php";
            let valor = new FormData($(".Ingresar-conductor")[0]);
            
            let ajax = new Ajax(url,valor);
                ajax.funcionAjax().done(function(data){
                        if(data){
                            $(".respuestaInsertarConductor").html('<img src="../../img/comprobando/true.svg" style="width: 2%">');
                        }else{
                            $(".respuestaInsertarConductor").html('<img src="../../img/comprobando/false.svg" style="width: 2%">');
                        }
                    });
            
            
        }
        
        Numeropagina(pagina){
            let url = "../../Ejecutar-clases/Conductor/ListarConductores.php";
            $.post(url,{pagina:pagina},function(dato){
                $('.ListarConductores').html(dato);
            });
            
        }
        //se capta el valor para se refleje en la lista de conductores
       BuscarReistro(buscar){
           let url = "../../Ejecutar-clases/Conductor/ListarConductores.php";
           $.post(url,{buscar:buscar},function(dato){
                 $('.ListarConductores').html(dato);
           });
       } 
       
       
       ElimnarConductor(cedula){
           let url = "../../Ejecutar-clases/Conductor/EliminarConductor.php";
           $.post(url,{cedula:cedula},function(mensaje){
                    alert(mensaje);
                    setInterval(location.reload(),2000);
           });
           
       }
       //cuendo se preciona sobre el boton actulizar este metodo recoje los datos de Actulizar-conductor.php
       //y los envia a ejecutar clases y luego ActulizarConductor.php
       ActulizarConductor(){
           let url = "../../Ejecutar-clases/Conductor/ActulizarConductor.php";
           let valor = new FormData($('.actulizar-conductor')[0]);
           
           let ajax = new Ajax(url,valor);
               ajax.funcionAjax('.actulizar-conductor').done(function(mensaje){
                   if(mensaje){
                            $(".respuestaActulizarConductor").html('<img src="../../img/comprobando/true.svg" style="width: 2%">');
//                            $(".respuestaActulizarConductor").html(mensaje);
//                            setInterval(location.reload(),6000);
                            
                   }else{
                            $(".respuestaActulizarConductor").html('<img src="../../img/comprobando/false.svg" style="width: 2%">');
                        }
                   
                   
               });
           
       }
    
    
    
}

//este despues de validar que no  halla campos vacio llama al metodo IngresarConductor
//para que recoja los datos 
$(document).on('click','.IngresarCoductorBoton',function(){
                let validar = new ValidarCampos();
                    
                if(validar.validarletras(".nombre") &&
                   validar.validarletras(".apellido") &&
                   validar.validarNumeros(".cedula") &&
                   validar.validarNumeros(".numero_contacto") &&
                   validar.validarCampoVacio(".Fecha_nacimento")
                   ){
                    
                    let conductor = new Conductor();
                        conductor.IngresarConductor();
                }
    return false;
});
//esta funcion se encarga de visualizar la lista de conductores
$(document).ready(function(){
        let url = "../../Ejecutar-clases/Conductor/ListarConductores.php";
    $('.ListarConductores').load(url);
});

//cuando se encoje un numero de pagina llama a la funcion numeropagina de la clase conductor de este script
$(document).on('click','.paginaConductor',function(){
            
        let valor = $(this).attr('href');
        let conductor = new Conductor();
            conductor.Numeropagina(valor);
        return false;
});


///esta parte cuando el usuario decide hacer un busqueda 
 $('.BucarConductor').keyup(function(){
     
    let buscar = $(this).val(); 
        if(buscar != ""){
            let buscarConductor = new Conductor();
            buscarConductor.BuscarReistro(buscar);
        }
        
 });
 
 
 //cuendo se presione pa ra eliminar un registro se captara el numero de cedula a cual se va elimnar
 
 $(document).on('click',".elimiarConductor",function(){

                if(confirm("¿ Esta Seguro que Desea Elimanr El Conductor ?")){
          //obteniendo el valor donde se hace click
                let elimnarConductor = $(this).attr('href');
                 let eliminarCedula = new Conductor();
                        eliminarCedula.ElimnarConductor(elimnarConductor); 
                }
            
            return false;
 });
 
 
 $(document).on('click','.ActulizarCoductorBoton',function(){
            
                let validar = new ValidarCampos();
                
                   if( validar.validarCampoVacio('.nombreACT') &&
                       validar.validarCampoVacio('.cedulaACT') &&
                       validar.validarCampoVacio('.numero_contactoACT') &&
                       validar.validarCampoVacio('.Fecha_nacimentoACT') )
                         {
                             let actulizar = new Conductor();
                                 actulizar.ActulizarConductor();   
                         }
            
     return false;
 });
 
 
 
 $(document).on('click','.actulizarConductor',function(){
                
                 let url = $(this).attr('href');
                  window.open(url,"Actulizar Contrato","width=500,height=350,scrollbars=NO,titlebar=no,left=450,top=160,menubar=no");
                  return false;
        
});


//cuendo se preciona sobre el icono de forma de papel con nombre de clase .DocumentoConductor
$(document).on('click','.DocumentoConductor',function(){
    let NumeroConductor = $(this).attr('href');
    
    let url = "Documento-Conductor-Vista/AgregarDocumentoConductorFormulario.php?numeroCedula="+NumeroConductor;
    let propiedad  = "location=no,toolbar=no,menubar=no,scrollbars=yes";
    window.open(url,"Documento Conductor","width=998,height=400,left=170,top=120"+propiedad );
    
    return false;
    
});
 
 

 

