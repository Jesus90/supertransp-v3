class Fuec{
    
    InformacionContrato(numeroContrato){
        //se obtiene el numero contrato este numero va a  DetalleContrato.php donde muestra mas detalles del contrato 
        $.post( "../../Ejecutar-clases/Fuec/InformacionFuec/DetalleContrato.php", {numeroContrato:numeroContrato},function(informacionContrato){
                    //luego se obtiene la informacion y se ve reflejada en el formulario
                    $(".InformacionContratoSelecionado").html(informacionContrato);
                }) ;
                
         //esta parte sirve para listar los vehiculos que esten agarados a un contrato y si el valor es
         //vacio solo lista los propio de la empresa pero este dato es llevado a PlacaVehiculoActivo.php
         
         $.post("../../Ejecutar-clases/Fuec/InformacionFuec/PlacaVehiculoActiva.php",{numeroContrato:numeroContrato},function(informacionVehiculos){
             
             $(".selecionarPlaca").html(informacionVehiculos);
         });
         
    }
    //muestras informacion del vehiculo selecionado
    InformacionVehiculo(placa){
        let url = '../../Ejecutar-clases/Fuec/InformacionFuec/DetalleVehiculo.php';
        $.post(url,{placa:placa},function(detalleVehiculo){
            $(".detalleVehiculo").html(detalleVehiculo);
        });
    }
    
    //muestra de acuerdo al conductor escrito lo mostrara 
    BuscarConductor(conductor){
        let url = '../../Ejecutar-clases/Fuec/InformacionFuec/BuscarConductor.php';
        $.post(url,{conductor:conductor},function(resultradoConductor){
            $(".respuestaBusquedaConductor").html(resultradoConductor);
        });
        
        
    }
    
    //esta funcion recibira numero de cedula del conductor y numero que le corresponde en el fuec
            DetalleConductor(CedulaConductor,numeroConductor){
               let url = "../../Ejecutar-clases/Fuec/InformacionFuec/DetalleConductor.php";

                $.post(url,{CedulaConductor:CedulaConductor,numeroConductor:numeroConductor},function(AgregarConductor){
                    $('.listaConductoresfuec').append(AgregarConductor);
                });

                return false;
            }
            
            ListarCodigosFuec(){
                $.getJSON("../../codigoFuec.json").done(function(respuesta){
                
                      $.each(respuesta, function(i, field){
                         
                    //  $('.json').html(respuesta[2].departamento);
                      $(".Codigo_Departamento_list").append("<option value='"+field.codigo+"' >"+field.departamento+"</option>");
                        
                    });
            });
                
            }
            
            //aqui se colocan todo lo que va iniciar cuando cargue la web
            
            urlInicintes(){
                              //aqui muestra la informacion de la empresa para crear el fuec
                let URLinformacionEmpresa = "../../Ejecutar-clases/Fuec/InformacionFuec/InformacionEmpresa.php";
                $('.informacionEmpresa').load(URLinformacionEmpresa);

                //aqui muestra la informacion de la Contrato para crear el fuec
                let URLinformacionContrato = "../../Ejecutar-clases/Fuec/InformacionFuec/InformacionContrato.php";
                $('.informacionContrato').load(URLinformacionContrato);

                //lista los vehiculos activos que no tienen contrato
                $(".selecionarPlaca").load("../../Ejecutar-clases/Fuec/InformacionFuec/PlacaVehiculoActiva.php");

                //este sirve para listar los fuec todo los fuec creados
                 $(".listartablaFuec").load("../../Ejecutar-clases/Fuec/ListarFuec.php");
    
                
            }
            
            
   //---------------------------------------------------------------//
                IngresarFuec(){
                    
                    let url = "../../Ejecutar-clases/Fuec/IngresarFuec.php";
//                    
                     let formulario =  new FormData($('.IngresarFuec')[0]);
                    let ajax = new Ajax(url,formulario);
                        ajax.funcionAjax().done(function(respuestaFuec){
                            
                            $(".respuestafuec").html(respuestaFuec);
                        });
                    
                }
                
                BuscarFuec(buscar){
                   let  url = "../../Ejecutar-clases/Fuec/ListarFuec.php";
                    $.post(url,{buscar:buscar},function(resultadoFuec){
                        
                        $(".listartablaFuec").html(resultadoFuec);
                    });
                }
                
              
              MarcarFuecError(idFuec){
                  let url = "../../Ejecutar-clases/Fuec/Estados/ActulizarEstadoError.php";
                  $.post(url,{idFuec:idFuec},function(respuesta){
                          alert(respuesta);
                          setInterval(location.reload(),2000);
                  });
              }
              
               MarcarFuecActivo(idFuec){
                  let url = "../../Ejecutar-clases/Fuec/Estados/ActulizarEstadoActivo.php";
                  $.post(url,{idFuec:idFuec},function(respuesta){
                          alert(respuesta);
                          setInterval(location.reload(),2000);
                  });
              }
                
              listarFuec(pagina){
                  
                    let url  = "../../Ejecutar-clases/Fuec/ListarFuec.php";
                $.post(url,{pagina:pagina},function(data){
                    $(".listartablaFuec").html(data);
                });
              }      
}



$(document).ready(function(){
  let iniciar = new Fuec();
      iniciar.urlInicintes();  
});


//este se encarga de mostrar lista de veihculos que esten disponibles para hacer el fuec
//esta lista varia deacuerdo al numero de contrato 
$(document).on('change','.numerocontratos',function(){
    let numerocontrato = $(this).val();
    //si al selecionar un numero de contrato no viene vacio hace instancia de la clase
    if(numerocontrato !==""){
        let numeroContratos = new Fuec();
            numeroContratos.InformacionContrato(numerocontrato);
    }else{
        //pero si viene vacio vacia los campos
        $(".nombreContratante").val("");
        $(".objetocontrato").val("");
        $(".recorido").val("");
        $(".conevio").val("");
        $(".fechaInicontrato").val("");
        $(".fechaFincontrato").val("");
        //tambie vacia los campos de vehiculo selecionado
         $(".modelovehiculo").val("");
            $(".marcaVehiculo").val("");
            $(".clasevehiculo").val("");
            $(".numInterVehiculo").val("");
            $(".tarjetaOperacion").val("");
        
        
        //lista los vehiculos activos que no tienen contrato
         $(".selecionarPlaca").load("../../Ejecutar-clases/Fuec/InformacionFuec/PlacaVehiculoActiva.php");
    }
        
    
    return false;
});

//decuerdo al numero de placa que se escoja listara la informacion 
$(document).on('change','.placa',function(){
    
    let placa = $(this).val();
       //si es direferente a vacio hace llamado de la instancia
        if(placa !==""){
          let detalleVehiculo = new  Fuec();
                detalleVehiculo.InformacionVehiculo(placa);
        }else{
            $(".modelovehiculo").val("");
            $(".marcaVehiculo").val("");
            $(".clasevehiculo").val("");
            $(".numInterVehiculo").val("");
            $(".tarjetaOperacion").val("");
        }
    
   return false; 
});


//cuendo se preciona scheckbox para habilitar o desactivar fecha de vencmento
$(document).on('click','.fecha_vencimineto_check',function(){
    if($('.fecha_vencimineto_check').prop('checked')){
         $('.mostrar_fechaVencimiento').css('visibility','visible');
    }else{
        $('.mostrar_fechaVencimiento').css('visibility','hidden');
    }
    
});


//este se encarga de listar a los conductores que esten al dia con la documentacion
$(document).on('keyup','.BuscarConductor',function(){
    let conductor = $(this).val();
    
    if(conductor !== ""){
        
        let BuscarConductor = new Fuec();
            BuscarConductor.BuscarConductor(conductor);
    }
        
});

//cuando se listan los conductores al hacer se optiene el numero de cedula para traer los datos de la base de datos
$(document).on('click','.resultadobusqueda',function(){
            //optenidendo el numero de cedula del conductor
              let NumeroCedulaConductor =  $(this).attr('href');
              //optienido el numero 
              let numeroConductor = $('.conductordiv').val();
              
              
              //esta funcion solo permite que se hagan 2 click
              let click = 0;
              $('.resultadobusqueda').click(function(){
                  click +=1;
                  if(click > 0 && $('.conductordiv').val() === numeroConductor){
                       $(".resultadobusqueda").html("solo se puede un click escoja otro numero");
                       $('.conductordiv').focus();
                      return false;
                     
                  }
              });
          
        let fuecBuscarConductor = new Fuec();
         fuecBuscarConductor.DetalleConductor(NumeroCedulaConductor,numeroConductor);         
   return false; 
});


//cuendo se preciona sobre el boton crear valida que no hallan campos vacios
$(document).on('click','.CrearFuecUsuario',function(){
    
        let validarCampoFuec = new ValidarCampos();
         if(  validarCampoFuec.validarCampoVacio('.razonsocial') &&
              validarCampoFuec.validarCampoVacio('.nit') &&
              validarCampoFuec.validarCampoVacio('.modelovehiculo') &&
              validarCampoFuec.validarCampoVacio('.marcaVehiculo') &&
              validarCampoFuec.validarCampoVacio('.clasevehiculo') &&
              validarCampoFuec.validarCampoVacio('.numInterVehiculo') &&
              validarCampoFuec.validarCampoVacio('.tarjetaOperacion') &&
              validarCampoFuec.validarCampoVacio('.Codigo_Departamento')){
             
                if($(".respuestaBusqueda").attr('href') === ""){
                       $('.BuscarConductor').css('border-bottom','1px solid red');
                        $('.BuscarConductor').attr('placeholder','Debe seleccionar al menos un conductor');
                       return false;
                   }else{
                       //despues de validar los campos se hace instancia de la clase fuec y el metodo para ingresar fuec
                        
                        let crearFuec = new Fuec();
                            crearFuec.IngresarFuec();
                   
                   }
                   
                   return false;
             
         } 
    
    return false;
});



//cuendo se preciona sobre el boton Solicitar Fuec valida que no hallan campos vacios
//y macarcara el fuec como pendiente
$(document).on('click','.solicitar_Fuec_conductor',function(){
    
   
    
        let validarCampoFuec = new ValidarCampos();
         if(  validarCampoFuec.validarCampoVacio('.razonsocial') &&
              validarCampoFuec.validarCampoVacio('.nit') &&
              validarCampoFuec.validarCampoVacio('.modelovehiculo') &&
              validarCampoFuec.validarCampoVacio('.marcaVehiculo') &&
              validarCampoFuec.validarCampoVacio('.clasevehiculo') &&
              validarCampoFuec.validarCampoVacio('.numInterVehiculo') &&
              validarCampoFuec.validarCampoVacio('.tarjetaOperacion') &&
              validarCampoFuec.validarCampoVacio('.Codigo_Departamento')){
             
                if($(".respuestaBusqueda").attr('href') === ""){
                       $('.BuscarConductor').css('border-bottom','1px solid red');
                        $('.BuscarConductor').attr('placeholder','Debe seleccionar al menos un conductor');
                       return false;
                   }else{
                       //despues de validar los campos se hace instancia de la clase fuec y el metodo para ingresar fuec
                        
                        let crearFuec = new Fuec();
                            crearFuec.IngresarFuec();
                   
                   }
                   
                   return false;
             
         } 
    
    return false;
});


$(document).on('focus','.Codigo_Departamento',function(){
    let listar = new Fuec();
        listar.ListarCodigosFuec();
    
});

$(document).on('keyup','.BuscarFuec',function(){
    let valor = $(this).val();
    let buscarFuec = new Fuec();
        buscarFuec.BuscarFuec(valor);
});

//cuendo se preciona sobre el boto toma 2 valores 
$(document).on('click','.MarcarError',function(){
    
    //toma el valor del estado 
     let estado = $(this).attr('title');
     //si el estado es igual activo preguntara 
        if(estado === "activo"){
                if(confirm("¿Estas Seguro Que Desea Marcar Como error ?")){
//                        let idFuec = $(this).attr('href'); 
                     let idFuec = $(this).attr('href');  

                    let fuec = new  Fuec();
                        fuec.MarcarFuecError(idFuec);
                }
         }
         
         //si el estado es igual preccesando preguntara
         if(estado === "procesando"){
             if(confirm("¿Desea Marcar Como Activo ?")){
//                        let idFuec = $(this).attr('href'); 
                        let idFuec = $(this).attr('href');  

                        let fuec = new  Fuec();
                        fuec.MarcarFuecActivo(idFuec);
                 
             }
             
         }
    
    return false;
});





$(document).on('click','.paginaFuec',function(){
    
    let pagina = $(this).attr('href');
         
         let paginar =  new Fuec();
             paginar.listarFuec(pagina); 
         
        return false;
});


    
           




