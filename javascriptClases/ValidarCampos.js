class ValidarCampos{
    
    
    validarLEtrasNumeros(id){
//        exprecion regular
        let letras = /^[0-9a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ _\s]+$/;
//        condicional para decir si el campo vacio o no
        if($(id).val().length <3 || !$(id).val().match(letras)){
            //si el campo llegara a estar vacio en foca el campo y coloca un mesaje 
           $(id).focus();
           $(id).attr('placeholder','Campo obligatorio');
           $(id).css('border-bottom','1px solid red');
            return false;
         }else{
             //verdadero si el campo cumple con las condiones
             $(id).css('border-bottom','1px solid green');
             return true;
         } }//fin funcion validarCampovacio
     
     
         validarNumeros(id){
//        exprecion regular
        let numeros = /^[0-9]+$/;
//        condicional para decir si el campo vacio o no
        if($(id).val().length <3 || !$(id).val().match(numeros)){
            //si el campo llegara a estar vacio en foca el campo y coloca un mesaje 
           $(id).focus();
           $(id).attr('placeholder','Campo obligatorio');
           $(id).css('border-bottom','1px solid red');
            return false;
         }else{
             //verdadero si el campo cumple con las condiones
             $(id).css('border-bottom','1px solid green');
             return true;
         } }//fin funcion validarCampovacio
     
     
     
     validarletras(id){
//        exprecion regular
        let letras = /^[0-9a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ_\s]+$/;
//        condicional para decir si el campo vacio o no
        if($(id).val().length <3 || !$(id).val().match(letras)){
            //si el campo llegara a estar vacio en foca el campo y coloca un mesaje 
           $(id).focus();
           $(id).attr('placeholder','Campo obligatorio');
           $(id).css('border-bottom','1px solid red');
            return false;
         }else{
             //verdadero si el campo cumple con las condiones
             $(id).css('border-bottom','1px solid green');
             return true;
         } }//fin funcion validarCampovacio
     
     
     validarEmail(id){
//        exprecion regular
        let email = /^[a-zA-Z0-9\._-]+@[a-zA-Z0-9-]{2,}[.][a-zA-Z]{2,4}$/;
//        condicional para decir si el campo vacio o no
        if($(id).val().length <3 || !$(id).val().match(email)){
            //si el campo llegara a estar vacio en foca el campo y coloca un mesaje 
           $(id).focus();
           $(id).attr('placeholder','Campo obligatorio');
           $(id).css('border-bottom','1px solid red');
            return false;
         }else{
             //verdadero si el campo cumple con las condiones
             $(id).css('border-bottom','1px solid green');
             return true;
         } }//fin funcion validarCampovacio
     
     
     

          validarUrl(id){
//        exprecion regular
        let url = /^(ht|f)tps?:\/\/\w+([\.\-\w]+)?\.([a-z]{2,6})?([\.\-\w\/_]+)$/i;
//        condicional para decir si el campo vacio o no
        if($(id).val().length <3 || !$(id).val().match(url)){
            //si el campo llegara a estar vacio en foca el campo y coloca un mesaje 
           $(id).focus();
           $(id).attr('placeholder','Campo obligatorio');
           $(id).css('border-bottom','1px solid red');
            return false;
         }else{
             //verdadero si el campo cumple con las condiones
             $(id).css('border-bottom','1px solid green');
             return true;
         } }//fin funcion validarCampovacio
     
     
     validarFechas(id){
//        exprecion regular
        let fecha =  /^(0[1-9]|[12][0-9]|3[01])[- \/.](0[1-9]|1[012])[- \/.](19|20)\d\d$/;

//        condicional para decir si el campo vacio o no
        if($(id).val().length <3 || !$(id).val().match(fecha)){
            //si el campo llegara a estar vacio en foca el campo y coloca un mesaje 
           $(id).focus();
           $(id).attr('placeholder','Campo obligatorio');
           $(id).css('border-bottom','1px solid red');
            return false;
         }else{
             //verdadero si el campo cumple con las condiones
             $(id).css('border-bottom','1px solid green');
             return true;
         } }//fin funcion validarCampovacio
     
     
        validarFormato(campo,extencion){    
            
                    let  archivo = $(campo).val();
                    let extensiones = archivo.substring(archivo.lastIndexOf("."));
                    if(extensiones !== extencion)
                        {
                            return false;
                        }else{
                            return true;
                        }
                             }
        
        
        
                   validarCampoVacio(campo){
                        if($(campo).val().length <3 ){
                            $(campo).attr('placeholder','Campo obligatorio');
                            $(campo).css('border-bottom','1px solid red');
                            $(campo).focus();
                            return false;

                        }else{
                            $(campo).css('border-bottom','1px solid green');
                            return true;
                        }} 
                    
                   validarRadio(campo){
                        if(!$(campo).is(':checked') ){
                            $(campo).focus();   
                            $(campo).siblings('label').css('color','red');
                            return false;

                        }else{
                           $(campo).siblings('label').css('color','green');
                            return true;
                        }}   


                  ValidarCampoContrasena(id){
                                      //        exprecion regular
                        let url = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;
                //        condicional para decir si el campo vacio o no
                        if($(id).val().length <3 || !$(id).val().match(url)){
                            //si el campo llegara a estar vacio en foca el campo y coloca un mesaje 
                           $(id).focus();
                           $(id).attr('placeholder','Campo obligatorio');
                           $(id).css('border-bottom','1px solid red');
                            return false;
                         }else{
                             //verdadero si el campo cumple con las condiones
                             $(id).css('border-bottom','1px solid green');
                             return true;
                         } }//fin funcion validarCampovacio
                     
                     
                     
                            
    
}//fin de validar



