﻿
<?php 


require_once '../../vendor/autoload.php';
require_once '../../conexion/conexion.php';
require_once '../../conexion/sessionlogin.php';
require_once '../../clases/baseDeDatos.php';
require_once '../../clases/Fuec.php';

require_once '../../QR/autoload.php';
use Endroid\QrCode\QrCode;





if(isset($_GET['imprimirPDF'])){
    
    $imprimirFuec = new Fuec();
    $imprimirFuec->BucarFuecid($_GET['imprimirPDF']);
    $FuecPDF = $imprimirFuec->GetRespuesta();
    
              
            function CodigoQR($valor=null){
                
                $qrCode = new QrCode($valor);
                 $qrCode->setMargin(0); //margen
                 $qrCode->setSize(106);
                 //color
	         //$qrCode->Color(51, 153, 255);
                 //esto para el codigo QR
                 $qrCode->setLogoPath('../../img/logosupertransp.png');//logo de fondo
                 $qrCode->setLogoSize(30, 30);
                //para colorcar la imagen en el centreo 
                $image= $qrCode->writeString();//Salida en formato de texto
                $imageData = base64_encode($image);//Codifico la imagen usando base64_encode
 
                return '<img style="margin-left: 35px;" src="data:image/png;base64,'.$imageData.'">';
                
            }
   
    
    
   foreach ($FuecPDF as $elemento){  
      //esto es una marca de agua que aparece si el fuec es marcado como error
    if($elemento['estado'] == "error" || $elemento['estado'] == "inactivo" ){
        
         $html .= '<div class="MarcaAgua" ><img src="../../img/iconos/cerrar.svg"></div> <div class="textofuec" >[ Este Fuec No Es Valido ]</div>';  
    }
    //si el rol es igual a conductor mostrara la marca de agua
    if($elemento['estado'] == "procesando" && $_SESSION['rangoUsuario'] == 'conductor' ){
        
         $html .= '<div class="MarcaAgua" ><img src="../../img/Alerta/AlertIconAmarillo.svg"></div> <div class="textofuec" >[  Se Esta Revisando ]</div>';  
    }
    
 
       
       
$html .= '<table >
             <tr class="titulo" ><td colspan="6" align="center" ><h4>Ficha tecnica del formato unico del extracto del contrato "FUEC"</h4></td></tr>
             
            <tr class="subtitulo" ><td colspan="3">Expedicion: '.$elemento['fechaCreacion'].' - Vence: '.$elemento['fecha_vencimineto'].'</td><td  colspan="1">ANVERSO</td><td colspan="1"></td></tr>
            
            <tr class="logo"><td colspan="3"><img src="'.$elemento['logotranscito'].'"  height="65" width="200"></td><td colspan="3"><img src="'.$elemento['logoempresa'].'"  height="65" width="280"></td></tr>
            
            <tr class="tituloi" ><td colspan="6"  align="center"><h4>FORMATO UNICO DE EXTRACTO DEL CONTRATO DEL SERVICIO PUBLICO DE TRASPORTE TERRESTRE AUTOMOTOR ESPECIAL</h4><p>N°.</p></td></tr>
            
            <tr class="linea_cinco"><td colspan="4">RAZON SOCIAL : '.$elemento['nombreEmpresa'].' </td><td colspan="2"> NIT : '.$elemento['nitEmpresa'].'</td></tr>  
            <tr class="linea_seis"><td colspan="6">CONTRATO N°. '.$elemento['numeroContrato'].'</td></tr>    
            <tr class="linea_Siete"><td colspan="6"><p>OBJETIVO DEL CONTRATO:</p><pt>'.$elemento['objetocontrato'].'</pt></td></tr>
            <tr ></tr>
                
            <tr class="linea_Siete"><td colspan="6"><p>ORIGEN-DESTINO-DESCRIBIENDO EL RECORRIDO:</p><pt>'.$elemento['recorido'].'</pt></td></tr>
            <tr ></tr>   
                
                <tr class="linea_ocho" ><td colspan="3"><h5>CONVENIO : '.$elemento['conevio'].'</h5></td><td colspan="3"><h5>UNION TEMPORAL: '.$elemento['nombreContratante'].'</h5></td></tr>
            
                <tr class="vigencia"><td colspan="6"><h4>VIGENCIA DEL CONTRATO</h4></td></tr>  
                                                                                                        
                <tr class="vigencia_uno"><td colspan="3"><h5>FECHA INICIAL</h5></td><td colspan="1"><p>DIA: '.$dates = date("d",strtotime($elemento['fechaInicontrato'])).'</p></td  ><td colspan="1"><P> MES : '.$dates = date("m",strtotime($elemento['fechaInicontrato'])).'</P></td><td colspan="1"><P> AÑO: '.$dates = date("Y",strtotime($elemento['fechaInicontrato'])).'</P></td></tr>
                
                <tr class="vigencia_uno"><td colspan="3"><h5>FECHA VENCMIENTO</h5></td><td colspan="1"><p>DIA: '.$dates = date("d",strtotime($elemento['fechaFincontrato'])).'</p></td  ><td colspan="1"><P> MES : '.$dates = date("m",strtotime($elemento['fechaFincontrato'])).'</P></td><td colspan="1"><P> AÑO: '.$dates = date("Y",strtotime($elemento['fechaFincontrato'])).'</P></td></tr>
                
                <tr class="vigencia"><td colspan="6"><h4>CARACTERISTICAS DEL VEHICULO</h4></td></tr> 
                <tr class="vehiculo-titulo"><td colspan="1">PLACA</td><td colspan="2">MODELO</td><td colspan="1">MARCA</td><td colspan="2">CLASE</td></tr>
                
                <tr class="vehiculo-dato"><td colspan="1">'.$elemento['placa'].'</td><td colspan="2">'.$elemento['modelovehiculo'].'</td><td colspan="1">'.$elemento['marcaVehiculo'].'</td><td colspan="2">'.$elemento['clasevehiculo'].'</td></tr>
                
                <tr class="vigencia"><td colspan="3"><h4>NUMERO DE INTERNO</h4></td><td colspan="3"><h4>NUMER DE TARJETA DE OPERACION</h4></td></tr>
                <tr class="vehiculo-dato"><td colspan="3">'.$elemento['numInterVehiculo'].'</td><td colspan="3">'.$elemento['tarjetaOperacion'].'</td></tr>
                
                <tr class="linea_nueve"><td rowspan="2">DATOS DEL CONDUCTOR 1</td><td colspan="2">NOMBRE Y APELLLIDO</td><td colspan="1">N°: CEDULA</td><td colspan="1">LICENCIA CONDUCION</td><td colspan="1">VIGENCIA</td></tr>
                
                <tr class="texto"><td colspan="2">'.$elemento['nombreconductor1'].' </td><td colspan="1">'.$elemento['cedulaconductor1'].' </td><td colspan="1">'.$elemento['licenciaconductor1'].' </td><td colspan="1">'.$elemento['vigencia1'].' </td></tr>
                
                <tr class="linea_nueve"  ><td rowspan="2">DATOS DEL CONDUCTOR 2</td><td colspan="2">NOMBRE Y APELLLIDO</td><td colspan="1">N°: CEDULA</td><td colspan="1">LICENCIA CONDUCION</td><td colspan="1">VIGENCIA</td></tr>
                
                <tr class="texto"><td colspan="2">'.$elemento['nombreconductor2'].'</td><td colspan="1">'.$elemento['cedulaconductor2'].' </td><td colspan="1">'.$elemento['licenciaconductor2'].' </td><td colspan="1">'.$elemento['vigencia2'].' </td></tr>
                
                <tr class="linea_nueve"><td rowspan="2">RESPONSABLE DEL CONTRATANTE</td><td colspan="2">NOMBRE Y APELLLIDO</td><td colspan="1">N°: CEDULA</td><td colspan="1">TELEFONO</td><td colspan="1">DIRECCION</td></tr>
                
                <tr class="texto"><td colspan="2">'.$elemento['representante'].'</td><td colspan="1">'.$elemento['cedula'].'</td><td colspan="1">'.$elemento['telefono'].'</td><td colspan="1">'.$elemento['direccion'].'</td></tr>
                
                <tr class="firma" >
			<td colspan="1" >
				<p>Direccion : '.$elemento['direccion'].'</p>
				<p>  Celular : '.$elemento['movil'].'</p>
				<p>Email : '.$elemento['email'].' </p>
				<p> Ciudad : '.$elemento['ciudad'].'</p>
			</td> 
			<td colspan="3" >
				<div class="qr"  >'.CodigoQR($elemento['idfuec']).'</div>
			</td>
			<td colspan="2" align="center" ><img src="'.$elemento['firma'].'"  height="90" width="200"></td></tr>
                    
                <tr class="verificacion"><td colspan="3" style="font-size: 14px; ">Cidigo de verificacion de Fuec  '.$elemento['idfuec'].' </td><td colspan="3" style="font-size: 14px;">PAGINA WEB: Supertransp.co</td></tr>
 </table>';


$css = file_get_contents('../../css/Fuec.css');
$mpdf = new \Mpdf\Mpdf(['format' => 'Letter']);


$mpdf->WriteHTML($css,1);
$mpdf->WriteHTML($html);
$mpdf->SetTitle('Fuec');

$mpdf->SetMargins('0', '0', '0');
$mpdf->SetAuthor('jesus D peña');
$mpdf->Output('fuec.pdf','I');

       
       
       echo $html;
   }



}else{
    
}