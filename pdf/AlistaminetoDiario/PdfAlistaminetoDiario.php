<?php
require_once '../../vendor/autoload.php';
require_once '../../conexion/sessionlogin.php';
require_once '../../conexion/conexion.php';
require_once '../../clases/baseDeDatos.php';
require_once '../../clases/Conductor.php';


function comproandoPDF($ruta=null,$clases=null,$valor){
        
        switch ($valor){
            case 1:{
                    return '<img src="'.$ruta.'img/BienMal/true.svg"
                            alt=""
                            class="'.$clases.'"
                            title="" 
                            width="20"> ';}
                
            break;
        
            case 0:{
                    return '<img src="'.$ruta.'img/BienMal/false.svg"
                            alt=""
                            class="'.$clases.'"
                            title="" 
                            width="20"> ';}
                
            break;    
             
        }
        
    }//fin commprobando



//libreria pdf

//require_once '../../QR/autoload.php';
//use Endroid\QrCode\QrCode;
//$_GET['buscarAlitamiento'] = 8;
if(isset($_SESSION['rangoUsuario'])){
    
    //hago instancia de la clase alistamineto que esta dentro de archivo clases/conductor.php
    $AlistamientoDiario = new AlistamientoDiario();
    //si el usuario quiere hacer una busqueda solo es pasar el valor por el input con name=BuscarAlistamientoDiario
    if(!empty($_GET['buscarAlitamiento'])){
        $AlistamientoDiario->SetDato($_GET['buscarAlitamiento']);
    }
    //este es para pasar el numero pagina 
//    $_POST['paginaAlistamiento'] = 3;
//    if(!empty($_POST['paginaAlistamiento']) ){
//      $AlistamientoDiario->SetNumeroPagina($_POST['paginaAlistamiento']);  
//    }
    
    
    $AlistamientoDiario->BuscarAlitamientoPorID();
    $ver = $AlistamientoDiario->getResultado();
    $numeroRegistro = $AlistamientoDiario->contar_registro();

}//fin si

foreach ($ver  as $row){
    




$html = '<style>
     table {
        width: 100%;
        }

//        td{border: 1px solid #808080;
//         }

      .b{
            background-color: #eeeeee;
            }
      .c{
        border: 1px solid #808080;
        text-align: center;
         width: 108px;
        } 
        
      .t{
        font-size: 18px;
        border: 1px solid #808080;
        text-align: center;
        background-color: #eeeeee; 
         padding: 6px;
         
        }
        
        .e{
             border-bottom:  1px solid #808080; 
        }
            
        
      
    
    </style>
    
    <table>
        <tr>
            <td colspan="4" class="t" > Datos </td>
        </tr>
        <tr>
            <td colspan="1">Placa Vehiculo :</td>
            <td colspan="2">
                '.$row['placa_vehiculo'].'
            </td>
        </tr>    
        <tr>
            <td>Nombre : </td>
            <td>'.$row['nombre_conductor'].'</td>
            <td>Fecha Creado :</td>
            <td>'.$row['fecha_actual'].'</td>
        </tr>
        
        <tr>
            <td>Ciudad :</td>
            <td>'.$row['ciudad'].'</td>
            <td>Kilometraje :</td>
            <td>'.$row['kilometraje'].'</td>
        </tr>
       
                
            <!--        Elementos Que Se Inspeccionan-->
        <tr>
            <td colspan="4"  class="t">Elementos Que Se Inspeccionan</td>
        </tr>
        <tr>
            <td rowspan="3" class="e">
               Direccionales 
            </td>
        </tr>
        <tr  >
            <td class="b" >
                Delanteras
            </td>
            <td class="c">
                '.comproandoPDF("../../", "",$row['direccionales_delanteras']).'
            
            </td>
            <td>
                <textarea rows="2" cols="20" maxlength="36" >
                '.$row['direccionales_delanteras_observacion'].'
                </textarea>
            </td>
            
        </tr>
        <tr>
            <td class="b">
                Traseras
            </td>
            <td class="c">
                '.comproandoPDF("../../", "",$row['direccionales_traseras']).'
            </td>
            <td>
                <textarea name="direccionales_traseras_observacion" class="direccionales_traseras_observacion"  >'.$row['direccionales_traseras_observacion'].' </textarea>
            </td>
        </tr>
        
        
                <!--        luces-->
        <tr>
            <td rowspan="6" class="e">
                Luces    
            </td>
        </tr>
        <tr>
            <td class="b">Altas</td>
            <td class="c">
                '.comproandoPDF("../../", "",$row['luces_altas']).'
            </td>
            <td>
                <textarea name="luces_altas_observacion" class="luces_altas_observacion" > '.$row['luces_altas_observacion'].'</textarea>
            </td>
        </tr>
        <tr>
            <td class="b">Bajas</td>
            <td class="c">
                '.comproandoPDF("../../", "",$row['luces_bajas']).'
            </td>
            <td>
                <textarea name="luces_bajas_observacion" class="luces_bajas_observacion" >'.$row['luces_bajas_observacion'].' </textarea>
            </td>
        </tr>
        <tr>
            <td class="b">Stop</td>
            <td class="c" >
                '.comproandoPDF("../../", "",$row['luces_stops']).'
            </td>
            <td>
                <textarea name="luces_stops_observacion" class="luces_stops_observacion" >'.$row['luces_bajas_observacion'].' </textarea>
            </td>
        </tr>
        <tr>
            <td class="b">Reversa</td>
            <td class="c">
                '.comproandoPDF("../../", "",$row['luces_reversa']).'
            </td>
            <td>
                <textarea name="luces_reversa_observacion" class="luces_reversa_observacion" >'.$row['luces_reversa_observacion'].' </textarea>
            </td>
        </tr>
        <tr>
            <td class="b">Parqueo</td>
            <td class="c">
                '.comproandoPDF("../../", "",$row['luces_parqueo']).'
            </td>
            <td>
                <textarea name="luces_parqueo_observacion" class="luces_parqueo_observacion" >'.$row['luces_parqueo_observacion'].'</textarea>
            </td>
        </tr>
        
<!--        limpiaBrisas-->
        <tr >
            <td rowspan="3" class="e">LimpiaBrisas</td>
        </tr>
        <tr>
            <td class="b" >Der/Izq</td>
            <td class="c">
                '.comproandoPDF("../../", "",$row['limpiabrisas_der_izq']).'
            </td>
            <td>
                <textarea name="limpiabrisas_der/izq_observacion" class="limpiabrisas_der_izq_observacion" >'.$row['limpiabrisas_der_izq_observacion'].' </textarea>
            </td>
        </tr>
        <tr>
            <td class="b" >Atraz</td>
            <td class="c">
                '.comproandoPDF("../../", "",$row['limpiabrisas_atras']).'
            </td>
            <td>
                <textarea name="limpiabrisas_atras_observacion" class="limpiabrisas_atras_observacion" >'.$row['limpiabrisas_atras_observacion'].' </textarea>
            </td>
        </tr>
        
<!--        principal-->
        <tr>
            <td rowspan="3" class="e">Frenos</td>
        </tr>
        <tr>
            <td class="b" >Principal</td>
            <td class="c">
                '.comproandoPDF("../../", "",$row['frenos_principal']).'
            </td>
            <td>
                <textarea name="frenos_principal_observacion" class="frenos_principal_observacion" >'.$row['frenos_principal_observacion'].' </textarea>
            </td>
        </tr>
        <tr>
            <td class="b" >Emergencia</td>
            <td class="c" >
                '.comproandoPDF("../../", "",$row['frenos_emergencia']).'
            </td>
            <td>
                <textarea name="frenos_emergencia_observacion" class="frenos_emergencia_observacion" > '.$row['frenos_emergencia_observacion'].'</textarea>
            </td>
        </tr>
        
        <!--        Llantas-->
        <tr>
            <td rowspan="4" class="e"> Llantas</td>
        </tr>
        <tr>
            <td class="b" >Delanteras</td>
            <td class="c" >
                '.comproandoPDF("../../", "",$row['llantas_delanteras']).'
            </td>
            <td>
                <textarea name="llantas_delanteras_observacion" class="llantas_delanteras_observacion" >'.$row['llantas_delanteras_observacion'].' </textarea>
            </td>
        </tr>

        <tr>
            <td class="b" >Traseras</td>
            <td class="c" >
                '.comproandoPDF("../../", "",$row['llantas_traseras']).'
            </td>
            <td>
                <textarea name="llantas_traseras_observacion" class="llantas_traseras_observacion" >'.$row['llantas_traseras_observacion'].' </textarea>
            </td>
        </tr>

        <tr>
            <td class="b" >Respuesto</td>
            <td class="c">
                '.comproandoPDF("../../", "",$row['llantas_repuestos']).'
            </td>
            <td>
                <textarea name="llantas_repuestos_observacion" class="llantas_repuestos_observacion" > '.$row['llantas_repuestos_observacion'].'</textarea>
            </td>
        </tr>
        
        <!--        Espejos-->
        <tr>
            <td rowspan="3" class="e">Espejos</td>
        </tr>
        <tr>
            <td class="b" >Laterales DeR/Izq</td>
            <td class="c" >
                '.comproandoPDF("../../", "",$row['espejos_lateral_der_izq']).'
            </td>
            <td>
                <textarea name="espejos_lateral_der_izq_observacion" class="espejos_lateral_der_izq_observacion" > '.$row['espejos_lateral_der_izq_observacion'].'</textarea>
            </td>
        </tr>
        <tr>
            <td class="b" >Retrovisor</td>
            <td class="c" >
                '.comproandoPDF("../../", "",$row['espejos_retrovisor']).'
            </td>
            <td>
                <textarea name="espejos_retrovisor_observacion"  class="espejos_retrovisor_observacion" > '.$row['espejos_retrovisor_observacion'].'</textarea>
            </td>
        </tr>
        <!--Pito-->
        <tr>
            <td colspan="1" class="e">Pito</td>
            <td class="b">
                Accionar Antes De Iniciar Marcha
            </td>

            <td class="c" >
                '.comproandoPDF("../../", "",$row['pito']).'
            </td>
            <td>
                <textarea name="pito_observacion"  class="pito_observacion" > '.$row['pito_observacion'].'</textarea>
            </td>
        </tr>
        <!--        Niveles De Fluido-->
        <tr>
            <td rowspan="4" class="e">Niveles De Fluidos</td>
        </tr>
        <tr>
            <td class="b" >Frenos</td>
            <td class="c" >
                '.comproandoPDF("../../", "",$row['fluidos_frenos']).'
            </td>
            <td>
                <textarea name="fluidos_frenos_observacion" class="fluidos_frenos_observacion" > '.$row['fluidos_frenos_observacion'].'</textarea>
            </td>
        </tr>
        <tr>
            <td class="b" >Aceites</td>
            <td class="c" >
                '.comproandoPDF("../../", "",$row['fluidos_aceites']).'
            </td>
           <td>
               <textarea name="fluidos_aceites_observacion" class="fluidos_aceites_observacion" > '.$row['fluidos_aceites_observacion'].' </textarea>
            </td>
        </tr>
        <tr>
            <td class="b" >Refrigerante</td>
            <td class="c" >
                '.comproandoPDF("../../", "",$row['fluidos_refriguerante']).'
            </td>
            <td>
                <textarea name="fluidos_refriguerante_observacion"  class="fluidos_refriguerante_observacion"> '.$row['fluidos_refriguerante_observacion'].' </textarea>
            </td>
        </tr>
        </table>';
     $htmll = ' <table>
         <tr  > 
        <td  colspan="4" class="t">Equipo de Seguridad</td>
        </tr>
        
        <tr>
            <td class="e" >Herramientas</td>
            <td class="b">
                <ul>
                    <li>Alicate</li>
                    <li>Destornilladores</li>
                    <li>Llaves De Expacion</li>
                    <li>Llaves Fijas</li>
                </ul>
            </td>
            <td class="c" >
                '.comproandoPDF("../../", "",$row['herraminetas']).'
            </td>
            
            <td>
                <textarea name="herramientas_observacion" class="herramientas_observacion"  > '.$row['herramientas_observacion'].' </textarea>
            </td>
        </tr>
        <tr>
            <td colspan="1" class="e">Crucetas</td>
            <td class="b" >Apta Para Vehiculo</td>
            <td class="c" >
                '.comproandoPDF("../../", "",$row['crucetas']).'
            </td>
            <td>
                <textarea name="crucetas_observacion" class="crucetas_observacion"  > '.$row['crucetas_observacion'].'</textarea>
            </td>
        </tr>
<!--        Gato-->
        <tr>
            <td colspan="1" class="e">Gato</td>
            <td class="b" >Con Capacidad para Elevar El Vehiculo</td>
            <td class="c" >
                '.comproandoPDF("../../", "",$row['gato']).'
            </td>
            <td>
                <textarea name=""  > '.$row['gato_observacion'].'</textarea>
            </td>
        </tr>
<!--        Tacos-->
        <tr>
            <td colspan="1" class="e">Taco</td>
            <td class="b">Dos Tacos Apto Para Bloquear El Vehiculo</td>
            <td class="c">
                '.comproandoPDF("../../", "",$row['taco']).'
            </td>
            <td>
                <textarea name="taco_observacion" class="taco_observacion" > '.$row['taco_observacion'].' </textarea>
            </td>
        </tr>
<!--        Señales-->
        <tr>
            <td class="e"  >Señales</td>
            <td class="b">
                <ul>
                    <li>Dos Señales Forma de Triangulo</li>
                    <li>Material Reflectivo</li>
                    <li>Lampara De Señal Luz Amarilla</li>
                </ul>    
             </td>
             <td class="c">
                 '.comproandoPDF("../../", "",$row['senales']).'
            </td>
             <td>
                 <textarea name="senales_observacion" class="senales_observacion" > '.$row['senales_observacion'].' </textarea>
            </td>
        </tr>
        
        <!--        chaleco-->
        <tr>
            <td class="e">Chaleco</td>
            <td class="b">Debe Ser Reflectivo</td>
            <td class="c">
               '.comproandoPDF("../../", "",$row['chaleco']).'
            </td>
            <td>
                <textarea name="chaleco_observacion" class="chaleco_observacion" > '.$row['chaleco_observacion'].' </textarea>
            </td>
        </tr>
<!--        Botiquin-->
        <tr>
            <td class="e">
                Botiquin
            </td>
            <td class="b">
                <ul>
                    <li>Yodopovidona Solucion Antiseptico        Bolsa (120 ML)</li>
                    <li>Jabon</li>
                    <li>Gasas</li>
                    <li>Curas</li>
                    <li>Venda Elastica</li>
                    <li>Micropore Rollo</li>
                    <li>Algodon Paquete (25 GR)</li>
                    <li>Acetaminofen Tabletas</li>
                    <li>Mareol Tabletas</li>
                    <li>Sales De Rehidratacion Oral</li>
                    <li>Baja Lenguas</li>
                    <li>Suero Fisiologico(250 ML)</li>
                    <li>Guante Latex Desechables</li>
                    <li>Toallas Higienicas</li>
                    <li>Tijeras</li>
                    <li>Termometro</li>
                </ul>
            </td>
            <td class="c" >
                '.comproandoPDF("../../", "",$row['botiquin']).'
            </td>
            <td>
                <textarea name="botiquin_observacion"  class="botiquin_observacion" > '.$row['botiquin_observacion'].' </textarea>
            </td>
        </tr>
        
        
        
       
        
    </table>
    ';
}

//$css = file_get_contents('../../css/Fuec.css');
$mpdf = new \Mpdf\Mpdf(['format' => 'Letter']);


//$mpdf->WriteHTML($css,1);
$mpdf->WriteHTML($html);

$mpdf->AddPage();

$mpdf->WriteHTML($htmll);

$mpdf->SetTitle('Alistamiento Diario');



$mpdf->SetMargins('0', '0', '0');
$mpdf->SetAuthor('jesus D peña');
$mpdf->Output('Alistamiento Diario.pdf','I');
    


echo $html;