<?php
class conexion  extends PDO{
    
     function __construct() {
        //variables para la conexion a la base de datos
        $tipo_base = "mysql";
        $localhost = "localhost";
        $nombre_bd = "supertranspdb";
        $usuario_bd = "root";
        $contrasena_bd = "";
        
        try{
            //sobre escribiendo el metodo constructor de la clase PDO
            parent::__construct($tipo_base.':host='.$localhost.';dbname='.$nombre_bd,$usuario_bd,$contrasena_bd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
//           
            
            
        } catch (PDOException $e){
            echo 'ha surgido un error en la conexion de la base de datos' .$e->getCode();
            exit;
        }
        
        
    }

    
}
